var _SERVICEPATHServer = "phpfile/client.php";

/*declare array for the select box*/
var service_provider_list = [];
var delete_email = 1;
$('#add-notifyEmail').on("click", function () {
    delete_email = delete_email + 1;
    add_notify_email(delete_email);
});


/*function to add notify email*/

function add_notify_email(delete_email) {


    if (delete_email == 2) {
        $('#notify_emails').append('<div id="notify_email_' + delete_email + '"><div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Secondary eMail:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="email" class=" form-control" id="notify-email-' + delete_email + '" placeholder="Secondary eMail for notifications"></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_email(' + delete_email + ',0)" id="delete-notifyEmail"><i style="font-size:18px;" class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" style="margin-left:-15px;" class="btn btn-primary" onclick="clear_email(' + delete_email + ',0)">clear</button></div></div></div>');
    } else {

        $('#notify_emails').append('<div id="notify_email_' + delete_email + '"><div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Additional eMail:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="email" class=" form-control" id="notify-email-' + delete_email + '" placeholder="Additional eMail for notifications"></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_email(' + delete_email + ',0)" id="delete-notifyEmail"><i style="font-size:18px;" class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" style="margin-left:-15px;" class="btn btn-primary" onclick="clear_email(' + delete_email + ',0)">clear</button></div></div></div>');

    }
}

/*function to delete email */

function del_email(delete_email, delet_id) {


    if (delet_id == 0) {
        $('#notify_email_' + delete_email).remove();
        delete_email = delete_email - 1;

    } else {

        delete_email_data(delet_id);
        delete_email = delete_email - 1;
    }

}

/*function to click on add mobile number*/
var add_mobile = 1;
$('#add-notifySMS').on('click', function () {
    add_mobile = add_mobile + 1;
    add_sms_mobile(add_mobile);
});


/*add mobile number for notification*/

function add_sms_mobile(add_mobile) {


    if (add_mobile == 2) {
        $('#notify_sms').append('<div id="notify_sms_' + add_mobile + '"> <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Secondary SMS:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="text" class=" form-control" id="notify-sms-' + add_mobile + '"  onkeyup="validPhoneNumber(this)" name="phoneNumber"><span>@</span> <select name="service_provider" id="service_provider-' + add_mobile + '" class=" form-control"> <option value="0">WIRELESS PROVIDER</option> <option value="msg.acsalaska.com">Alaska Communications (ACS)</option> <option value="message.alltel.com">Alltel</option> <option value="paging.acswireless.com">Ameritech (ACSWireless)</option> <option value="txt.att.net">AT&amp;T Wireless</option> <option value="page.att.net">ATT Business Messaging (US)</option> <option value="txt.bellmobility.ca">Bell Mobility Canada</option> <option value="blsdcs.net">BellSouth Mobility</option> <option value="blueskyfrog.com">Blue Sky Frog</option> <option value="myboostmobile.com">Boost Mobile</option> <option value="sms-c1.bm">Cellone - Bermuda</option> <option value="csouth1.com">Cellular South</option> <option value="mobile.celloneusa.com">CellularOne (Dobson)</option> <option value="cwemail.com">Centennial Wireless</option> <option value="cingularme.com">Cingular</option> <option value="mobile.mycingular.com">Cingular</option> <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option> <option value="sms.ctimovil.com.ar">Claro Argentina</option> <option value="mms.cricketwireless.net">Cricket</option> <option value="digitextbm.com">Digicel Bermuda</option> <option value="digitextjm.com">Digicel Jamaica</option> <option value="sms.edgewireless.com">Edge Wireless</option> <option value="fido.ca">Fido Canada</option> <option value="mobile.gci.net">General Communications Inc.</option> <option value="sms.goldentele.com">Golden Telecom</option> <option value="ideacellular.net">Idea Cellular</option> <option value="text.mtsmobility.com">Manitoba Telecom Systems</option> <option value="pcsms.com.au">Message Media (AU)</option> <option value="sms.messagebird.com">MessageBird</option> <option value="mymetropcs.com">Metro PCS</option> <option value="page.metrocall.com">Metrocall Pager</option> <option value="page.mobilfone.com">Mobilfone</option> <option value="sms.co.za">MTN (South Africa)</option> <option value="wirefree.informe.ca">NBTel</option> <option value="messaging.nextel.com">Nextel</option> <option value="npiwireless.com">NPI Wireless</option> <option value="pcs.ntelos.com">NTELOS</option> <option value="mmail.co.uk">O2 (UK)</option> <option value="optusmobile.com.au">Optus (Australia)</option> <option value="orange-gsm.ro">Orange (Romania)</option> <option value="orange.net">Orange (UK)</option> <option value="pagenet.net">Pagenet</option> <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option> <option value="ptel.net">Powertel</option> <option value="sms.pscel.com">PSC Wireless</option> <option value="pager.qualcomm.com">Qualcomm</option> <option value="qwestmp.com">Qwest</option> <option value="smtext.com">Simple Mobile</option> <option value="skytel.com">Skytel - Alphanumeric</option> <option value="sms.smsglobal.com.au">SMS Global</option> <option value="solavei.net">Solavei</option> <option value="messaging.sprintpcs.com">Sprint PCS</option> <option value="tms.suncom.com">SunCom</option> <option value="mobile.surewest.com">SureWest Communications</option> <option value="movistar.net">Telefonica Movistar</option> <option value="onlinesms.telstra.com">Telstra</option> <option value="msg.telus.com">Telus Mobility</option> <option value="t-mobile.uk.net">T-Mobile (UK)</option> <option value="tmomail.net" selected>T-Mobile USA</option> <option value="email.uscc.net">US Cellular</option> <option value="vtext.com">Verizon Wireless</option> <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option> <option value="myairmail.com">Verizon Wireless (myairmail.com)</option> <option value="vmobile.ca">Virgin (Canada)</option> <option value="vxtras.com">Virgin Mobile (UK)</option> <option value="vmobl.com">Virgin Mobile USA</option> <option value="voda.co.za">Vodacom (South Africa)</option> <option value="vodafone.net.au">Vodafone (AUS)</option> <option value="vodafone.net">Vodafone (UK)</option> <option value="airmessage.net">Weblink Wireless</option> <option value="wyndtell.com">WyndTell</option> </select><small class="help-block" data-fv-validator="callback" data-fv-for="phoneNumber" data-fv-result="NOT_VALIDATED" style="display: none;">Please enter a valid value</small></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_mobile(' + add_mobile + ',0)" id="delete-notifyEmail"><i style="font-size:22px;" class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1"  style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" style="margin-left:-15px;" class="btn btn-primary" onclick="clear_mobile(' + add_mobile + ',0)">clear</button></div></div>');
    } else {

        $('#notify_sms').append('<div id="notify_sms_' + add_mobile + '"> <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Additional SMS:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="text" class=" form-control" id="notify-sms-' + add_mobile + '"  onkeyup="validPhoneNumber(this)" name="phoneNumber"><span>@</span> <select name="service_provider" id="service_provider-' + add_mobile + '" class=" form-control"> <option value="0">WIRELESS PROVIDER</option> <option value="msg.acsalaska.com">Alaska Communications (ACS)</option> <option value="message.alltel.com">Alltel</option> <option value="paging.acswireless.com">Ameritech (ACSWireless)</option> <option value="txt.att.net">AT&amp;T Wireless</option> <option value="page.att.net">ATT Business Messaging (US)</option> <option value="txt.bellmobility.ca">Bell Mobility Canada</option> <option value="blsdcs.net">BellSouth Mobility</option> <option value="blueskyfrog.com">Blue Sky Frog</option> <option value="myboostmobile.com">Boost Mobile</option> <option value="sms-c1.bm">Cellone - Bermuda</option> <option value="csouth1.com">Cellular South</option> <option value="mobile.celloneusa.com">CellularOne (Dobson)</option> <option value="cwemail.com">Centennial Wireless</option> <option value="cingularme.com">Cingular</option> <option value="mobile.mycingular.com">Cingular</option> <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option> <option value="sms.ctimovil.com.ar">Claro Argentina</option> <option value="mms.cricketwireless.net">Cricket</option> <option value="digitextbm.com">Digicel Bermuda</option> <option value="digitextjm.com">Digicel Jamaica</option> <option value="sms.edgewireless.com">Edge Wireless</option> <option value="fido.ca">Fido Canada</option> <option value="mobile.gci.net">General Communications Inc.</option> <option value="sms.goldentele.com">Golden Telecom</option> <option value="ideacellular.net">Idea Cellular</option> <option value="text.mtsmobility.com">Manitoba Telecom Systems</option> <option value="pcsms.com.au">Message Media (AU)</option> <option value="sms.messagebird.com">MessageBird</option> <option value="mymetropcs.com">Metro PCS</option> <option value="page.metrocall.com">Metrocall Pager</option> <option value="page.mobilfone.com">Mobilfone</option> <option value="sms.co.za">MTN (South Africa)</option> <option value="wirefree.informe.ca">NBTel</option> <option value="messaging.nextel.com">Nextel</option> <option value="npiwireless.com">NPI Wireless</option> <option value="pcs.ntelos.com">NTELOS</option> <option value="mmail.co.uk">O2 (UK)</option> <option value="optusmobile.com.au">Optus (Australia)</option> <option value="orange-gsm.ro">Orange (Romania)</option> <option value="orange.net">Orange (UK)</option> <option value="pagenet.net">Pagenet</option> <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option> <option value="ptel.net">Powertel</option> <option value="sms.pscel.com">PSC Wireless</option> <option value="pager.qualcomm.com">Qualcomm</option> <option value="qwestmp.com">Qwest</option> <option value="smtext.com">Simple Mobile</option> <option value="skytel.com">Skytel - Alphanumeric</option> <option value="sms.smsglobal.com.au">SMS Global</option> <option value="solavei.net">Solavei</option> <option value="messaging.sprintpcs.com">Sprint PCS</option> <option value="tms.suncom.com">SunCom</option> <option value="mobile.surewest.com">SureWest Communications</option> <option value="movistar.net">Telefonica Movistar</option> <option value="onlinesms.telstra.com">Telstra</option> <option value="msg.telus.com">Telus Mobility</option> <option value="t-mobile.uk.net">T-Mobile (UK)</option> <option value="tmomail.net" selected>T-Mobile USA</option> <option value="email.uscc.net">US Cellular</option> <option value="vtext.com">Verizon Wireless</option> <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option> <option value="myairmail.com">Verizon Wireless (myairmail.com)</option> <option value="vmobile.ca">Virgin (Canada)</option> <option value="vxtras.com">Virgin Mobile (UK)</option> <option value="vmobl.com">Virgin Mobile USA</option> <option value="voda.co.za">Vodacom (South Africa)</option> <option value="vodafone.net.au">Vodafone (AUS)</option> <option value="vodafone.net">Vodafone (UK)</option> <option value="airmessage.net">Weblink Wireless</option> <option value="wyndtell.com">WyndTell</option> </select><small class="help-block" data-fv-validator="callback" data-fv-for="phoneNumber" data-fv-result="NOT_VALIDATED" style="display: none;">Please enter a valid value</small></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_mobile(' + add_mobile + ',0)" id="delete-notifyEmail"><i style="font-size:22px;" class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" style="margin-left:-15px;" class="btn btn-primary" onclick="clear_mobile(' + add_mobile + ',0)">clear</i></button></div> </div>');
    }

    $('#contactForm')
        .find('[name="phoneNumber"]')
        .intlTelInput({
            utilsScript: 'build/js//utils.js',
            autoPlaceholder: true,
            preferredCountries: ['us', 'fr', 'gb']
        });

    /*$('#contactForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: '',
                invalid: '',
                validating: ''
            },
            fields: {
                phoneNumber: {
                    validators: {
                        callback: {
                            callback: function (value, validator, $field) {
                                var isValid = value === '' || $field.intlTelInput('isValidNumber'),
                                    err = $field.intlTelInput('getValidationError'),
                                    message = null;
                                switch (err) {
                                    case intlTelInputUtils.validationError.INVALID_COUNTRY_CODE:
                                        message = 'The country code is not valid';
                                        break;

                                    case intlTelInputUtils.validationError.TOO_SHORT:
                                        message = 'The phone number is too short';
                                        break;
                                    case intlTelInputUtils.validationError.TOO_LONG:
                                        message = 'The phone number is too long';
                                        break;

                                    case intlTelInputUtils.validationError.NOT_A_NUMBER:
                                        message = 'The value is not a number';
                                        break;

                                    default:
                                        message = 'The phone number is not valid';
                                        break;
                                }

                                return {
                                    valid: isValid,
                                    message: message
                                };
                            }
                        }
                    }
                }
            }
        })
        // Revalidate the number when changing the country
        .on('click', '.country-list', function () {
            $('#contactForm').formValidation('revalidateField', 'phoneNumber');
        });*/
}

/*function to clear enmil from input box*/

function clear_email(clear_email) {

    $('#notify-email-' + clear_email).val('');

}

/*function to clear mobile number for sms*/
function clear_mobile(clear_mobile) {

    $('#notify-sms-' + clear_mobile).val('');

}

/*function to delete mobile number for sms*/
function del_mobile(delete_mobile, delete_id) {

    if (delete_id == 0) {
        $('#notify_sms_' + delete_mobile).remove();
        add_mobile = delete_mobile - 1;
    } else {

        delete_mobile_data(delete_id);
        add_mobile = delete_mobile - 1;
    }
}

/*function to update mobile*/
function delete_mobile_data(delete_id) {

    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);

    }

    var user_id = userInfoObj[0].id;
    $.ajax({

        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=delete_mobile&user_id=" + user_id + "&delete_id=" + delete_id,

        success: function (response) {

            alert('mobile number deleted succesfully');

            getEmailNotifyData();
        }

    });


}

/*function to update email*/

/*function to update mobile*/
function delete_email_data(delete_id) {

    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);

    }

    var user_id = userInfoObj[0].id;
    $.ajax({

        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=delete_email&user_id=" + user_id + "&delete_id=" + delete_id,

        success: function (response) {

            alert('Email deleted succesfully');

            getEmailNotifyData();
        }

    });


}

/*function to save data to server */

$('#updateNotifySetup').on('click', function () {

    $('#refresh_overlay').css("display", "block");
    var email_address = [];
    var mobile_numbers = [];

    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);

    }
    var user_id = userInfoObj[0].id;

    /*get the all value of email on click of the submit button*/
    for (k = 1; k <= delete_email; k++) {
        if ($('#notify-email-' + k).val() != '' && $('#notify-email-' + k).val() != undefined) {

            email_address.push($('#notify-email-' + k).val());

        }
    }
    /*get the all value of mobile number on click of submit button*/
    for (m = 1; m <= add_mobile; m++) {
        if ($('#notify-sms-' + m).val() != '' && $('#notify-sms-' + m).val() != undefined) {


            if ($($('#service_provider-' + m).val() != "")) {

                mobile_numbers.push($('#notify-sms-' + m).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164) + '@' + $('#service_provider-' + m).val());

            } else {

                alert('Please Select Service Provider.');
                return false;
            }
            //mobile_numbers.push($('#notify-sms-'+m).val());

        }
    }

    if(intlTelInputValid === 1){
        $.ajax({

            url: _SERVICEPATHServer,
            type: 'POST',
            data: "action=notify_email&user_id=" + user_id + "&email_notify=" + email_address + "&mobile_notify=" + mobile_numbers,

            success: function (response) {

                var result = response;
                if (typeof(result) == 'string') {

                    result = JSON.parse(result);
                }

                if (result.status == 200) {

                    if ($('#updateNotifySetup').val() == 'Update') {

                        alert('data updated succesfully');

                    } else {

                        alert('data inserted succesfully');
                    }
                    getEmailNotifyData();
                } else {

                    alert('Email And Mobile not updated.')
                }
                $('#refresh_overlay').css("display", "none");

            }

            //$("#refresh_overlay").css("display","none");
        });
    }


});


/*function to get the eamil and mobile data if it exists*/

function getEmailNotifyData() {


    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);

    }
    var user_id = userInfoObj[0].id;
    $('#refresh_overlay').css("display", "block");
    $.ajax({

        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=get_notify_email&user_id=" + user_id,

        success: function (response) {

            var result = response;
            if (typeof(result) == 'string') {

                result = JSON.parse(response);

            }

            var mobileHtml = '';
            var emailHtml = '';

            if (result.status == 200) {
                service_provider_list = [];
                var k = 0;
                var m = 0;
                for (i = 0; i < result.data.length; i++) {

                    var mobile_data = result.data[i].mobile_notify;
                    var mobile_number = mobile_data.split('@');
                    var mobile_no = "+" + $.trim(mobile_number[0]);
                    var service_provider = mobile_number[1];
                    service_provider_list.push(service_provider);

                    if (i == 0 && result.data[i].mobile_notify == '') {

                        mobileHtml += '<div id="notify_sms_1"> <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Primary SMS:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="text" class=" form-control" id="notify-sms-1"  onkeyup="validPhoneNumber(this)" name="phoneNumber"><span>@</span> <select name="service_provider" id="service_provider-1" class=" form-control"> <option value="0">WIRELESS PROVIDER</option> <option value="msg.acsalaska.com">Alaska Communications (ACS)</option> <option value="message.alltel.com">Alltel</option> <option value="paging.acswireless.com">Ameritech (ACSWireless)</option> <option value="txt.att.net">AT&amp;T Wireless</option> <option value="page.att.net">ATT Business Messaging (US)</option> <option value="txt.bellmobility.ca">Bell Mobility Canada</option> <option value="blsdcs.net">BellSouth Mobility</option> <option value="blueskyfrog.com">Blue Sky Frog</option> <option value="myboostmobile.com">Boost Mobile</option> <option value="sms-c1.bm">Cellone - Bermuda</option> <option value="csouth1.com">Cellular South</option> <option value="mobile.celloneusa.com">CellularOne (Dobson)</option> <option value="cwemail.com">Centennial Wireless</option> <option value="cingularme.com">Cingular</option> <option value="mobile.mycingular.com">Cingular</option> <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option> <option value="sms.ctimovil.com.ar">Claro Argentina</option> <option value="mms.cricketwireless.net">Cricket</option> <option value="digitextbm.com">Digicel Bermuda</option> <option value="digitextjm.com">Digicel Jamaica</option> <option value="sms.edgewireless.com">Edge Wireless</option> <option value="fido.ca">Fido Canada</option> <option value="mobile.gci.net">General Communications Inc.</option> <option value="sms.goldentele.com">Golden Telecom</option> <option value="ideacellular.net">Idea Cellular</option> <option value="text.mtsmobility.com">Manitoba Telecom Systems</option> <option value="pcsms.com.au">Message Media (AU)</option> <option value="sms.messagebird.com">MessageBird</option> <option value="mymetropcs.com">Metro PCS</option> <option value="page.metrocall.com">Metrocall Pager</option> <option value="page.mobilfone.com">Mobilfone</option> <option value="sms.co.za">MTN (South Africa)</option> <option value="wirefree.informe.ca">NBTel</option> <option value="messaging.nextel.com">Nextel</option> <option value="npiwireless.com">NPI Wireless</option> <option value="pcs.ntelos.com">NTELOS</option> <option value="mmail.co.uk">O2 (UK)</option> <option value="optusmobile.com.au">Optus (Australia)</option> <option value="orange-gsm.ro">Orange (Romania)</option> <option value="orange.net">Orange (UK)</option> <option value="pagenet.net">Pagenet</option> <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option> <option value="ptel.net">Powertel</option> <option value="sms.pscel.com">PSC Wireless</option> <option value="pager.qualcomm.com">Qualcomm</option> <option value="qwestmp.com">Qwest</option> <option value="smtext.com">Simple Mobile</option> <option value="skytel.com">Skytel - Alphanumeric</option> <option value="sms.smsglobal.com.au">SMS Global</option> <option value="solavei.net">Solavei</option> <option value="messaging.sprintpcs.com">Sprint PCS</option> <option value="tms.suncom.com">SunCom</option> <option value="mobile.surewest.com">SureWest Communications</option> <option value="movistar.net">Telefonica Movistar</option> <option value="onlinesms.telstra.com">Telstra</option> <option value="msg.telus.com">Telus Mobility</option> <option value="t-mobile.uk.net">T-Mobile (UK)</option> <option value="tmomail.net" selected>T-Mobile USA</option> <option value="email.uscc.net">US Cellular</option> <option value="vtext.com">Verizon Wireless</option> <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option> <option value="myairmail.com">Verizon Wireless (myairmail.com)</option> <option value="vmobile.ca">Virgin (Canada)</option> <option value="vxtras.com">Virgin Mobile (UK)</option> <option value="vmobl.com">Virgin Mobile USA</option> <option value="voda.co.za">Vodacom (South Africa)</option> <option value="vodafone.net.au">Vodafone (AUS)</option> <option value="vodafone.net">Vodafone (UK)</option> <option value="airmessage.net">Weblink Wireless</option> <option value="wyndtell.com">WyndTell</option> </select><small class="help-block" data-fv-validator="callback" data-fv-for="phoneNumber" data-fv-result="NOT_VALIDATED" style="display: none;">Please enter a valid value</small></div></div>';
                    }

                    if (i == 0 && result.data[i].email_notify == '') {

                        emailHtml += '<div id="notify_email_1"><div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Primary eMail:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="email" class=" form-control" id="notify-email-1" placeholder="Primary eMail address for notifications"></div></div></div>';

                    }

                    if (result.data[i].mobile_notify != '') {
                        k++;
                        if (k == 1) {
                            mobileHtml += '<div id="notify_sms_' + k + '"> <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Primary SMS:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="text" class=" form-control" id="notify-sms-' + k + '"  onkeyup="validPhoneNumber(this)" name="phoneNumber" value="' + mobile_no + '"><span>@</span> <select name="service_provider" id="service_provider-' + k + '" class=" form-control"> <option value="0">WIRELESS PROVIDER</option> <option value="msg.acsalaska.com">Alaska Communications (ACS)</option> <option value="message.alltel.com">Alltel</option> <option value="paging.acswireless.com">Ameritech (ACSWireless)</option> <option value="txt.att.net">AT&amp;T Wireless</option> <option value="page.att.net">ATT Business Messaging (US)</option> <option value="txt.bellmobility.ca">Bell Mobility Canada</option> <option value="blsdcs.net">BellSouth Mobility</option> <option value="blueskyfrog.com">Blue Sky Frog</option> <option value="myboostmobile.com">Boost Mobile</option> <option value="sms-c1.bm">Cellone - Bermuda</option> <option value="csouth1.com">Cellular South</option> <option value="mobile.celloneusa.com">CellularOne (Dobson)</option> <option value="cwemail.com">Centennial Wireless</option> <option value="cingularme.com">Cingular</option> <option value="mobile.mycingular.com">Cingular</option> <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option> <option value="sms.ctimovil.com.ar">Claro Argentina</option> <option value="mms.cricketwireless.net">Cricket</option> <option value="digitextbm.com">Digicel Bermuda</option> <option value="digitextjm.com">Digicel Jamaica</option> <option value="sms.edgewireless.com">Edge Wireless</option> <option value="fido.ca">Fido Canada</option> <option value="mobile.gci.net">General Communications Inc.</option> <option value="sms.goldentele.com">Golden Telecom</option> <option value="ideacellular.net">Idea Cellular</option> <option value="text.mtsmobility.com">Manitoba Telecom Systems</option> <option value="pcsms.com.au">Message Media (AU)</option> <option value="sms.messagebird.com">MessageBird</option> <option value="mymetropcs.com">Metro PCS</option> <option value="page.metrocall.com">Metrocall Pager</option> <option value="page.mobilfone.com">Mobilfone</option> <option value="sms.co.za">MTN (South Africa)</option> <option value="wirefree.informe.ca">NBTel</option> <option value="messaging.nextel.com">Nextel</option> <option value="npiwireless.com">NPI Wireless</option> <option value="pcs.ntelos.com">NTELOS</option> <option value="mmail.co.uk">O2 (UK)</option> <option value="optusmobile.com.au">Optus (Australia)</option> <option value="orange-gsm.ro">Orange (Romania)</option> <option value="orange.net">Orange (UK)</option> <option value="pagenet.net">Pagenet</option> <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option> <option value="ptel.net">Powertel</option> <option value="sms.pscel.com">PSC Wireless</option> <option value="pager.qualcomm.com">Qualcomm</option> <option value="qwestmp.com">Qwest</option> <option value="smtext.com">Simple Mobile</option> <option value="skytel.com">Skytel - Alphanumeric</option> <option value="sms.smsglobal.com.au">SMS Global</option> <option value="solavei.net">Solavei</option> <option value="messaging.sprintpcs.com">Sprint PCS</option> <option value="tms.suncom.com">SunCom</option> <option value="mobile.surewest.com">SureWest Communications</option> <option value="movistar.net">Telefonica Movistar</option> <option value="onlinesms.telstra.com">Telstra</option> <option value="msg.telus.com">Telus Mobility</option> <option value="t-mobile.uk.net">T-Mobile (UK)</option> <option value="tmomail.net" selected>T-Mobile USA</option> <option value="email.uscc.net">US Cellular</option> <option value="vtext.com">Verizon Wireless</option> <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option> <option value="myairmail.com">Verizon Wireless (myairmail.com)</option> <option value="vmobile.ca">Virgin (Canada)</option> <option value="vxtras.com">Virgin Mobile (UK)</option> <option value="vmobl.com">Virgin Mobile USA</option> <option value="voda.co.za">Vodacom (South Africa)</option> <option value="vodafone.net.au">Vodafone (AUS)</option> <option value="vodafone.net">Vodafone (UK)</option> <option value="airmessage.net">Weblink Wireless</option> <option value="wyndtell.com">WyndTell</option> </select><small class="help-block" data-fv-validator="callback" data-fv-for="phoneNumber" data-fv-result="NOT_VALIDATED" style="display: none;">Please enter a valid value</small></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary"  id="clear-notifymobile">clear</button></div></div>';
                            add_mobile = k;
                        } else if (k == 2) {

                            mobileHtml += '<div id="notify_sms_' + k + '"> <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Secondary SMS:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="text" class=" form-control" id="notify-sms-' + k + '" onkeyup="validPhoneNumber(this)" name="phoneNumber" value="' + mobile_no + '"><span>@</span> <select name="service_provider" id="service_provider-' + k + '" class=" form-control"> <option value="0">WIRELESS PROVIDER</option> <option value="msg.acsalaska.com">Alaska Communications (ACS)</option> <option value="message.alltel.com">Alltel</option> <option value="paging.acswireless.com">Ameritech (ACSWireless)</option> <option value="txt.att.net">AT&amp;T Wireless</option> <option value="page.att.net">ATT Business Messaging (US)</option> <option value="txt.bellmobility.ca">Bell Mobility Canada</option> <option value="blsdcs.net">BellSouth Mobility</option> <option value="blueskyfrog.com">Blue Sky Frog</option> <option value="myboostmobile.com">Boost Mobile</option> <option value="sms-c1.bm">Cellone - Bermuda</option> <option value="csouth1.com">Cellular South</option> <option value="mobile.celloneusa.com">CellularOne (Dobson)</option> <option value="cwemail.com">Centennial Wireless</option> <option value="cingularme.com">Cingular</option> <option value="mobile.mycingular.com">Cingular</option> <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option> <option value="sms.ctimovil.com.ar">Claro Argentina</option> <option value="mms.cricketwireless.net">Cricket</option> <option value="digitextbm.com">Digicel Bermuda</option> <option value="digitextjm.com">Digicel Jamaica</option> <option value="sms.edgewireless.com">Edge Wireless</option> <option value="fido.ca">Fido Canada</option> <option value="mobile.gci.net">General Communications Inc.</option> <option value="sms.goldentele.com">Golden Telecom</option> <option value="ideacellular.net">Idea Cellular</option> <option value="text.mtsmobility.com">Manitoba Telecom Systems</option> <option value="pcsms.com.au">Message Media (AU)</option> <option value="sms.messagebird.com">MessageBird</option> <option value="mymetropcs.com">Metro PCS</option> <option value="page.metrocall.com">Metrocall Pager</option> <option value="page.mobilfone.com">Mobilfone</option> <option value="sms.co.za">MTN (South Africa)</option> <option value="wirefree.informe.ca">NBTel</option> <option value="messaging.nextel.com">Nextel</option> <option value="npiwireless.com">NPI Wireless</option> <option value="pcs.ntelos.com">NTELOS</option> <option value="mmail.co.uk">O2 (UK)</option> <option value="optusmobile.com.au">Optus (Australia)</option> <option value="orange-gsm.ro">Orange (Romania)</option> <option value="orange.net">Orange (UK)</option> <option value="pagenet.net">Pagenet</option> <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option> <option value="ptel.net">Powertel</option> <option value="sms.pscel.com">PSC Wireless</option> <option value="pager.qualcomm.com">Qualcomm</option> <option value="qwestmp.com">Qwest</option> <option value="smtext.com">Simple Mobile</option> <option value="skytel.com">Skytel - Alphanumeric</option> <option value="sms.smsglobal.com.au">SMS Global</option> <option value="solavei.net">Solavei</option> <option value="messaging.sprintpcs.com">Sprint PCS</option> <option value="tms.suncom.com">SunCom</option> <option value="mobile.surewest.com">SureWest Communications</option> <option value="movistar.net">Telefonica Movistar</option> <option value="onlinesms.telstra.com">Telstra</option> <option value="msg.telus.com">Telus Mobility</option> <option value="t-mobile.uk.net">T-Mobile (UK)</option> <option value="tmomail.net" selected>T-Mobile USA</option> <option value="email.uscc.net">US Cellular</option> <option value="vtext.com">Verizon Wireless</option> <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option> <option value="myairmail.com">Verizon Wireless (myairmail.com)</option> <option value="vmobile.ca">Virgin (Canada)</option> <option value="vxtras.com">Virgin Mobile (UK)</option> <option value="vmobl.com">Virgin Mobile USA</option> <option value="voda.co.za">Vodacom (South Africa)</option> <option value="vodafone.net.au">Vodafone (AUS)</option> <option value="vodafone.net">Vodafone (UK)</option> <option value="airmessage.net">Weblink Wireless</option> <option value="wyndtell.com">WyndTell</option> </select><small class="help-block" data-fv-validator="callback" data-fv-for="phoneNumber" data-fv-result="NOT_VALIDATED" style="display: none;">Please enter a valid value</small></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_mobile(' + k + ',' + result.data[i].id + ')" id="delete-notifyEmail"><i style="font-size:18px;"class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button style="margin-left:-15px;" type="button" class="btn btn-primary" onclick="clear_mobile(' + k + ',' + result.data[i].id + ')">clear</button></div> </div>';

                            add_mobile = k;

                        } else {

                            mobileHtml += '<div id="notify_sms_' + k + '"> <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Additional SMS:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="text" class=" form-control" id="notify-sms-' + k + '"  onkeyup="validPhoneNumber(this)" name="phoneNumber" value="' + mobile_no + '"><span>@</span> <select name="service_provider" id="service_provider-' + k + '" class=" form-control"> <option value="0">WIRELESS PROVIDER</option> <option value="msg.acsalaska.com">Alaska Communications (ACS)</option> <option value="message.alltel.com">Alltel</option> <option value="paging.acswireless.com">Ameritech (ACSWireless)</option> <option value="txt.att.net">AT&amp;T Wireless</option> <option value="page.att.net">ATT Business Messaging (US)</option> <option value="txt.bellmobility.ca">Bell Mobility Canada</option> <option value="blsdcs.net">BellSouth Mobility</option> <option value="blueskyfrog.com">Blue Sky Frog</option> <option value="myboostmobile.com">Boost Mobile</option> <option value="sms-c1.bm">Cellone - Bermuda</option> <option value="csouth1.com">Cellular South</option> <option value="mobile.celloneusa.com">CellularOne (Dobson)</option> <option value="cwemail.com">Centennial Wireless</option> <option value="cingularme.com">Cingular</option> <option value="mobile.mycingular.com">Cingular</option> <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option> <option value="sms.ctimovil.com.ar">Claro Argentina</option> <option value="mms.cricketwireless.net">Cricket</option> <option value="digitextbm.com">Digicel Bermuda</option> <option value="digitextjm.com">Digicel Jamaica</option> <option value="sms.edgewireless.com">Edge Wireless</option> <option value="fido.ca">Fido Canada</option> <option value="mobile.gci.net">General Communications Inc.</option> <option value="sms.goldentele.com">Golden Telecom</option> <option value="ideacellular.net">Idea Cellular</option> <option value="text.mtsmobility.com">Manitoba Telecom Systems</option> <option value="pcsms.com.au">Message Media (AU)</option> <option value="sms.messagebird.com">MessageBird</option> <option value="mymetropcs.com">Metro PCS</option> <option value="page.metrocall.com">Metrocall Pager</option> <option value="page.mobilfone.com">Mobilfone</option> <option value="sms.co.za">MTN (South Africa)</option> <option value="wirefree.informe.ca">NBTel</option> <option value="messaging.nextel.com">Nextel</option> <option value="npiwireless.com">NPI Wireless</option> <option value="pcs.ntelos.com">NTELOS</option> <option value="mmail.co.uk">O2 (UK)</option> <option value="optusmobile.com.au">Optus (Australia)</option> <option value="orange-gsm.ro">Orange (Romania)</option> <option value="orange.net">Orange (UK)</option> <option value="pagenet.net">Pagenet</option> <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option> <option value="ptel.net">Powertel</option> <option value="sms.pscel.com">PSC Wireless</option> <option value="pager.qualcomm.com">Qualcomm</option> <option value="qwestmp.com">Qwest</option> <option value="smtext.com">Simple Mobile</option> <option value="skytel.com">Skytel - Alphanumeric</option> <option value="sms.smsglobal.com.au">SMS Global</option> <option value="solavei.net">Solavei</option> <option value="messaging.sprintpcs.com">Sprint PCS</option> <option value="tms.suncom.com">SunCom</option> <option value="mobile.surewest.com">SureWest Communications</option> <option value="movistar.net">Telefonica Movistar</option> <option value="onlinesms.telstra.com">Telstra</option> <option value="msg.telus.com">Telus Mobility</option> <option value="t-mobile.uk.net">T-Mobile (UK)</option> <option value="tmomail.net" selected>T-Mobile USA</option> <option value="email.uscc.net">US Cellular</option> <option value="vtext.com">Verizon Wireless</option> <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option> <option value="myairmail.com">Verizon Wireless (myairmail.com)</option> <option value="vmobile.ca">Virgin (Canada)</option> <option value="vxtras.com">Virgin Mobile (UK)</option> <option value="vmobl.com">Virgin Mobile USA</option> <option value="voda.co.za">Vodacom (South Africa)</option> <option value="vodafone.net.au">Vodafone (AUS)</option> <option value="vodafone.net">Vodafone (UK)</option> <option value="airmessage.net">Weblink Wireless</option> <option value="wyndtell.com">WyndTell</option> </select><small class="help-block" data-fv-validator="callback" data-fv-for="phoneNumber" data-fv-result="NOT_VALIDATED" style="display: none;">Please enter a valid value</small></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_mobile(' + k + ',' + result.data[i].id + ')" id="delete-notifyEmail"><i style="font-size:18px;"class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button style="margin-left:-15px;"  type="button" class="btn btn-primary" onclick="del_mobile(' + k + ',' + result.data[i].id + ')" >clear</button></div> </div>';

                            add_mobile = k;

                        }
                        //$('#service_provider-1 option[value='+service_provider+']').attr("selected", "selected");


                    }

                    if (result.data[i].email_notify != '') {
                        m++;
                        if (m == 1) {
                            emailHtml += '<div id="notify_email_' + m + '"><div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Primary eMail:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="email" class=" form-control" id="notify-email-' + m + '" placeholder="Primary eMail address for notifications" value="' + result.data[i].email_notify + '"></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" id="clear-notifyemail">clear</button></div></div></div>';
                            delete_email = m;
                        } else if (m == 2) {

                            emailHtml += '<div id="notify_email_' + m + '"><div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Secondary eMail:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="email" class=" form-control" id="notify-email-' + m + '" placeholder="Secondary eMail address for notifications" value="' + result.data[i].email_notify + '"></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_email(' + m + ',' + result.data[i].id + ')" id="delete-notifyEmail"><i style="font-size:18px;"class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button style="margin-left:-15px;" type="button" class="btn btn-primary" onclick="clear_email(' + m + ',' + result.data[i].id + ')">clear</button></div></div></div>';
                            delete_email = m;

                        } else {

                            emailHtml += '<div id="notify_email_' + m + '"><div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;"><label >Q&R Notify Additional eMail:</label></div> <div class="col-sm-6" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input type="email" class=" form-control" id="notify-email-' + m + '" placeholder="Additional eMail address for notifications" value="' + result.data[i].email_notify + '"></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button type="button" class="btn btn-primary" onclick="del_email(' + m + ',' + result.data[i].id + ')" id="delete-notifyEmail"><i style="font-size:18px;" class="glyphicon glyphicon-trash"></i></button></div><div class="col-sm-1" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><button style="margin-left:-15px;" type="button" class="btn btn-primary" onclick="clear_email(' + m + ',' + result.data[i].id + ')">clear</button></div></div></div>';
                            delete_email = m;

                        }
                    }
                }

                $('#updateNotifySetup').val('Update');
                $('#notify_emails').html(emailHtml);
                $('#notify_sms').html(mobileHtml);

                for (j = 0; j < service_provider_list.length; j++) {
                    var service_length = j + 1;
                    $('#service_provider-' + service_length).val(service_provider_list[j]);
                }

                $('#refresh_overlay').css("display", "none");
                $('#contactForm').find('[name="phoneNumber"]').intlTelInput({
                    utilsScript: 'build/js//utils.js',
                    autoPlaceholder: true,
                    autoFormat: true,
                    formatOnDisplay: true,
                    preferredCountries: ['us', 'fr', 'gb']
                });
                // $('#contactForm')
                //    .formValidation({
                //        framework: 'bootstrap',
                //        icon: {
                //            valid: '',
                //            invalid: '',
                //            validating: ''
                //        },
                //        fields: {
                //            phoneNumber: {
                //                validators: {
                //                    callback: {
                //                        callback: function(value, validator, $field) {
                //                            var isValid = value === '' || $field.intlTelInput('isValidNumber'),
                //                                err     = $field.intlTelInput('getValidationError'),
                //                                message = null;
                //                            switch (err) {
                //                                case intlTelInputUtils.validationError.INVALID_COUNTRY_CODE:
                //                                    message = 'The country code is not valid';
                //                                    break;

                //                                case intlTelInputUtils.validationError.TOO_SHORT:
                //                                    message = 'The phone number is too short';
                //                                    break;

                //                                case intlTelInputUtils.validationError.TOO_LONG:
                //                                    message = 'The phone number is too long';
                //                                    break;

                //                                case intlTelInputUtils.validationError.NOT_A_NUMBER:
                //                                    message = 'The value is not a number';
                //                                    break;

                //                                default:
                //                                    message = 'The phone number is not valid';
                //                                    break;
                //                            }

                //                            return {
                //                                valid: isValid,
                //                                message: message
                //                            };
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    })
                //    // Revalidate the number when changing the country
                //    .on('click', '.country-list', function() {
                //        // $('#contactForm').formValidation('revalidateField', 'phoneNumber');
                //    });

            } else {

                $('#refresh_overlay').css("display", "none");
                //location.reload();
                $('#updateNotifySetup').val('Submit');

            }

            $('#clear-notifymobile').on('click', function () {

                $('#notify-sms-1').val('');

            });

            $('#clear-notifyemail').on('click', function () {

                $('#notify-email-1').val('');

            });

        }

    });

}

