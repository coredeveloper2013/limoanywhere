

/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: Airport
    Author: Mylimoproject
---------------------------------------------*/
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHHoliday="phpfile/holiday_client.php";
var ADDMILEAGE =0;
/*onload function call on page load*/
$(function(){

	
    /*click on back button*/ 
	$('#back_button').on("click",function(){
		location.reload(true);
	});
    /*set out function start*/
	setTimeout(function () {
		$('.time-picker').datetimepicker({
		    pickDate: false,
		    useCurrent: false,
		    language: 'ru'
		});

		$(".datepicker").datetimepicker({
			pickTime: false,
		    changeMonth: true,
		    changeYear: true,
		    "dateFormat": "dd-mm-yy"
		});

    },1500);

	$("#amount").keypress(function (e) {

         //if the letter is not digit then display error and don't type anything

	     if(e.which != 46 && e.which != 45 && e.which != 46 &&

	      !(e.which >= 48 && e.which <= 57))  {

	        return false;
	    }

    });

    /*call the function on page load*/
	getSMA();
    getServiceTypes();
	getVehicleType();
	getRateMatrixList();

	/***************************************************************
		@Method Name	:On clicking edit button of the list
		@Desc			:Populate All the details of the Rate matrix 
    ****************************************************************/
	$('.rate_matrix_onedit').on("click",function(){
		getSequence=$(this).attr("seq");
		editRateMatrix(getSequence);

	 });
});

/*click on the onload */
isUserApiKey();

/*---------------------------------------------
    Function Name: isUserApiKey()
    Input Parameter: user_id
    return:json data
 ---------------------------------------------*/
	function isUserApiKey()
	{	
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;	
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}	
		$.ajax({
			url: _SERVICEPATHServer,
			type: 'POST',
			data: "action=getApiKey&user_id="+userInfoObj[0].id,
			success: function(response) {		
			    var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);			
				}
				if(responseObj.code=="1017")
				{		
				    window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));		  		  
				  	var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
					if(typeof(getLocalStoragevalueUserInformation)=="string")
				    {
				   	    getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
				    }
				    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
					$('#add_sma_button').on('click',function(){
						$('#add_sma').show();								
					})	
					$("#popup_close_add_sma").off();
					$('#popup_close_add_sma').on("click",function(){
					    $('#add_sma').hide();
			        });	

				}
				else
				{			
					window.location.href="connectwithlimo.html";			
				}			
			}

		});
	
	}


  /*---------------------------------------------
    Function Name: isUserApiKey()
    Input Parameter: user_id
    return:json data
  ---------------------------------------------*/
  function getSMA()
  {
	    var userInfo=window.localStorage.getItem("companyInfo");
	    var userInfoObj=userInfo;
	    if(typeof(userInfo)=="string")
	    {
		    userInfoObj=JSON.parse(userInfo);

	    }
		$.ajax({
		    url: _SERVICEPATHSma,
		    type: 'POST',
		    data: "action=getSMA&user_id="+userInfoObj[0].id,
		    success: function(response) {
			    var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}	
				var responseHTML='';
                for(var i=0; i<responseObj.data.length; i++)
  				{	
                    if(responseObj.data[i].id!=null){
					    responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
                    }
				}
               	$('#apply_sma').html(responseHTML);
				setTimeout(function(){				
					$("#apply_sma").multiselect('destroy');
						$('#apply_sma').multiselect({
						maxHeight: 200,
						buttonWidth: '155px',
						includeSelectAllOption: true
					});

				},400);
		    },
		    error:function(){
			     alert("Some Error");
			}
		});
    }

		
   /**********************************************
	    Method: 			getServiceTypes()
        Return:Rate 		List Of service type Name
	*************************************************/
	function getServiceTypes()
	{
	    var userInfo=window.localStorage.getItem("companyInfo");
	    var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
	    }  
	    var serviceTypeData = [];
	    $.ajax({
	        url : "phpfile/service_type.php",
	        type : 'post',
	        data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
	        dataType : 'json',
	        success : function(data){
	         
	            if(data.ResponseText == 'OK'){
	                var ResponseHtml='';
	                $.each(data.ServiceTypes.ServiceType, function( index, result){
	                         ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
	                });
	               	$('#service_type').html(ResponseHtml);
	            }
	            $("#service_type").multiselect('destroy');
	            $('#service_type').multiselect({
	                maxHeight: 200,
	                buttonWidth: '178px',
	                includeSelectAllOption: true,
    onChange: function(element, checked) {
        var brands = $('#service_type option:selected');
        var pps = 0;
        $(brands).each(function(index, brand){
            var service_tp = $(this).val();
            if(service_tp=='PPS'){
            	  pps = 1;

            }
        });


if(pps==1){
            	 
 $('.per_pax_holiday').addClass('per_pax_holiday2').removeClass('per_pax_holiday');
            }else{
$('.per_pax_holiday2').addClass('per_pax_holiday').removeClass('per_pax_holiday2');
            }
       
    }

	            });
	             $('input:checkbox[value="HRLY"]').attr('disabled', true);
	        }
	    });
	}

	
   /*---------------------------------------------
       Function Name: getVehicleType()
       Input Parameter: user_id
       return:json data
   ---------------------------------------------*/
    function getVehicleType()
	{
	    var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	    if(typeof(getLocalStoragevalue)=="string")
	    {
	        getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
     	}

     	var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

	    $.ajax({
		    url: _SERVICEPATH,
		    type: 'POST',
		    data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
		    success: function(response) {
		        var responseHTML='';
			    var responseObj=response;
			    if(typeof(response)=="string")
			    {
				    responseObj=JSON.parse(response);					
			    }
			    responseHTML='';
			    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
			    {
					responseHTML +='<tr><td><input type="checkbox" class="add_rate_check" id="checked_by_id_'+i+'"  name="vehicle" value="'+i+'"></td><td class="rate_vehicle_code_box_'+i+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td> <td><input placeholder="$" class="rate_input_box_'+i+'"  type="number" disabled style="width:56%;"></td></tr>';

			    }
			    $('#vehicl_rate_name').html(responseHTML);
                $('.add_rate_check').on("change",function()
                 {

			        var getseq=$(this).attr("value");
			        if($(this).is(":checked"))
			        {	
					    $('.rate_input_box_'+getseq).removeAttr("disabled");
			        }
			        else
			        {
						$('.rate_input_box_'+getseq).val("");
						$('.rate_input_box_'+getseq).attr("disabled","true");	
				    }

			    });

                /*click on selected vehicle rate button*/     
				$('#selected_vehicle_rat_btn').on("click",function(){
					var getVecleValue=[];
					var i=0;
					var checkFullArrayFlag='enable';
					$('.add_rate_check').each(function(index,selectedValue){
						var getSeq=$(this).attr("value");
						if($(this).is(":checked"))
						{	
						    if($('.rate_input_box_'+getSeq).val()!="")
							{
								getVecleValue.push({"vehicle_rate":$('.rate_input_box_'+getSeq).val(),
												"vehicle_code":$('.rate_vehicle_code_box_'+getSeq).html()
						        });
							
								i++;
							}
							else
							{
								checkFullArrayFlag="desable";
								alert("Enter Rate For All Selected Vehicle");
							}
						}

					});

					if(getVecleValue.length>0 && checkFullArrayFlag=="enable")
					{
						window.localStorage.setItem("rateSetupValue",JSON.stringify(getVecleValue));
						$('#add_rate_modal').hide();
					}

				});

			}

		});

	}

			

	$('#save_rate').on("click",function(){
		$('#refresh_overlay').css("display","block");
		var calender=$('#calender').val();
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
	    var holiday_name = $('#holiday_name').val();
		var service_type = $('#service_type option:selected');
	    var service_typeObject = [];
		$(service_type).each(function(index, service_type){
		    service_typeObject .push($(this).val());
		});
		var selectedVehicle = $('#apply_vehicle_type option:selected');
		var selectedVehicleObject = [];
		var getvehicleRateSetUp=window.localStorage.getItem("rateSetupValue");
		$(selectedVehicle).each(function(index, selectedVehicle){
		    selectedVehicleObject .push($(this).val());
		});
	    var selectedSma = $('#apply_sma option:selected');
		var selectedSmaObject = [];
		$(selectedSma).each(function(index, selectedSma){
		    selectedSmaObject .push($(this).val());
		});


	
        var allowORES = $('input[name=allowORES]:checked', '#holiday_Form').val();
        var Cut_off_time = $('#Cut_off_time').val();
        var blackout_Msg = $('#blackout_Msg').val();
        var holiday_surcg_per_pax = $('#holiday_surcg_per_pax').val();
        var holiday_surcg_type = $('#holiday_surcg_type').val();



	    if(holiday_name=='')
	    {
		    $('#refresh_overlay').css("display","none");
			alert("Please Enter Holiday Name");
		}
	    else if(getvehicleRateSetUp==null)
		{
		    $('#refresh_overlay').css("display","none");
			alert("Please Enter Vehicle Price");
		}
		else if(calender=='')
	    {
		    $('#refresh_overlay').css("display","none"); 
			alert("Please Select Date");
		}
		else if(selectedSmaObject=='')
		{
		    $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One SMA ");
		}
	    else if(service_typeObject=='')
		{
			$('#refresh_overlay').css("display","none");
			alert("Please Select At Least One service type ");
	    } 
		else
		{
			var start_time=$('#start_time').val();
			var end_time=$('#end_time').val();
			var newDateStart=new Date("01/01/2016"+" "+start_time);
			var endDateStart=new Date("01/01/2016"+" "+end_time);
		    if(newDateStart>endDateStart)
			{
			    alert("End time is greater than start time");
				$('#refresh_overlay').css("display","none");
				return 0;
			}

			if(start_time!='' && start_time!="null" && start_time!=null && start_time!=undefined && end_time!='' && end_time!="null" && end_time!=null && end_time!=undefined)
			{

				var start_time_array=start_time.split(':');							
				var end_time_array=end_time.split(':');							
				var start_hour=start_time_array[1].split(' ');
				var end_hour=end_time_array[1].split(' ');
				if(end_hour[1]!=start_hour[1])
				{

				}
				else
				{
                    if(start_hour[1]=="PM")
					{
						start_time_array[0]=parseInt(start_time_array[0])+12;
					}
					if(end_hour[1]=="PM")
					{
						end_time_array[0]=parseInt(end_time_array[0])+12;
					}
					var initial_time=parseInt(start_time_array[0])*60+parseInt(start_hour[0]);
					var final_time=parseInt(end_time_array[0])*60+parseInt(end_hour[0]);

			    }

			}

				if($('#save_rate').html()=='Update')
			    {
					$('#save_rate').html('Processing...');
					$('#save_rate').attr("disable","true");
					var holiday_id=$('#save_rate').attr('seq')

						$.ajax({
							url: _SERVICEPATHHoliday,
							type: 'POST',
						    data: "action=deleteHoliday&holiday_id="+holiday_id,
							success: function(response) {
								$('#refresh_overlay').css("display","none");
								if(typeof(response)=="string")
						        {
							        responseObj=JSON.parse(response);
						        }
						        if(responseObj.code==1010)
						        {
								$.ajax({
									url: _SERVICEPATHHoliday,
									type: 'POST',
									data: "action=setHoliday&user_id="+userInfoObj[0].id+"&name="+holiday_name+"&vehicle_code="+getvehicleRateSetUp+"&calender="+calender+"&start_time="+start_time+"&end_time="+end_time+"&sma_id="+selectedSmaObject+"&service_typeObject="+service_typeObject+"&allowORES="+allowORES+"&Cut_off_time="+Cut_off_time+"&blackout_Msg="+blackout_Msg+"&holiday_surcg_per_pax="+holiday_surcg_per_pax+"&holiday_surcg_type="+holiday_surcg_type,
									success: function(response) {
										$('#refresh_overlay').css("display","none");
										alert("Holiday Updated Successfully");
										//console.log(response);
										location.reload(true);
									},
									error:function(){
										$('#save_rate').html('Update');
					    				$('#save_rate').attr("disable","false");
										alert("Some Error");
									}
										
								});

						    }
							else
						    {
							    alert("Some Server Error while Updating.");
							    $('#save_rate').html('Update');
					    	    $('#save_rate').attr("disable","false");
						    }
	                    },
					    error:function(){
		                    $('#refresh_overlay').css("display","none"); 
						    $('#save_rate').html('Update');
					        $('#save_rate').attr("disable","false");
						    alert("Some Error");

						}

							

					});

				}
				else
				{	
					$('#save_rate').html('Processing...');
					$('#save_rate').attr("disable","true");
					$.ajax({
						url: _SERVICEPATHHoliday,
						type: 'POST',
						data: "action=setHoliday&user_id="+userInfoObj[0].id+"&name="+holiday_name+"&vehicle_code="+getvehicleRateSetUp+"&calender="+calender+"&start_time="+start_time+"&end_time="+end_time+"&sma_id="+selectedSmaObject+"&service_typeObject="+service_typeObject+"&allowORES="+allowORES+"&Cut_off_time="+Cut_off_time+"&blackout_Msg="+blackout_Msg+"&holiday_surcg_per_pax="+holiday_surcg_per_pax+"&holiday_surcg_type="+holiday_surcg_type,
						success: function(response) {
						    $('#refresh_overlay').css("display","none");
							if(response=='"sameName"')
								{
									alert("Holiday Name Must Be Unique ");
									$('#save_rate').html('Save');
							    }	
								else
								{
									localStorage.removeItem("rateSetupValue");
									alert("Holiday Surcharge inserted Successfully");	
									location.reload(true);
								}

					        },
							error:function(){
								$('#refresh_overlay').css("display","none");
								$('#save_rate').html('Save');
					            $('#save_rate').attr("disable","false");
						        alert("Some Error");

						    }
						
					});

			    }

			}

		});

		/*********************************************************
			@MethodName			:getRateMatrix
			@Description		:Get list of all Rate matrixes
			@param				:user_id
			@return			    :list of rate matrix
	    *********************************************************/
		function getRateMatrixList()
		{
			$('#refresh_overlay').css("display","block");
			var userInfo=window.localStorage.getItem("companyInfo");
			var userInfoObj=userInfo;
			if(typeof(userInfo)=="string")
			{
				userInfoObj=JSON.parse(userInfo);
			}	
			$.ajax({
				url:_SERVICEPATHHoliday,
				type:'POST',
				data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
				success:function(response)
				{
					var responseObj=response;
					var responseHTML='';
					var responseVeh='';
					var responseSma='';
					var vehicle_code='';
					var sma_name='';
					var vehicle_code_array='';
					var vehicle_rate_new_array='';
					var sma_name_array='';
					var service_type_array='';
					var service_name_array='';
					var cd_value='';
					var sma_id='';
					var allowORES='';
					var Cut_off_time='';
					var blackout_Msg='';
					var holiday_surcg_per_pax='';
					var holiday_surcg_type='';

					    
					if(typeof(response)=="string")
					{
						responseObj=JSON.parse(response);
					}
					if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
						 responseObj.data!="")
					{

						for(var i=0; i<responseObj.data.length; i++)
						{
							responseVeh=0;
						 	responseSma=0;
							responseValue=0;
							vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
							vehicle_rate = responseObj.data[i].vehicle_rate.replace(/,\s*$/, "");
							vehicle_code_array=vehicle_code.split(',');
							vehicle_rate_new_array=vehicle_rate.split(',');
							sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
							sma_name_array=sma_name.split(',');
							service_type = responseObj.data[i].service_type.replace(/,\s*$/, "");
							service_type_array=service_type.split(',');
							service_name = responseObj.data[i].service_name.replace(/,\s*$/, "");
							service_name_array=service_name.split(',');
							cd_value = responseObj.data[i].value;
							if(cd_value=="holiday")
							{
								cd_value="Holiday";
							}
							else if(cd_value=="after_early")
							{
								cd_value="After Hours/Early Morning PickUp";
							}
							var s=0;
							for(s=0;s<vehicle_code_array.length;s++)
							{
								responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
							}
							var t=0;
							for(t=0;t<sma_name_array.length;t++)
							{
								responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
							}
							var service_type_arrayHtml='';
							var t=0;
							for(t=0;t<service_type_array.length;t++)
							{
								if(service_type_array[t]!='')
								{
								    service_type_arrayHtml +='<option selected value="'+service_type_array[t]+'" disabled>'+service_name_array[t]+'</option>';
								}
							}
							sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");

					       /* allowORES=responseObj.data[i].allowORES.replace(/,\s*$/, "");
							Cut_off_time=responseObj.data[i].Cut_off_time.replace(/,\s*$/, "");
							blackout_Msg=responseObj.data[i].blackout_Msg.replace(/,\s*$/, "");
							holiday_surcg_per_pax=responseObj.data[i].holiday_surcg_per_pax.replace(/,\s*$/, "");
							holiday_surcg_type=responseObj.data[i].holiday_surcg_type.replace(/,\s*$/, "");*/
						 	responseHTML +='<tr seq="'+responseObj.data[i].id+'"><input type="hidden" id="allowORES_'+responseObj.data[i].id+'" value="'+responseObj.data[i].allowORES+'"><input type="hidden" id="Cut_off_time_'+responseObj.data[i].id+'" value="'+responseObj.data[i].Cut_off_time+'"><input type="hidden" id="blackout_Msg_'+responseObj.data[i].id+'" value="'+responseObj.data[i].blackout_Msg+'"><input type="hidden" id="holiday_surcg_per_pax_'+responseObj.data[i].id+'" value="'+responseObj.data[i].holiday_surcg_per_pax+'"><input type="hidden" id="holiday_surcg_type_'+responseObj.data[i].id+'" value="'+responseObj.data[i].holiday_surcg_type+'"><td style="text-align:center" id="rate_matrix_name_'+responseObj.data[i].id+'">'+responseObj.data[i].name+'</td><td style="text-align:center;" id="rate_matrix_cal_'+responseObj.data[i].id+'">'+responseObj.data[i].calender+'</td><td style="text-align:center" id="cd_start_'+responseObj.data[i].id+'">'+responseObj.data[i].start_time+'</td><td style="text-align:center" id="cd_end_'+responseObj.data[i].id+'">'+responseObj.data[i].end_time+'</td><td style="text-align:center;" id="rate_matrix_service_type_'+responseObj.data[i].id+'" serviceType="'+service_type +'" ><select class="refresh_multi_select_service_type" multiple>'+service_type_arrayHtml+'</select></td><td style="text-align:center;" id="rate_matrix_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td><a class="btn btn-xs view_vehicle_code" style="margin-left: 24%;" id="view_'+responseObj.data[i].id+'" vehicle_seq="'+vehicle_code+'" rate_seq="'+vehicle_rate+'" >View</a></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_'+responseObj.data[i]['id']+' inc_percent="'+responseObj.data[i].miles_increase_percent+'" onclick="editRateMatrix('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deleteHoliday('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';
						} 

					}else{
						responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
				    }

					$('#rate_matrix_list').html(responseHTML)
					$('.view_vehicle_code').on("click",function(){
						var getVehicleCode=$(this).attr('vehicle_seq');
						var getVehicleRate=$(this).attr('rate_seq');
						var getVehicleCodeArray=getVehicleCode.split(',');
						var getVehicleRateArray=getVehicleRate.split(',');
						var getFullHtml='';
						var counterView=0;
						$(getVehicleCodeArray).each(function(index,	totalValue){
							getFullHtml+='<tr><td>'+(parseInt(index)+1)+'</td><td>'+getVehicleCodeArray[counterView]+'</td><td>'+getVehicleRateArray[counterView]+'</td></tr>';
							counterView++;
						});

						$('#view_vehicl_rate_name').html(getFullHtml);
						$('#view_rate_vehicle_modal').show();
				    });
					setTimeout(function(){
						$(".refresh_multi_select_service_type").multiselect('destroy');
						$('.refresh_multi_select_service_type').multiselect({
						    maxHeight: 200,
							buttonWidth: '155px',
							includeSelectAllOption: true
						});

					    $(".refresh_multi_select").multiselect('destroy');

							$('.refresh_multi_select').multiselect({
							    maxHeight: 200,
							    buttonWidth: '155px',
							    includeSelectAllOption: true
							});
					},800);
					$('#refresh_overlay').css("display","none");
				},
			    error:function(){
						alert("Some Error");
						$('#refresh_overlay').css("display","none");					
				}

			});
        }



    /*********************************************************
	    @MethodName			:editRateMatrix
		@Description		:edit the rate matrix
		@param				:user_id
		@return			    :list of rate matrix
	*********************************************************/
	function editRateMatrix(getSequence,status)
	{
		var getVehicleCode=$('#view_'+getSequence).attr('vehicle_seq');
		var getVehicleRate=$('#view_'+getSequence).attr('rate_seq');
		var setArraylocalStorage=[];
		var getVehicleCodeArray=getVehicleCode.split(',');
		var getVehicleRateArray=getVehicleRate.split(',');
		var iCount=0;
		$('.add_rate_check').each(function(index,dataFull){
			var getSeq=$(this).val();
			if($.inArray($('.rate_vehicle_code_box_'+getSeq).html(),getVehicleCodeArray)==-1)
			{
				$('#checked_by_id_'+getSeq).prop('checked',false);
				$('.rate_input_box_'+getSeq).val("");
				$('.rate_input_box_'+getSeq).attr('disabled',true);
			}
			else
			{
									
				$('.rate_input_box_'+getSeq).val(getVehicleRateArray[iCount]);
				setArraylocalStorage.push({"vehicle_rate":getVehicleRateArray[getSeq],"vehicle_code":getVehicleCodeArray[getSeq]});
				$('#checked_by_id_'+getSeq).prop('checked',true);
				$('.rate_input_box_'+getSeq).removeAttr('disabled');
				iCount++;
			}
	    });
		window.localStorage.setItem("rateSetupValue",JSON.stringify(setArraylocalStorage));
	    $('#refresh_overlay').css("display","block");
	    $('#save_rate').html("Update");
		$('#add_vehicle_rate').html("<i class='glyphicon glyphicon-plus'></i>&nbsp;Update Rates");
		$('#selected_vehicle_rat_btn').html("Update Rates");
		$('#save_rate').attr("seq",getSequence);
		$('#back_button').css("display","inline-block");
		$("#apply_vehicle_type").multiselect('refresh');
		var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
			rate_matrix_sma=rate_matrix_sma.split(',');
		$("#apply_sma").val(rate_matrix_sma);
		$("#apply_sma").multiselect('refresh');

		var rate_matrix_service_type=$('#rate_matrix_service_type_'+getSequence).attr("servicetype");
			rate_matrix_service_type=rate_matrix_service_type.split(',');
			
if($.inArray("PPS", rate_matrix_service_type) !== -1){


	$('.per_pax_holiday').addClass('per_pax_holiday2').removeClass('per_pax_holiday');
}else{

	$('.per_pax_holiday2').addClass('per_pax_holiday').removeClass('per_pax_holiday2');
	
}
	
    


		$("#service_type").val(rate_matrix_service_type);
		$("#service_type").multiselect('refresh');

		if(status==1)
		{

		}
		$("#holiday_name").val($('#rate_matrix_name_'+getSequence).html());
		$("#amount").val($('#rate_matrix_amnt_'+getSequence).html());
		$("#calender").val($('#rate_matrix_cal_'+getSequence).html());
		$("#start_time").val($('#cd_start_'+getSequence).html());
		$("#end_time").val($('#cd_end_'+getSequence).html());
		$("#Cut_off_time").val($('#Cut_off_time_'+getSequence).val());
		$("#holiday_surcg_per_pax").val($('#holiday_surcg_per_pax_'+getSequence).val());
		$("#blackout_Msg").val($('#blackout_Msg_'+getSequence).val());
		
		$("#holiday_surcg_type").val($('#holiday_surcg_type_'+getSequence).val());

		$("input[name=allowORES][value=" + $('#allowORES_'+getSequence).val() + "]").attr('checked', 'checked');
		setTimeout(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");	
			$('#refresh_overlay').css("display","none");
		},200);
         $('input:checkbox[value="HRLY"]').attr('disabled', true);
	}

    
    /*********************************************************
	    @MethodName			:deleteHoliday
		@Description		:delete the holiday
		@param				:getSequence
		@return			    :list of rate matrix
	*********************************************************/
    function deleteHoliday(getSequence){
		var res = confirm("Do You Really Want To Delete This?");
        if (res == true) {
			$('#refresh_overlay').css("display","block");
			$.ajax({
				url: _SERVICEPATHHoliday,
				type: 'POST',
			    data:"action=deleteHoliday&holiday_id="+getSequence,
				success: function(response) {
				$('#refresh_overlay').css("display","none");
				  getRateMatrixList();
				},
			    error:function(){
					alert("Some Error");
					$('#refresh_overlay').css("display","none");
				}

			});

	    }
		else
	    {
			
		}
    }





