/*---------------------------------------------
   Template Name: Mylimoproject
   Page Name: man fees
   Author: Mylimoproject
---------------------------------------------*/
/*set the web service path for the web service file load*/
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHManFees="phpfile/manfees_client.php";
var ADDMILEAGE =0;

$(function(){	
	/*click on back button*/
	$('#back_button').on("click",function(){
		location.reload(true);
	});

	$('#value').on("change",function(){
		if($('#value').val()=="custom"){

			$('#enter_value_percent').css("display","block");
		}
		else{
			$('#enter_value_percent').css("display","none");
		}
	});

	$("#amount_stc").keypress(function (e){
		if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		!(e.which >= 48 && e.which <= 57)) {
			return false;
		}
	})

  	$("#amount_fuel").keypress(function (e){
		if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		!(e.which >= 48 && e.which <= 57)) {
			return false;
		}
	})	 

  	$("#amount_admin").keypress(function (e){
    	if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		!(e.which >= 48 && e.which <= 57)){
 			return false;
		}
	})

	$("#amount_reserve").keypress(function (e){
		if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		!(e.which >= 48 && e.which <= 57)){
			return false;
		}
	})	  

  	$("#amount_credit").keypress(function (e){
		if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		!(e.which >= 48 && e.which <= 57)){
			return false;
		}
	})

  	$("#amount_black").keypress(function (e){
		if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		!(e.which >= 48 && e.which <= 57)){
			return false;
		}
	})

	getSMA();
    getServiceTypes();
	getVehicleType();	
	getRateMatrixList();

/***************************
@Method Name	:On clicking edit button of the list
@Desc			:Populate All the details of the Rate matrix 
*****************************/

$('.rate_matrix_onedit').on("click",function(){
	getSequence=$(this).attr("seq");
	editRateMatrix(getSequence);
});

});

isUserApiKey();

function isUserApiKey(){	
	var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;	
	if(typeof(userInfo)=="string"){
		userInfoObj=JSON.parse(userInfo);
	}	

	$.ajax({
		url: _SERVICEPATHServer,
		type: 'POST',
		data: "action=getApiKey&user_id="+userInfoObj[0].id,
		success: function(response){		
			var responseObj=response;
			if(typeof(response)=="string"){
				responseObj=JSON.parse(response);			
			}
			
			if(responseObj.code=="1017"){		
				window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));		  		  
				var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");

				if(typeof(getLocalStoragevalueUserInformation)=="string"){
					getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
				}

				$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
				$('#add_sma_button').on('click',function(){
					$('#add_sma').show();								
				})	

				$("#popup_close_add_sma").off();
				$('#popup_close_add_sma').on("click",function(){
					$('#add_sma').hide();
		  		});	

			}
			
			else{			
				window.location.href="connectwithlimo.html";			
			}			
		}

	});
				
}

/*--function getSMA--*/
function getSMA(){
	var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;
	if(typeof(userInfo)=="string"){
		userInfoObj=JSON.parse(userInfo);
	}

	$.ajax({
		url: _SERVICEPATHSma,
		type: 'POST',
		data: "action=getSMA&user_id="+userInfoObj[0].id,
		success: function(response) {
			var responseObj=response;
			var responseHTML='';
			if(typeof(response)=="string"){
				responseObj=JSON.parse(response);
			}	

			var responseHTML='';
            if(responseObj.data[0].sma_name){
				for(var i=0; i<responseObj.data.length; i++){	
					responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
				}
            }
			
			$('#apply_sma').html(responseHTML);
			setTimeout(function(){				
				$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
				});
			},400);
		},

		error:function(){
			alert("Some Error");}
		});
}

/**********************************************
	Method: 			getServiceTypes()
	Return:Rate 		List Of service type Name
*************************************************/
	
function getServiceTypes(){
  	var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;
		if(typeof(userInfo)=="string"){
		         userInfoObj=JSON.parse(userInfo);
		}

    var serviceTypeData = [];
	$.ajax({
        url : "phpfile/service_type.php",
        type : 'post',
        data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
        dataType : 'json',
        success : function(data){               
            if(data.ResponseText == 'OK'){                  
                var ResponseHtml='';
                $.each(data.ServiceTypes.ServiceType, function( index, result){
                	ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                });
            
            	$('#service_type').html(ResponseHtml);	
			}

            $("#service_type").multiselect('destroy');
        	$('#service_type').multiselect({
           		maxHeight: 200,
            	buttonWidth: '178px',
            	includeSelectAllOption: true
        	});
  		}
  	});
 }

/**********************************************
	Method: 			getRateMatrix()
	Parameter:			userId
	Return:Rate 		List Of Rate Matrix Name
*************************************************/

function getRateMatrix(){
	var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;
	if(typeof(userInfo)=="string"){
			userInfoObj=JSON.parse(userInfo);
	}

	$.ajax({
		url: _SERVICEPATHManFees,
		type: 'POST',
		data: "action=getRateMatrix&user_id="+userInfoObj[0].id,
		success: function(response){
			var responseObj=response;
			var responseHTML='';
			if(typeof(response)=="string"){
				responseObj=JSON.parse(response);
			}	
			
			var responseHTML='<option value="">--Select--</option>';
			for(var i=0; i<responseObj.data.length; i++){	
				responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].name+'</option>';	
			}

			$('#select_matrix').html(responseHTML);
		},

		error:function(){
			alert("Some Error");
		}

	});

}

/*-- function getVehicleType --*/		
function getVehicleType(){
	var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	if(typeof(getLocalStoragevalue)=="string"){
		getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
	}

	var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
		success: function(response){
			var responseHTML='';
			var responseObj=response;
			if(typeof(response)=="string"){
				responseObj=JSON.parse(response);					
			}

			responseHTML='';
			for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++){
				responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
			}

			$('#apply_vehicle_type').html(responseHTML);
			setTimeout(function(){
				$("#apply_vehicle_type").multiselect('destroy');
            	$('#apply_vehicle_type').multiselect({
					maxHeight: 200,				
	                buttonWidth: '155px',
	                includeSelectAllOption: true
            	});

			},400);

		}

	});
}

	
$('#save_rate').on("click",function(){
   $('#refresh_overlay').css("display","block");
    var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;
	if(typeof(userInfo)=="string"){
		userInfoObj=JSON.parse(userInfo);
	}

	var selectedVehicle = $('#apply_vehicle_type option:selected');
	var selectedVehicleObject = [];
	$(selectedVehicle).each(function(index, selectedVehicle){
		selectedVehicleObject .push($(this).val());
	});

	var selectedSma = $('#apply_sma option:selected');
	var selectedSmaObject = [];
	$(selectedSma).each(function(index, selectedSma){
		selectedSmaObject .push($(this).val());
	});


	var serviceTypeOptionBtn = $('#service_type option:selected');
	var serviceTypeOptionBtnArray=[];
	$(serviceTypeOptionBtn).each(function(index, selectedSma){
		serviceTypeOptionBtnArray .push($(this).val());
	});


    /* service type in drop box end here*/
	var amount_stc=$('#amount_stc').val();
	var amount_fuel=$('#amount_fuel').val();
	var amount_admin=$('#amount_admin').val();
	var amount_reserve=$('#amount_reserve').val();
	var amount_credit=$('#amount_credit').val();
	var amount_black=$('#amount_black').val();
	var amount_toll=$('#man_fees_name').val();
	var sub_amount_tax=$('#sub_amount_tax').val();
	var ischeckDublicateValue=0;
	var getSrchgValue=[];
	var getSecondSrchgArray=[];
		$('.commanRowId').each(function(){
			var getSeq=$(this).attr('seq');
			var checkGrandTotal='grand total';
			if($('.subType_amount1_'+getSeq).is(":checked")){
					checkGrandTotal="subtotal";
			}

			var feeName=$('.removerow_'+getSeq).find('.feeName').val().trim();
			var feeRate=$('.removerow_'+getSeq).find('.feeRate').val();
			var typeRate=$('.removerow_'+getSeq).find('.typeRate').val();
			if($.inArray(feeName,getSecondSrchgArray)!=-1){	
				ischeckDublicateValue=1;
			}
			getSecondSrchgArray.push(feeName);
			getSrchgValue.push({
				"feeName":feeName,
				"feeRate":feeRate,
				"typeRate":typeRate,
				"checkGrandTotal":checkGrandTotal
			});
		});

		var getSaveAsName=$()
		if(selectedVehicleObject==''){
            $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Vehicle Code");
		}
		else if(amount_toll==''){
			$('#refresh_overlay').css("display","none");
			alert("Please enter mandatory fees setUp name");
		}
		else if(sub_amount_tax==''){
			$('#refresh_overlay').css("display","none");
			alert("Please enter  tax");
		}
		else if(ischeckDublicateValue==1){
			$('#refresh_overlay').css("display","none");
			alert("Custom mandatory fee name must be unique");
		}
		else if(selectedSmaObject==''){
             $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One SMA ");
		}
		else if(serviceTypeOptionBtnArray==''){
             $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Service Type ");
		}

		else{	
			getSrchgValue=JSON.stringify(getSrchgValue);
			if($('#save_rate').html()=='Update' ){
				$('#save_rate').html('Processing...');
			    $('#save_rate').attr("disable","true");
				var mf_id=$('#save_rate').attr('seq')
				$.ajax({
					url: _SERVICEPATHManFees,
					type: 'POST',
				    data: "action=deleteMandatoryFees&mf_id="+mf_id,
					success: function(response){
                      	$('#refresh_overlay').css("display","block");
						if(typeof(response)=="string"){
							responseObj=JSON.parse(response);
						}

						if(responseObj.code==1010){
                  
							$.ajax({
								url: _SERVICEPATHManFees,
								type: 'POST',
							    data: "action=setManFees&user_id="+userInfoObj[0].id+"&vehicle_code="+selectedVehicleObject+"&amount_stc="+amount_stc+"&amount_fuel="+amount_fuel+"&amount_admin="+amount_admin+"&amount_reserve="+amount_reserve+"&amount_credit="+amount_credit+"&amount_black="+amount_black+"&amount_toll="+amount_toll+"&sma_id="+selectedSmaObject+"&serviceTypeOptionBtnArray="+serviceTypeOptionBtnArray+"&getSrchgValue="+getSrchgValue+"&sub_amount_tax="+sub_amount_tax,
								success: function(response){
                                    $('#refresh_overlay').css("display","none");
									if(typeof(response)=="string"){
										responseObj=JSON.parse(response);										
									}
									if(responseObj.code==1003){
										alert("Mandatory fees setup name should be unquie");
									}
									if(responseObj.code==1008){
										alert("Mandatory Fees Updated Successfully");
										location.reload(true);
									}
									else{
										$('#save_rate').html('Update');
			   							$('#save_rate').attr("disable","false");
									}

								},

								error:function(){
									$('#refresh_overlay').css("display","none");
									$('#save_rate').html('Update');
									$('#save_rate').attr("disable","false");
									alert("Some Error");
								}
							});
						}
						else{
							alert("Some Server Error while Updating.");
							$('#save_rate').html('Update');
					    	$('#save_rate').attr("disable","false");
						}
					},

					error:function(){
						$('#refresh_overlay').css("display","none");
						$('#save_rate').html('Update');
			    		$('#save_rate').attr("disable","false");
						alert("Some Error");
					}
				});

			}

			else{
				$('#save_rate').html('Processing...');
				$('#save_rate').attr("disable","true");
				$.ajax({
					url: _SERVICEPATHManFees,
					type: 'POST',
					data: "action=setManFees&user_id="+userInfoObj[0].id+"&vehicle_code="+selectedVehicleObject+"&amount_stc="+amount_stc+"&amount_fuel="+amount_fuel+"&amount_admin="+amount_admin+"&amount_reserve="+amount_reserve+"&amount_credit="+amount_credit+"&amount_black="+amount_black+"&amount_toll="+amount_toll+"&sma_id="+selectedSmaObject+"&serviceTypeOptionBtnArray="+serviceTypeOptionBtnArray+"&getSrchgValue="+getSrchgValue+"&sub_amount_tax="+sub_amount_tax,
					success: function(response){
						$('#refresh_overlay').css("display","none");
						if(typeof(response)=="string"){
							responseObj=JSON.parse(response);
						}
						if(responseObj.code==1003){
							alert("Mandatory fees setup name should be unquie");
						}
						if(responseObj.code==1008){
							alert("Mandatory Fees Added Successfully");
							location.reload(true);
						}
						else{
							$('#save_rate').html('Save');
			   				$('#save_rate').attr("disable","false");
						}
					},
					error:function(){
                        $('#refresh_overlay').css("display","none");
						$('#save_rate').html('Save');
						$('#save_rate').attr("disable","false");
						alert("Some Error");
					}
				});
			}
		}
})

/***********************************
@MethodName			:getRateMatrix
@Description		:Get list of all Rate matrixes
@param				:user_id
@return			    :list of rate matrix
************************************/
function getRateMatrixList(){
	$('#refresh_overlay').css("display","block");
	var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;
	if(typeof(userInfo)=="string"){
		userInfoObj=JSON.parse(userInfo);
	}

	$.ajax({
		url:_SERVICEPATHManFees,
		type:'POST',
		data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
		success:function(response){
			var responseObj=response;
			var responseHTML='';
			var responseVeh='';
			var responseSma='';
			var vehicle_code='';
			var service_type='';
			var sma_name='';
			var vehicle_code_array='';
			var sma_name_array='';
			var sma_id='';
			if(typeof(response)=="string"){
				responseObj=JSON.parse(response);
			}
			
			if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
				responseObj.data!=""){
					for(var i=0; i<responseObj.data.length; i++){
						responseVeh=0;
						responseSma=0;
				 	 	var service_typeArrayHtml='';
				 	 	var service_nameArrayHtml='';
						vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
						vehicle_code_array=vehicle_code.split(',');
						service_type = responseObj.data[i].service_type.replace(/,\s*$/, "");
						service_typeArray=service_type.split(',');
						service_name = responseObj.data[i].service_name.replace(/,\s*$/, "");
						service_nameArray=service_name.split(',');
						sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
						sma_name_array=sma_name.split(',');
						for(j=0;j<service_typeArray.length;j++){
							service_typeArrayHtml +='<option selected value="'+service_typeArray[j]+'" disabled>'+service_nameArray[j]+'</option>';
						}

					var s=0;
					for(s=0;s<vehicle_code_array.length;s++){
						responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
					}
					var t=0;
					for(t=0;t<sma_name_array.length;t++){
						responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
					}

					sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");
					var getsrchvalueConvert=JSON.stringify(responseObj.data[i].srchrg);
					responseHTML +="<tr seq='"+responseObj.data[i].id+"'><td style='text-align:center'  id='rate_matrix_"+responseObj.data[i].id+"' admin='"+responseObj.data[i].admin+"' subtotal_tax='"+responseObj.data[i].subtotal_tax+"' reserve='"+responseObj.data[i].reserve+"' srchg='"+getsrchvalueConvert+"' stcAttribute="+responseObj.data[i].stc+" >"+responseObj.data[i].id+"</td><td style='display:none' id='rate_matrix_hidden_"+responseObj.data[i].id+"'> <input type='hidden' name='country' value='"+getsrchvalueConvert+"'></td><td style='text-align:center' id='rate_matrix_fuel_"+responseObj.data[i].id+"' credit='"+responseObj.data[i].credit+"' black_car='"+responseObj.data[i].black+"' toll='"+responseObj.data[i].toll+"' fuelAttribute='"+responseObj.data[i].fuel+"'>"+responseObj.data[i].toll+"</td><td style='text-align:center;width: 21%; text-align: justify;' id='rate_matrix_veh_"+responseObj.data[i].id+"' veh_code='"+vehicle_code +"'><select class='refresh_multi_select' multiple>"+responseVeh+"</select></td><td style='text-align:center;width: 21%; text-align: justify;' id='rate_matrix_service_type_"+responseObj.data[i].id+"' service_type='"+service_type +"'><select class='refresh_service_type' multiple>"+service_typeArrayHtml+"</select></td><td style='text-align:center;width: 21%; text-align: justify;' id='rate_matrix_sma_"+responseObj.data[i].id+"' sma_id='"+sma_id +"' ><select class='refresh_multi_select' multiple>"+responseSma+"</select></td><td style='text-align:center'><div class='as' ng-show='!rowform.$visible'> <a class='btn btn-xs rate_matrix_onedit' id=edit_rate_"+responseObj.data[i]['id']+" onclick='editRateMatrix("+responseObj.data[i].id+",1)'>Edit</a> <a class='btn btn-xs rate_matrix_ondelete'  seq="+responseObj.data[i]['id']+" onclick='deleteMandatoryFees("+responseObj.data[i].id+")'>Delete</a> </div></td></tr>";
				}
			   }
			   else{
 					responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
				}

				$('#rate_matrix_list').html(responseHTML)
					setTimeout(function(){
						$(".refresh_service_type").multiselect('destroy');
						$('.refresh_service_type').multiselect({
							maxHeight: 200,
							buttonWidth: '155px',
							includeSelectAllOption: true
						});

					$(".refresh_multi_select").multiselect('destroy');
					$('.refresh_multi_select').multiselect({
						maxHeight: 200,
						buttonWidth: '155px',
						includeSelectAllOption: true
					});

				},800);
               $('#refresh_overlay').css("display","none");
			},

			error:function(){
				alert("Some Error");
				$('#refresh_overlay').css("display","none");					
			}

		})
}

/*-- function editRateMatrix --*/
function editRateMatrix(getSequence,status){
	$('#refresh_overlay').css("display","block");
	$('#save_rate').html("Update");
	$('#save_rate').attr("seq",getSequence);
	$('#back_button').css("display","inline-block");
	
	var rate_matrix_service_type=$('#rate_matrix_service_type_'+getSequence).attr("service_type");
	rate_matrix_service_type=rate_matrix_service_type.split(',');
	$("#service_type").val(rate_matrix_service_type);
	$("#service_type").multiselect('refresh');

	var rate_matrix_veh=$('#rate_matrix_veh_'+getSequence).attr("veh_code");
	rate_matrix_veh=rate_matrix_veh.split(',');
	$("#apply_vehicle_type").val(rate_matrix_veh);
	$("#apply_vehicle_type").multiselect('refresh');

	var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
	rate_matrix_sma=rate_matrix_sma.split(',');
	$("#apply_sma").val(rate_matrix_sma);
	$("#apply_sma").multiselect('refresh');
	$('#amount_stc').val($('#rate_matrix_'+getSequence).attr("stcattribute"));
	$('#amount_fuel').val($('#rate_matrix_fuel_'+getSequence).attr("fuelattribute"));
	$('#amount_admin').val($('#rate_matrix_'+getSequence).attr('admin'));
	$('#sub_amount_tax').val($('#rate_matrix_'+getSequence).attr('subtotal_tax'));

	var getSrchgValue=$('#rate_matrix_'+getSequence).attr('srchg');

	if(getSrchgValue!='true'){
		var getVehicle =JSON.parse(getSrchgValue);

		$('#getHtml').html('');
		for(var i=0; i<getVehicle.length; i++){
			var getSeq=i+1;
			$('#getHtml').append('<tr class="commanRowId removerow_'+getSeq+'"  seq="'+getSeq+'"><td style="width: 26%;">Custom Mandatory fee Name</td><td><input type="text" class="feeName" value="'+getVehicle[i].custom_fee_name+'"></td><td>Value</td><td><input type="text" required value='+getVehicle[i].custom_fee+' class="feeRate"> </td><td><select class="typeRate" id="typeRate'+getSeq+'" value="'+getVehicle[i].type_rate+'" required><option value="%">%</option><option value="$">$</option></select></td><td> <label><input type="radio" class="subType_amount1_'+getSeq+'" name="optradio'+getSeq+'" checked>SubTotal Item</label> <label><input type="radio" class="subType_amount2_'+getSeq+'" name="optradio'+getSeq+'">Total Item</label></td><td style="padding-bottom: 1%;"><button class="btn btn-primary removeRow" seq='+getSeq+'>X</button></td></tr>');
			if(getVehicle[i].is_subtamount_check!="grand total"){
			 	$('.subType_amount1_'+getSeq).prop("checked",true);
		    }
			else{
				$('.subType_amount2_'+getSeq).prop("checked",true);
			}
			$("#typeRate"+getSeq).val(getVehicle[i].type_rate);
		}

		$('.removeRow').off();
	    $('.removeRow').on("click",function(){
	    var getseq=$(this).attr('seq')
	    	$('.removerow_'+getseq).remove();
	  	})
	}
	
	$('#amount_reserve').val($('#rate_matrix_'+getSequence).attr('reserve'));
	$('#amount_credit').val($('#rate_matrix_fuel_'+getSequence).attr('credit'));
	$('#amount_black').val($('#rate_matrix_fuel_'+getSequence).attr('black_car'));
	$('#man_fees_name').val($('#rate_matrix_fuel_'+getSequence).attr('toll'));

	setTimeout(function(){
		$("html, body").animate({ scrollTop: 0 }, "slow");	
		$('#refresh_overlay').css("display","none");
	},200);

}


/*-- function deleteMandatoryFees --*/
function deleteMandatoryFees(getSequence){
	var res = confirm("Do You Really Want To Delete This?");
	if (res == true) {
		$('#refresh_overlay').css("display","block");

		$.ajax({
			url: _SERVICEPATHManFees,
		    type: 'POST',
			data: "action=deleteMandatoryFees&mf_id="+getSequence,
			success: function(response) {
				$('#refresh_overlay').css("display","none");
				getRateMatrixList();
			},

			error:function(){
				alert("Some Error");
				$('#refresh_overlay').css("display","none");
			}
		});
	}

	else{
	}
					
}

/********************************
@Method			:checknegative
@Description	:restrict entering negative numbers
@Param			:none
@RETURN			:none
**********************************/
function checknegative(){
	if ($('#percent_increase').val() < 0){
		$('#percent_increase').val(0) ;
		$('#percent_increase').focus();
 		alert('Negative Values Not allowed');
 		return false;
	}
}
	
/* setTimeout function */
setTimeout(function(){
	$("#apply_vehicle_type").multiselect('destroy');
		$('#apply_vehicle_type').multiselect({
		maxHeight: 200,
		buttonWidth: '155px',
		includeSelectAllOption: true
		});
	$("#value").multiselect('destroy');
		$('#value').multiselect({
		maxHeight: 200,
		buttonWidth: '155px',
		includeSelectAllOption: true
		});
	$("#apply_sma").multiselect('destroy');
		$('#apply_sma').multiselect({
		maxHeight: 200,
		buttonWidth: '155px',
		includeSelectAllOption: true
		});					
	}
	,900);

				