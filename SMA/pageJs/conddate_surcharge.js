
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Conddate Surcharge
  Author: Mylimoproject
---------------------------------------------*/ 

/*Add the php path for web service call*/
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHConDate="phpfile/conddate_client.php";
var ADDMILEAGE =0;

/*call the function on page load*/
$(function(){

    /*click on back button*/  
	$('#back_button').on("click",function(){
		location.reload(true);
	});

	/*start the set timeout function*/
	setTimeout(function () {
        /*select the time*/  
	    $('.time-picker').datetimepicker({
	        pickDate: false,
	        useCurrent: false,
	        language: 'ru'
	    });
		
        /*select the date*/
		$(".datepicker").datetimepicker({
		    pickTime: false,
            changeMonth: true,
            changeYear: true,
            "dateFormat": "dd-mm-yy"
   		 });

        }, 1500);

    /*click on amount keypress*/ 
	$("#amount").keypress(function (e) {
    
	    if(e.which != 46 && e.which != 45 && e.which != 46 &&

	      !(e.which >= 48 && e.which <= 57)){

	        return false;

	    }

    });
    
    /*call the function on onload*/
  	getSMA();
    getServiceTypes();
	getVehicleType();	
	getRateMatrixList();
    
    /*click on edit button */
	$('.rate_matrix_onedit').on("click",function(){
		getSequence=$(this).attr("seq");
		editRateMatrix(getSequence);
	});

});

/*load the isUserApiKey*/
isUserApiKey();

    /*---------------------------------------------
        Function Name: isUserApiKey()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/
	function isUserApiKey()
	{	
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;	
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}	
		$.ajax({
			url: _SERVICEPATHServer,
		    type: 'POST',
			data: "action=getApiKey&user_id="+userInfoObj[0].id,
			success: function(response){		
			    var responseObj=response;
			    if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);			
				}
				if(responseObj.code=="1017")
				{		
				    window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));		  		  
				  	var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
			    	if(typeof(getLocalStoragevalueUserInformation)=="string")
			        {
			   	        getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);

			        }
	
				    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
					$('#add_sma_button').on('click',function(){
						$('#add_sma').show();								
					});	
					$("#popup_close_add_sma").off();
					$('#popup_close_add_sma').on("click",function(){
					    $('#add_sma').hide();
			        });	
				}
				else
				{			
					window.location.href="connectwithlimo.html";			
				}			
			}

	    });
	}

    /*---------------------------------------------
        Function Name: isUserApiKey()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/
	function getSMA()
	{
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		$.ajax({
			url: _SERVICEPATHSma,
			type: 'POST',
			data: "action=getSMA&user_id="+userInfoObj[0].id,
			success: function(response) {
				var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}	
				var responseHTML='';
				for(var i=0; i<responseObj.data.length; i++)
				{	
	                if(responseObj.data[i].id!=null){
						responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
	                }
			    }

				$('#apply_sma').html(responseHTML);
				setTimeout(function(){				
					$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
						maxHeight: 200,
						buttonWidth: '155px',
						includeSelectAllOption: true
					});
				},400);
					
			},
			error:function(){

				alert("Some Error");
			}

		});

	}

    /*---------------------------------------------
        Function Name: getServiceTypes()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function getServiceTypes()
	{
	    var userInfo=window.localStorage.getItem("companyInfo");
	    var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
	    var serviceTypeData = [];
	    $.ajax({
	        url : "phpfile/service_type.php",
	        type : 'post',
	        data :'action=GetServiceTypes&user_id='+userInfoObj[0].id,
	        dataType : 'json',
	        success : function(data){
	             
	            if(data.ResponseText == 'OK'){
	                var ResponseHtml='';
	                $.each(data.ServiceTypes.ServiceType, function( index, result){
	                    ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
	                });
	                $('#service_type').html(ResponseHtml);
	            }
	            $("#service_type").multiselect('destroy');
	            $('#service_type').multiselect({
	                maxHeight: 200,
	                buttonWidth: '178px',
	                includeSelectAllOption: true
	            });
	        }
	    });
	}

    /*---------------------------------------------
        Function Name: getVehicleType()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function getVehicleType()
	{
	    var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
		if(typeof(getLocalStoragevalue)=="string")
		{
		   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}

		
	    var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

	    $.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}
				responseHTML='';
				for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
				{
					responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
				}
				$('#apply_vehicle_type').html(responseHTML);
				setTimeout(function(){
					$("#apply_vehicle_type").multiselect('destroy');
	            	$('#apply_vehicle_type').multiselect({
	                    maxHeight: 200,
	                    buttonWidth: '155px',
	                    includeSelectAllOption: true
	                });
				},400);
			}
		});
	}

    /*click on sve rate button*/  
	$('#save_rate').on("click",function(){
	
        $('#refresh_overlay').css("display","block");
	    var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
		    userInfoObj=JSON.parse(userInfo);
		}
		var amount = $('#amount').val();
    	var selectedVehicle = $('#apply_vehicle_type option:selected');
        var selectedVehicleObject = [];
        $(selectedVehicle).each(function(index, selectedVehicle){
            selectedVehicleObject .push($(this).val());
        });
		var service_type = $('#service_type option:selected');
        var service_typeObject = [];
        $(service_type).each(function(index, service_type){
            service_typeObject .push($(this).val());
        });
    	var selectedSma = $('#apply_sma option:selected');
        var selectedSmaObject = [];
        $(selectedSma).each(function(index, selectedSma){
            selectedSmaObject .push($(this).val());
        });
		if(amount=='')
		{   
			$('#refresh_overlay').css("display","none");
			alert("Please Enter Amount");
		}
		else if(selectedVehicleObject=='')
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Vehicle Code");
		}
		else if(selectedSmaObject=='')
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One SMA ");
		}
		else if(service_typeObject=='')
		{
			$('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Service ");
		}
		else
		{
			var start_time=$('#start_time').val();
			var end_time=$('#end_time').val();
			var newDateStart=new Date("01/01/2016"+" "+start_time);
		    var endDateStart=new Date("01/01/2016"+" "+end_time);
			if(newDateStart>endDateStart)
			{
				alert("End time is greater than start time");
				$('#refresh_overlay').css("display","none");
				return 0;
			}
			if(start_time!='' && start_time!="null" && start_time!=null && start_time!=undefined && end_time!='' && end_time!="null" && end_time!=null && end_time!=undefined)
			{
				var start_time_array=start_time.split(':');							
				var end_time_array=end_time.split(':');							
				var start_hour=start_time_array[1].split(' ');
				var end_hour=end_time_array[1].split(' ');
				if(end_hour[1]!=start_hour[1])
				{
								
			    }
				else
				{
                    if(start_hour[1]=="PM")
					{
						start_time_array[0]=parseInt(start_time_array[0])+12;
					}
					if(end_hour[1]=="PM")
					{
						end_time_array[0]=parseInt(end_time_array[0])+12;
					}
					    var initial_time=parseInt(start_time_array[0])*60+parseInt(start_hour[0]);
					    var final_time=parseInt(end_time_array[0])*60+parseInt(end_hour[0]);
				}
			}
			if($('#save_rate').html()=='Update')
			{
				$('#save_rate').html('Processing...');
			    $('#save_rate').attr("disable","true");
				var cd_id=$('#save_rate').attr('seq')
				$.ajax({
					url: _SERVICEPATHConDate,
					type: 'POST',
				    data: "action=deleteCondDate&cd_id="+cd_id,
					success: function(response) {
						$('#refresh_overlay').css("display","none");
						if(typeof(response)=="string")
				        {
					        responseObj=JSON.parse(response);
				        }
				        if(responseObj.code==1010)
				        {
						    $.ajax({
							    url: _SERVICEPATHConDate,
							    type: 'POST',
								data: "action=setCondDate&user_id="+userInfoObj[0].id+"&vehicle_code="+selectedVehicleObject+"&service_typeObject="+service_typeObject+"&amount="+amount+"&start_time="+start_time+"&end_time="+end_time+"&sma_id="+selectedSmaObject,
									success: function(response){
										$('#refresh_overlay').css("display","none");
										alert("Conditional Date Updated Successfully");
										location.reload(true);
									},
									error:function(){
										$('#refresh_overlay').css("display","none");
										$('#save_rate').html('Update');
				                        $('#save_rate').attr("disable","false");
					                    alert("Some Error");
								    }

							});
				        }
				        else
				        {
					        alert("Some Server Error while Updating.");
					        $('#save_rate').html('Update');
			    	        $('#save_rate').attr("disable","false");
				        }

				    },
					error:function(){
						$('#refresh_overlay').css("display","none");
				        $('#save_rate').html('Update');
			            $('#save_rate').attr("disable","false");
				        alert("Some Error");

			        }

				});

			}
			else
			{
				$('#save_rate').html('Processing...');
			    $('#save_rate').attr("disable","true");
				$.ajax({
					url: _SERVICEPATHConDate,
					type: 'POST',
					data: "action=setCondDate&user_id="+userInfoObj[0].id+"&vehicle_code="+selectedVehicleObject+"&service_typeObject="+service_typeObject+"&amount="+amount+"&start_time="+start_time+"&end_time="+end_time+"&sma_id="+selectedSmaObject,
					success: function(response) {
						$('#refresh_overlay').css("display","none");
						alert("Conditional Date Added Successfully");
						location.reload(true);
					},
				    error:function(){
                        $('#refresh_overlay').css("display","none");
						$('#save_rate').html('Save');
			            $('#save_rate').attr("disable","false");
				        alert("Some Error");
				    }
			    });
			}
		}
	});

	

	/*********************************************************
		@MethodName			:getRateMatrix
		@Description		:Get list of all Rate matrixes
		@param				:user_id
		@return			    :list of rate matrix
    **********************************************************/
	function getRateMatrixList()
	{
		$('#refresh_overlay').css("display","block"); 
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
	    if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
			$.ajax({
				url:_SERVICEPATHConDate,
				type:'POST',
				data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
				success:function(response)
				{
					var responseObj=response;
					var responseHTML='';
					var responseVeh='';
					var responseSma='';
					var vehicle_code='';
					var sma_name='';
					var vehicle_code_array='';
					var sma_name_array='';
					var cd_value='';
					var sma_id='';
					var service_type_arrayHtml='';
					var service_type='';
					var service_type_array=[];
					var service_name='';
					var service_name_array=[];
					if(typeof(response)=="string")
					{
						responseObj=JSON.parse(response);
					}
					if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
					 responseObj.data!="")
					{
					    for(var i=0; i<responseObj.data.length; i++)
					    {
						    responseVeh=0;
					 	    responseSma=0;
						    responseValue=0;
						    vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
						    vehicle_code_array=vehicle_code.split(',');
						    sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
 						    sma_name_array=sma_name.split(',');
						    var s=0;
							for(s=0;s<vehicle_code_array.length;s++)
							{
								responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
							}
							var t=0;
							for(t=0;t<sma_name_array.length;t++)
							{
								responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
							}

						    service_type = responseObj.data[i].service_type.replace(/,\s*$/, "");
						    service_name = responseObj.data[i].service_name.replace(/,\s*$/, "");
						    service_type_array=service_type.split(',');
						    service_name_array=service_name.split(',');
						    var t=0;
							for(t=0;t<service_type_array.length;t++)
							{
								service_type_arrayHtml +='<option value="'+service_type_array[t]+'" selected disabled>'+service_name_array[t]+'</option>';
							}
						    sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");
							end_time =responseObj.data[i].end_time.split(':');

			                if(end_time[0]>12){
			                var end_time1=end_time[0]-12;
			                   end_time1=end_time1+":"+end_time[1]+" "+"pm"
		                    }else{
		                     	if(end_time[0]==12)
		                     	{
		                     		var result = responseObj.data[i].end_time.split(':');   
		                     		var final = result.pop();    
		                     		var previous = result.join(':');  
		                     		end_time1=previous+" "+"pm"
		                     	}
		                     	else
		                     	{
		                     		var result = responseObj.data[i].end_time.split(':');   
		                     		var final = result.pop(); 
		                     		if(result[0]=="00")
				                    {
		        		             	result[0]=12;
		                     		}
		                     		var previous = result.join(':');    
		                     		end_time1=previous+" "+"am"
		                     	}
		                    }
		                    start_time =responseObj.data[i].start_time.split(':');
		             		if(start_time[0]>12){
		                        var start_time1=start_time[0]-12;
		                        start_time1=start_time1+":"+start_time[1]+" "+"pm"
	                        }else{

		                     	if(start_time[0]==12)
		                     	{
		                     		var result = responseObj.data[i].start_time.split(':');   
	            	                var final = result.pop();    
		                     		var previous = result.join(':');  
		                     	    start_time1=previous+" "+"pm"
		                     	}
		                     	else
		                     	{
		                     		var result = responseObj.data[i].start_time.split(':');   
		                     		var final = result.pop();    
		                     		if(result[0]=="00")
				                    {
		        		             	result[0]=12;
		                     		}
		                     		var previous = result.join(':');  
		                     	        start_time1=previous+" "+"am"
		                     	}
	                        }
				 	            responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="rate_matrix_val_'+responseObj.data[i].id+'" >'+responseObj.data[i].amount+'</td><td style="text-align:center" id="cd_start_'+responseObj.data[i].id+'">'+start_time1+'</td><td style="text-align:center" id="cd_end_'+responseObj.data[i].id+'">'+end_time1+'</td><td style="text-align:center;" id="rate_matrix_veh_'+responseObj.data[i].id+'" veh_code="'+vehicle_code +'"><select class="refresh_multi_select" multiple>'+responseVeh+'</select></td><td style="text-align:center;" id="rate_matrix_service_type_'+responseObj.data[i].id+'" service_type="'+service_type_array +'"><select class="refresh_multi_service_type" multiple>'+service_type_arrayHtml+'</select></td><td style="text-align:center;" id="rate_matrix_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_'+responseObj.data[i]['id']+' inc_percent="'+responseObj.data[i].miles_increase_percent+'" onclick="editRateMatrix('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deleteCondDate('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';
					} 

					}else{
					 	responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
					}
				    $('#rate_matrix_list').html(responseHTML)
						setTimeout(function(){
						    $(".refresh_multi_service_type").multiselect('destroy');
							$('.refresh_multi_service_type').multiselect({
							    maxHeight: 200,
							    buttonWidth: '155px',
							    includeSelectAllOption: true
							});

					$(".refresh_multi_select").multiselect('destroy');
						$('.refresh_multi_select').multiselect({
						    maxHeight: 200,
						    buttonWidth: '155px',
						    includeSelectAllOption: true
						});

			        },800);
                  $('#refresh_overlay').css("display","none");
				},
				error:function(){
					alert("Some Error");
					$('#refresh_overlay').css("display","none");					
				}
			
			});
	    }




    /*---------------------------------------------
        Function Name: editRateMatrix()
        Input Parameter: getSequence,status
        return:json data
    ---------------------------------------------*/
	function editRateMatrix(getSequence,status)
	{
		$('#refresh_overlay').css("display","block");
		$('#save_rate').html("Update");
		$('#save_rate').attr("seq",getSequence);
		$('#back_button').css("display","inline-block");
       	var rate_matrix_veh=$('#rate_matrix_veh_'+getSequence).attr("veh_code");
			rate_matrix_veh=rate_matrix_veh.split(',');
		$("#apply_vehicle_type").val(rate_matrix_veh);
		$("#apply_vehicle_type").multiselect('refresh');
	    var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
			rate_matrix_sma=rate_matrix_sma.split(',');
		$("#apply_sma").val(rate_matrix_sma);
		$("#apply_sma").multiselect('refresh');
		var rate_matrix_service_type=$('#rate_matrix_service_type_'+getSequence).attr("service_type");
			rate_matrix_service_type=rate_matrix_service_type.split(',');
		$("#service_type").val(rate_matrix_service_type);
		$("#service_type").multiselect('refresh');
		if(status==1)
		{

		}
	    $("#amount").val($('#rate_matrix_val_'+getSequence).html());
		$("#calender").val($('#rate_matrix_cal_'+getSequence).html());
		$("#start_time").val($('#cd_start_'+getSequence).html());
		$("#end_time").val($('#cd_end_'+getSequence).html());
		setTimeout(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");	
			$('#refresh_overlay').css("display","none");
		},200);
	}

   /*---------------------------------------------
        Function Name: deleteCondDate()
        Input Parameter: getSequence
        return:json data
    ---------------------------------------------*/
    function deleteCondDate(getSequence){
					
		var res = confirm("Do You Really Want To Delete this Matrix?");
        if (res == true) {
		    $('#refresh_overlay').css("display","block");
		    $.ajax({
			    url: _SERVICEPATHConDate,
			    type: 'POST',
			    data:"action=deleteCondDate&cd_id="+getSequence,
				success: function(response) {
					$('#refresh_overlay').css("display","none");
					getRateMatrixList();
				},
				error:function(){
					alert("Some Error");
					$('#refresh_overlay').css("display","none");
				}
			});
		}
		else
        {

		}

	}





