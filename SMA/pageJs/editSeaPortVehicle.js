/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Edit Seaport Page
  Author: Mylimoproject
---------------------------------------------*/
/*declare a class name for edit seaport page*/
var EditSeaPortVehicle={
  /*set the web service path using php file*/
	_Serverpath:"phpfile/addVehicle_client.php",
/*---------------------------------------------
  Function Name: editSeaportInformationShow()
  Input Parameter:rowId
  return:json data
---------------------------------------------*/
	editSeaportInformationShow:function(rowId){
    $('#refresh_overlay').css("display","block");
		var getForm={"action":"editSeaportInformationShow","rowId":rowId};
    $.ajax({
       url:EditSeaPortVehicle._Serverpath,
       type:'POST',
       dataType:'json',
       data:getForm
    }).done(function(response){

       	$('#seaport_country_name').val(response.data[0].seaport_country);
       	$('#seaport_state_name').val(response.data[0].state_name);
       	$('#seaport_city_name').val(response.data[0].city_name);
       	$('#seaport_city_zipCode').val(response.data[0].city_code);
       	$('#seaport_name').val(response.data[0].seaport_code[0].seaport_name);
       	$('#seaport_code').val(response.data[0].seaport_code[0].seaport_code);
		    $('#seaport_selected_vehicle_rat_btn').attr("seq",response.data[0].id);
       	$('#showSeaportData').show();
        $('#refresh_overlay').css("display","none");
      
    });

	},

/*---------------------------------------------
  Function Name: deleteSeaportInformation()
  Input Parameter:rowId
  return:json data
---------------------------------------------*/
	deleteSeaportInformation:function(rowId){
    $('#refresh_overlay').css("display","block");
      var getForm={"action":"deleteSeaportInformation","rowId":rowId};
      $.ajax({
           url:EditSeaPortVehicle._Serverpath,
           type:'POST',
           dataType:'json',
           data:getForm
      }).done(function(response){
       	  EditSeaPortVehicle.getSeaportInformation();

       });
     $('#refresh_overlay').css("display","none");
	},
  
/*---------------------------------------------
     Function Name: getSeaportInformation()
     Input Parameter:rowId
     return:json data
---------------------------------------------*/
    getSeaportInformation:function(rowId)
    {
        $('#refresh_overlay').css("display","block");
        var getUserId=window.localStorage.getItem('companyInfo');
            getUserId=JSON.parse(getUserId);
      	var getForm={"action":"getSeaportVechileInformation","rowId":rowId,"user_id":getUserId[0].id};
        $.ajax({
           url:EditSeaPortVehicle._Serverpath,
           type:'POST',
           dataType:'json',
           data:getForm
        }).done(function(response){

        if(response.code==1003)
        {
          	var getHTMl='';
      		  for(var i=0; i<response.data.length; i++)
      		  {
      		      getHTMl+='<tr><td>'+(i+1)+'</td><td>'+response.data[i].seaport_country+'</td><td>'+response.data[i].state_name+'</td> <td>'+response.data[i].city_name+'</td><td>'+response.data[i].city_code+'</td> <td>'+response.data[i].seaport_code[0].seaport_name+'</td> <td>'+response.data[i].seaport_code[0].seaport_code+'</td><td><button class="btn btn-primary editClass" style="margin-right: 5%;" seq="'+response.data[i].id+'">Edit</button><button class="btn btn-primary deleteClass" seq="'+response.data[i].id+'">Delete</button></td></tr>';
            }
        }else{

            getHTMl+='<tr><td colspan="4">Data not Found.</td></tr>';
        }
        $('#view_seaport_information_db_table').html(getHTMl);
        $('#refresh_overlay').css("display","none");
        $('.editClass').on("click",function(){
            var getSeq=$(this).attr("seq");
            EditSeaPortVehicle.editSeaportInformationShow(getSeq);

        });
        $('.deleteClass').on("click",function(){
            var getAns=confirm("Are you sure. you want to delete item");
        if(getAns)
        {
            var getSeq=$(this).attr("seq");
            EditSeaPortVehicle.deleteSeaportInformation(getSeq);
        }
            
    });

   });

  }
}

EditSeaPortVehicle.getSeaportInformation();
/*click on submit form button*/
$('#seaPortinformation').on("submit",function(event){
  $('#refresh_overlay').css("display","block");
    event.preventDefault();
    var getNewForm=new FormData($("#seaPortinformation")[0]);
    var getSeq=$('#seaport_selected_vehicle_rat_btn').attr("seq");
        getNewForm.append("rowId",getSeq);
    	  getNewForm.append("action","editClassSeaport")
        $.ajax({
            url:EditSeaPortVehicle._Serverpath,
            type:'POST',
            dataType:'json',
            data:getNewForm,
            contentType: false,
            processData: false,
        }).done(function(response) {
            alert('Successfully Updated');
            EditSeaPortVehicle.getSeaportInformation();
            $('#seaPortinformation')[0].reset(); 
            $('#showSeaportData').hide();

        });

  })