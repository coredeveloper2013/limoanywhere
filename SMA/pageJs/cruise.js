/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Edit Airport Page
  Author: Mylimoproject
---------------------------------------------*/

/*declare a class edit vehicle*/
var EditVehicle = {
    /*set a web service path using php*/
    _Serverpath: "phpfile/cruise_client.php",

 /*---------------------------------------------
          Function Name: setImportDatabaseFIle()
          Input Parameter: user_id,
          return:json data
       ---------------------------------------------*/

        setImportDatabaseFIle:function(){

            $("#refresh_overlay").css('display','block');
            var getUserId=window.localStorage.getItem('companyInfo');

            if(typeof(getUserId)=="string")
              {
                getUserId=JSON.parse(getUserId);

              }

             var newformdata=new FormData($('#getformdata')[0]);
             newformdata.append("action","setImportDatabaseFIle");  
             newformdata.append("user_id",getUserId[0].id);

             $.ajax({
                type: "POST",
                url:this._Serverpath,
                data:newformdata,
                enctype: 'multipart/form-data',
                processData: false,  
                contentType: false ,
                dataType:'json',

              success: function(response) {

                    $("#refresh_overlay").css('display','none');
                     
                    if(response.code==1006)
                      {
                  
                     alert("Data Imported successfully");
                     EditVehicle.getAirportInformation();
                     
                      }
                    else{
                       $("#refresh_overlay").css('display','none');
                       alert("Some Error.");
                     }
            }

          });

      },


    /*---------------------------------------------
      Function Name: editAirportInformationShow
      Input Parameter:rowId
      return:json data
    ---------------------------------------------*/
    editAirportInformationShow: function(rowId) {
        $('#refresh_overlay').css("display", "block");
        var getForm = { "action": "editAirlineInfoShow", "rowId": rowId };

        console.log("getForm....", getForm);
        $.ajax({
            url: EditVehicle._Serverpath,
            type: 'POST',
            dataType: 'json',
            data: getForm
        }).done(function(response) {

            $('#edit_cruise_name').val(response.data[0].name);
            $('#edit_cruise_code').val(response.data[0].cruise_code);
            //   $('#city_name').val(response.data[0].city_name);
            //   $('#city_zipCode').val(response.data[0].city_code);
            //   $('#airport_name').val(response.data[0].airport_code[0].airport_name);
            //   $('#airport_code').val(response.data[0].airport_code[0].airport_code);
            $('#cruiseInformation').attr("seq", response.data[0].id);
            $('#showAirportData').show();
            $('#refresh_overlay').css("display", "none");
        });

    },
    /*---------------------------------------------
      Function Name: deleteAirline
      Input Parameter:rowId
      return:json data
    ---------------------------------------------*/
    deleteAirportInformation: function(rowId) {
        var getForm = { "action": "deleteAirline", "rowId": rowId };
        $.ajax({
            url: EditVehicle._Serverpath,
            type: 'POST',
            dataType: 'json',
            data: getForm
        }).done(function(response) {

            EditVehicle.getAirportInformation();

        });

    },

    /*---------------------------------------------
        Function Name: openFile
        Input Parameter:rowId
        return:json data
      ---------------------------------------------*/
    openFile: function() {


        $('#importCruise').click();
    },


    /*---------------------------------------------
       Function Name: deleteAirportInformation
       Input Parameter:rowId
       return:json data
     ---------------------------------------------*/
    getAirportInformation: function(rowId) {
        $('#refresh_overlay').css("display", "block");
        var getUserId = window.localStorage.getItem('companyInfo');
        getUserId = JSON.parse(getUserId);
        var getForm = { "action": "getAirlinesInformation", "rowId": rowId, "user_id": getUserId[0].id };
        $.ajax({
            url: EditVehicle._Serverpath,
            type: 'POST',
            dataType: 'json',
            data: getForm
        }).done(function(response) {

            if (response.code == 1003) {
                var getHTMl = '';

                for (var i = 0; i < response.data.length; i++) {


                    getHTMl += '<tr><td>' + (i + 1) + '</td><td>' + response.data[i].name + '</td><td>' + response.data[i].cruise_code + '</td><td> <button class="btn btn-primary editClass" style="margin-right: 5%;" seq="' + response.data[i].id + '">Edit</button><button class="btn btn-primary deleteClass" seq="' + response.data[i].id + '">Delete</button></td></tr>';


                }

            } else {

                getHTMl += '<tr><td colspan="4">Data not Found.</td></tr>';

            }
            $('#view_airport_information_db_table').html(getHTMl);
            $('#refresh_overlay').css("display", "none");
            $('.editClass').on("click", function() {
                var getSeq = $(this).attr("seq");
                console.log("getSeq....", getSeq);
                EditVehicle.editAirportInformationShow(getSeq);

            });
            $('.deleteClass').on("click", function() {
                var getAns = confirm("Are you sure. you want to delete item");
                if (getAns) {
                    var getSeq = $(this).attr("seq");
                    EditVehicle.deleteAirportInformation(getSeq);

                }

            });

        });

    }
}
EditVehicle.getAirportInformation();






$('#addCruise').on('click', function() {

    $('#CruisePopupStart').show();
})

$('#cruiseform').on("submit", function(event) {
    $('#refresh_overlay').css("display", "block");
    event.preventDefault();

    var getUserId = window.localStorage.getItem('companyInfo');
    getUserId = JSON.parse(getUserId);



    var getNewForm = new FormData($("#cruiseform")[0]);
    getNewForm.append("action", "addAirlines");
    getNewForm.append("user_id", getUserId[0].id);
    $.ajax({
        url: EditVehicle._Serverpath,
        type: 'POST',
        dataType: 'json',
        data: getNewForm,
        contentType: false,
        processData: false,
    }).done(function(response) {
        alert('Successfully Added');
        EditVehicle.getAirportInformation();
        $('#cruiseform')[0].reset();
        $('#CruisePopupStart').hide();
        $('#refresh_overlay').css("display", "none");

    }).fail(function(jqXHR, exception) {

        $('#refresh_overlay').css("display", "none");

    });
});


$('#cruiseInformation').on("submit", function(event) {
    $('#refresh_overlay').css("display", "block");
    event.preventDefault();
    var getNewForm = new FormData($("#cruiseInformation")[0]);
    var getSeq = $('#cruiseInformation').attr("seq");
    getNewForm.append("rowId", getSeq);
    getNewForm.append("action", "editAirline")
    $.ajax({
        url: EditVehicle._Serverpath,
        type: 'POST',
        dataType: 'json',
        data: getNewForm,
        contentType: false,
        processData: false,
    }).done(function(response) {
        alert('Successfully Updated');
        EditVehicle.getAirportInformation();
        $('#cruiseInformation')[0].reset();
        $('#showAirportData').hide();
        $('#refresh_overlay').css("display", "none");

    }).fail(function(jqXHR, exception) {

        $('#refresh_overlay').css("display", "none");

    });
});

  $('#importCruise').on("change",function(){

      EditVehicle.setImportDatabaseFIle();
});

$('#csvFileId').on('click', function() {
    EditVehicle.openFile();
})
$('.close_popup').on('click', function() {
    $('#showAirportData').hide();
    $('#CruisePopupStart').hide();
})