/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: Promo Discount
    Author: Mylimoproject
---------------------------------------------*/
var discountDbClass={
	/*set a web service path for webservice*/
	_Serverpath:"phpfile/promo_discount_client.php",
	_SERVICEPATH2:"phpfile/service.php",
    _SERVICEPATHSma:"phpfile/sma_client.php",
	

/*---------------------------------------------
   Function Name: getSpclPackage()
   Input Parameter: 
   return:json data
---------------------------------------------*/ 
getSpclPackage:function(){
	var userInfo=window.localStorage.getItem("companyInfo");
	var userInfoObj=userInfo;
	if(typeof(userInfo)=="string"){
		userInfoObj=JSON.parse(userInfo);
	}  
    var serviceTypeData = [];
    $.ajax({
	    url : discountDbClass._Serverpath,
	    type : 'post',
	    data : 'action=specialPackage&user_id='+userInfoObj[0].id,
	    dataType : 'json',
	    success : function(data){
	     	var responseObj=data;
	    	if(typeof(data)=="string"){
	            responseObj=JSON.parse(data);
			}
				
	        var ResponseHtml='';
	        if(responseObj.code == 1007){
	            $.each(responseObj.data, function( index, result){
	            	ResponseHtml+="<option value='"+result.id+"'>"+result.package_code+"</option>";
	            });
	        }
	        $('#promo_specila_package').html(ResponseHtml);
	        $("#promo_specila_package").multiselect('destroy');
	    	$('#promo_specila_package').multiselect({
	          	maxHeight: 200,
	            buttonWidth: '210px',
	            includeSelectAllOption: true
	        });
	    }
  	});
 },

/*---------------------------------------------
   Function Name: getDiscountCuponList()
   Input Parameter: 
   return:json data
---------------------------------------------*/ 
getDiscountCuponList:function(){
	$('#refresh_overlay').css("display","block");
 	var getUserId=window.localStorage.getItem('companyInfo');
	if(typeof(getUserId)=="string"){	
		getUserId=JSON.parse(getUserId);
	}

	var fd = new FormData();
	fd.append("action","getDiscountCuponList");
	fd.append("user_id",getUserId[0].id);
	
	$.ajax({
		url: discountDbClass._Serverpath,
		type: 'POST',
		processData:false,
		contentType:false,
		data:fd
	}).done(function(result){
        var responseObj=result;
		var responseHTML='';
        var package_name='';
		var package_name_array='';
		if(typeof(result) === "string"){
			responseObj=JSON.parse(result);
		}
		console.log(responseObj);
		if(responseObj.data.length > 0){
			for(var i=0; i<responseObj.data.length; i++){
				package_name = responseObj.data[i].package_name;
				package_name_array=package_name.split(',');
				var packageResponse='';
				var s=0;
				for(s=0;s<package_name_array.length;s++){
					if(package_name_array[s]!=''){
						packageResponse +='<option selected disabled>'+package_name_array[s]+'</option>';	
					}
				}
				var discount_value;
                if(responseObj.data[i].promcop_discount_type=='%'){
					discount_value=responseObj.data[i].promo_couppon_value+'%';
				}
				else{
                    discount_value='$'+responseObj.data[i].promo_couppon_value;
				}
			 	responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="promo_cuppon_name_'+responseObj.data[i].id+'">'+responseObj.data[i].promo_couppon_name+'</td> <td style="text-align:center" id="promo_cuppon_code_'+responseObj.data[i].id+'">'+responseObj.data[i].promo_couppon_code+'</td> <td style="text-align:center" id="discount_value'+responseObj.data[i].id+'">'+discount_value+'</td> <td style="text-align: center;" id="promo_cuppon_start_date_'+responseObj.data[i].id+'">'+responseObj.data[i].promo_newstart_date+'</td> <td style="text-align: center;" id="promo_cuppon_end_date_'+responseObj.data[i].id+'">'+responseObj.data[i].promo_newend_date+'</td><td style="text-align: center;" id="promo_cuppon_service_start_date_'+responseObj.data[i].id+'">'+responseObj.data[i].promo_start_date+'</td> <td style="text-align: center;" id="promo_cuppon_service_end_date_'+responseObj.data[i].id+'">'+responseObj.data[i].promo_end_date+'</td> <td style="text-align:center;" id="promo_pakage_'+responseObj.data[i].id+'" package_name="'+package_name +'"><select class="refresh_multi_select" multiple>'+packageResponse+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs promo_onedit" id=edit_rate_'+responseObj.data[i]['id']+' onclick="discountDbClass.viewSpecialDiscountCuppon('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs promo_ondelete"  seq='+responseObj.data[i]['id']+' onclick="discountDbClass.deleteSpecialCuponList('+responseObj.data[i].id+')">Delete</a> </div></td>';					
					responseHTML+='</tr>';
			} 

		}
		else{
			responseHTML+='<tr><td colspan="7">Data not found</td></tr>';
		}
   		
   		$('#promo_code_cuppon_list').html(responseHTML);
   		 $('#refresh_overlay').css("display","none");
	   	setTimeout(function(){
			$(".refresh_multi_select").multiselect('destroy');
			$('.refresh_multi_select').multiselect({
				maxHeight: 200,
				buttonWidth: '155px',
				includeSelectAllOption: true
			});
		},800);
           
       
		}).fail(function(jqXHR, exception)
        {

           $('#refresh_overlay').css("display","none");
        });
      
	},

	/*---------------------------------------------
       Function Name: deleteSpecialCuponList()
       Input Parameter: rowId
       return:json data
    ---------------------------------------------*/ 
	deleteSpecialCuponList:function(rowId){    
		$('#refresh_overlay').css("display","block");	          
      	var fd = new FormData();
		fd.append("action","deleteSpecialCuponList");
		fd.append("row_Id",rowId);
     	var getAns=confirm("Are you sure. you want to delete item");
       	if(getAns){		
		$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: fd
			}).done(function(result){
		   		alert('Special Request Deleted Successfully');         
            	$('#refresh_overlay').css("display","none");
            	discountDbClass.getDiscountCuponList();
          	}).fail(function(jqXHR, exception)
          {

           $('#refresh_overlay').css("display","none");
        });
		}
        	
	},

	/*---------------------------------------------
       Function Name: viewSpecialDiscountCuppon()
       Input Parameter: rowId
       return:json data
    ---------------------------------------------*/ 
	viewSpecialDiscountCuppon:function(rowId){     
		$('#refresh_overlay').css("display","block");
   		$('#promo_save_rate12').html("Update");
    	$('#promo_save_rate12').attr("seq",rowId);
    	$('#promo_back_button').css("display","block");
 		var viewSpecialDiscount = new FormData();
		viewSpecialDiscount.append("action","viewSpecialDiscountCuppon");
		viewSpecialDiscount.append("view_Id",rowId);
		$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: viewSpecialDiscount
		}).done(function(result){
	           	console.log(result);
	           	var responseObj=result;
	           	var package_code_array='';
				var package_code='';
				if(typeof(result)=="string"){
					responseObj=JSON.parse(result);
	            }           
	            if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
				responseObj.data!=""){
	       			package_code = responseObj.data[0].special_package_id;
					package_code_array=package_code.split(',');
	                var start_date=responseObj.data[0].promo_newstart_date;
	               	var start_date=start_date.split('-');
	               	var start_date=start_date[1]+"/"+start_date[2]+"/"+start_date[0];
	           		var end_date=responseObj.data[0].promo_newend_date;
	                var end_date=end_date.split('-');
	                var end_date=end_date[1]+"/"+end_date[2]+"/"+end_date[0];
					var service_start_date=responseObj.data[0].promo_start_date;
	                var service_start_date=service_start_date.split('-');
	                var service_start_date=service_start_date[1]+"/"+service_start_date[2]+"/"+service_start_date[0];
	           		var service_end_date=responseObj.data[0].promo_end_date;
	                var service_end_date=service_end_date.split('-');
	                var service_end_date=service_end_date[1]+"/"+service_end_date[2]+"/"+service_end_date[0];
	                    
	                $("#promo_couppon_name").val(responseObj.data[0].promo_couppon_name);
	              	$("#promo_couppon_code").val(responseObj.data[0].promo_couppon_code);
	              	$("#promo_couppon_value").val(responseObj.data[0].promo_couppon_value);
	              	$("#promcop_discount_type").val(responseObj.data[0].promcop_discount_type);
	              	$("#promo_newstart_date").val(start_date);
	                $("#promo_newend_date").val(end_date);
	              	$("#promo_service_start_date").val(service_start_date);
	              	$("#promo_service_end_date").val(service_end_date);
	              	$("#promo_specila_package").val(package_code_array);
	                $("#promo_specila_package").multiselect('destroy');
					$('#promo_specila_package').multiselect({
						maxHeight: 200,
						buttonWidth: '210',
						includeSelectAllOption: true
					});
					
					if(responseObj.data[0].promo_autoapply_code=="2"){                        
	                    $(".promoCode").prop('checked', true);
	                }
	                else{
	                    $(".autoApply").prop('checked', true);
	                }
	                if(responseObj.data[0].is_combine_discount=="1"){
	                  	$('#combine_discount').prop('checked', true);
	              	}
		     	}	
				setTimeout(function(){
			        $("html, body").animate({ scrollTop: 0 }, "slow");	
			        $('#refresh_overlay').css("display","none");
				},200);
       		}).fail(function(jqXHR, exception)
        {

           $('#refresh_overlay').css("display","none");
        });           
	}

       
}

/*on click of back button */
$('#promo_back_button').on("click",function(){
	location.reload(true);
});

/*on click of form submit on update and add button*/
$('#promo_form').on("submit",function(event){
    event.preventDefault();
	$('#refresh_overlay').css("display","block");
	var promo_newstart_date=$('#promo_newstart_date').val();
    var promo_newend_date=$('#promo_newend_date').val();
	promo_newstart_date=new Date(promo_newstart_date);
    promo_newend_date=new Date(promo_newend_date);
	if(promo_newstart_date>promo_newend_date){
		alert("Service Start Date is greater than Service End Date");
		$('#refresh_overlay').css("display","none");
		return 0;
	}  
				
	var service_start_date=$('#promo_service_start_date').val();
    var service_end_date=$('#promo_service_end_date').val();
	if(service_start_date>service_end_date){
		alert("Booking Start Date is greater than Booking End Date");
		$('#refresh_overlay').css("display","none");
		return 0;
    }
	
	var row_id = $('#promo_save_rate12').attr('seq');
    if(row_id){
        var edit_row_id = new FormData($(this)[0]);
        edit_row_id.append("action","specialPromocupponUpdate");
        edit_row_id.append('edit_row_id',row_id);
		var selectspecialPackage = $('#promo_specila_package option:selected');
		var selectedPackageObject = [];
		$(selectspecialPackage).each(function(index, selectspecialPackage){
			selectedPackageObject .push($(this).val());
		});
	
		edit_row_id.append("promo_special_package",selectedPackageObject);

        $.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: edit_row_id
	     }).done(function(result){
				result=JSON.parse(result);
				if(result.code==1005){
			   		alert("Enter promo code unique or name ");
				}
				else{
					alert('Updated Successfully');  
		     		location.reload();	
				}
				$('#refresh_overlay').css("display","none");
	        }).fail(function(jqXHR, exception)
            {

           $('#refresh_overlay').css("display","none");
         });
    }
    else{
		var fd = new FormData($('#promo_form')[0]);
		fd.append("action","setPromoDiscount");
		var getUserId=window.localStorage.getItem('companyInfo');
		var selectspecialPackage = $('#promo_specila_package option:selected');
		var selectedPackageObject = [];
		$(selectspecialPackage).each(function(index, selectspecialPackage){
			selectedPackageObject .push($(this).val());
		});
        
        fd.append("promo_special_package",selectedPackageObject);
		
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}
        
        fd.append("user_id",getUserId[0].id);

        $.ajax({
			url: discountDbClass._Serverpath,

			type: 'POST',
			processData:false,
			contentType:false,

			data: fd
	   }).done(function(result){
		   	result=JSON.parse(result);
		   	if(result.code==1006){
		   		alert("Enter promo code unique or name ");
		   	}
		   else{
	   	        alert('Added Successfully');  
	   	  		location.reload();
			}

			$('#refresh_overlay').css("display","none");
        }).fail(function(jqXHR, exception)
        {

           $('#refresh_overlay').css("display","none");
         });
	}
});
	
discountDbClass.getSpclPackage();
discountDbClass.getDiscountCuponList();
