/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: SMA Airport
    Author: Mylimoproject
---------------------------------------------*/
$(function () {
    $('#refresh_overlay').show();
});
var getServiceVehicleType={

    /*set the web service path using php file */
    _SERVICEPATH:"phpfile/service.php",
    /*---------------------------------------------
        Function Name: getVehicle()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
    getVehicle:function()
    {
        var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
        if(typeof(getLocalStoragevalue)=="string")
        {
            getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
        }
        var getuserId=window.localStorage.getItem("companyInfo");
        if(typeof(getuserId)=="string")
        {
            getuserId=JSON.parse(getuserId);
        }
        $('#refresh_overlay').show();
        $.ajax({
            url: getServiceVehicleType._SERVICEPATH,
            type: 'POST',
            data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
            success: function(response) {
                $('#refresh_overlay').hide();
                var k=1;
                var responseHTML='';
                var responseObj=response;
                if(typeof(response)=="string")
                {
                    responseObj=JSON.parse(response);
                }
                var getFinal=[];
                for(var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {
                    getFinal[i]=responseObj.VehicleTypes.VehicleType[i].PassengerCapacity+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'@'+responseObj.VehicleTypes.VehicleType[i].VehTypeId;
                }
                getFinal.sort();
                var d=0;
                for(var j=0; j<getFinal.length; j++)
                {
                    var carsplitResult=getFinal[j].split('@');
                    var k=0;
                    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
                    {
                        if(carsplitResult[1]==responseObj.VehicleTypes.VehicleType[i].VehTypeId)
                        {

                            if(typeof(responseObj.VehicleTypes.VehicleType[i].VehTypeImg1)!="undefined" && typeof(responseObj.VehicleTypes.VehicleType[i].VehTypeImg1)!=undefined)
                            {

                                if(d==0)
                                {
                                    responseHTML+='<tr><td style="width:2%;"><input type="checkbox" class="cmnclass" seq="'+d+'" name="vehicle" value="checked"></td><td class="vehicle_'+d+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td><td><input type="text" style="width:50%" class="phpfeesCommand phpfess'+d+'" disabled><input type="checkbox" name="vehicle"   class="phpfesscheck'+d+'" value="Bike"></td><td><input type="text" style="width:50%" disabled class="domaticCLass'+d+'"><input type="checkbox" name="vehicle"  class="domaticCLasscheck'+d+'" value="Bike"></td><td><input type="text" style="width:50%" disabled class="intrfclass'+d+'"><input type="checkbox" name="vehicle" value="Bike"  class="intrfclasscheck'+d+'"></td><td><input type="text" disabled class="fhvclass'+d+'" style="width:50%"><input type="checkbox"  class="fhvclasscheck'+d+'" name="vehicle" value="Bike"></td><td><input type="text" disabled class="fboclass'+d+'" style="width:50%"><input type="checkbox"  class="fboclasscheck'+d+'" name="vehicle" value="Bike"></td></tr>';
                                }
                                else
                                {
                                    responseHTML+='<tr><td style="width:2%;"><input type="checkbox" class="cmnclass" seq="'+d+'" name="vehicle" value="checked"></td><td class="vehicle_'+d+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td><td><input type="text" style="width:50%" class=" phpfeesCommand phpfess'+d+'" disabled></td><td><input type="text" style="width:50%" disabled class="domaticCLass'+d+'"></td><td><input type="text" style="width:50%" disabled class="intrfclass'+d+'"></td><td><input type="text" disabled class="fhvclass'+d+'" style="width:50%"></td><td><input type="text" disabled class="fboclass'+d+'" style="width:50%"></td></tr>';
                                }
                                d++;
                            }

                        }

                    }
                }

                $('.airportVehicleInfo').html(responseHTML+'<br><br>');
                $('.cmnclass').on("click",function(){
                    var getSeq=$(this).attr('seq');
                    if(!$('#setFBO').is(":checked"))
                    {

                        if($(this).is(":checked"))
                        {
                            $('.phpfess'+getSeq).removeAttr("disabled").prop("required",true).val('');
                            $('.domaticCLass'+getSeq).removeAttr("disabled").prop("required",true).val('');
                            $('.intrfclass'+getSeq).removeAttr("disabled").prop("required",true).val('');
                            $('.fhvclass'+getSeq).removeAttr("disabled").prop("required",true).val('');

                        }
                        else
                        {

                            $('.phpfess'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.domaticCLass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.intrfclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.fhvclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.fboclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                        }
                    }
                    else
                    {

                        if($(this).is(":checked"))
                        {
                            $('.fboclass'+getSeq).removeAttr("disabled").prop("required",true).val('');


                        }
                        else
                        {
                            $('.fboclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.phpfess'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.domaticCLass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.intrfclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                            $('.fhvclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                        }

                    }
                });


                /* FHV code start here */
                $('.fhvclasscheck0').on("change",function(){
                    var getUndesablevalue=0;
                    $('.cmnclass').each(function(){
                        var getSeq=$(this).attr('seq');
                        if($(this).is(":checked"))
                        {
                            if(!$('.fhvclass'+getSeq).attr('disabled'))
                            {
                                if(getUndesablevalue==0)
                                {
                                    getUndesablevalue=$('.fhvclass0').val();
                                    $('.fhvclass'+getSeq).val(getUndesablevalue);

                                }
                                else
                                {
                                    $('.fhvclass'+getSeq).val(getUndesablevalue);
                                }
                            }
                        }
                    });

                });

                /* international code start here  */

                $('.intrfclasscheck0').on("change",function(){
                    var getUndesablevalue=0;
                    $('.cmnclass').each(function(){
                        var getSeq=$(this).attr('seq');
                        if($(this).is(":checked"))
                        {
                            if(!$('.intrfclass'+getSeq).attr('disabled'))
                            {
                                if(getUndesablevalue==0)
                                {
                                    getUndesablevalue=$('.intrfclass0').val();
                                    $('.intrfclass'+getSeq).val(getUndesablevalue);
                                }
                                else
                                {

                                    $('.intrfclass'+getSeq).val(getUndesablevalue);
                                }
                            }
                        }
                    });
                });

                /* international code end here*/


                /* domestic code start here */
                $('.domaticCLasscheck0').on("change",function(){
                    var getUndesablevalue=0;
                    $('.cmnclass').each(function(){
                        var getSeq=$(this).attr('seq');
                        if($(this).is(":checked"))
                        {
                            if(!$('.domaticCLass'+getSeq).attr('disabled'))
                            {
                                if(getUndesablevalue==0)
                                {
                                    getUndesablevalue=$('.domaticCLass0').val();
                                    //getUndesablevalue=$('.domaticCLass'+getSeq).val();
                                    $('.domaticCLass'+getSeq).val(getUndesablevalue);

                                }
                                else
                                {

                                    $('.domaticCLass'+getSeq).val(getUndesablevalue);
                                }

                            }

                        }

                    });

                });
                /* code end here */




                /* fbo code start here */

                /* domestic code start here */

                $('.fboclasscheck0').on("change",function(){
                    var getUndesablevalue=0;
                    $('.cmnclass').each(function(){
                        var getSeq=$(this).attr('seq');
                        if($(this).is(":checked"))
                        {
                            if(!$('.fboclass'+getSeq).attr('disabled'))
                            {
                                if(getUndesablevalue==0)
                                {
                                    getUndesablevalue=$('.fboclass0').val();
                                    $('.fboclass'+getSeq).val(getUndesablevalue);

                                }
                                else
                                {
                                    $('.fboclass'+getSeq).val(getUndesablevalue);
                                }

                            }

                        }

                    });

                });
                /* code end here */
                /* fbo code end here */

                $('.phpfesscheck0').on("change",function(){
                    var getUndesablevalue=0;
                    $('.cmnclass').each(function(){
                        var getSeq=$(this).attr('seq');
                        if($(this).is(":checked"))
                        {
                            if(!$('.phpfess'+getSeq).attr('disabled'))
                            {
                                if(getUndesablevalue==0)
                                {
                                    getUndesablevalue=$('.phpfess0').val();
                                    //getUndesablevalue=$('.phpfess'+getSeq).val();
                                    $('.phpfess'+getSeq).val(getUndesablevalue);

                                }
                                else
                                {

                                    $('.phpfess'+getSeq).val(getUndesablevalue);
                                }


                            }
                        }
                    });

                });

            }
        });

    }
}

/* new client service start here */
getServiceVehicleType.getVehicle();
/* new client service end here */


/*get city information class start here*/
var getCityInfomation={

    /*set the webservice path using php*/
    _Serverpath:"phpfile/sma_client.php",

    /*---------------------------------------------
        Function Name: getSmaCityInformation()
        Input Parameter: smaId
        return:json data
    ---------------------------------------------*/
    getSmaCityInformation:function(smaId)
    {
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
    },

    /*---------------------------------------------
        Function Name: delseSmaAirportInformation()
        Input Parameter: getRowId
        return:json data
    ---------------------------------------------*/
    delseSmaAirportInformation:function(getRowId)
    {
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=delseSmaAirportInformation&rowId="+getRowId+"&type=airport&user_id="+getUserId[0].id,
            success: function(response) {
                $('#refresh_overlay').hide();
            }
        });
    },
    /*---------------------------------------------
        Function Name: showEditItemSecond()
        Input Parameter: getid
        return:json data
    ---------------------------------------------*/
    showEditItemSecond:function(getid){
        $('#refresh_overlay').css("display","block");
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        // $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=showEditItem&type=airport&rowId="+getid+"&user_id="+getUserId[0].id,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                if(typeof(response)=="string")
                {
                    responseJson=JSON.parse(response);
                }
                // $('#add_sma_country_1').val(responseJson.data[0][0].sma_id);
                if(typeof(responseJson.data[1])!= 'boolean'){
                    $('#comment').val(responseJson.data[1][0].q_and_r_parking_msg);
                    $('#greeterFee').val(responseJson.data[1][0].fhv_fee);
                    $('#greeterContent').val(responseJson.data[1][0].fhv_content);
                    if(responseJson.data[1][0].is_insidemeet_greet == 1){
                        $('.insideMG').prop("checked",true);
                        $('#insideMeetGreet').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].insidemeet_msg);
                    }
                    else{
                        $('.insideMG').prop("checked",false);
                        $('#insideMeetGreet').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_curbside == 1){
                        $('.curb').prop("checked",true);
                        $('#curbside').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].curbside_msg);
                    }
                    else{
                        $('.curb').prop("checked",false);
                        $('#curbside').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_domestic_flight == 1){
                        $('.dmFlight').prop("checked",true);
                        $('#domFlight').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].domestic_msg);
                    }
                    else{
                        $('.dmFlight').prop("checked",false);
                        $('#domFlight').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_int_flight == 1){
                        $('.iFlight').prop("checked",true);
                        $('#intFlight').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].int_flight_msg);
                    }
                    else{
                        $('.iFlight').prop("checked",false);
                        $('#intFlight').attr("disabled");
                    }
                    $('#schgFee1').val(responseJson.data[1][0].fbo_fee);
                    $('#schgFee2').val(responseJson.data[1][0].fbo_comment);
                }
                $('.cmnclass').each(function(){
                    var getSeq=$(this).attr('seq');
                    $(this).removeAttr("checked");
                    $('.phpfess'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.domaticCLass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.intrfclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.fhvclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.fboclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                });
                if(typeof(responseJson.data[2])!= 'boolean'){
                    for(var x=0; x < responseJson.data[2].length; x++){
                        $('.cmnclass').each(function(){
                            var getSeq=$(this).attr('seq');
                            var vehicleName = $('.vehicle_'+getSeq).html();
                            if(vehicleName == responseJson.data[2][x].vehicle_code){			       $(this).prop("checked",true);
                                if(responseJson.data[2][x].fbo_srchg==0)
                                {
                                    $('.phpfess'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].per_hour_parking_fees);
                                    $('.domaticCLass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].domestic_flt_schg);
                                    $('.intrfclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].int_flt_schg);
                                    $('.fhvclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].fhv_access_fees);
                                    $('#FboDesclaimer').css("visibility","hidden");
                                }
                                else
                                {
                                    $('.fboclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].fbo_srchg);
                                    $('#setFBO').prop("checked",true);
                                    $('#FboDesclaimer').css("visibility","visible");
                                }

                            }

                        });

                    }

                }
                $('#refresh_overlay').css("display","none");
            }
        });
    },

    /*---------------------------------------------
        Function Name: showEditItem()
        Input Parameter: getid
        return:json data
    ---------------------------------------------*/
    showEditItem:function(getid){
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=showEditItem&type=airport&rowId="+getid+"&user_id="+getUserId[0].id,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                if(typeof(response)=="string")
                {
                    responseJson=JSON.parse(response);
                }
                $('#add_sma_country_1').val(responseJson.data[0][0].sma_id);
                $('#airport_zipcode').val(responseJson.data[0][0].type_zip);
                $('#airport_name').val(responseJson.data[0][0].type_name).prop("disabled",true);
                $('#airport_code').val(responseJson.data[0][0].save_as).prop("disabled",true);
                if(typeof(responseJson.data[1])!= 'boolean'){
                    $('#comment').val(responseJson.data[1][0].q_and_r_parking_msg);
                    if(responseJson.data[1][0].fhv_fee!="0")
                    {
                        $('.concergeFeesClass').removeAttr("disabled");
                        $('.conciergeFeeCheckBox').prop("checked",true);
                    }
                    $('#greeterFee').val(responseJson.data[1][0].fhv_fee);
                    $('#greeterContent').val(responseJson.data[1][0].fhv_content);
                    if(responseJson.data[1][0].is_insidemeet_greet == 1){
                        $('.insideMG').prop("checked",true);
                        $('#insideMeetGreet').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].insidemeet_msg);
                    }
                    else{
                        $('.insideMG').prop("checked",false);
                        $('#insideMeetGreet').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_curbside == 1){
                        $('.curb').prop("checked",true);
                        $('#curbside').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].curbside_msg);
                    }
                    else{
                        $('.curb').prop("checked",false);
                        $('#curbside').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_domestic_flight == 1){
                        $('.dmFlight').prop("checked",true);
                        $('#domFlight').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].domestic_msg);
                    }
                    else{
                        $('.dmFlight').prop("checked",false);
                        $('#domFlight').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_int_flight == 1){
                        $('.iFlight').prop("checked",true);
                        $('#intFlight').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].int_flight_msg);
                    }
                    else{
                        $('.iFlight').prop("checked",false);
                        $('#intFlight').attr("disabled");
                    }
                    $('#schgFee1').val(responseJson.data[1][0].fbo_fee);
                    $('#schgFee2').val(responseJson.data[1][0].fbo_comment);
                }
                $('.cmnclass').each(function(){
                    var getSeq=$(this).attr('seq');
                    $(this).removeAttr("checked");
                    $('.phpfess'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.domaticCLass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.intrfclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.fhvclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.fboclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                });
                if(typeof(responseJson.data[2])!= 'boolean'){
                    for(var x=0; x < responseJson.data[2].length; x++){
                        $('.cmnclass').each(function(){
                            var getSeq=$(this).attr('seq');
                            var vehicleName = $('.vehicle_'+getSeq).html();
                            if(vehicleName == responseJson.data[2][x].vehicle_code){
                                $(this).prop("checked",true);
                                if(responseJson.data[2][x].fbo_srchg==0)
                                {
                                    $('.phpfess'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].per_hour_parking_fees);
                                    $('.domaticCLass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].domestic_flt_schg);
                                    $('.intrfclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].int_flt_schg);
                                    $('.fhvclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].fhv_access_fees);
                                    $('#FboDesclaimer').css("visibility","hidden");
                                }
                                else
                                {
                                    $('.fboclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].fbo_srchg);
                                    $('#setFBO').prop("checked",true);
                                    $('#FboDesclaimer').css("visibility","visible").val(responseJson.data[1][0].airport_disclaimer);
                                }

                            }

                        });
                    }
                }

                $('#add_sma_location').html("Update");
                $('#add_sma_location').attr("seq",getid);
                $('#refresh_overlay').css("display","none");
            }
        });
    },

    /*---------------------------------------------
        Function Name: viewItem()
        Input Parameter: getid
        return:json data
    ---------------------------------------------*/
    viewItem:function(getid){
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=showEditItem&type=airport&rowId="+getid+"&user_id="+getUserId[0].id,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                if(typeof(response)=="string")
                {
                    responseJson=JSON.parse(response);
                }
                $('#add_sma_country_1').val(responseJson.data[0][0].sma_id);
                $('#airport_zipcode').val(responseJson.data[0][0].type_zip);
                $('#airport_name').val(responseJson.data[0][0].type_name);
                $('#airport_code').val(responseJson.data[0][0].save_as);
                if(typeof(responseJson.data[1])!= 'boolean'){
                    $('#comment').val(responseJson.data[1][0].q_and_r_parking_msg);
                    $('#greeterFee').val(responseJson.data[1][0].fhv_fee);
                    $('#greeterContent').val(responseJson.data[1][0].fhv_content);
                    if(responseJson.data[1][0].is_insidemeet_greet == 1){
                        $('.insideMG').prop("checked",true);
                        $('#insideMeetGreet').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].insidemeet_msg);
                    }
                    else{
                        $('.insideMG').prop("checked",false);
                        $('#insideMeetGreet').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_curbside == 1){
                        $('.curb').prop("checked",true);
                        $('#curbside').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].curbside_msg);
                    }
                    else{
                        $('.curb').prop("checked",false);
                        $('#curbside').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_domestic_flight == 1){
                        $('.dmFlight').prop("checked",true);
                        $('#domFlight').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].domestic_msg);
                    }
                    else{
                        $('.dmFlight').prop("checked",false);
                        $('#domFlight').attr("disabled");
                    }
                    if(responseJson.data[1][0].is_int_flight == 1){
                        $('.iFlight').prop("checked",true);
                        $('#intFlight').removeAttr("disabled").prop("required",true).val(responseJson.data[1][0].int_flight_msg);
                    }
                    else{
                        $('.iFlight').prop("checked",false);
                        $('#intFlight').attr("disabled");
                    }
                    $('#schgFee1').val(responseJson.data[1][0].fbo_fee);
                    $('#schgFee2').val(responseJson.data[1][0].fbo_comment);
                }
                /*click on cmc*/
                $('.cmnclass').each(function(){
                    var getSeq=$(this).attr('seq');
                    $(this).removeAttr("checked");
                    $('.phpfess'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.domaticCLass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.intrfclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.fhvclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                    $('.fboclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
                });
                if(typeof(responseJson.data[2])!= 'boolean'){
                    for(var x=0; x < responseJson.data[2].length; x++){
                        $('.cmnclass').each(function(){
                            var getSeq=$(this).attr('seq');
                            var vehicleName = $('.vehicle_'+getSeq).html();
                            if(vehicleName == responseJson.data[2][x].vehicle_code){
                                $(this).prop("checked",true);
                                if(responseJson.data[2][x].fbo_srchg==0)
                                {
                                    $('.phpfess'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].per_hour_parking_fees);
                                    $('.domaticCLass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].domestic_flt_schg);
                                    $('.intrfclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].int_flt_schg);
                                    $('.fhvclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].fhv_access_fees);
                                    $('#FboDesclaimer').css("visibility","hidden");
                                }
                                else
                                {
                                    $('.fboclass'+getSeq).removeAttr("disabled").prop("required",true).val(responseJson.data[2][x].fbo_srchg);
                                    $('#setFBO').prop("checked",true);
                                    $('#FboDesclaimer').css("visibility","visible");
                                    $('#FboDesclaimer').css("visibility","visible").val(responseJson.data[1][0].airport_disclaimer);
                                }
                            }

                        });
                    }
                }
                $('#add_sma_location').hide();
                $('#add_sma_location').attr("seq",getid);
            }
        });
    },

    /*---------------------------------------------
        Function Name: getSmaAirportInformation()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
    getSmaAirportInformation:function()
    {
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }

        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=getSmaAirportInformation&type=airport&user_id="+getUserId[0].id,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                var finalSelectBox='';
                var selectBoxMatrix='';
                if(typeof(response)=="string")
                {
                    responseJson=JSON.parse(response);
                }
                selectBoxMatrix+='<option>Select</option>';
                $.each(responseJson.data,function(index,result){
                    if(result.id!=null)
                    {
                        finalSelectBox+='<tr><td>'+result.sma_name+'</td><td>'+result.type_name+'</td><td>'+result.save_as+'</td><td>'+result.type_zip+'</td><td><a class="btn btn-primary viewAirport" seq="'+result.id+'" >View</a></td><td><a class="btn btn-xs editAirport" seq="'+result.id+'" >Edit</a><a class="btn btn-xs deleteAirport" seq="'+result.id+'" >Delete</a></td></tr>';
                        selectBoxMatrix+="<option value="+result.id+">"+result.type_name+"</option>";
                    }
                    else
                    {
                        finalSelectBox='<tr><td colspan="4">Data not Found.</td></tr>';
                        selectBoxMatrix+="<option value='noairport'>No Airport</option>";
                    }
                });
                $('#sma_airport_list').html(finalSelectBox);
                $('#select_matrix_db').html(selectBoxMatrix);
                $('.editAirport').on("click",function(){
                    $('#add_sma_location').show();
                    var getseq=$(this).attr("seq");
                    getCityInfomation.showEditItem(getseq);

                });
                /*click on delete airport button*/
                $('.deleteAirport').on("click",function(e){
                    e.preventDefault();
                    var getseq=$(this).attr("seq");
                    var confirmValue=confirm("Warning!  Are you sure, you wants to deactivate zone airport. Deactivating will erase all saved data for zone airport from system.");
                    if(confirmValue)
                    {
                        getCityInfomation.delseSmaAirportInformation(getseq);
                    }
                });

                /*click on copy rate matrix*/
                $('#copy_rate_matrix').on("click",function(){
                    var getseq=$('#select_matrix_db').val();
                    $('#airport_name').removeAttr("disabled");
                    $('#airport_code').removeAttr("disabled");
                    getCityInfomation.showEditItemSecond(getseq);

                })
                $('.viewAirport').on("click",function(){
                    var getseq=$(this).attr("seq");
                    getCityInfomation.viewItem(getseq);

                });

            }

        });
    },
    /*---------------------------------------------
        Function Name: isUserApiKey()
        Input Parameter: user_id
        return:json data
   ---------------------------------------------*/
    getSmaInformation:function()
    {
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        var airportDB = JSON.parse(localStorage.airport_db_table_id);
        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=getSMA&user_id="+getUserId[0].id+"&zip_code="+airportDB.airport_db_zipcode,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                var finalSelectBox='';
                if(typeof(response)=="string")
                {
                    responseJson=JSON.parse(response);
                }
                finalSelectBox='<option value="">select SMA</option>';
                var exist = 0;
                $.each(responseJson.data,function(index,result){
                    if(result.selected === 1){
                        exist = 1;
                        finalSelectBox+='<option selected="selected" value="'+result.id+'">'+result.sma_name+'</option>';
                    } else {
                        finalSelectBox+='<option value="'+result.id+'">'+result.sma_name+'</option>';
                    }
                });
                $('#add_sma_country_1').html(finalSelectBox);
                if(exist === 0){
                    $('#add_sma_country_1').parent().append('<p class="zipVerify" style="color: red;">Verify that zipcode/city exist within existing database or edit and add it or create new SMA and include zipcode</p>');
                }
                $('#add_sma_country_1').on("change",function(){
                    var getValue=$(this).val();
                    getCityInfomation.getSmaCityInformation(getValue);
                    $('.zipVerify').remove();
                });

            }
        });

    }

}

/*call the function on page load*/
getCityInfomation.getSmaInformation();
getCityInfomation.getSmaAirportInformation();

/*click on airport zone form*/
$('#airportZoneForm').on("submit",function(e){
    $('#refresh_overlay').css("display","block");
    e.preventDefault();
    if($('#add_sma_location').html()=="Submit")
    {
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        var getForm=new FormData($('#airportZoneForm')[0]);
        var smaId=$('#add_sma_country_1').val();
        var airport_zipcode=$('#airport_zipcode').val();
        var airport_name=$('#airport_name').val();
        var airport_code=$('#airport_code').val();
        getForm.append("action","createSmaDataAirportZone");
        var getAirportDbLocalStorage2=window.localStorage.getItem("airport_db_table_id");
        if(getAirportDbLocalStorage2!="string12")
        {
            getAirportDbLocalStorage2=JSON.parse(getAirportDbLocalStorage2);
            getForm.append("airport_db_id",getAirportDbLocalStorage2.airport_db_id);
        }
        getForm.append("smaId",smaId);
        getForm.append("airport_zipcode",airport_zipcode);
        getForm.append("airport_name",airport_name);
        getForm.append("airport_code",airport_code);
        getForm.append("type","airport");
        getForm.append("user_id",getUserId[0].id);
        var vehiclearr =[];
        /*click on the cmn button*/
        $('.cmnclass').each(function(){
            var getSeq=$(this).attr('seq');
            if($(this).is(":checked"))
            {
                var vehicleName = $('.vehicle_'+getSeq).html();
                var phpfess = $('.phpfess'+getSeq).val();
                var domesticCLass = $('.domaticCLass'+getSeq).val();
                var intrfclass = $('.intrfclass'+getSeq).val();
                var fhvclass = $('.fhvclass'+getSeq).val();
                var fboclass = $('.fboclass'+getSeq).val();
                vehiclearr.push({"vehicleName":vehicleName,"phpfess": phpfess,"domesticCLass":domesticCLass,"intrfclass":intrfclass,"fhvclass":fhvclass,"fboclass":fboclass});
            }

        });
        var parkingFeeComment = $('#comment').val();
        var greeterFee = $('#greeterFee').val();
        var greeterContent= $('#greeterContent').val();
        var insideCheck = 0;
        var curbCheck = 0;
        var dnFlightCheck = 0;
        var iFlightCheck = 0;
        if($('.insideMG').is(":checked"))
        {
            insideCheck = 1
            var insideMeetGreet = $('#insideMeetGreet').val();
        }
        if($('.curb').is(":checked"))
        {
            curbCheck = 1;
            var curbside = $('#curbside').val();
        }
        if($('.dmFlight').is(":checked"))
        {
            dnFlightCheck = 1;
            var domFlight = $('#domFlight').val();
        }
        if($('.iFlight').is(":checked"))
        {
            iFlightCheck = 1;
            var intFlight  = $('#intFlight').val();
        }

        var fboFee = $('#schgFee1').val();
        var fboComment = $('#schgFee2').val();

        var FboDesclaimer=$('#FboDesclaimer').val();


        var getJsonValue2 = {
            "parkingFeeComment":parkingFeeComment,
            "greeterFee":greeterFee,
            "greeterContent":greeterContent,
            "insideCheck":insideCheck,
            "insideMeetGreet":insideMeetGreet,
            "curbCheck":curbCheck,
            "curbside":curbside,
            "dnFlightCheck":dnFlightCheck,
            "domFlight":domFlight,
            "iFlightCheck":iFlightCheck,
            "intFlight":intFlight,
            "fboFee":fboFee,
            "fboComment":fboComment,
            "FboDesclaimer":FboDesclaimer
        }
        getForm.append("vehicle",JSON.stringify(vehiclearr));
        getForm.append("fees",JSON.stringify(getJsonValue2));
        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            processData:false,
            contentType:false,
            data: getForm,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                if(typeof(responseJson)=="string")
                {
                    responseJson=JSON.parse(responseJson);
                }
                if(responseJson.code==1006)
                {

                    if(responseJson.data=='airportCode')
                    {
                        alert("Airport code should be unique.");

                    } else if(responseJson.data=='airportName')
                    {
                        alert("Airport name should be unique.");

                    }else if(responseJson.data=='zipCode')
                    {
                        alert("Airport Zip code should be unique.");

                    }
                }
                else
                {
                    alert("Suceessfully inserted");
                    $('#airport_zipcode').val('');
                    $('#airport_name').val('');
                    $('#airport_code').val('');
                    // $('#refresh_overlay').css("display","none");
                    location.reload();
                    //window.location.href="airport_db.html";
                }
                $('#refresh_overlay').css("display","none");
            }

        });
    }
    else
    {
        $('#refresh_overlay').css("display","block");
        var getId=$('#add_sma_location').attr("seq");
        var getUserId=window.localStorage.getItem('companyInfo');
        if(typeof(getUserId)=="string")
        {
            getUserId=JSON.parse(getUserId);
        }
        var getForm=new FormData($('#airportZoneForm')[0]);
        var smaId=$('#add_sma_country_1').val();
        var airport_zipcode=$('#airport_zipcode').val();
        var airport_name=$('#airport_name').val();
        var airport_code=$('#airport_code').val();
        getForm.append("action","updateSmaDataAirportZone");
        getForm.append("smaId",smaId);
        getForm.append("airport_zipcode",airport_zipcode);
        getForm.append("airport_name",airport_name);
        getForm.append("airport_code",airport_code);
        getForm.append("type","airport");
        getForm.append("user_id",getUserId[0].id);
        getForm.append("rowId",getId);
        var vehiclearr =[];
        /*click on button to get data*/
        $('.cmnclass').each(function(){
            var getSeq=$(this).attr('seq');
            if($(this).is(":checked"))
            {
                var vehicleName = $('.vehicle_'+getSeq).html();
                var phpfess = $('.phpfess'+getSeq).val();
                var domesticCLass = $('.domaticCLass'+getSeq).val();
                var intrfclass = $('.intrfclass'+getSeq).val();
                var fhvclass = $('.fhvclass'+getSeq).val();
                var fboclass = $('.fboclass'+getSeq).val();
                vehiclearr.push({"vehicleName":vehicleName,"phpfess": phpfess,"domesticCLass":domesticCLass,"intrfclass":intrfclass,"fhvclass":fhvclass,"fboclass":fboclass});
            }
        });
        var parkingFeeComment = $('#comment').val();
        var greeterFee = $('#greeterFee').val();
        var greeterContent= $('#greeterContent').val();
        var insideCheck = 0;
        var curbCheck = 0;
        var dnFlightCheck = 0;
        var iFlightCheck = 0;
        if($('.insideMG').is(":checked"))
        {
            insideCheck = 1
            var insideMeetGreet = $('#insideMeetGreet').val();
        }
        if($('.curb').is(":checked"))
        {
            curbCheck = 1;
            var curbside = $('#curbside').val();
        }
        if($('.dmFlight').is(":checked"))
        {
            dnFlightCheck = 1;
            var domFlight = $('#domFlight').val();
        }
        if($('.iFlight').is(":checked"))
        {
            iFlightCheck = 1;
            var intFlight  = $('#intFlight').val();
        }

        var fboFee = $('#schgFee1').val();
        var fboComment = $('#schgFee2').val();
        var getJsonValue2 = {
            "parkingFeeComment":parkingFeeComment,
            "greeterFee":greeterFee,
            "greeterContent":greeterContent,
            "insideCheck":insideCheck,
            "insideMeetGreet":insideMeetGreet,
            "curbCheck":curbCheck,
            "curbside":curbside,
            "dnFlightCheck":dnFlightCheck,
            "domFlight":domFlight,
            "iFlightCheck":iFlightCheck,
            "intFlight":intFlight,
            "fboFee":fboFee,
            "fboComment":fboComment
        }

        getForm.append("vehicle",JSON.stringify(vehiclearr));
        getForm.append("fees",JSON.stringify(getJsonValue2));

        $('#refresh_overlay').show();
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            processData:false,
            contentType:false,
            data: getForm,
            success: function(response) {
                $('#refresh_overlay').hide();
                var responseJson=response;
                if(typeof(responseJson)=="string")
                {
                    responseJson=JSON.parse(responseJson);
                }
                if(responseJson.code==1006)
                {
                    if(responseJson.data=='airportCode')
                    {
                        alert("Airport code should be unique.");

                    } else if(responseJson.data=='airportName')
                    {
                        alert("Airport name should be unique.");

                    }else if(responseJson.data=='zipCode')
                    {
                        alert("Airport Zip code should be unique.");

                    }
                }
                else
                {
                    alert("Suceessfully updated.");
                    // $('#refresh_overlay').css("display","none");
                    // location.reload();
                    window.location.href="airport_db.html";
                }
                $('#refresh_overlay').css("display","none");
            }
        });
    }
    // $('#refresh_overlay').css("display","none");
});

/*check the data from localstorage exists or not*/
var getAirportDbLocalStorage=window.localStorage.getItem("airport_db_table_id");

if(typeof(getAirportDbLocalStorage)!="object") {
    if(getAirportDbLocalStorage=="string12")
    {
        $('#airport_zipcode').removeAttr("disabled");
        $('#airport_name').removeAttr("disabled");
        $('#airport_code').removeAttr("disabled");
    }
    else
    {
        getAirportDbLocalStorage=JSON.parse(getAirportDbLocalStorage);
        if(getAirportDbLocalStorage.mainIdLocal=="NoId")
        {

            $('#airport_zipcode').val(getAirportDbLocalStorage.airport_db_zipcode);
            $('#airport_name').val(getAirportDbLocalStorage.airport_db_name).prop("disabled",true);
            $('#airport_code').val(getAirportDbLocalStorage.airport_db_code).prop("disabled",true);
        }
        else
        {
            setTimeout(function(){
                if(getAirportDbLocalStorage.type=="edit")
                {
                    getCityInfomation.showEditItem(getAirportDbLocalStorage.mainIdLocal);

                }
                else if(getAirportDbLocalStorage.type=="view")
                {
                    getCityInfomation.viewItem(getAirportDbLocalStorage.mainIdLocal);
                }
                else{
                    getCityInfomation.delseSmaAirportInformation(getAirportDbLocalStorage.mainIdLocal);
                }
            },3000);

        }

    }
}