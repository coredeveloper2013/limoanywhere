/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: add account
    Author: Mylimoproject
---------------------------------------------*/
/*create a class name for the add account*/
var discountDbClass={
	/*set a web service path for webservice*/
	_Serverpath:"phpfile/addaccount_client.php",

	/*---------------------------------------------
       Function Name: getuserAccounList()
       Input Parameter: smaId
       return:json data
    ---------------------------------------------*/ 
    getuserAccounList:function(){
        $('#refresh_overlay').css("display","block");
        var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){	
			getUserId=JSON.parse(getUserId);
	    }

 		var fd = new FormData();
     	fd.append("action","getuserAccounList");
	 	fd.append("user_id",getUserId[0].id);

		$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data:fd
	    }).done(function(result){        
            var responseObj=result;
			var responseHTML=' ';
			var account_type_array='';
			var account_type='';			
		    if(typeof(result)=="string"){
				responseObj=JSON.parse(result);
            }
			
			if(responseObj.data[0].id!=null){
			   	for(var i=0; i<responseObj.data.length; i++){
				    responseAccount=0;
					account_type = responseObj.data[i].ac_type;
					account_type_array=account_type.split(',');
					var t=0;
					if(account_type_array.length>0){
					    for(t=0;t<account_type_array.length;t++){
	                       if(account_type_array[t]!=''){
							  responseAccount +='<option selected disabled>'+account_type_array[t]+'</option>';
	                        }
					    }
                    }
                    var imageName;
			        if(responseObj.data[i].ac_status=="Active" || responseObj.data[i].ac_status=="Yes" )
			        {
			      	   imageName="active.png";
			        }
				   else{
				   	   imageName="diactivate.png";
				    }

		 	        responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="user_name'+responseObj.data[i].id+'">'+responseObj.data[i].f_name+'</td> <td style="text-align:center" id="user_userId'+responseObj.data[i].id+'">'+responseObj.data[i].eml_address+'</td>  <td style="text-align: center;" id="user_accoiun_no'+responseObj.data[i].id+'">'+responseObj.data[i].ac_number+'</td> <td style="text-align:center;" id="user_country_code'+responseObj.data[i].id+'">'+responseObj.data[i].country_name+'</td> <td style="text-align:center;" id="user_account_type'+responseObj.data[i].id+'"><select class="user_account_select" multiple>'+responseAccount+'</select></td><td style="text-align: center;"><img src="images/'+imageName+'" height="15"></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs user_info_edit" id=edit_user_'+responseObj.data[i]['id']+' onclick="discountDbClass.viewuserInfo('+responseObj.data[i].id+',1)">Edit</a><a class="btn btn-xs user_info_delete" id=delete_user_'+responseObj.data[i]['id']+' onclick="discountDbClass.deleteUser('+responseObj.data[i].id+',1)">Delete</a> </div></td></tr>';
				} 

			}
			else{
				responseHTML+='<tr><td colspan="7">Data not found</td></tr>';
			}
                    
		    $('#user_account_list').html(responseHTML)
		    setTimeout(function(){
			$(".user_account_select").multiselect('destroy');
				$('.user_account_select').multiselect({
                   maxHeight: 200,
                   buttonWidth: '155px',
                   includeSelectAllOption: true
                  }); 

			},800);
       
        });
        $('#refresh_overlay').css("display","none");
  	},

  	/*---------------------------------------------
       Function Name: viewuserInfo()
       Input Parameter: rowId
       return:json data
    ---------------------------------------------*/ 
	viewuserInfo:function(rowId){
		localStorage.setItem('view_id',rowId);
	    window.location.href="addaccount.html";
	},

	/*---------------------------------------------
       Function Name: viewuserDetails()
       Input Parameter: rowId
       return:json data
    ---------------------------------------------*/ 
	viewUserDetails:function(rowId){
	     
	    $('#refresh_overlay').css("display","block");
	    $('#add_account').html("Update");
	    $('#add_account').attr("seq",rowId);
	    $('#back_button').css("display","block");
	   
	    var viewuserInfo = new FormData();
			viewuserInfo.append("action","viewuserInfo");
		    viewuserInfo.append("view_Id",rowId);

			$.ajax({
				url: discountDbClass._Serverpath,
				type: 'POST',
				processData:false,
				contentType:false,
				data: viewuserInfo
		    }).done(function(result){
	                var responseObj=result;
					var responseHTML=' ';
					var account_type_array='';
					var account_type='';
				    if(typeof(result)=="string"){
					   responseObj=JSON.parse(result);
					}

	    		    if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
						responseObj.data!=""){
	                      	account_type = responseObj.data[0].ac_type;
							account_type_array=account_type.split(',');
						    localStorage.setItem('account_id',responseObj.data[0].account_id);
		                    $("#prefix").val(responseObj.data[0].prefix);
		                    $("#first_name").val(responseObj.data[0].f_name);
		                    $("#last_name").val(responseObj.data[0].l_name);
		                    $("#email_address").val(responseObj.data[0].eml_address);
		                    $("#office_country_code").val(responseObj.data[0].ofc_coun_code);
		                    $("#office_phone").val(responseObj.data[0].ofc_number);
		                    $("#home_country_code").val(responseObj.data[0].home_count_code);
		                    $("#home_phone").val(responseObj.data[0].hm_number);
		                    $("#cellular_country_code").val(responseObj.data[0].cerl_count_code);
		                    $("#cellular_phone").val(responseObj.data[0].cerl_number);
		                    $("#fax_number").val(responseObj.data[0].fax_number);
		                    $("#user_password").val(responseObj.data[0].user_password);
		                    $("#confirm_password").val(responseObj.data[0].confirm_password);
		                    $("#company_name").val(responseObj.data[0].c_name);
		                    $("#department_name").val(responseObj.data[0].department);
		                    $("#job_title").val(responseObj.data[0].job_title);
		                    $("#primery_Address").val(responseObj.data[0].primry_address);
		                    $("#country_name").val(responseObj.data[0].country_name);
		                    $("#state_name").val(responseObj.data[0].state);
		                    $("#city_name").val(responseObj.data[0].city);
		                    $("#zip_code").val(responseObj.data[0].zip_code);
		                    $("#ac_priority").val(responseObj.data[0].ac_priority);
		                    $("#account_status").val(responseObj.data[0].ac_status);
		                    $("#web_access").val(responseObj.data[0].web_access);
		                    $("#acount_type").val(account_type_array);
		                    $("#email_address").prop("readonly", true);
		                    $("#user_password").prop("readonly", true);
		                    $("#confirm_password").prop("readonly", true);
		                    $("#acount_type").multiselect('destroy');
							$('#acount_type').multiselect({
								maxHeight: 200,
								buttonWidth: '155px',
								includeSelectAllOption: true
							});	           
				     	}	
	        
		     	});	           
	    	$('#refresh_overlay').css("display","none");
	},

   	/*---------------------------------------------
       Function Name: deleteUser()
       Input Parameter: rowId
       return:json data
    ---------------------------------------------*/ 
	deleteUser:function(rowId){
		$('#refresh_overlay').css("display","block");
	    var deleteUserInfo = new FormData();
		deleteUserInfo.append("action","deleteUserInfo");
		deleteUserInfo.append("delete_id",rowId);
		$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: deleteUserInfo
	    }).done(function(result){
		        var responseObj=result;
				if(typeof(result)=="string"){
					responseObj=JSON.parse(result);
				}
				if(responseObj.code==1010){
					alert('Deleted Successfully from intermediate system.Please sync. LimoAnyWhere backoffice.');		
	                discountDbClass.getuserAccounList();  
	            }
		    });
	    
	    $('#refresh_overlay').css("display","none");
	}
}


/*click on back button*/ 

$('#back_button').on("click",function(){
	window.location.href="accoutnlist.html";
});

/*add user on submit form start*/
$('#addAccount_form').on("submit",function(event){
	$('#refresh_overlay').css("display","block");
	
    event.preventDefault();
    var row_id = $('#add_account').attr('seq');
    var account_id=window.localStorage.getItem('account_id');
    if(row_id){
        if($("#user_password").val()==$("#confirm_password").val()){
            var edit_row_id = new FormData($('#addAccount_form')[0]);
	        edit_row_id.append("action","updateUserAccount");
	        edit_row_id.append('edit_row_id',row_id);
	        edit_row_id.append('account_id',account_id);
            var selectAccount = $('#acount_type option:selected');
            var selectAccountType = [];
            $(selectAccount).each(function(index, selectAccount){
				selectAccountType .push($(this).val());
            });

       	edit_row_id.append("acount_type",selectAccountType);
        $.ajax({
	      url: discountDbClass._Serverpath,
	      type: 'POST',
	      processData:false,
	      contentType:false,
	      data: edit_row_id
        }).done(function(result){
				if(result.code!=1007){
		           	alert("Successfully Updated");
		           	$('#addAccount_form')[0].reset();
		           	$("#acount_type").multiselect('destroy');
		           	$('#acount_type').multiselect({
		                maxHeight: 200,
		                buttonWidth: '155px',
		                includeSelectAllOption: true
		           	}); 
				}
				else{
					alert(result.data.ResponseText);
				}
							 
			    window.location.href="accountList.html";
	            $('#refresh_overlay').css("display","none");
			});
		}
		else{
			alert('password do not match');
            $('#refresh_overlay').css("display","none");
		}      
    }
    else{
		var getUserId=window.localStorage.getItem('companyInfo');
	    var get_api_key = window.localStorage.getItem('apiInformation');
		if(typeof(getUserId)=="string"){	
			getUserId=JSON.parse(getUserId);
		}

		if(typeof(get_api_key)=="string"){
	        get_api_key=JSON.parse(get_api_key);
	    }
        var office_phone = $('#office_phone').intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
        var home_phone = $('#home_phone').intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
        var cellular_phone = $('#cellular_phone').intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
        if(cellular_phone == ''){
        	alert('please provide cellular phone');
        	return true;
		}


			    
		var fd = new FormData($('#addAccount_form')[0]);
		fd.append("action","addAccount");
		fd.append("user_id",getUserId[0].id);
		fd.append("office_phone", office_phone);
		fd.append("home_phone", home_phone);
		fd.append("cellular_phone", cellular_phone);
		fd.append("limo_any_where_api_key",get_api_key.limo_any_where_api_key);
	    var selectAccount = $('#acount_type option:selected');
		var selectAccountType = [];
		$(selectAccount).each(function(index, selectAccount){
			selectAccountType .push($(this).val());
		});

	    fd.append("acount_type",selectAccountType);
			
        $.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
            dataType:'json',
			data: fd
	    }).done(function(result){
            if(result.code!=1007){
	            alert("Successfully Added");
	           	$('#addAccount_form')[0].reset();
	      	 	$("#acount_type").multiselect('destroy');
	           	$('#acount_type').multiselect({
	                maxHeight: 200,
	                buttonWidth: '155px',
	                includeSelectAllOption: true
	            }); 
            }
            else{
            	alert(result.data.ResponseText);		   
            }
            window.location.href="accountList.html";
            $('#refresh_overlay').css("display","none");
		});
	}      
});


