
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Airport
  Author: Mylimoproject
---------------------------------------------*/
/*declare a class for the page*/
var getPointToPointClass={
    
    /*set php url for the webservice call*/ 
	_Serverpath:"phpfile/rate_point_client.php",
	_SERVICEPATHCAR:"phpfile/service.php",
   
    /*---------------------------------------------
       Function Name: peakHourRate()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
		peakHourRate:function(){
			var getCompanyInfo=localStorage.getItem("companyInfo");
			if(typeof(getCompanyInfo)=="string")
			{
				getCompanyInfo=JSON.parse(getCompanyInfo);
			}
			var getJson={"action":"getCommanpeakHourRate","user_id":getCompanyInfo[0].id};
			$.ajax({
			    url: getPointToPointClass._Serverpath,
			    type: 'POST',
			    dataType:'json',
			    data: getJson,
		    	success: function(response) {
			      	var responseHTML='<option value="">Select</option>';
				    if(response.code==1007)
				    {
						$.each(response.data,function(index,result){
							responseHTML+='<option value='+result.id+'>'+result.peak_hour_name+'</option>';	
						});
				    }
				    else
				   {
						responseHTML+='<option value="">Data Not Found</option>';	
				   }
				   $('#pickHrsDatabase').html(responseHTML);

			    }

		    });

		},

	/*---------------------------------------------
       Function Name: deletePointToPointVehicleRate()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/	  
	deletePointToPointVehicleRate:function(JsonData)
	{
		$.ajax({
		   url: getPointToPointClass._Serverpath,
		   type: 'POST',
		   dataType:'json',
		   data: JsonData,
		   success: function(response) {
			    getPointToPointClass.getPointToPointRetSetUp();

		    }
	    });
	},
    
    /*---------------------------------------------
       Function Name: getCarFromLimoAnyWhere()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
	getCarFromLimoAnyWhere:function(){
      
      var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	   if(typeof(getLocalStoragevalue)=="string")
	   {
	   	  getLocalStoragevalue=JSON.parse(getLocalStoragevalue);

	   }

       var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

		var getUserInformationJSon={
			"action":"GetVehicleTypes",
			"limoApiID":getLocalStoragevalue.limo_any_where_api_id,
			"limoApiKey":getLocalStoragevalue.limo_any_where_api_key,
			"user_id":getuserId[0].id
		};

		$.ajax({
			url: getPointToPointClass._SERVICEPATHCAR,
			type: 'POST',
			dataType:'json',
			data: getUserInformationJSon,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
				{
				 	if(i==0)
				 	{
				 		responseHTML +='<tr><td><input type="checkbox" class="add_rate_check" id="checked_by_id_'+i+'"  name="vehicle" value="'+i+'"></td><td class="rate_vehicle_code_box_'+i+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td> <td><input placeholder="$" class="checkvalidation rate_input_box_'+i+' "  type="number" min="1" disabled style="width:87%;"></td><td><input type="number" style="width:67%" min="1" class=" checkvalidation toleAmountCheck'+i+'" disabled ></td><td><input type="checkbox" class="tollAmount'+i+' checkAllInputBox" seq="'+i+'"  ></td></tr>';
				 	}
				 	else
				 	{
				 		responseHTML +='<tr><td><input type="checkbox" class="add_rate_check" id="checked_by_id_'+i+'"  name="vehicle" value="'+i+'"></td><td class="rate_vehicle_code_box_'+i+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td> <td><input placeholder="$" class=" checkvalidation rate_input_box_'+i+' "  min="1" type="number" disabled style="width:87%;"></td><td><input type="number" style="width:67%" min="1" class=" checkvalidation toleAmountCheck'+i+'" disabled></td></tr>';	
				 	}

				}
				$('#vehicl_rate_name').html(responseHTML);
				$('.checkAllInputBox').on("change",function(){
			    var getSeq=$(this).attr("seq");
			    var getSmallValue=0;
			    if( $(this).is(':checked') )
					{
						while($('.checkvalidation ').hasClass('toleAmountCheck'+getSeq))
						{
							if($('.toleAmountCheck'+getSeq).attr('disabled')==undefined)
							{
						
								if($('.toleAmountCheck'+getSeq).val()!='')	
								{
									getSmallValue=$('.toleAmountCheck'+getSeq).val();

									var i=0;
									while($('.checkvalidation ').hasClass('toleAmountCheck'+i))
									{	
									
										if($('.toleAmountCheck'+i).attr('disabled')==undefined)
									    {
						
											$('.toleAmountCheck'+i).val(getSmallValue);
										}
									    i++;	

									}
									break;
								}
							}
							
							getSeq++;

						}

					}
					else
					{
						var elsegetSeq=0;
						while($('.checkvalidation ').hasClass('toleAmountCheck'+elsegetSeq))
						{
								$('.toleAmountCheck'+elsegetSeq).val('');
								elsegetSeq++;
						}

					}
			
				});


				$('.checkvalidation').on("keyup",function(){

					var getValue=$(this).val();
					if(getValue<1 && getValue!='')
					{
						$(this).val(1);
					}

				});

				$('.add_rate_check').on("change",function()
				{
					var getseq=$(this).attr("value");
					if($(this).is(":checked"))
					{	
						$('.rate_input_box_'+getseq).removeAttr("disabled");
						$('.toleAmountCheck'+getseq).removeAttr("disabled");
					}
					else
					{
						$('.rate_input_box_'+getseq).val("");
						$('.toleAmountCheck'+getseq).val("");
						$('.rate_input_box_'+getseq).attr("disabled","true");	
						$('.toleAmountCheck'+getseq).attr("disabled","true");
				    }

				});
			
			}

		});

	},

    /*---------------------------------------------
       Function Name: savePointToPointVehicleRate()
       Input Parameter:seq
       return:json data
    ---------------------------------------------*/
	savePointToPointVehicleRate:function(seq)
	{
		$.ajax({
			url: getPointToPointClass._Serverpath,
			type: 'POST',
			data:seq,
			dataType:'json',
			success: function(response) {
				if(response.code==1007)
				{
				    var responseHTML='';
					$.each(response.data,function(index,result){
						responseHTML+='<tr><td>'+result.vehicle_id+'</td><td>'+result.amount+'</td><td>'+result.toll_amt+'</td></tr>';

					});
				}
				else
				{
				    responseHTML="<tr><td calspan=5>Data Not Found</td</tr>";
				}
				$('#view_vehicl_rate_name').html(responseHTML);
				$('#view_rate_vehicle_modal').show();
				$("#refresh_overlay").css('display','none');
	
			}
		});
	},
	/*---------------------------------------------
       Function Name: savePointToPointRate()
       Input Parameter:jsonData
       return:json data
    ---------------------------------------------*/
	savePointToPointRate:function(jsonData)
	{

    	$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data:jsonData,
		    dataType:'json',
		    success: function(response) {
			    getPointToPointClass.getPointToPointRetSetUp();
			    localStorage.removeItem('rateSetupValuePoint');
		        $('#pick_name').val('');
			    $('#drop_name').val('');
			    $('#getIncrementValue').val('');
			    $('#pickHrsDatabase').val('');
			    $('#getpointName').val('');
			    getPointToPointClass.getCarFromLimoAnyWhere();
		    }	
	    });
	},

	/*---------------------------------------------
       Function Name: showPointToPointVehicleRate()
       Input Parameter:getJson
       return:json data
    ---------------------------------------------*/
	showPointToPointVehicleRate:function(getJson)
	{
	    var getUserId=window.localStorage.getItem('companyInfo');
		$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data: getJson,
		    success: function(response) {
			    if(typeof(response)=="string")
			    {
				    response=JSON.parse(response);
			    }
				$('#pick_name').val(response.data['singleData'][0]['pickup_zone_id']);
				$('#drop_name').val(response.data['singleData'][0]['drop_off_zone']);
				$('#pickHrsDatabase').val(response.data['singleData'][0]['peak_hour_db']);
				$('#getIncrementValue').val(response.data['singleData'][0]['peak_increase_rate']);
	            $('#getIncrementValue').keyup();
				$('#getpointName').val(response.data['singleData'][0]['point_to_name']);
				var currency_type;
				if(response.data['singleData'][0]['currency_type']=="per")
				{
					currency_type="%";
				}
				else
				{
					currency_type="$";
				}
			
			    $('#currencyTypePoint').val(response.data['singleData'][0]['currency_type']);
			    $('#back_button').css({"display":"inline-block"});
			    responseHTML='';
				var getArray=[];
				var i=0;
				while($('.rate_vehicle_code_box_'+i).html()!=undefined)
				{	
					getArray.push({"number":i,"vehicle_code":$('.rate_vehicle_code_box_'+i).html()});
					i++;
					$( '#checked_by_id_'+i ).prop( "checked", false );
					$('.rate_input_box_'+i).attr("disabled","true").val('');	
					$('.tollAmount'+i).attr("disabled","true").val('');	
					$('.toleAmountCheck'+i).attr("disabled","true").val('');	
				}
				var getInc=[];
				var getJson1=[];
				$.each(response.data['secondData'],function(index,result){
					for(var j=0; j<getArray.length; j++)
					{
						if(getArray[j]['vehicle_code']==result.vehicle_id)
						{
							getInc[j]=getArray[j]['number'];	
						    $( '#checked_by_id_'+getArray[j]['number'] ).prop( "checked", true );
							$('.rate_input_box_'+getArray[j]['number']).val(result.amount).removeAttr("disabled");
							$('.toleAmountCheck'+getArray[j]['number']).val(result.toll_amt).removeAttr("disabled");
							$('.tollAmount'+getArray[j]['number']).removeAttr("disabled");
						    getJson1.push({"vehicle_rate":result.amount,"vehicle_code":result.vehicle_id,"tollRate":result.toll_amt});
						
				    	}

					}	

				});
				window.localStorage.setItem("rateSetupValuePoint",JSON.stringify(getJson1));
				$("#refresh_overlay").css('display','none');

		    }

	    });
	},

    /*---------------------------------------------
       Function Name: click on add button of passenger rate
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
 	getPointToPointRetSetUp:function()
	{
		$("#refresh_overlay").css('display','block');
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string")
		{
			getUserId=JSON.parse(getUserId);
		}

		$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data: "action=getPointToPointRetSetUp&user_id="+getUserId[0].id,
		    success: function(response) {
			    var responseObj='';
    			if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				var responseHTML='';
				if(responseObj.code==1007){
					$.each(responseObj.data,function(index,result){
						responseHTML+='<tr><td>'+result.point_to_name+'</td><td>'+result.pickup_zone_id+'</td><td>'+result.drop_off_zone+'</td><td>'+result.peak_increase_rate+'</td><td>'+result.peak_hour_db+'</td><td><button class="btn btn-primary viewRate" seq="'+result.id+'">view</button></td><td><button class="btn btn-primary editRateSetup" seq="'+result.id+'" >Edit</button></td><td><button class="btn btn-primary deleteRateSetup" seq="'+result.id+'">Delete</button></td></tr>';
					});
                 }else{
                    responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
                 }
				$('#rate_matrix_list').html(responseHTML);
				$("#refresh_overlay").css('display','none');
				/*click on edit rate button*/
				$('.editRateSetup').on("click",function(){
					$("#refresh_overlay").css('display','block');
					var getseq=$(this).attr("seq");
					var getJson={"getSeq":getseq,"action":"showPointToPointVehicleRate"};
					$('#save_rate').html("Update");
					$('#add_vehicle_rate').html("Update Rate");		
					$('#save_rate').attr("seq",getseq);
					getPointToPointClass.showPointToPointVehicleRate(getJson);
				});
                /*click on delete rate button*/
				$('.deleteRateSetup').on("click",function(){
					var r = confirm("Are you sure you want to delete this item?");
					if(r==true)
					{
						var getseq=$(this).attr("seq");
						var getJson={"getSeq":getseq,"action":"deletePointToPointVehicleRate"};
							getPointToPointClass.deletePointToPointVehicleRate(getJson);
					}

			    });

                /*click on view rate button*/
			    $('.viewRate').on("click",function(){
				    $("#refresh_overlay").css('display','block');
					var getSeq=$(this).attr("seq");
					var getJson={"getSeq":getSeq,"action":"savePointToPointVehicleRate"};
					getPointToPointClass.savePointToPointVehicleRate(getJson);
						
				});

		    }
	    });
	},

    /*---------------------------------------------
       Function Name: updatePointToPointRate();
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/
	updatePointToPointRate:function(getJsonData)
	{
		$.ajax({
		   url: getPointToPointClass._Serverpath,
		   type: 'POST',
		   data: getJsonData,
		   success: function(response) {
				$('#pick_name').val('');
				$('#drop_name').val('');
				$('#getIncrementValue').val('');
				$('#pickHrsDatabase').val('');
				$('#getpointName').val('');
				$('#save_rate').html("save");
				$('#back_button').css("display","none");
				$('#add_vehicle_rate').html('<i class="glyphicon glyphicon-plus"></i>&nbsp; Add Rates');
			    getPointToPointClass.getCarFromLimoAnyWhere();
			    getPointToPointClass.getPointToPointRetSetUp();
		    }

	    }); 
	},

	/*---------------------------------------------
     Function Name: updatePointToPointRate();
     Input Parameter:user_id
     return:json data
    ---------------------------------------------*/
	getPointToPointZone:function()
	{
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string")
		{
			getUserId=JSON.parse(getUserId);
		}
		var availableTags = [];
  		$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data: "action=getPointToPointZone&user_id="+getUserId[0].id,
		    success: function(response) {
				var responseOBJ='';
			    if(typeof(response)=="string")
			    {
				   responseOBJ=JSON.parse(response);
			    }
                var getHtml='<option>Select Zone </option>';
			    if(responseOBJ.code==1007)
			    {
				    $.each(responseOBJ.data,function(index,result){
						if(result.type=="seaport")
						{
                            availableTags.push(result.type_name)
                            getHtml+='<option value="'+result.type_name+'">'+result.type_name+'</option>';
							// availableTags.push(result.save_as);
							// getHtml+='<option value="'+result.save_as+'">'+result.save_as+'</option>';
						}	
						else
						{	
							availableTags.push(result.type_name)
							getHtml+='<option value="'+result.type_name+'">'+result.type_name+'</option>';

   					    }
			
					});
					$('#drop_name').html(getHtml);
					$('#pick_name').html(getHtml);
					$( "#pick_name,#drop_name" ).autocomplete({
	    				  source: availableTags
	    			});
				
			    }
		   	}
	    });
	}
}

   /*---------------------------------------------
     Function Name: click on rate set button;
     Input Parameter:all form value
     return:json data
    ---------------------------------------------*/
	$('#rateSetupForm').on("submit",function(eve){

		eve.preventDefault();
		var getLocalStorgavalue=window.localStorage.getItem('rateSetupValuePoint');
		if(typeof(getLocalStorgavalue)=="string")
		{	
			if($('#save_rate').html()!='Update')
			{
				var getInfo=window.localStorage.getItem("rateSetupValuePoint");
					getInfo=JSON.parse(getInfo);
				var pick_name=$('#pick_name').val();
				var drop_name=$('#drop_name').val();
				var getIncrementValue=$('#getIncrementValue').val();
				var getIncrementValue=$('#getIncrementValue').val();
				var pickHrsDatabase=$('#pickHrsDatabase').val();
				var getpointName=$('#getpointName').val();
				var currencyTypePoint=$('#currencyTypePoint').val();
		        var getUserId=window.localStorage.getItem('companyInfo');
		            getUserId=JSON.parse(getUserId);
				var  getJson={
						"pick_name":pick_name,
						"drop_name":drop_name,
						"getIncrementValue":getIncrementValue,
						"pickHrsDatabase":pickHrsDatabase,
						"getpointName":getpointName,
						"vehicle_rate":getInfo,
						"user_id":getUserId[0].id,
						"currencyTypePoint":currencyTypePoint,
						"action":"savePointToPointRate"
					};
				getPointToPointClass.savePointToPointRate(getJson);
			}
			else
			{
				var getInfo=window.localStorage.getItem("rateSetupValuePoint");
					getInfo=JSON.parse(getInfo);
				var getuserId=window.localStorage.getItem("companyInfo");
					getuserId=JSON.parse(getuserId);
				var getSeq=$('#save_rate').attr("seq");
				var pick_name=$('#pick_name option:selected').text();
				var drop_name=$('#drop_name option:selected').text();
				var getIncrementValue=$('#getIncrementValue').val();
				var getIncrementValue=$('#getIncrementValue').val();
				var pickHrsDatabase=$('#pickHrsDatabase').val();
				var getpointName=$('#getpointName').val();
				var currencyTypePoint=$('#currencyTypePoint').val();
			    var  getJson={
					"pick_name":pick_name,
					"drop_name":drop_name,
					"getIncrementValue":getIncrementValue,
					"pickHrsDatabase":pickHrsDatabase,
					"getpointName":getpointName,
					"vehicle_rate":getInfo,
					"action":"updatePointToPointRate",
					"getSeq":getSeq,
					"user_id":getuserId[0].id,
					"currencyTypePoint":currencyTypePoint
					};
				getPointToPointClass.updatePointToPointRate(getJson);
			}
		}
		else
		{
			alert("Set Rate")
		}

	});

    /*---------------------------------------------
     Function Name: click on rate set button;
     Input Parameter:all form value
     return:json data
    ---------------------------------------------*/
	$('#selected_vehicle_rat_btn').on("click",function(){
		var getVecleValue=[];
		var i=0;
		var checkFullArrayFlag='enable';
		$('.add_rate_check').each(function(index,selectedValue){
			var getSeq=$(this).attr("value");
			if($(this).is(":checked"))
			{	
				if($('.rate_input_box_'+getSeq).val()!="")
				{						
					getVecleValue.push({"vehicle_rate":$('.rate_input_box_'+getSeq).val(),
									     "vehicle_code":$('.rate_vehicle_code_box_'+getSeq).html(),
									     "tollRate":$('.toleAmountCheck'+getSeq).val(),
									   });
				
						i++;
				}
				else
				{
					checkFullArrayFlag="desable";
					alert("Enter Rate For All Selected Vehicle");
				}
				
			}

		});
   
        /*---------------------------------------------
          Function Name: click on rate set button;
          Input Parameter:all form value
          return:json data
        ---------------------------------------------*/ 
		if(getVecleValue.length>0 && checkFullArrayFlag=="enable")
		  {
			  window.localStorage.setItem("rateSetupValuePoint",JSON.stringify(getVecleValue));
			  $('#add_rate_modal').hide();
		  }

	    });

        /*call the function on load of the page*/
        getPointToPointClass.getPointToPointZone();
        getPointToPointClass.getCarFromLimoAnyWhere();
        getPointToPointClass.getPointToPointRetSetUp();
        getPointToPointClass.peakHourRate();





