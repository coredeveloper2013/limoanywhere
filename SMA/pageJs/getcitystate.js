/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: get city state page
    Author: Mylimoproject
---------------------------------------------*/
/*set the web service path using php file*/
var _SERVICEPATHServer = "phpfile/client.php";
var _SERVICEPATH = "phpfile/sma_client.php";
GetCountryList();
/*call the function on page load*/
citiesAllClickBtn();

/*---------------------------------------------
  Function Name: GetCountryList()
  Input Parameter:user_id
  return:json data
---------------------------------------------*/
function GetCountryList() {
    var getLocalStoragevalue = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);
    }
    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=getSmaCountry&user_id=" + getLocalStoragevalue[0].id,
        success: function (response) {
            var responseHTML = '';
            var responseOption = "<option value=''>Select Country</option>";
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            $.each(responseObj.data, function (index, dataUse) {
                responseOption += '<option value="' + dataUse.id + '">' + dataUse.country_name + '</option>';
            });

            $('#country_list_name').html(responseOption);
            $('#country_list_name').on('change', function () {
                var getCountries_id = $('#country_list_name :selected').val();
                localStorage.setItem('country_id', getCountries_id);
                getStateOnOptionButton(getCountries_id);
            })

        }
    });

}

/*---------------------------------------------
    Function Name: getStateOnOptionButton()
    Input Parameter:user_id
    return:json data
  ---------------------------------------------*/
function getStateOnOptionButton(selected_country_id, state_name) {
    var getLocalStoragevalueUserInformation = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalueUserInformation) == "string") {
        getLocalStoragevalueUserInformation = JSON.parse(getLocalStoragevalueUserInformation);
    }
    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
    var getLocalStoragevalue = window.localStorage.getItem("apiInformation");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);

    }

    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=getSmaStateAll&country_id=" + selected_country_id + "&user_id=" + getLocalStoragevalueUserInformation[0].id,
        success: function (response) {
            var responseHTML = "<option value=''>Select State</option>";
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            for (var i = 0; i < responseObj.data.length; i++) {
                responseHTML += '<option  value="' + responseObj.data[i].state_name + '" seq=' + responseObj.data[i].id + '>' + responseObj.data[i].state_name + '</option>';
            }
            $('#select_cities').html(responseHTML);
            $('#select_cities').val(state_name);
            $('#select_cities').on('change', function () {
                var state_id = $('#select_cities :selected').attr('seq');
                var country_id = localStorage.getItem('country_id');
                getCounty(country_id, state_id);

            })
        }
    });
}

/*get list of county according to the country id and state id*/

/*---------------------------------------------
    Function Name: getCounty()
    Input Parameter:country_id,state_id,county_id
    return:json data
  ---------------------------------------------*/
function getCounty(country_id, state_id, county_id) {

    var getLocalStoragevalueUserInformation = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalueUserInformation) == "string") {
        getLocalStoragevalueUserInformation = JSON.parse(getLocalStoragevalueUserInformation);
    }
    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
    var getLocalStoragevalue = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);
    }

    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=getCouontyList&country_id=" + country_id + "&state_id=" + state_id + "&user_id=" + getLocalStoragevalue[0].id,
        success: function (response) {
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            var responseHTML = "<option value=''>Select County</option>";
            $.each(responseObj.data, function (index, dataUse) {
                if (dataUse.id != null) {
                    responseHTML += '<option value="' + dataUse.id + '">' + dataUse.county_name + '</option>';
                }
            });

            $('#county_list').html(responseHTML);
            $('#county_list').val(county_id);

        }
    });
}

/*---------------------------------------------
    Function Name: citiesAllClickBtn()
    Input Parameter:
    return:json data
  ---------------------------------------------*/
function citiesAllClickBtn() {
    $('#save_add_city_data').on("click", function () {
        if ($('#save_add_city_data').html() == "UPDATE") {
            var update_city_id = $(this).attr("seq");
            var cityName = $('#cityName').val();
            var lat_longName = $('#Lat_name').val() + ',' + $('#long_name').val();
            var state_name = $('#select_cities').val();
            var country_Id = $('#country_list_name').find(':selected').val();
            var state_id = $('#select_cities').find(':selected').attr('seq');
            var county_id = $('#county_list').find(':selected').val();
            var zipcode = $('#zipcode').val();
            updateCitiesState(update_city_id, cityName, lat_longName, state_name, country_Id, state_id, county_id, zipcode);
            $('#add_city_popup').hide();

        }
        else {

            var sequence = $(this).attr("seq");
            var cityName = $('#cityName').val();
            var lat_longName = $('#Lat_name').val() + ',' + $('#long_name').val();
            var state_name = $('#select_cities').val();
            var country_Id = $('#country_list_name').find(':selected').val();
            var state_id = $('#select_cities').find(':selected').attr('seq');
            var county_id = $('#county_list').find(':selected').val();
            var zipcode = $('#zipcode').val();
            setCitiesState(cityName, lat_longName, state_name, country_Id, state_id, county_id, zipcode);
            $('#add_city_popup').hide();
        }

    });

    $('.popup_edit_city').on("click", function () {
        $('#cityName').val("");
        $('#cityAbbr').val("");
        $('#zipcode').val("");
        $('#country_list_name').val('');
        $('#select_cities').val('');
        $('#county_list').val('');
        $('#save_add_city_data').html("SAVE");
        $('#add_city_popup').show();

    })

    $('#popup_close_addrate').on("click", function () {
        $('#add_city_popup').hide();
    })

}

/*-------------------------------------------------------------
   Function Name: updateCitiesState()
   Input Parameter:update_city_id,cityName,
                   lat_longName,state_name,country_Id,
                   state_id,county_id,zipcode
   return:json data
---------------------------------------------------------------*/
function updateCitiesState(update_city_id, cityName, lat_longName, state_name, country_Id, state_id, county_id, zipcode) {
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'post',
        data: 'action=updateCitiesState&rowId=' + update_city_id + '&cityName=' + cityName + '&stateName=' + state_name + '&stateCode=' + state_id + '&county_id=' + county_id + '&country_id=' + country_Id + '&zipcode=' + zipcode + '&getLatLang=' + lat_longName,
        success: function (response) {
            var responseHTML = '';
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
                alert('City Updated Successfully');
            }
            getState();
        }
    });

}

/*---------------------------------------------
   Function Name: getState()
   Input Parameter:
   return:json data
 ---------------------------------------------*/
getState(1);
function getStateWithPage(trigger) {
    var trigger = $(trigger);
    var pageNo = trigger.val();
    getState(pageNo);
}
function getStateWithSearch(trigger) {
    var trigger = $(trigger);
    var keyword = trigger.val();
    getState(1, keyword);
}
function getState(pageNo, keyword) {
    if(keyword === undefined || keyword === null){
        keyword = '';
    }
    $('#refresh_overlay').css("display", "block");
    var getUserInfo = window.localStorage.getItem("companyInfo");
    var getUserInfoObj = getUserInfo;
    if (typeof(getUserInfo) == "string") {
        getUserInfoObj = JSON.parse(getUserInfo);
    }

    var paramPost = {
        action : 'getState',
        user_id : getUserInfoObj[0].id,
        pageNo : pageNo,
        keyword : keyword
    };
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'post',
        data: paramPost,
        success: function (response) {
            var responseHTML = '';
            var responseObj = response;
            if (typeof(response) === "string") {
                responseObj = JSON.parse(response);
            }

            if (typeof(response) !== undefined) {

                var totalC = responseObj.data.total;
                var limit = 100;
                var totalPage = totalC / limit;
                totalPage = parseInt(totalPage) + 1;
                var paginatoionC = '';
                for (var pg = 0; pg < totalPage; pg++) {
                    if (parseInt(pageNo-1) === parseInt(pg)) {
                        paginatoionC += '<option value="' + (pg + 1) + '" selected>' + (pg + 1) + '</option>';
                    } else {
                        paginatoionC += '<option value="' + (pg + 1) + '">' + (pg + 1) + '</option>';
                    }
                }
                $('#cityPagination').html(paginatoionC);

                var cities = responseObj.data.data !== undefined ? responseObj.data.data : [];
                for (var i = 0; i < cities.length; i++) {
                    responseHTML += '<tr id="city_' + cities[i].id + '"> ' +
                        '<td  class="indexNumber">' + ((pageNo - 1) * 1000 + i + 1) + '</td> ' +
                        '<td id="cityStateName_' + cities[i].id + '">' + cities[i].country_name + '</td> ' +
                        '<td id="cityAbbr_' + cities[i].id + '">' + cities[i].sma_state + '</td>  ' +
                        '<td id="cityName_' + cities[i].id + '">' + cities[i].city_name + '</td>' +
                        '<td id="zipcode_' + cities[i].id + '">' + cities[i].postal_code + '</td> ' +
                        '<td> <div class="as">' +
                        '<a class="btn btn-xs popup_edit_city_state" country_id="' + cities[i].country_id + '" ' +
                        ' county_id="' + cities[i].county_id + '" state_id="' + cities[i].state_id + '" ' +
                        ' state_name="' + cities[i].sma_state + '" edit_city_id="' + cities[i].id + '">Edit</a> ' +
                        '<a class="btn btn-xs delele_add_rate_row" seq=' + cities[i].id + ' >Delete</a> ' +
                        '</div> ' +
                        '</td> ' +
                        '</tr>';
                }
                if(responseHTML === ''){
                    responseHTML += '<tr><td colspan="4">Data not Found.</td></tr>';
                }
            } else {
                responseHTML += '<tr><td colspan="4">Data not Found.</td></tr>';
            }
            $('#cityState').html(responseHTML);
            $('#refresh_overlay').css("display", "none");

            $('.popup_edit_city_state').on("click", function () {

                var edit_city_id = $(this).attr("edit_city_id");
                var state_id = $(this).attr("state_id");
                var county_id = $(this).attr("county_id");
                var country_id = $(this).attr("country_id");
                var state_name = $(this).attr("state_name");
                $('#save_add_city_data').attr("seq", edit_city_id);
                $('#save_add_city_data').html("UPDATE");
                $('#cityName').val($('#cityName_' + edit_city_id).html());
                $('#cityAbbr').val($('#cityAbbr_' + edit_city_id).html());
                $('#country_list_name').val(country_id);
                $('#zipcode').val($('#zipcode_' + edit_city_id).html());
                getStateOnOptionButton(country_id, state_name);
                getCounty(country_id, state_id, county_id);
                $('#add_city_popup').show();
            });

            $('.delele_add_rate_row').on("click", function () {
                var sequence = $(this).attr("seq");
                var rowId = sequence.split('_');
                deleteCities(rowId)
            });
        }
    });
}

/*---------------------------------------------
   Function Name: setCitiesState()
   Input Parameter:cityName,lat_longName,
                   state_name,country_Id,state_id,
                   county_id,zipcode
   return:json data
 ---------------------------------------------*/
function setCitiesState(cityName, lat_longName, state_name, country_Id, state_id, county_id, zipcode) {
    var getUserInfo = window.localStorage.getItem("companyInfo");
    var getUserInfoObj = getUserInfo;
    if (typeof(getUserInfo) == "string") {
        getUserInfoObj = JSON.parse(getUserInfo);
    }
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'post',
        data: 'action=setCitiesState&user_id=' + getUserInfoObj[0].id + '&cityName=' + cityName + '&lat_longName=' + lat_longName + '&stateName=' + state_name + '&country_id=' + country_Id + '&stateCode=' + state_id + '&county_id=' + county_id + '&zipcode=' + zipcode,
        success: function (response) {
            var responseHTML = '';
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
                alert('City Added Successfully');
            }
            getState();
            $('.popup_edit_city_state').on("click", function () {
                var sequence = $(this).attr("seq");
                var sequence2 = $(this).attr("seq2");
                $('#save_add_city_data').attr("seq", sequence);
                $('#save_add_city_data').html("UPDATE");
                $('#cityName').val($('#cityName_' + sequence).html());
                $('#cityAbbr').val($('#cityAbbr_' + sequence).html())
                $('#zipcode').val($('#zipcode_' + sequence).html())
                $('#select_cities').val(sequence2);
                $('#add_city_popup').show();
            });

            $('.delele_add_rate_row').on("click", function () {
                var sequence = $(this).attr("seq");
                var rowId = sequence.split('_');
                $('#city_' + sequence).remove();
                deleteCities(rowId)
            });

        }
    });

}

/*---------------------------------------------
    Function Name: getState()
    Input Parameter:
    return:json data
---------------------------------------------*/
function deleteCities(rowId) {
    var getUserInfo = window.localStorage.getItem("companyInfo");
    var getUserInfoObj = getUserInfo;
    if (typeof(getUserInfo) == "string") {
        getUserInfoObj = JSON.parse(getUserInfo);
    }
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: 'action=deleteCities&user_id=' + getUserInfoObj[0].id + '&id=' + rowId,
        success: function (response) {
            alert('City Deleted Successfully');
            getState();
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }

        }
    });
}

/*call the getSmaState function on page load*/
//getSmaState();
/*---------------------------------------------
    Function Name: getState()
    Input Parameter:
    return:json data
---------------------------------------------*/
/*function getSmaState()
{
    var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
    if(typeof(getLocalStoragevalueUserInformation)=="string")
    {
           getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
    }
    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
    var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
     if(typeof(getLocalStoragevalue)=="string")
    {
           getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
    }
    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=getStateProvidence&user_id="+getLocalStoragevalue[0].id,
        success: function(response) {
            var ResponseHtml='<option value="">Select</option>';
            var responseObj=response;
            if(typeof(response)=="string")
            {
                responseObj=JSON.parse(response);
            }
            $.each(responseObj.data,function(index,dataUse){

                if(dataUse.id!=null){
                    ResponseHtml+="<option seq='"+dataUse.id+"' value='"+dataUse.state_name+"'>"+dataUse.state_name+"</option>";
                }
            });
            $('#select_cities').html(ResponseHtml);
        }

    });

}

*/



