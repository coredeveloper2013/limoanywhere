<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
Method:             getRateMatrix()
InputParameter:     userId
Return:             get Rate Matrix
*****************************************************************/
	function getRateMatrix()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	  	{
			$query1="SELECT id,name from passenger_rate_matrix where user_id='".$_REQUEST['user_id']."'";
		 	$resource1 = operations($query1);
			for($j=0; $j<count($resource1); $j++)
			{	
				$arr[] = array(
	                "id" => $resource1[$j]['id'],
	                "name" => $resource1[$j]['name']
	            );			
			}  
		   	if(count($arr)>0 && gettype($arr)!="boolean")
		   {
			   $result=global_message(200,1007,$arr);	   
		   }
		   else
		   {
			   $result=global_message(200,1006);
		   }	  
	  	}
	  	else
	  	{
		  $result=global_message(201,1003);
		}
	  	return  $result;
	}

/*****************************************************************
Method:             setPassengerRate()
InputParameter:     vehicle_code,sma_id,user_id,matrix_name
Return:             set Passenger Rate
*****************************************************************/
	function setPassengerRate()
	{	
	 	if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) &&(isset($_REQUEST['matrix_name']) && !empty($_REQUEST['matrix_name'])) && (isset($_REQUEST['add_passengers']) && !empty($_REQUEST['add_passengers']))&&(isset($_REQUEST['add_passengers_rate']) && !empty($_REQUEST['add_passengers_rate'])))
	   	{
	      	$userId=$_REQUEST['user_id'];
		  	$minimum_base_rate=(isset($_REQUEST['minimum_base_rate']) && !empty($_REQUEST['minimum_base_rate']))?$_REQUEST['minimum_base_rate']:0;
		  
	   		$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$addSma=explode(',',$_REQUEST['sma_id']);
		   	$addPerPass=explode(',',$_REQUEST['add_passengers']);

		   	$addPerPassRate=explode(',',$_REQUEST['add_passengers_rate']);
		   	$miles_increase_percent=$_REQUEST['percent_increase'];
		
			$query ="insert into passenger_rate_matrix(name,user_id,miles_increase_percent,peak_hour_id,currency_type,pickup_location,drop_off_location) value('".$_REQUEST['matrix_name']."','".$userId."','".$miles_increase_percent."','".$_REQUEST['pickHrsDatabase']."','".$_REQUEST['currencyType']."','".$_REQUEST['pick_zone_name']."','".$_REQUEST['drop_zone_name']."')";
            $passenger_matrix_id = operations($query);

	  		for($i=0;$i<count($VehicleCode);$i++)
	  		{
		  		$Vehquery="insert into passenger_vehicle(passenger_matrix_id,vehicle_code,user_id) value('".$passenger_matrix_id."','".$VehicleCode[$i]."','".$userId."')";	
		  		$resource1 = operations($Vehquery);
	  		}
		  	for($j=0;$j<count($addSma);$j++)
  			{
				$Smaquery="insert into passenger_sma(passenger_matrix_id,sma_id,user_id) value('".$passenger_matrix_id."','".$addSma[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}
		 
		   	$final_passengr=array();
		   		$toPassenger=0;
	  		for($k=0;$k<count($addPerPass);$k++)
			{
				if($k==0)
				{
					$fromPassenger=0;
					$toPassenger=$addPerPass[$k];
				}
				else
				{
					$fromPassenger=$toPassenger;
					$toPassenger+=$addPerPass[$k];					
				}
		
		   		$Passquery="insert into rate_calculate_passenger(passenger_matrix_id,rate,from_passenger,to_passenger,user_id) value('".$passenger_matrix_id."','".$addPerPassRate[$k]."','".$fromPassenger."','".$toPassenger."','".$userId."')";
                $insertId = operations($Passquery);
			}
		   		$result=global_message(200,1008,$insertId);		   
	   	}
	   	else
	   	{
		    $result=global_message(201,1003);
   		}	
		return $result;	
	}

/*****************************************************************
Method:             getRateMatrixList()
InputParameter:     user_id
Return:             get Rate Matrix List
*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
  		{
			$query="Select * from passenger_rate_matrix where user_id=".$_REQUEST['user_id']." order by name asc";
			$resource= operations($query);
			$contents = array();
		    if(count($resource)>0 && gettype($resource)!="boolean"){
			for($i=0; $i<count($resource); $i++)
			{
				$vehicle_code=''; 
				$sma_name='';
				$sma_id='';
				$Vehquery="Select vehicle_code from passenger_vehicle where passenger_matrix_id=".$resource[$i]['id'];
				$resource1= operations($Vehquery);
				for($j=0; $j<count($resource1); $j++)
					{
						$vehicle_code .=$resource1[$j]['vehicle_code'].',';
					}
				$Smaquery="Select sma_id,sma_name from passenger_sma,sma where sma.id=passenger_sma.sma_id AND passenger_sma.passenger_matrix_id=".$resource[$i]['id'];
				$resource2= operations($Smaquery);
				for($k=0; $k<count($resource2); $k++)
					{
						$sma_name .=$resource2[$k]['sma_name'].', ';
						$sma_id .=$resource2[$k]['sma_id'].',';
					}
				$Minquery="Select rate from rate_calculate_passenger where passenger_matrix_id=".$resource[$i]['id']." AND from_passenger=0";
				$resource3= operations($Minquery);
			
				$contents[$i]['id']=$resource[$i]['id'];
				$contents[$i]['min_fare']= count($resource3) > 0 ? $resource3[0]['rate'] : '';
				$contents[$i]['sma_id'] = $sma_id;
				$contents[$i]['sma_name'] = $sma_name;
				$contents[$i]['vehicle_code']=$vehicle_code;
				$contents[$i]['matrix_name'] = $resource[$i]['name'];
				$contents[$i]['miles_increase_percent'] = $resource[$i]['miles_increase_percent'];
				$contents[$i]['peak_hour_id'] = $resource[$i]['peak_hour_id'];

				$contents[$i]['currency_type'] = $resource[$i]['currency_type'];
				$contents[$i]['pickup_location'] = $resource[$i]['pickup_location'];
				$contents[$i]['drop_off_location'] = $resource[$i]['drop_off_location'];

			}

        	}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
			   	$result=global_message(200,1007,$contents);
				   
		   	}
		   	else
		   	{
		   		$result=global_message(200,1006);
		   	}		  
	  	}
	 	else
	  	{
		  	$result=global_message(201,1003);
	  	}
	 	return  $result;
	}

/*****************************************************************
Method:             editRateMatrixList()
InputParameter:     rate_matrix_id
Return:             edit Rate Matrix List
*****************************************************************/
	function editRateMatrix()
	{
		if((isset($_REQUEST['rate_matrix_id']) && !empty($_REQUEST['rate_matrix_id'])))
	  	{
			$query="Select * from rate_calculate_passenger where passenger_matrix_id=".$_REQUEST['rate_matrix_id'];
			$resource= operations($query);

			$contents = array();

			for($i=0; $i<count($resource); $i++)
			{
				$contents[$i]['passengers']=$resource[$i]['to_passenger']-$resource[$i]['from_passenger'];
				$contents[$i]['from_passenger']=$resource[$i]['from_passenger'];
				$contents[$i]['rate'] = $resource[$i]['rate'];
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
		   		$result=global_message(200,1007,$contents);			   
		   	}
		   	else
		   	{
		   		$result=global_message(200,1006);
		   	}		  
		}
 		else
  		{
	  		$result=global_message(201,1003);
  		}
  		return  $result;	
	}

/*****************************************************************
Method:             deletePassengerRate()
InputParameter:     rate_matrix_id
Return:             delete Rate Matrix List
*****************************************************************/
	function deletePassengerRate()
	{
 		if((isset($_REQUEST['rate_matrix_id']) && !empty($_REQUEST['rate_matrix_id'])))
	   	{
		  	$rowId=$_REQUEST['rate_matrix_id'];		 
			$query="delete from rate_calculate_passenger where passenger_matrix_id='".$rowId."'";
	    	$resource = operations($query);
			$queryDelete="delete  from passenger_rate_matrix where id='".$rowId."'";
			$resource1 = operations($queryDelete);
			$queryDelete1="delete  from passenger_sma where passenger_matrix_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete2="delete  from passenger_vehicle where passenger_matrix_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$result=global_message(200,1010);   
   	   	}
  		else
  		{
  	 		$result=global_message(201,1003);
  		}
		return $result;
	}


/*****************************************************************
Method:             checkUniqueMatrix()
InputParameter:     rate_matrix_id
Return:             check Unique Matrix
*****************************************************************/
	function checkUniqueMatrix()
	{
		$query1="Select id from passenger_rate_matrix where name='".$_REQUEST['matrix_name']."' AND user_id='".$_REQUEST['user_id']."'";
		$resource1 = operations($query1);
        $arr = [];
		for($j=0; $j<count($resource1); $j++)
		{	
			$arr[] = array(
	                "id" => $resource1[$j]['id']
	            );			
		}
		if(count($arr) == 0){
            $arr[] = array(
                'id' => null
            );
        }
        $result=global_message(200,1007,$arr);
  		return  $result;
	}