<?php
include_once 'addVehicle_server.php';

$action = $_REQUEST['action'];

$response=array();

switch ($action) {
	
	case "addVehicleManualy":
		$response=addVehicleManualy();
		echo json_encode($response);
		break;

	case "updateVehicleManualy":
		$response=updateVehicleManualy();
		echo json_encode($response);
		break;

	case "deleteVehicleInformation":
		$response=deleteVehicleInformation();
		echo json_encode($response);
		break;

	case "viewVehicleInformation":
		$response=viewVehicleInformation();
		echo json_encode($response);
		break;

	case "getAirportInformation":
	    $response=getAirportInformation();
	    echo json_encode($response);
	    break;

	case "viewAllVehicleInformation":
		$response=viewAllVehicleInformation();
		echo json_encode($response);
		break;
		
	case "getVehicleInformation":
		$response=getVehicleInformation();
		 echo json_encode($response);
		break;

	case "editClassAirport":
		$response=editClassAirport();
		echo json_encode($response);
		break;

	case "deleteAirportInformation":
		$response=deleteAirportInformation();
		echo json_encode($response);
		break;
		
	case "editAirportInformationShow":
		$response=editAirportInformationShow();
		echo json_encode($response);
		break;

	case "editSeaportInformationShow":
		$response=editSeaportInformationShow();
		echo json_encode($response);
		break;

	case "editClassSeaport":
		$response=editClassSeaport();
		echo json_encode($response);
		break;
		
    case "getSeaportInformation":
	    $response=getSeaportInformation();
	    echo json_encode($response);
	    break;
	
    case "deleteSeaportInformation":
		$response=deleteSeaportInformation();
		echo json_encode($response);
		break;

	case "getSeaportVechileInformation":
	    $response=getSeaportVechileInformation();
	    echo json_encode($response);
	    break;

   	case "getTrainVechileInformation":
	    $response=getTrainVechileInformation();
	    echo json_encode($response);
	    break;

    case "editSeaportInformationShow":
		$response=editSeaportInformationShow();
		echo json_encode($response);
		break;

	case "editTrainInformationShow":
		$response=editTrainInformationShow();
		echo json_encode($response);
		break;

	case "editClassTrain":
		$response=editClassTrain();
		echo json_encode($response);
		break;
		
	case "deleteTrainInformation":
		$response=deleteTrainInformation();
		echo json_encode($response);
		break;
	
}


