<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
    Method:             deleteVehicleInformation()
    InputParameter:     rowId,password,LegalEntity
    Return:             deleted vehicle info
*****************************************************************/
	function deleteVehicleInformation()
	{
		$query2="delete  from vehicle_information where id='".$_REQUEST['rowId']."'";
		$resource = operations($query2);
		$queryImageUrl="delete from vehicle_extra_info  where parent_id='".$resource['rowId']."'";
		$queryImageUrlResult = operations($queryImageUrl);
		$result=global_message(200,1006);
		return $result;

	}



/*****************************************************************
	Method:             setImportDatabaseFIle()
	InputParameter:     name
	Return:             set Import Database FIle
	*****************************************************************/
	function setImportDatabaseFIle()
	{
		$csv_file = "csvFile/".$_FILES["importAirline"]["name"];
		move_uploaded_file($_FILES["importAirline"]["tmp_name"],"csvFile/".$_FILES["importAirline"]["name"]);
		$csvfile = fopen($csv_file,'r');
	  	$theData = fgets($csvfile);
	  	$i = 0;

	  	while (!feof($csvfile)) {  	  	
	  	  	$last_insertid=0;
	  	  	$csv_data[] = fgets($csvfile);
	  	  	$csv_array = explode(",",$csv_data[$i]);
	  	  	//code
			$csv2=$csv_array[0];
			$csv2=mysql_real_escape_string($csv2);
			//Name
			$csv1=$csv_array[1];
			$csv1=mysql_real_escape_string($csv1);
			if(isset($csv1) && !empty($csv1))
	  		{
		  		
				$selectQuery='select count(*) as tatalCount from airlines where name="'.$csv1.'" and airline_code="'.$csv2.'" and user_id="'.
				$_REQUEST['user_id'].'"';
  				$selectQueryResult = operations($selectQuery);
  			
		  		if($selectQueryResult[0]['tatalCount']==0)
  				{	
  					
  					$query='insert into airlines(name,airline_code,user_id) values("'.$csv1.'","'.$csv2.'","'.$_REQUEST["user_id"].'")';
  					operations($query);	

				}
  				// else
  				// {
  				// 	$query='update cruise set cruise_code="'.$csv1.'",name="'.$csv2.'" where cruise_code="'.$csv2.'"';
  				// 	operations($query);	
  				// }
	  		}
	  		$i++;
  		}
		$result=global_message(200,1006);
		return  $result;
 	}

/*****************************************************************
    Method:             viewVehicleInformation()
    InputParameter:     rowId
    Return:             view vehicle info
*****************************************************************/
	function viewVehicleInformation()
	{
		$query2="select * from vehicle_information where id='".$_REQUEST['rowId']."'";
		$resource = operations($query2);
		$queryImageUrl="select img_name from vehicle_extra_info  where parent_id='".$resource[0]['id']."' order by image_seq ASC";
		$queryImageUrlResult = operations($queryImageUrl);
		$resource[0]['imageName']=$queryImageUrlResult;
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1006);

		}
		return $result;

	}

	/*****************************************************************
    Method:             viewVehicleInformation()
    InputParameter:     user_id
    Return:             view vehicle info
	*****************************************************************/

	function viewAllVehicleInformation()
   {
		$query2="select * from vehicle_information where user_id='".$_REQUEST['user_id']."'";
		$resource = operations($query2);
	    
		for($i=0; $i<count($resource); $i++)
		{
			$queryImageUrl="select img_name from vehicle_extra_info  where parent_id='".$resource[$i]['id']."' order by img_name ASC";			
			$queryImageUrlResult = operations($queryImageUrl);
			$resource[$i]['imageName']=$queryImageUrlResult;
		}	
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			$result=global_message(200,1003,$resource);

		}
		else
		{
			$result=global_message(200,1006);

		}
		return $result;	
	}


	/*****************************************************************
    Method:             editAirline()
    InputParameter:     name, airline_code, 
    Return:             edit airline code
	*****************************************************************/

	function editAirline()
	{	
		$query ="update airlines set name='".$_REQUEST['name']."', airline_code='".$_REQUEST['airline_code']."' where id='".$_REQUEST['rowId']."'";
		$resource = operations($query);		
		return $result;
	}





/*****************************************************************
    Method:             addAirlines()
    InputParameter:     rowId
    Return:             edit airport information
	*****************************************************************/
	function addAirlines()
	{

		$query2="insert into airlines(name,airline_code,user_id) values('".$_REQUEST['name']."','".$_REQUEST['airline_code']."','".$_REQUEST['user_id']."')";
	
		$resource = operations($query2);
		
		return $resource;
	}


	/*****************************************************************
    Method:             editAirlineInfoShow()
    InputParameter:     rowId
    Return:             edit airport information
	*****************************************************************/
	function editAirlineInfoShow()
	{

		$query2="select * from airlines where id='".$_REQUEST['rowId']."'";
		$resource = operations($query2);
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			
			$result=global_message(200,1003,$resource);

		}
		else
		{
			$result=global_message(200,1006);

		}
		return $result;
	}

	/*****************************************************************
    Method:             getAirportInformation()
    InputParameter:     
    Return:             get airport information
	*****************************************************************/
	function getAirportInformation()
	{
		$query2="select * from airport_child_table";
		$resource = operations($query2);
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{	
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1003);
		}
	}

	/*****************************************************************
    Method:             deleteAirportInformation()
    InputParameter:     rowId
    Return:             delete airport information
	*****************************************************************/
	function deleteAirline()
	{
		$queryDeletequery="delete from airlines where id='".$_REQUEST['rowId']."'";
		$queryDeleteResult = operations($queryDeletequery);
		return true;
	}

	/*****************************************************************
    Method:             getVehicleInformation()
    InputParameter:     rowId
    Return:             get vehicle information
	*****************************************************************/
	function getAirlinesInformation(){
		$query2="select * from airlines where user_id='".$_REQUEST['user_id']."' ";
		$resource = operations($query2);
		if(count($resource)>=1 && gettype($resource)!='boolean')
		{
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1006);
		}
		return $result;
	}

	/*****************************************************************
    Method:             updateVehicleManualy()
    InputParameter:     vehicle_code, vehicle_passenger_capacity, vehicle_luggage_capacity, 					saveSeq, imageNumber
    Return:             update vehicle manually
	*****************************************************************/
	function updateVehicleManualy()
	{
		$query="update vehicle_information set code='".$_REQUEST['vehicle_code']."',vehicle_type='".$_REQUEST['vehicle_type']."',vehicle_title='".$_REQUEST['vehicle_title']."',passenger_capacity='".$_REQUEST['vehicle_passenger_capacity']."',luggage_capacity='".$_REQUEST['vehicle_luggage_capacity']."' where id='".$_REQUEST['saveSeq']."'";
		$resource = operations($query);
		$query2="delete from vehicle_extra_info  where parent_id='".$_REQUEST['saveSeq']."'";	
		$query2Result = operations($query2);
		$target_dir = "vehicle_image/";
		$imageNumber=json_decode($_REQUEST['imageNumber']);
		$counterImage=0;
		for($i=0; $i<count($_FILES["files2"]["name"]); $i++)
		{
			$target_file = $target_dir . basename($_FILES["files2"]["name"][$i]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$check = @getimagesize($_FILES["files2"]["tmp_name"][$i]);
			if($check !== false){
			        $uploadOk = 1;
			} else 
			{
			   $uploadOk = 0;			 
		    }
		    if($uploadOk==1)
		    {
	    	 	$pic = rand(100,100000)."-".$_FILES['files2']['name'][$i];
    		 	$pic_loc = $_FILES['files2']['tmp_name'][$i];
		 		move_uploaded_file($pic_loc,$target_dir.$pic);
    		 	$query="insert into vehicle_extra_info(img_name,parent_id,image_seq) values('".$pic."','".$_REQUEST['saveSeq']."','".$imageNumber[$counterImage]."')";
		 		$counterImage++;
				$resource = operations($query);
		    }
		}

		$fullExternalImage=json_decode($_REQUEST['externalUrl']);
		$imageNumberUploadImage=json_decode($_REQUEST['imageNumberUpload']);
		for($j=0; $j<count($fullExternalImage); $j++)
		{
			$urlDecoded=urldecode($fullExternalImage[$j]); 			
			$urlDecoded=explode("/",$urlDecoded);
			$content = file_get_contents('vehicle_image/'.$urlDecoded[2]);
			$pic = rand(100,100000);
			$imageFIleName='image_'.$pic.'.jpg';
			file_put_contents('vehicle_image/'.$imageFIleName,$content);
			$query="insert into vehicle_extra_info(img_name,parent_id,image_seq) values('".$imageFIleName."','".$_REQUEST['saveSeq']."','".$imageNumberUploadImage[$j]."')";
			$resource = operations($query);
		}
		$result=global_message(200,1007,$resource);
		return $result;
	}


	/*****************************************************************
    Method:             addVehicleManualy()
    InputParameter:     vehicle_code, vehicle_passenger_capacity, vehicle_luggage_capacity, 					user_id, imageNumber
    Return:             add vehicle manually
	*****************************************************************/
	function addVehicleManualy()
	{	
		$isRecordExist="select * from vehicle_information where code='".$_REQUEST['vehicle_code']."'";
		$isRecordExistResult = operations($isRecordExist);
		if(count($isRecordExistResult)>0 && gettype($isRecordExistResult)!="boolean")
	  	{
	  	  	$result=global_message(200,1003);
	  	}
	  	else
	  	{
	  		$allVehicle=json_decode($_REQUEST['imageNumber']);
			$query="insert into vehicle_information(code,vehicle_type,vehicle_title,passenger_capacity,luggage_capacity,user_id) values('".$_REQUEST['vehicle_code']."','".$_REQUEST['vehicle_type']."','".$_REQUEST['vehicle_title']."','".$_REQUEST['vehicle_passenger_capacity']."','".$_REQUEST['vehicle_luggage_capacity']."','".$_REQUEST['user_id']."')";
            $lastInsertedId = operations($query);
			$target_dir = "vehicle_image/";
			for($i=0; $i<count($_FILES["files2"]["name"]); $i++)
			{
				$target_file = $target_dir . basename($_FILES["files2"]["name"][$i]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$check = @getimagesize($_FILES["files2"]["tmp_name"][$i]);
				if($check !== false) {
			        $uploadOk = 1;
				} 
				else{
			  	 $uploadOk = 0;
			 	}
		     	if($uploadOk==1){
		    	 	$pic = rand(100,100000)."-".$_FILES['files2']['name'][$i];
        		 	$pic_loc = $_FILES['files2']['tmp_name'][$i];
        		 	move_uploaded_file($pic_loc,$target_dir.$pic);
        		 	$query="insert into vehicle_extra_info(img_name,parent_id,image_seq) values('".$pic."','".$lastInsertedId."','".$allVehicle[$i]."')";
					$resource = operations($query);
			    }
			}

			$result=global_message(200,1007,$resource);

	  	}
		return $result;
	}

	/*****************************************************************
    Method:             editSeaportInformationShow()
    InputParameter:     rowId
    Return:             edit Seaport Information Show
	*****************************************************************/
	function editSeaportInformationShow()
	{
		$query2="select * from seaport_child_table where id='".$_REQUEST['rowId']."'";
		$resource = operations($query2);
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			for($i=0; $i<count($resource); $i++)
			{
				
				$queryResultQuery="select * from seaport_master_table where seaport_chield_id='".$_REQUEST['rowId']."' ";
				$queryResultResult = operations($queryResultQuery);	
				$resource[$i]['seaport_code']=$queryResultResult;
			}
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1006);

		}
		return $result;
	}

	/*****************************************************************
    Method:             editClassSeaport()
    InputParameter:     seaport_state_name,seaport_city_zipCode,seaport_country_name,
    					rowId
    Return:             edit Seaport Class
	*****************************************************************/
	function editClassSeaport()
	{
		$query ="update seaport_child_table set state_name='".$_REQUEST['seaport_state_name']."' ,city_name='".$_REQUEST['seaport_city_name']."' ,city_code='".$_REQUEST['seaport_city_zipCode']."',seaport_country='".$_REQUEST['seaport_country_name']."' where id='".$_REQUEST['rowId']."'";
		$resource = operations($query);
		$querySelectquery="select seaport_master_id from  seaport_child_table where id='".$_REQUEST['rowId']."'";		
		$querySelectqueryResult = operations($querySelectquery);
		$queryUpdateMaster="update seaport_master_table set seaport_code='".$_REQUEST['seaport_code']."', seaport_name='".$_REQUEST['seaport_name']."' where id= '".$querySelectqueryResult[0]['seaport_master_id']."'";
		$queryUpdateMasterResult = operations($queryUpdateMaster);
		$result=global_message(200,1003);
		return $result;
	}

	/*****************************************************************
    Method:             getSeaportInformation()
    InputParameter:     
    Return:             get Seaport Information
	*****************************************************************/
	function getSeaportInformation()
	{
		$query2="select * from seaport_child_table ";
		$resource = operations($query2);
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{	
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1003);
		}
	}

	/*****************************************************************
    Method:             deleteSeaportInformation()
    InputParameter:     
    Return:             delete Seaport Information
	*****************************************************************/
	function deleteSeaportInformation()
	{
		$selectchildId= "select seaport_master_id from seaport_child_table where id='".$_REQUEST['rowId']."'";
		$selectedId = operations($selectchildId);
		$queryDeletequery="delete from seaport_child_table where id='".$_REQUEST['rowId']."'";
		$queryDeleteResult = operations($queryDeletequery);

		$queryDeletequery2="delete from seaport_extra_info where parent_id='".$selectedId[0]["seaport_master_id"]."'";
		$queryDeletequery2Result = operations($queryDeletequery2);
		$queryDelete="delete from seaport_master_table where id='".$selectedId[0]["seaport_master_id"]."'";
		$queryDeleteResult = operations($queryDelete);
		$queryDelete="delete from seaport_vehicle where parent_id='".$selectedId[0]["seaport_master_id"]."'";
		$queryDeleteResult = operations($queryDelete);
		$result=global_message(200,1003);
		return $result;
	}


	/*****************************************************************
    Method:             getSeaportVechileInformation()
    InputParameter:     user_id
    Return:             get Seaport Vechile Information
	*****************************************************************/
	function getSeaportVechileInformation()
	{
		$query2="select * from seaport_child_table where user_id='".$_REQUEST['user_id']."'";
		$resource = operations($query2);

		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			for($i=0; $i<count($resource); $i++)
		 	{
			    $queryResultQuery="select * from seaport_master_table where seaport_chield_id='".$resource[$i]['id']."' and user_id='".$_REQUEST['user_id']."'";
				$queryResultResult = operations($queryResultQuery);	
				$resource[$i]['seaport_code']=$queryResultResult;
			}			
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1006);

		}
		return $result;
	}


	/*****************************************************************
    Method:             getTrainVechileInformation()
    InputParameter:     user_id
    Return:             get Train Vechile Information
	*****************************************************************/
	function getTrainVechileInformation()
	{
		$query2="select * from train_child_table where user_id='".$_REQUEST['user_id']."'";
		$resource = operations($query2);

		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			for($i=0; $i<count($resource); $i++)
		 	{
			    $queryResultQuery="select * from train_master_table where train_chield_id='".$resource[$i]['id']."' and user_id='".$_REQUEST['user_id']."' ";
				$queryResultResult = operations($queryResultQuery);	
				$resource[$i]['train_code']=$queryResultResult;
			}		
			$result=global_message(200,1003,$resource);
		}
		else
		{
			$result=global_message(200,1006);

		}
		return $result;
	}

	/*****************************************************************
    Method:             editTrainInformationShow()
    InputParameter:     user_id
    Return:             edit Train InformationShow
	*****************************************************************/
	function editTrainInformationShow(){
		$query2="select * from train_child_table where id='".$_REQUEST['rowId']."'";
		$resource = operations($query2);
		if(count($resource)>=1 && gettype($resource)!= 'boolean')
		{
			for($i=0; $i<count($resource); $i++)
			{
				$queryResultQuery="select * from train_master_table where train_chield_id='".$_REQUEST['rowId']."' ";
				$queryResultResult = operations($queryResultQuery);	
				$resource[$i]['train_master_id']=$queryResultResult;
			}		
			$result=global_message(200,1003,$resource);	
		}
		else
		{
			$result=global_message(200,1006);
		}
		return $result;
	}

	/*****************************************************************
    Method:             editClassTrain()
    InputParameter:     train_state_name, train_city_name, train_city_zipCode,   								train_country, train_country_name, rowId
    Return:             edit Class Train
	*****************************************************************/
	function editClassTrain()
	{		
		$query ="update train_child_table set state_name='".$_REQUEST['train_state_name']."' ,city_name='".$_REQUEST['train_city_name']."' ,city_code='".$_REQUEST['train_city_zipCode']."',train_country='".$_REQUEST['train_country_name']."' where id='".$_REQUEST['rowId']."'";
		$resource = operations($query);
		$querySelectquery="select train_master_id from  train_child_table where id='".$_REQUEST['rowId']."'";
		$querySelectqueryResult = operations($querySelectquery);		
		$queryUpdateMaster="update train_master_table set train_code='".$_REQUEST['train_code']."', train_name='".$_REQUEST['train_name']."' where id= '".$querySelectqueryResult[0]['train_master_id']."'";		
		$queryUpdateMasterResult = operations($queryUpdateMaster);
		$result=global_message(200,1003);
		return $result;
	}

	/*****************************************************************
    Method:             deleteTrainInformation()
    InputParameter:     train_state_name, train_city_name, train_city_zipCode,   								train_country, train_country_name, rowId
    Return:             delete Train Information
	*****************************************************************/
	function deleteTrainInformation()
	{
		$selectchildId= "select train_master_id	 from train_child_table where id='".$_REQUEST['rowId']."'";
		$selectedId = operations($selectchildId);
		$queryDeletequery="delete from train_child_table where id='".$_REQUEST['rowId']."'";
		$queryDeleteResult = operations($queryDeletequery);
		$queryDeletequery2="delete from train_extra_info where parent_id='".$selectedId[0]["seaport_master_id"]."'";
		$queryDeletequery2Result = operations($queryDeletequery2);
		$queryDelete="delete from train_master_table where id='".$selectedId[0]["seaport_master_id"]."'";
		$queryDeleteResult = operations($queryDelete);
		$result=global_message(200,1003);
		return $result;
	}

?>