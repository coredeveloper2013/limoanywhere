<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');
/*****************************************************************
Method:             seo_friendly_url()
InputParameter:    	string
Return:             seo friendly url
*****************************************************************/
	function seo_friendly_url($string)
	{
	    $string = str_replace(array('[\', \']'), '', $string);
	    $string = preg_replace('/\[.*\]/U', '', $string);
	    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
	    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
	    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
	    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
	    return strtolower(trim($string, '-'));
	}	

/*****************************************************************
Method:             setHoliday()
InputParameter:    	string
Return:             set Holiday
*****************************************************************/
	function setHoliday()
	{	
		if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	   	{
	      	$userId=$_REQUEST['user_id'];
		  	$start_time=(isset($_REQUEST['start_time']) && !empty($_REQUEST['start_time']))?$_REQUEST['start_time']:'12:00 AM';
		  	$end_time=(isset($_REQUEST['end_time']) && !empty($_REQUEST['end_time']))?$_REQUEST['end_time']:'11:59 PM';		  
			$VehicleCode=json_decode($_POST['vehicle_code']);
			$addSma=explode(',',$_REQUEST['sma_id']);
			$value=explode(',',$_REQUEST['value']);
			$query2='select * from holidays where name LIKE "%'.$_REQUEST["name"].'%"';
			$resourceResult = operations($query2);
			if(count($resourceResult) > 0)
			{	
				return "sameName";
			}
            if(!isset($_REQUEST["amount"])){
                $_REQUEST["amount"] = 0;
            }
			$query ='insert into holidays(name,amount,calender,start_time,end_time,user_id,allowORES,Cut_off_time,blackout_Msg,holiday_surcg_per_pax,holiday_surcg_type) value("'.$_REQUEST["name"].'","'.$_REQUEST["amount"].'","'.$_REQUEST["calender"].'","'.$start_time.'","'.$end_time.'","'.$userId.'","'.$_REQUEST["allowORES"].'","'.$_REQUEST["Cut_off_time"].'","'.$_REQUEST["blackout_Msg"].'","'.$_REQUEST["holiday_surcg_per_pax"].'","'.$_REQUEST["holiday_surcg_type"].'")';
            $holiday_id = operations($query);
	 		$serviceType=explode(',',$_REQUEST['service_typeObject']);
			for($k=0; $k<count($serviceType); $k++)
		   	{
				$Smaquery="insert into holiday_service_type(parent_id,service_type,user_id) value('".$holiday_id."','".$serviceType[$k]."','".$userId."')";	
			  	$resource2 = operations($Smaquery);
		 	}
			
			for($i=0;$i<count($VehicleCode);$i++)
		  	{
			  	$Vehquery="insert into holiday_vehicle(holiday_id,vehicle_code,vehicle_rate,user_id) value('".$holiday_id."','".$VehicleCode[$i]->vehicle_code."','".$VehicleCode[$i]->vehicle_rate."','".$userId."')";	
			  	$resource1 = operations($Vehquery);
		  	}
		  	for($j=0;$j<count($addSma);$j++)
		  	{
				$Smaquery="insert into holiday_sma(holiday_id,sma_id,user_id) value('".$holiday_id."','".$addSma[$j]."','".$userId."')";	
			  	$resource2 = operations($Smaquery);
		 	}
				
			$result=global_message(200,1008,$holiday_id);		   
	  	}
	   	else
	   	{
	    	$result=global_message(201,1003);
	   	}	
		return $result;	
	}

/*****************************************************************
Method:             getRateMatrixList()
InputParameter:    	user_id
Return:             get Rate MatrixList
*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
  		{
			$query="Select * from holidays where user_id=".$_REQUEST['user_id'];
			$resource= operations($query);
			$contents = array();
	    	if(count($resource)>0 && gettype($resource)!="boolean")
	    	{
				for($i=0; $i<count($resource); $i++)
				{
					$vehicle_code=''; 
					$vehicle_rate_new='';
					$sma_name='';
					$sma_id='';
					$value='';
					$Vehquery="Select vehicle_code,vehicle_rate from holiday_vehicle where holiday_id=".$resource[$i]['id'];
					$resource1= operations($Vehquery);
					for($j=0; $j<count($resource1); $j++)
					{
						$vehicle_code .=$resource1[$j]['vehicle_code'].',';
						$vehicle_rate_new .=$resource1[$j]['vehicle_rate'].',';
					}
					$driver_gratuity_query="Select service_type ,b.service_name from holiday_service_type a inner join comman_service_type b on a.service_type=b.service_code  where a.parent_id=".$resource[$i]['id']." and b.user_id='".$_REQUEST['user_id']."'";
					$driver_gratuity_query_result= operations($driver_gratuity_query);
					$service_typeArrayDublicate=[];
					$service_type='';
					$service_nameArrayDublicate=[];
					$service_name='';
					for($j=0; $j<count($driver_gratuity_query_result); $j++)
					{
						if(!in_array($driver_gratuity_query_result[$j]['service_type'],$service_typeArrayDublicate))
						{
							array_push($service_typeArrayDublicate,$driver_gratuity_query_result[$j]['service_type']);
							$service_type .=$driver_gratuity_query_result[$j]['service_type'].',';
							$service_name .=$driver_gratuity_query_result[$j]['service_name'].',';				
						}
					}

					$Smaquery="Select sma_id,sma_name from holiday_sma,sma where sma.id=holiday_sma.sma_id AND holiday_sma.holiday_id=".$resource[$i]['id'];
					$resource2= operations($Smaquery);
					for($k=0; $k<count($resource2); $k++)
					{
						$sma_name .=$resource2[$k]['sma_name'].',';
						$sma_id .=$resource2[$k]['sma_id'].',';
					}
					$contents[$i]['id']=$resource[$i]['id'];
					$contents[$i]['name']=$resource[$i]['name'];
					$contents[$i]['amount']=$resource[$i]['amount'];
					$contents[$i]['sma_id'] = $sma_id;
					$contents[$i]['sma_name'] = $sma_name;
					$contents[$i]['vehicle_code']=$vehicle_code;
					$contents[$i]['vehicle_rate']=$vehicle_rate_new;
					$contents[$i]['service_type']=$service_type;
					$contents[$i]['service_name']=$service_name;
					$contents[$i]['calender']=$resource[$i]['calender'];
					$contents[$i]['start_time'] = $resource[$i]['start_time'];
					$contents[$i]['end_time'] = $resource[$i]['end_time'];
					$contents[$i]['allowORES'] = $resource[$i]['allowORES'];
					$contents[$i]['Cut_off_time'] = $resource[$i]['Cut_off_time'];
					$contents[$i]['blackout_Msg'] = $resource[$i]['blackout_Msg'];
					$contents[$i]['holiday_surcg_per_pax'] = $resource[$i]['holiday_surcg_per_pax'];
					$contents[$i]['holiday_surcg_type'] = $resource[$i]['holiday_surcg_type'];

				}
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
		   		$result=global_message(200,1007,$contents);
   		   	}
		   	else
		   	{
		   		$result=global_message(200,1006);
		   	}		  
		}
	 	else
	  	{
		  	$result=global_message(201,1003);
	  	}
	  	return  $result;
	}


/*****************************************************************
Method:             deleteHoliday()
InputParameter:    	holiday_id
Return:             delete Holiday
*****************************************************************/
	function deleteHoliday()
	{
		if((isset($_REQUEST['holiday_id']) && !empty($_REQUEST['holiday_id'])))
	   	{
	  		$rowId=$_REQUEST['holiday_id'];		 
			$query="delete from holidays where id='".$rowId."'";
	    	$resource = operations($query);
			$queryDelete1="delete  from holiday_sma where holiday_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete2="delete  from holiday_vehicle where holiday_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$queryDelete4="delete  from holiday_service_type where parent_id='".$rowId."'";
			$resource4 = operations($queryDelete4);
			$result=global_message(200,1010);   		   
	   	}
	  	else
	  	{
	   		$result=global_message(201,1003);
	  	}
		return $result;
	}

