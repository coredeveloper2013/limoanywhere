<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

	/*****************************************************************
	Method:             setBlackoutDate()
	InputParameter:     sma_id,user_id,vehicle_code,value
	Return:             set Blackout Date
	*****************************************************************/
	function setBlackoutDate()
	{	
	 	if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) &&(isset($_REQUEST['value']) && !empty($_REQUEST['value'])) && (isset($_REQUEST['calender']) ))
	   	{
      		$userId=$_REQUEST['user_id'];
		  	$start_time=(isset($_REQUEST['start_time']) && !empty($_REQUEST['start_time']))?$_REQUEST['start_time']:'12:00 AM';
		  	$end_time=(isset($_REQUEST['end_time']) && !empty($_REQUEST['end_time']))?$_REQUEST['end_time']:'11:59 PM';
		  
		  	$end_time=explode(' ',$end_time);
		  	if($end_time[1]==pm){
			  	$end_time1=explode(':',$end_time[0]);
			  	$end_time2=$end_time1[0]+12;
			  	$end_time=$end_time2.":".$end_time1[1];
	          	echo $end_time;
		  	}
		  	else{
			  	$end_time=$end_time[0];
		  	}

		  	$start_time=explode(' ',$start_time);
		  	if($start_time[1]==pm){
			  	$start_time1=explode(':',$start_time[0]);	
			  	$start_time2=$start_time1[0]+12;
			  	$start_time= $start_time2.":".$start_time1[1];
		  	}
		  	else{
		  		$start_time=$start_time[0];
		  	}

		  	$start_date = $_REQUEST['calender'];
	      	$start_date=explode('/',$start_date);
	      	$start_date=$start_date[2]."-".$start_date[0]."-".$start_date[1];

	      	$end_date = $_REQUEST['toCalender'];
	      	$end_date=explode('/',$end_date);
	      	$end_date=$end_date[2]."-".$end_date[0]."-".$end_date[1];

		   	$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$addSma=explode(',',$_REQUEST['sma_id']);
		   	$value=explode(',',$_REQUEST['value']);

		
			$query ="insert into blackout_date(value,calender,start_time,end_time,msg,user_id,end_calender) value('".$_REQUEST['value']."','".$start_date."','".$start_time."','".$end_time."','".$_REQUEST['msg']."','".$userId."','".$end_date."')";
            $bd_id = operations($query);

	  		for($i=0;$i<count($VehicleCode);$i++)
		  	{
			  	$Vehquery="insert into bd_vehicle(bd_id,vehicle_code,user_id) value('".$bd_id."','".$VehicleCode[$i]."','".$userId."')";	
			  	$resource1 = operations($Vehquery);
		  	}
		  	for($j=0;$j<count($addSma);$j++)
	  		{
				$Smaquery="insert into bd_sma(bd_id,sma_id,user_id) value('".$bd_id."','".$addSma[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}

	  	 	$result=global_message(200,1008,$bd_id);		   
	   	}
	   	else
	   	{
	    	$result=global_message(201,1003);
   		}	
		return $result;	
	}

	/*****************************************************************
	Method:             getRateMatrixList()
	InputParameter:     user_id
	Return:             get Rate Matrix List
	*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	  	{
			$query="Select * from blackout_date where user_id='".$_REQUEST['user_id']."' order by value ";
			$resource= operations($query);
			$contents = array();
		   	if(count($resource)>0 && gettype($resource)!="boolean")
		   	{
				for($i=0; $i<count($resource); $i++)
				{
					$vehicle_code=''; 
					$sma_name='';
					$sma_id='';
					$value='';
					$Vehquery="Select vehicle_code from bd_vehicle where bd_id=".$resource[$i]['id'];
					$resource1= operations($Vehquery);
					for($j=0; $j<count($resource1); $j++)
						{
							$vehicle_code .=$resource1[$j]['vehicle_code'].',';
						}
					$Smaquery="Select sma_id,sma_name from bd_sma,sma where sma.id=bd_sma.sma_id AND bd_sma.bd_id=".$resource[$i]['id'];
					$resource2= operations($Smaquery);
					for($k=0; $k<count($resource2); $k++)
					{
						$sma_name .=$resource2[$k]['sma_name'].',';
						$sma_id .=$resource2[$k]['sma_id'].',';
					}
					
					$contents[$i]['id']=$resource[$i]['id'];
					$contents[$i]['tocalender']=$resource[$i]['end_calender'];
					$contents[$i]['value']=$resource[$i]['value'];
					$contents[$i]['sma_id'] = $sma_id;
					$contents[$i]['sma_name'] = $sma_name;
					$contents[$i]['vehicle_code']=$vehicle_code;
					$contents[$i]['calender']=$resource[$i]['calender'];
					$contents[$i]['start_time'] = $resource[$i]['start_time'];
					$contents[$i]['end_time'] = $resource[$i]['end_time'];
					$contents[$i]['msg'] = $resource[$i]['msg'];
				}
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
			   $result=global_message(200,1007,$contents);
			}
		   	else
		   	{
			   $result=global_message(200,1006);
		   	}		  
	  	}
	 	else
	  	{
	  		$result=global_message(201,1003);
	  	}
	  	return  $result;
	}

	/*****************************************************************
	Method:             deleteBlackOutDate()
	InputParameter:     bd_id
	Return:             delete BlackOut Date
	*****************************************************************/
	function deleteBlackOutDate()
	{
 		if((isset($_REQUEST['bd_id']) && !empty($_REQUEST['bd_id'])))
	   	{
		  	$rowId=$_REQUEST['bd_id'];		 
			$query="delete from blackout_date where id='".$rowId."'";
	    	$resource = operations($query);
				$queryDelete1="delete  from bd_sma where bd_id='".$rowId."'";
				$resource2 = operations($queryDelete1);
				$queryDelete2="delete  from bd_vehicle where bd_id='".$rowId."'";
				$resource3 = operations($queryDelete2);
			$result=global_message(200,1010);   
   	    }
  		else
  		{
	   		$result=global_message(201,1003);
  		}
		return $result;
	}