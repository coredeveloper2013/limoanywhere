<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
 * Method:             deleteVehicleInformation()
 * InputParameter:     rowId,password,LegalEntity
 * Return:             deleted vehicle info
 *****************************************************************/
function getImportDatabaseFIleTrain()
{
    $query = "select * from  train_master_table where user_id=" . $_REQUEST['user_id'] . " and (status=1 or status=2) order by train_name asc ";
    $resource = operations($query);
    if (count($resource) > 0 && gettype($resource) != "boolean") {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getImportDatabaseFIleSeaPort()
 * InputParameter:     user_id
 * Return:             get Import Database FIle SeaPort
 *****************************************************************/
function getImportDatabaseFIleSeaPort()
{
    $query = "select * from  seaport_master_table where user_id=" . $_REQUEST['user_id'] . " and (status=1 or status=2) order by seaport_name asc";
    $resource = operations($query);
    if (count($resource) > 0 && gettype($resource) != "boolean") {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             getImportDatabaseFIle()
 * InputParameter:     user_id
 * Return:             get Import Database FIle
 *****************************************************************/
function getImportDatabaseFIle()
{
    $query = "select * from airport_master_table where user_id=" . $_REQUEST['user_id'] . " and (status=1 or status=2) order by airport_name asc";
    $resource = operations($query);
    if (count($resource) > 0 && gettype($resource) != "boolean") {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             setImportDatabaseFIleSeaport()
 * InputParameter:     name
 * Return:             Set Import Database FIle
 *****************************************************************/
function setImportDatabaseFIleSeaport()
{
    $csv_file = "csvFile/" . $_FILES["input24"]["name"];
    move_uploaded_file($_FILES["input24"]["tmp_name"], "csvFile/" . $_FILES["input24"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile);
        $csv_array = explode(",", $csv_data[$i]);
        $csv6 = $csv_array[6];
        $csv6 = mysql_real_escape_string($csv6);
        $csv5 = $csv_array[5];
        $csv5 = mysql_real_escape_string($csv5);
        $csv4 = $csv_array[4];
        $csv4 = mysql_real_escape_string($csv4);
        $csv3 = $csv_array[3];
        $csv3 = mysql_real_escape_string($csv3);
        $csv2 = $csv_array[2];
        $csv2 = mysql_real_escape_string($csv2);
        $csv1 = $csv_array[1];
        $csv1 = mysql_real_escape_string($csv1);
        $last_insertid = 0;
        if (isset($csv2) && !empty($csv2)) {

            $selectQuery = "select count(*) as tatalCount from seaport_master_table where seaport_name='" . $csv2 . "' and seaport_code='" . $csv5 . "' and user_id='" . $_REQUEST['user_id'] . "'";
            $selectQueryResult = operations($selectQuery);
            if ($selectQueryResult[0]['tatalCount'] == 0) {
                $querySelectVehicle = "select * from seaport_child_table where (seaport_country='" . $csv_array[0] . "' and state_name='" . $csv1 . "' and city_name='" . $csv2 . "' and  city_code='" . $csv3 . "' and user_id='" . $_REQUEST['user_id'] . "')";
                $querySelectVehicleResult = operations($querySelectVehicle);
                $last_insertid = 12;
                if (count($querySelectVehicleResult) > 0 && gettype($querySelectVehicleResult) != "boolean") {
                    $last_insertid = $querySelectVehicleResult[0][id];

                } else {
                    $query = 'insert into seaport_child_table(seaport_master_id,state_name,city_name,city_code,seaport_country,user_id,category) values("' . $last_insertid . '","' . $csv1 . '","' . $csv2 . '","' . $csv3 . '","' . $csv_array[0] . '","' . $_REQUEST['user_id'] . '","")';
                    $last_insertid = operations($query);
                }
                $query = 'insert into seaport_master_table(seaport_chield_id,seaport_code,seaport_name,seaport_zip_code,user_id,seaport_id) values("' . $last_insertid . '","' . $csv5 . '","' . $csv4 . '","' . $csv3 . '","' . $_REQUEST['user_id'] . '","0")';
                $resource = operations($query);
            } else {
                $query = "update seaport_master_table set seaport_code='" . $csv_array[0] . "',seaport_zip_code='" . $csv2 . "' where seaport_name='" . $csv1 . "'";
                operations($query);
            }

        }
        $i++;
    }
}


/*****************************************************************
 * Method:             setManualSeaport()
 * InputParameter:     name
 * Return:             set Manual Seaport
 *****************************************************************/
function setManualSeaport()
{
    if (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) {
        $selectQuery = "select count(*) as tatalCount from seaport_master_table where seaport_name='" . $_REQUEST['seaport_name1'] . "' and seaport_code='" . $_REQUEST['seaport_code1'] . "' and user_id='" . $_REQUEST['user_id'] . "'";
        $selectQueryResult = operations($selectQuery);
        if ($selectQueryResult[0]['tatalCount'] == 0) {
            $querySelectVehicle = "select * from seaport_child_table where (seaport_country='" . $_REQUEST['country_name1'] . "' and state_name='" . $_REQUEST['state_name1'] . "' and city_name='" . $_REQUEST['city_name1'] . "' and  city_code='" . $_REQUEST['city_zip_code1'] . "' and user_id='" . $_REQUEST['user_id'] . "')";
            $querySelectVehicleResult = operations($querySelectVehicle);
            $last_insertid = 12;
            if (count($querySelectVehicleResult) > 0 && gettype($querySelectVehicleResult) != "boolean") {
                $last_insertid = $querySelectVehicleResult[0][id];
            } else {

                $query = 'insert into seaport_child_table(seaport_master_id,state_name,city_name,city_code,seaport_country,user_id) values("' . $last_insertid . '","' . $_REQUEST['state_name1'] . '","' . $_REQUEST['city_name1'] . '","' . $_REQUEST['city_zip_code1'] . '","' . $_REQUEST['country_name1'] . '","' . $_REQUEST['user_id'] . '")';
                $last_insertid = operations($query);

            }
            $query = 'insert into seaport_master_table(seaport_chield_id,seaport_code,seaport_name,seaport_zip_code,status,user_id) values("' . $last_insertid . '","' . $_REQUEST['seaport_code1'] . '","' . $_REQUEST['seaport_name1'] . '","' . $_REQUEST['city_zip_code1'] . '",1,"' . $_REQUEST['user_id'] . '" )';
            $resource = operations($query);
        } else {
            $query = "update seaport_master_table set seaport_code='" . $_REQUEST['seaport_code1'] . "',seaport_zip_code='" . $_REQUEST['city_zip_code1'] . "' where seaport_name='" . $_REQUEST['seaport_name1'] . "'";
            operations($query);
        }
    }
}

/*****************************************************************
 * Method:             setImportDatabaseFIleTrain()
 * InputParameter:     name
 * Return:             set Import Database FIle Train
 *****************************************************************/
function setImportDatabaseFIleTrain()
{
    $csv_file = "csvFile/" . $_FILES["input24"]["name"];
    move_uploaded_file($_FILES["input24"]["tmp_name"], "csvFile/" . $_FILES["input24"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;
    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile);
        $csv_array = explode(",", $csv_data[$i]);
        $csv6 = $csv_array[6];
        $csv6 = mysql_real_escape_string($csv6);
        $csv5 = $csv_array[5];
        $csv5 = mysql_real_escape_string($csv5);
        $csv4 = $csv_array[4];
        $csv4 = mysql_real_escape_string($csv4);
        $csv3 = $csv_array[3];
        $csv3 = mysql_real_escape_string($csv3);
        $csv2 = $csv_array[2];
        $csv2 = mysql_real_escape_string($csv2);
        $csv1 = $csv_array[1];
        $csv1 = mysql_real_escape_string($csv1);
        if (isset($csv2) && !empty($csv2)) {
            $selectQuery = 'select count(*) as tatalCount from train_master_table where train_name="' . $csv2 . '" and user_id="' . $_REQUEST['user_id'] . '"';
            $selectQueryResult = operations($selectQuery);
            if ($selectQueryResult[0]['tatalCount'] == 0) {
                $querySelectVehicle = "select * from train_child_table where (train_country='" . $csv_array[0] . "' and state_name='" . $csv1 . "' and city_name='" . $csv2 . "' and  city_code='" . $csv3 . "' and user_id='" . $_REQUEST['user_id'] . "')";
                $querySelectVehicleResult = operations($querySelectVehicle);

                $last_insertid = 12;
                if (count($querySelectVehicleResult) > 0) {
                    $last_insertid = $querySelectVehicleResult[0]['id'];
                } else {
                    $query = 'insert into train_child_table(train_master_id,state_name,city_name,city_code,train_country,user_id,category) values("' . $last_insertid . '","' . $csv1 . '","' . $csv2 . '","' . $csv3 . '","' . $csv_array[0] . '","' . $_REQUEST['user_id'] . '","")';

                    $last_insertid = operations($query);
                }

                $query = 'insert into train_master_table(train_chield_id,train_code,train_name,train_zip_code,user_id,train_id) values("' . $last_insertid . '","' . $csv5 . '","' . $csv4 . '","' . $csv3 . '","' . $_REQUEST['user_id'] . '","0")';

                $resource = operations($query);
            } else {
                $query = "update train_master_table set train_code='" . $csv_array[0] . "',train_zip_code='" . $csv2 . "' where train_name='" . $csv1 . "'";
                operations($query);
            }

        }
        $i++;
    }
    $result = global_message(200, 1006);
    return $result;
}

/*****************************************************************
 * Method:             setManualTrain()
 * InputParameter:     user_id
 * Return:             set Manual Train
 *****************************************************************/
function setManualTrain()
{
    if (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) {
        $selectQuery = 'select count(*) as tatalCount from train_master_table where train_name="' . $_REQUEST['train_name1'] . '" and user_id="' . $_REQUEST['user_id'] . '"';
        $selectQueryResult = operations($selectQuery);
        if ($selectQueryResult[0]['tatalCount'] == 0) {
            $querySelectVehicle = "select * from train_child_table where (train_country='" . $_REQUEST['country_name1'] . "' and state_name='" . $_REQUEST['state_name1'] . "' and city_name='" . $_REQUEST['city_name1'] . "' and  city_code='" . $_REQUEST['city_zip_code1'] . "' and user_id='" . $_REQUEST['user_id'] . "')";
            $querySelectVehicleResult = operations($querySelectVehicle);
            $last_insertid = 12;
            if (count($querySelectVehicleResult) > 0) {
                $last_insertid = $querySelectVehicleResult[0]['id'];
            } else {
                $query = 'insert into train_child_table(train_master_id,state_name,city_name,city_code,train_country,user_id) values("' . $last_insertid . '","' . $_REQUEST['state_name1'] . '","' . $_REQUEST['city_name1'] . '","' . $_REQUEST['city_zip_code1'] . '","' . $_REQUEST['country_name1'] . '","' . $_REQUEST['user_id'] . '")';
                $last_insertid = operations($query);
            }

            $query = 'insert into train_master_table(train_chield_id,train_code,train_name,train_zip_code,status,user_id) values("' . $last_insertid . '","' . $_REQUEST['train_code1'] . '","' . $_REQUEST['train_name1'] . '","' . $_REQUEST['city_zip_code1'] . '",1,"' . $_REQUEST['user_id'] . '")';
            $resource = operations($query);
        } else {
            $query = "update train_master_table set train_code='" . $_REQUEST['train_code1'] . "',train_zip_code='" . $_REQUEST['city_zip_code1'] . "' where train_name='" . $_REQUEST['train_name1'] . "'";
            operations($query);
        }
    }
    $result = global_message(200, 1006);
    return $result;
}

/*****************************************************************
 * Method:             getSeaportCityAndCode()
 * InputParameter:     stateName
 * Return:             get Seaport City AndCode
 *****************************************************************/
function getSeaportCityAndCode()
{
//    $query = "select * from seaport_child_table where `state_name`='" . $_REQUEST['stateName'] . "'  and user_id='" . $_REQUEST['user_id'] . "'";
    $query = "select seaport_child_table.*, seaport_master_table.seaport_chield_id, seaport_master_table.status from seaport_child_table left JOIN seaport_master_table ON seaport_master_table.seaport_chield_id = seaport_child_table.id where seaport_child_table.`state_name`='".$_REQUEST['stateName']."' and seaport_child_table.user_id='".$_REQUEST['user_id']."'";
    $selectQueryResult = operations($query);
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $rv = [];
        foreach ($selectQueryResult as $data){
            if($data['status'] != 1){
                $rv[] = $data;
            }
        }

        if(count($rv) > 0){
            $result = global_message(200, 1007, $rv);
        } else {
            $result = global_message(200, 1006);
        }

    } else {
        $result = global_message(200, 1006);
    }

    return $result;
}

/*****************************************************************
 * Method:             getTrainCityAndCode()
 * InputParameter:     stateName
 * Return:             get Train City And Code
 *****************************************************************/
function getTrainCityAndCode()
{
//    $query = "select * from train_child_table where `state_name`='" . $_REQUEST['stateName'] . "' and user_id='" . $_REQUEST['user_id'] . "'";
    $query = "select train_child_table.*, train_master_table.train_chield_id, train_master_table.status from train_child_table left JOIN train_master_table ON train_master_table.train_chield_id = train_child_table.id where train_child_table.`state_name`='".$_REQUEST['stateName']."' and train_child_table.user_id='".$_REQUEST['user_id']."'";
    $selectQueryResult = operations($query);
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $rv = [];
        foreach ($selectQueryResult as $data){
            if($data['status'] != 1){
                $rv[] = $data;
            }
        }

        if(count($rv) > 0){
            $result = global_message(200, 1007, $rv);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(200, 1006);
    }
    return $result;

}


/*****************************************************************
 * Method:             getAiportCityAndCode()
 * InputParameter:     stateName
 * Return:             get Aiport City And Code
 *****************************************************************/
function getAiportCityAndCode()
{
//    $query = "select * from airport_child_table where `state_name`='" . $_REQUEST['stateName'] . "' and category like '%" . $_REQUEST['categoryId'] . "%' and user_id='" . $_REQUEST['user_id'] . "' order by city_name";
    $query = "select airport_child_table.*, airport_master_table.airport_chield_id, airport_master_table.status from airport_child_table left JOIN airport_master_table ON  airport_master_table.airport_chield_id = airport_child_table.id where airport_child_table.`state_name`='".$_REQUEST['stateName']."' and airport_child_table.category like '%" . $_REQUEST['categoryId'] . "%' and airport_child_table.user_id='".$_REQUEST['user_id']."' order by airport_child_table.city_name";
    $selectQueryResult = operations($query);
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $rv = [];
        foreach ($selectQueryResult as $data){
            if($data['status'] != 1){
                $rv[] = $data;
            }
        }

        if(count($rv) > 0){
            $result = global_message(200, 1007, $rv);
        } else {
            $result = global_message(200, 1006);
        }

    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             seaport_activeFormSubmit()
 * InputParameter:     aiportId
 * Return:             seaport active Form Submit
 *****************************************************************/
function seaport_activeFormSubmit()
{
    $getFullString = explode(",", $_REQUEST['aiportId']);
    for ($i = 0; $i < count($getFullString); $i++) {
        $query = "update  seaport_master_table set status=1 where id=" . $getFullString[$i];
        operations($query);
    }
    $result = global_message(200, 1006);
    return $result;

}


/*****************************************************************
 * Method:             train_activeFormSubmit()
 * InputParameter:     aiportId
 * Return:             train_active Form Submit
 *****************************************************************/
function train_activeFormSubmit()
{
    $getFullString = explode(",", $_REQUEST['aiportId']);
    for ($i = 0; $i < count($getFullString); $i++) {
        $query = "update  train_master_table set status=1 where id=" . $getFullString[$i];
        operations($query);
    }
    $result = global_message(200, 1006);
    return $result;
}


/*****************************************************************
 * Method:             airport_activeFormSubmit()
 * InputParameter:     aiportId
 * Return:             airport_active Form Submit
 *****************************************************************/
function airport_activeFormSubmit()
{
    $getFullString = explode(",", $_REQUEST['aiportId']);
    for ($i = 0; $i < count($getFullString); $i++) {
        $query = "update airport_master_table set status=1 where id=" . $getFullString[$i];
        operations($query);
    }
    $result = global_message(200, 1006);
    return $result;
}


/*****************************************************************
 * Method:             isSeaportCheck()
 * InputParameter:     aiportId
 * Return:             Seaport Check
 *****************************************************************/
function isSeaportCheck()
{
    $airportDbDataArray = explode(",", $_REQUEST['airport_db_cityValue']);
    $airportDbArrayList = [];
    $airportDbList = [];
    for ($i = 0; $i < count($airportDbDataArray); $i++) {
        $airportDbData = explode("@", $airportDbDataArray[$i]);
        $checkArray = isset($airportDbData[1]) ? 1 : 2;
        $query = "SELECT id FROM `seaport_child_table` where city_name='" . $airportDbData[0] . "' and city_code='" . $airportDbData[1] . "'and user_id='" . $_REQUEST['user_id'] . "'";
        if ($checkArray == 2) {
            $query = "SELECT id FROM `seaport_child_table` where city_name='" . $airportDbData[0] . "'and user_id='" . $_REQUEST['user_id'] . "'";
        } else {
            $query = "SELECT id FROM `seaport_child_table` where city_name='" . $airportDbData[0] . "' and city_code='" . $airportDbData[1] . "'and user_id='" . $_REQUEST['user_id'] . "'";
        }
        $selectQueryResult = operations($query);
        array_push($airportDbArrayList, $selectQueryResult[0]['id']);
    }

    $airportDbArrayListComma = join(',', $airportDbArrayList);
    $queryAirportList = "SELECT id,seaport_name,seaport_code FROM seaport_master_table  WHERE seaport_chield_id IN (" . $airportDbArrayListComma . ") and status=0 and user_id='" . $_REQUEST['user_id'] . "'";
    $queryAirportListResult = operations($queryAirportList);
    $result = '';
    if (count($queryAirportListResult) > 0 && gettype($queryAirportListResult) != "boolean") {
        $result = global_message(200, 1007, $queryAirportListResult);
    } else {
        $result = global_message(200, 1006);

    }
    return $result;
}


/*****************************************************************
 * Method:             isTrainCheck()
 * InputParameter:     aiportId
 * Return:             Train Check
 *****************************************************************/

function isTrainCheck()
{
    $airportDbDataArray = explode(",", $_REQUEST['airport_db_cityValue']);
    $airportDbArrayList = [];
    $airportDbList = [];
    for ($i = 0; $i < count($airportDbDataArray); $i++) {
        $airportDbData = explode("@", $airportDbDataArray[$i]);
        $checkArray = isset($airportDbData[1]) ? 1 : 2;
        $query = "SELECT id FROM `train_child_table` where city_name='" . $airportDbData[0] . "' and city_code='" . $airportDbData[1] . "' and user_id='" . $_REQUEST['user_id'] . "'";
        if ($checkArray == 2) {
            $query = "SELECT id FROM `train_child_table` where city_name='" .
                $airportDbData[0] . "' and user_id='" . $_REQUEST['user_id'] . "'";
        } else {
            $query = "SELECT id FROM `train_child_table` where city_name='" .
                $airportDbData[0] . "' and city_code='" . $airportDbData[1] . "' and user_id='" .
                $_REQUEST['user_id'] . "'";
        }
        $selectQueryResult = operations($query);
        array_push($airportDbArrayList, $selectQueryResult[0]['id']);

    }

    $airportDbArrayListComma = join(',', $airportDbArrayList);
    $queryAirportList = "SELECT id,train_name,train_code FROM train_master_table  WHERE train_chield_id IN (" . $airportDbArrayListComma . ") and status=0 and user_id='" .
        $_REQUEST['user_id'] . "'";
    $queryAirportListResult = operations($queryAirportList);
    $result = '';
    if (count($queryAirportListResult) > 0 && gettype($queryAirportListResult) != "boolean") {
        $result = global_message(200, 1007, $queryAirportListResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             isAirportCheck()
 * InputParameter:     aiportId
 * Return:             Train Check
 *****************************************************************/
function isAirportCheck()
{
    $airportDbDataArray = explode(",", $_REQUEST['airport_db_cityValue']);
    $airportDbArrayList = [];
    $airportDbList = [];
    for ($i = 0; $i < count($airportDbDataArray); $i++) {
        $airportDbData = explode("@", $airportDbDataArray[$i]);
        $checkArray = isset($airportDbData[1]) ? 1 : 2;
        $query = "SELECT id FROM `airport_child_table` where city_name='" . $airportDbData[0] . "' and city_code='" . $airportDbData[1] . "' and user_id='" .
            $_REQUEST['user_id'] . "'";
        if ($checkArray == 2) {
            $query = "SELECT id FROM `airport_child_table` where city_name='" .
                $airportDbData[0] . "' and user_id='" . $_REQUEST['user_id'] . "'";
        } else {
            $query = "SELECT id FROM `airport_child_table` where city_name='" .
                $airportDbData[0] . "' and city_code='" . $airportDbData[1] . "' and user_id='" .
                $_REQUEST['user_id'] . "'";
        }
        $selectQueryResult = operations($query);
        array_push($airportDbArrayList, $selectQueryResult[0]['id']);
    }
    $airportDbArrayListComma = join(',', $airportDbArrayList);
    $queryAirportList = "SELECT id,airport_name,airport_code FROM airport_master_table  WHERE airport_chield_id IN (" . $airportDbArrayListComma . ") and status=0 and user_id='" . $_REQUEST['user_id'] . "'";
    $queryAirportListResult = operations($queryAirportList);
    $result = '';
    if (count($queryAirportListResult) > 0 && gettype($queryAirportListResult) != "boolean") {
        $result = global_message(200, 1007, $queryAirportListResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getStateSeaportDb()
 * InputParameter:     user_id
 * Return:             get State Seaport Db
 *****************************************************************/
function getStateSeaportDb()
{
    $query = "SELECT DISTINCT `state_name` FROM `seaport_child_table` where seaport_country='" . $_REQUEST['rowId'] . "' and user_id='" . $_REQUEST['user_id'] . "'";
    $selectQueryResult = operations($query);
    $result = '';
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $result = global_message(200, 1007, $selectQueryResult);
    } else {
        $result = global_message(200, 1006);
    }

    return $result;
}

/*****************************************************************
 * Method:             getCountryTrainDb()
 * InputParameter:     user_id
 * Return:             get Country TrainDb
 *****************************************************************/
function getCountryTrainDb()
{
    $query = "SELECT DISTINCT `train_country` FROM `train_child_table` where user_id='" . $_REQUEST['user_id'] . "'";
    $selectQueryResult = operations($query);
    $result = '';
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $result = global_message(200, 1007, $selectQueryResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getStateTrainDb()
 * InputParameter:     user_id
 * Return:             get State TrainDb
 *****************************************************************/
function getStateTrainDb()
{
    $query = "SELECT DISTINCT `state_name` FROM `train_child_table` where train_country='" . $_REQUEST['rowId'] . "' and user_id='" . $_REQUEST['user_id'] . "'";
    $selectQueryResult = operations($query);

    $result = '';
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $result = global_message(200, 1007, $selectQueryResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             getStateTrainDb()
 * InputParameter:     user_id
 * Return:             get State TrainDb
 *****************************************************************/
function getCountrySeaportDb()
{
    $query = "SELECT DISTINCT `seaport_country` FROM `seaport_child_table` where user_id='" . $_REQUEST['user_id'] . "'";
    $selectQueryResult = operations($query);

    $result = '';
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $result = global_message(200, 1007, $selectQueryResult);
    } else {
        $result = global_message(200, 1006);
    }

    return $result;
}

/*****************************************************************
 * Method:             getCountryAirportDb()
 * InputParameter:     user_id
 * Return:             get Country Airport Db
 *****************************************************************/
function getCountryAirportDb()
{
    $query = "SELECT DISTINCT `airport_country` FROM `airport_child_table` where user_id='" . $_REQUEST['user_id'] . "'";
    $selectQueryResult = operations($query);
    $result = '';
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $result = global_message(200, 1007, $selectQueryResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             getStateAirportDb()
 * InputParameter:     user_id
 * Return:             get State Airport Db
 *****************************************************************/
function getStateAirportDb()
{
    $query = "SELECT DISTINCT `state_name` FROM `airport_child_table` where airport_country='" . $_REQUEST['rowId'] . "' and user_id='" . $_REQUEST['user_id'] . "' order by state_name";
    $selectQueryResult = operations($query);
    $result = '';
    if (count($selectQueryResult) > 0 && gettype($selectQueryResult) != "boolean") {
        $result = global_message(200, 1007, $selectQueryResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             setImportDatabaseFIle()
 * InputParameter:     name
 * Return:             set Import Database FIle
 *****************************************************************/
function setImportDatabaseFIle()
{
    $csv_file = "csvFile/" . $_FILES["input24"]["name"];
    move_uploaded_file($_FILES["input24"]["tmp_name"], "csvFile/" . $_FILES["input24"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;
    while (!feof($csvfile)) {
        $last_insertid = 0;
        $csv_data[] = fgets($csvfile);
        $csv_array = explode(",", $csv_data[$i]);
        $csv6 = $csv_array[6];
        $csv6 = mysql_real_escape_string($csv6);
        $csv5 = $csv_array[5];
        $csv5 = mysql_real_escape_string($csv5);
        $csv4 = $csv_array[4];
        $csv4 = mysql_real_escape_string($csv4);
        $csv3 = $csv_array[3];
        $csv3 = preg_replace('/\s+/', '', $csv3);
        $csv2 = $csv_array[2];
        $csv2 = mysql_real_escape_string($csv2);
        $csv1 = $csv_array[1];
        $csv1 = mysql_real_escape_string($csv1);
        if (isset($csv2) && !empty($csv2)) {

            $selectQuery = 'select count(*) as tatalCount from airport_master_table where airport_name="' . $csv4 . '" and airport_code="' . $csv5 . '" and user_id="' .
                $_REQUEST['user_id'] . '"';
            $selectQueryResult = operations($selectQuery);
            if ($selectQueryResult[0]['tatalCount'] == 0) {
                $querySelectVehicle = "select * from airport_child_table where (airport_country='" . $csv_array[0] . "' and state_name='" . $csv1 . "' and city_name='" . $csv2 . "' and  city_code='" . $csv3 . "' and user_id='" . $_REQUEST['user_id'] . "')";
                $querySelectVehicleResult = operations($querySelectVehicle);
                $last_insertid = 12;
                if (count($querySelectVehicleResult) > 0 && gettype($querySelectVehicleResult) != "boolean") {
                    $last_insertid = $querySelectVehicleResult[0][id];

                } else {
                    $query2 = 'insert into airport_child_table(airport_master_id,state_name,city_name,city_code,airport_country,category,user_id) values("0","' . $csv1 . '","' . $csv2 . '","' . $csv3 . '","' . $csv_array[0] . '","' . strtolower($csv6) . '","' . $_REQUEST['user_id'] . '")';
                    $last_insertid = operations($query2);
                }
                $query = 'insert into airport_master_table(airport_chield_id,airport_code,airport_name,airport_zip_code,user_id,airport_id) values("' . $last_insertid . '","' . $csv5 . '","' . $csv4 . '","' . $csv3 . '","' . $_REQUEST['user_id'] . '","0")';

                $resource = operations($query);
            } else {
                $query = 'update airport_master_table set airport_code="' . $csv2 . '",airport_zip_code="' . $csv_array[0] . '" where airport_name="' . $csv_array[1] . '"';
                operations($query);
            }
        }
        $i++;
    }
    $result = global_message(200, 1006);
    return $result;
}

/*****************************************************************
 * Method:             setManualAirport()
 * InputParameter:     user_id
 * Return:             set Manual Airport
 *****************************************************************/

function setManualAirport()
{
    if (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) {
        $selectQuery = 'select count(*) as tatalCount from airport_master_table where airport_name="' . $_REQUEST['airport_name1'] . '" and airport_code="' . $_REQUEST['airport_code1'] . '" and user_id="' . $_REQUEST['user_id'] . '"';
        $selectQueryResult = operations($selectQuery);

        if ($selectQueryResult[0]['tatalCount'] == 0) {
            $querySelectVehicle = "select * from airport_child_table where (airport_country='" . $_REQUEST['country_name1'] . "' and state_name='" . $_REQUEST['state_name1'] . "' and city_name='" . $_REQUEST['city_name1'] . "' and  city_code='" . $_REQUEST['city_zip_code1'] . "' and user_id='" . $_REQUEST['user_id'] . "')";
            $querySelectVehicleResult = operations($querySelectVehicle);
            $last_insertid = 12;
            if (count($querySelectVehicleResult) > 0 && gettype($querySelectVehicleResult) != "boolean") {
                $last_insertid = $querySelectVehicleResult[0][id];
            } else {
                $query2 = 'insert into airport_child_table(airport_master_id,state_name,city_name,city_code,airport_country,category,user_id) values("0","' . $_REQUEST['state_name1'] . '","' . $_REQUEST['city_name1'] . '","' . $_REQUEST['city_zip_code1'] . '","' . $_REQUEST['country_name1'] . '","main","' . $_REQUEST['user_id'] . '")';
                $last_insertid = operations($query2);
            }

            $query = 'insert into airport_master_table(airport_chield_id,airport_code,airport_name,airport_zip_code,status,user_id,airport_id) values("' . $last_insertid . '","' . $_REQUEST['airport_code1'] . '","' . $_REQUEST['airport_name1'] . '","' . $_REQUEST['city_zip_code1'] . '",1,"' . $_REQUEST['user_id'] . '","0")';

            $resource = operations($query);
        } else {
            $query = 'update airport_master_table set airport_code="' . $_REQUEST['airport_code1'] . '",airport_zip_code="' . $_REQUEST['city_zip_code1'] . '" where airport_name="' . $_REQUEST['airport_name1'] . '"';
            operations($query);
        }
    }

    $result = global_message(200, 1006);
    return $result;
}


/*****************************************************************
 * Method:             getSmaCountry()
 * InputParameter:     user_id
 * Return:             get Sma Country
 *****************************************************************/
function getSmaCountry()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query1 = "SELECT distinct id,country_name,country_abbr from sma_countries where user_id='" . $_REQUEST['user_id'] . "' order by country_name asc,country_abbr asc ";
        $resource1 = operations($query1);
        for ($j = 0; $j < count($resource1); $j++) {
            $arr[] = array(
                "id" => $resource1[$j]['id'],
                "country_name" => $resource1[$j]['country_name'],
                "country_abbr" => $resource1[$j]['country_abbr']
            );
        }

        if (count($arr) > 0 && gettype($arr) != "boolean") {
            $result = global_message(200, 1007, $arr);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


/*****************************************************************
 * Method:             getSmaState()
 * InputParameter:     country_id
 * Return:             get Sma State
 *****************************************************************/

function getSmaState()
{
    if ((isset($_REQUEST['country_id']) && !empty($_REQUEST['country_id']))) {
        $query1 = "SELECT distinct id,sma_state,state_abbr from sma_state where sma_country_id='" . $_REQUEST['country_id'] . "' and user_id='" . $_REQUEST['user_id'] . "' order by sma_state asc,state_abbr asc ";
        $resource1 = operations($query1);
        for ($j = 0; $j < count($resource1); $j++) {
            $arr[] = array(
                "id" => $resource1[$j]['id'],
                "state_name" => $resource1[$j]['sma_state'],
                "state_code" => $resource1[$j]['state_abbr']
            );
        }
        if (count($arr) > 0 && gettype($arr) != "boolean") {
            $result = global_message(200, 1007, $arr);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             getSmaState()
 * InputParameter:     country_id
 * Return:             get Sma State
 *****************************************************************/
function getSmaCity()
{
    if ((isset($_REQUEST['state_id']) && !empty($_REQUEST['state_id']))) {
        $query1 = "SELECT  city_name, id from sma_city where country_id='" . $_REQUEST['country_id'] . "' and state_id='" . $_REQUEST['state_id'] . "' group by city_name  order by city_name asc ";
        $resource1 = operations($query1);
        for ($j = 0; $j < count($resource1); $j++) {
            $arr[] = array(
                "id" => $resource1[$j]['id'],
                "city_name" => $resource1[$j]['city_name']
            );
        }
        if (count($arr) > 0 && gettype($arr) != "boolean") {
            $result = global_message(200, 1007, $arr);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

?>