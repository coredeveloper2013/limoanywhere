<?php
include_once 'config.php';
include_once 'config_path.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '64M');

	$userId=$_REQUEST['user_id'];
	$query="select * from company_user_info where id='".$userId."' ";
	$userLogin = operations($query);
	
	if($userLogin[0]['isLAWCheck']!=0 && $userLogin[0]['limo_setup']=='off')
	{
		$querySelectVehicle="select * from vehicle_information where user_id='".$userId."' AND vehicle_type!='limo' order by passenger_capacity";
		$querySelectVehicleResult = operations($querySelectVehicle);				
		$finalResult=[];
		$alternateArray=[];
		$finalVehicleResultArray=[];
		for($i=0; $i<count($querySelectVehicleResult); $i++)
		{
			$querySelectImage="select img_name from vehicle_extra_info where parent_id='".$querySelectVehicleResult[$i]['id']."' ORDER BY image_seq asc LIMIT 1";
				$querySelectImageResult=operations($querySelectImage);
				if(count($querySelectImageResult)>0 && gettype($querySelectImageResult)!="boolean")
				{
					$alternateArray['VehTypeImg1']=$vehicle_image_path.$querySelectImageResult[0]['img_name'];
				}
				else
				{
					$alternateArray['VehTypeImg1']=$vehicle_image_path.'noimage.jpg';
				}
				$alternateArray['VehTypeId']=$querySelectVehicleResult[$i]['id'];
				$alternateArray['VehTypeCode']=$querySelectVehicleResult[$i]['code'];
				$alternateArray['VehTypeTitle']=$querySelectVehicleResult[$i]['vehicle_title'];
				$alternateArray['PassengerCapacity']=$querySelectVehicleResult[$i]['passenger_capacity'];
				$alternateArray['LuggageCapacity']=$querySelectVehicleResult[$i]['luggage_capacity'];
				$finalResult['VehicleType'][$i]=(object)$alternateArray;
		}		
				$finalVehicleResultArray=array("ResponseCode"=>0,"ResponseText"=>"ok","VehicleTypes"=>(object)$finalResult);	
				echo json_encode((object)$finalVehicleResultArray);
				exit;
	}
	else if($userLogin[0]['isLAWCheck']==0 && $userLogin[0]['limo_setup']=='on')
	{

        $querySelectVehicle="select * from vehicle_information where user_id='".$userId."' AND vehicle_type='limo' order by passenger_capacity";
        $querySelectVehicleResult = operations($querySelectVehicle);
        if(count($querySelectVehicleResult) > 1){
            $finalResult=[];
            $alternateArray=[];
            $finalVehicleResultArray=[];
            for($i=0; $i<count($querySelectVehicleResult); $i++)
            {
                $querySelectImage="select img_name from vehicle_extra_info where parent_id='".$querySelectVehicleResult[$i]['id']."' ORDER BY image_seq asc LIMIT 1";
                $querySelectImageResult=operations($querySelectImage);
                if(count($querySelectImageResult)>0 && gettype($querySelectImageResult)!="boolean")
                {
                    $alternateArray['VehTypeImg1']=$querySelectImageResult[0]['img_name'];
                }
                else
                {
                    $alternateArray['VehTypeImg1']=$vehicle_image_path.'noimage.jpg';
                }
                $alternateArray['VehTypeId']=$querySelectVehicleResult[$i]['id'];
                $alternateArray['VehTypeCode']=$querySelectVehicleResult[$i]['code'];
                $alternateArray['VehTypeTitle']=$querySelectVehicleResult[$i]['vehicle_title'];
                $alternateArray['PassengerCapacity']=$querySelectVehicleResult[$i]['passenger_capacity'];
                $alternateArray['LuggageCapacity']=$querySelectVehicleResult[$i]['luggage_capacity'];
                $finalResult['VehicleType'][$i]=(object)$alternateArray;
            }
            $finalVehicleResultArray=array("ResponseCode"=>0,"ResponseText"=>"ok","VehicleTypes"=>(object)$finalResult);
            echo json_encode((object)$finalVehicleResultArray);
            exit;
        } else {
            if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])&&isset($_REQUEST['limoApiKey'])
                && !empty($_REQUEST['limoApiKey'])&&isset($_REQUEST['limoApiID']) && !empty($_REQUEST['limoApiID']))
            {
                $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
                $sh_param = array(
                    'apiId'    =>    $_REQUEST['limoApiID'],
                    'apiKey'    =>    $_REQUEST['limoApiKey']);
                $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL','GetStates',$sh_param);
                $action =$_REQUEST['action'];
                $result = $action.'Result';
                $trans_vehicle = $soapClient->$action(array('apiId' => $_REQUEST['limoApiID'], 'apiKey' =>$_REQUEST['limoApiKey']))->$result;
                $final_vehicle_array=[];
                $parrentArray=$trans_vehicle->VehicleTypes->VehicleType;
                $childArray=$trans_vehicle->VehicleTypes->VehicleType;

                foreach ($childArray as $vehicle){
                    $sqlVehicle = "INSERT INTO `vehicle_information` ";
                    $sqlVehicle .= "(`id`, `code`, `vehicle_title`, `vehicle_status`, `passenger_capacity`,";
                    $sqlVehicle .= " `luggage_capacity`, `vehicle_type`, `user_id`) ";
                    $sqlVehicle .= " VALUES (NULL, '".$vehicle->VehTypeCode."', '".$vehicle->VehTypeTitle."', '1' ";
                    $sqlVehicle .= ", '".$vehicle->PassengerCapacity."', '".$vehicle->LuggageCapacity."', 'Limo', '".$userId."')";
                    operations($sqlVehicle);
                    $maxIdQ = 'SELECT id from `vehicle_information` ORDER BY id DESC LIMIT 1';
                    $maxId = operations($maxIdQ);
                    $maxId = $maxId[0]['id'];
                    $sqlVehicle = "INSERT INTO `vehicle_extra_info` ";
                    $sqlVehicle .= " (`id`, `img_name`, `status`, `parent_id`, `image_seq`) ";
                    $sqlVehicle .= " VALUES (NULL, '".$vehicle->VehTypeImg1."', '1', '".$maxId."', '1') ";
                    operations($sqlVehicle);
                }

                for($i=0; $i<count($parrentArray); $i++)
                {
                    for($j=$i+1; $j<count($parrentArray); $j++)
                    {
                        if($parrentArray[$i]->PassengerCapacity>$parrentArray[$j]->PassengerCapacity)
                        {
                            $finalVehicleResult=$parrentArray[$i];
                            $parrentArray[$i]=$parrentArray[$j];
                            $parrentArray[$j]=$finalVehicleResult;
                        }

                    }
                }
                $trans_vehicle->VehicleTypes->VehicleType=$parrentArray;
                echo json_encode($trans_vehicle);
                exit;
            }
            else{
                echo 'unauthorized url!';
            }
        }



	}
	else{


		$finalVehicleResultArray=array("ResponseCode"=>1,"ResponseText"=>"ok");	
		echo json_encode((object)$finalVehicleResultArray);
	}

	
?>