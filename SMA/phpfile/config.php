<?php
define("WP_MEMORY_LIMIT", "1024M");
define('DB_SERVER','localhost');
define('DB_USERNAME','root');
define('DB_PASSWORD','');
define('DB_NAME','limo');

/*****************************************************************
 * Method:             connection()
 * InputParameter:
 * Return:             connection
 *****************************************************************/


if (!function_exists('mysql_query')) {
    function mysql_query($sql)
    {
        $conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        return mysqli_query($conn, $sql);
    }
}

if (!function_exists('mysql_fetch_array')) {
    function mysql_fetch_array($result)
    {
        return mysqli_fetch_array($result);
    }
}

if (!function_exists('mysql_fetch_assoc')) {
    function mysql_fetch_assoc($result)
    {
        return mysqli_fetch_assoc($result);
    }
}

if (!function_exists('mysql_num_rows')) {
    function mysql_num_rows($result)
    {
        return mysqli_num_rows($result);
    }
}

if (!function_exists('mysql_insert_id')) {
    function mysql_insert_id()
    {
        $conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        return $conn->insert_id;
    }
}

if (!function_exists('mysql_real_escape_string')) {
    function mysql_real_escape_string($str, $link_identifier = null)
    {
        $conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        return mysqli_real_escape_string($conn, $str);
    }
}


/*function connection()
{
    ini_set('memory_limit', '1024M');
    ini_set('allow_url_fopen', '1');
    $GLOBALS['con'] = mysqli_connect('localhost', 'mylimo5_adminer', 'adminer123', 'mylimo5_limo') or die(mysql_error());
    print_r($GLOBALS['con']);
    return $GLOBALS['con'];

}*/
function connection()
{
    ini_set('memory_limit', '1024M');
    ini_set('allow_url_fopen', '1');
    $GLOBALS['con'] = mysql_connect('localhost', 'mylimo5_adminer', 'adminer123') or die(mysql_error());
    mysql_select_db('mylimo5_limo', $GLOBALS['con']);
    return $GLOBALS['con'];

}

/*****************************************************************
 * Method:             operations()
 * InputParameter:     query
 * Return:             operations
 *****************************************************************/
/*function operations($query)
{
    if ($query != null) {
        $conn = connection();
        $res = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($res) > 0) {
            $arr = array();
            while ($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }
            return $arr;

        } else {
            return TRUE;
        }

    }
}*/
function operations($sql)
{
    if ($sql != null) {
        $conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $result = $conn->query($sql);
        print_r($conn->error);
        if (isset($result->num_rows) && $result->num_rows > 0){
            $rv = array();

            while($row = $result->fetch_assoc()) {
                $rv[] = $row;
            }

            return $rv;
        }
        elseif (isset($conn->insert_id) && $conn->insert_id > 0){
            return $conn->insert_id;
        }
        elseif (isset($result->num_rows) && $result->num_rows == 0){
            return array();
        }
        else {
            return $result;
        }

    }
}
?>