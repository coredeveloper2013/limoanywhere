<?php

include_once 'config.php';
include_once 'comman.php';
// define('WP_MEMORY_LIMIT', '64M');



/* * *********************************************************
 * Method Name    	  : signUp
 * Description       : Sign Up By Company User
 * @Param            : all Param  
 * @return           : employee data json
 * ********************************************************* */

function vehicle_checkedFuntion() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query12 = "select isLAWCheck,limo_setup from company_user_info where id='" . $_REQUEST['user_id'] . "'";
        $queryResult = operations($query12);

        $result = global_message(200, 1002, $queryResult);
        return $result;
    }
}

function setUserSetUp() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $finalValue = ($_REQUEST['isCheckedValue'] != "no") ? 1 : 0;

        $isUserExist = "update company_user_info set isLAWCheck='" . $finalValue . "' where id='" . $_REQUEST['user_id'] . "'";


        $userLogin = operations($isUserExist);

        $result = global_message(200, 1002);
        return $result;
    }
}

function signUp() {

    if ((isset($_REQUEST['email']) && !empty($_REQUEST['email'])) && (isset($_REQUEST['name']) && !empty($_REQUEST['name'])) && (isset($_REQUEST['password']) && !empty($_REQUEST['password']))) {
        $email = $_REQUEST['email'];
        $userName = $_REQUEST['name'];
        $password = $_REQUEST['password'];
        $mobile_number = (isset($_REQUEST['mobile_number']) && !empty($_REQUEST['mobile_number'])) ? $_REQUEST['mobile_number'] : '';
        $company_name = (isset($_REQUEST['company_name']) && !empty($_REQUEST['company_name'])) ? $_REQUEST['company_name'] : '';

        $isUserExist = "select * from company_user_info where email='$email'";
        $userLogin = operations($isUserExist);


        if (count($userLogin) > 0 && gettype($userLogin) != "boolean") {

            $result = global_message(200, 1002);
        } else {


            $six_digit_random_number = mt_rand(100000, 999999);

            $query = 'insert into company_user_info(full_name,mobile_number,company_name,password,email,user_auth_key) values("' . $userName . '","' . $mobile_number . '","' . $company_name . '","' . $password . '","' . $email . '","' . $six_digit_random_number . '")';

            $resource = operations($query);
            $id = $resource;
            $selectInfo = "select * from company_user_info where id=$id";
            $resource = operations($selectInfo);
            $result = global_message(200, 1001, $resource);
        }
    } else {

        $result = global_message(201, 1003);
    }


    return $result;
}

function updateUserProfile() {

    if ((isset($_REQUEST['userId']) && !empty($_REQUEST['userId'])) && (isset($_REQUEST['UserName']) && !empty($_REQUEST['UserName'])) && (isset($_REQUEST['password']) && !empty($_REQUEST['password'])) && (isset($_REQUEST['mobileNumber']) && !empty($_REQUEST['mobileNumber'])) && (isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName'])) && (isset($_REQUEST['limo_setup']) && !empty($_REQUEST['limo_setup']))) {
        $name = $_REQUEST['UserName'];
        $password = $_REQUEST['password'];
        $mobileNumber = $_REQUEST['mobileNumber'];
        $companyName = $_REQUEST['companyName'];
        $limosetup = $_REQUEST['limo_setup'];
        $isalow_setup = $_REQUEST['isalow_setup'];
        $show_routing_map=$_REQUEST['show_routing_map'];
        $userID = $_REQUEST['userId'];
        $query1 = "UPDATE `company_user_info` SET  `full_name` = '" . $name . "', `mobile_number` = '" . $mobileNumber . "', `company_name` = '" . $companyName . "', `password` = '" . $password . "',`limo_setup`= '" . $limosetup . "' ,`isLAWCheck`= '" . $isalow_setup . "' ,`show_routing_map`= '" . $show_routing_map . "' WHERE `id` ='" . $userID . "'";
        $response = operations($query1);
        $selectInfo = "select * from company_user_info where id='" . $userID . "'";
        $resource = operations($selectInfo);
        $result = global_message(200, 1001, $resource);
    } else {

        $result = global_message(201, 1003);
    }


    return $result;
}

/* * *********************************************************
 * Method Name   : setDisclaimer
 * Description       : Insert add rate hourly
 * @Param            : vehicle Code and hour and rate  
 * @return            : employee data array object
 * ********************************************************* */

function setDisclaimer() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))
        && (isset($_REQUEST['ccDiclaimer']) && !empty($_REQUEST['ccDiclaimer']))
        && (isset($_REQUEST['get_cash_disclaimer']) && !empty($_REQUEST['get_cash_disclaimer']))
        && (isset($_REQUEST['reservAgreement']) && !empty($_REQUEST['reservAgreement']))) {

        $ccDiclaimer = $_REQUEST['ccDiclaimer'];
        $checkout_disclaimer = $_REQUEST['get_cash_disclaimer'];
        $reservAgreement = $_REQUEST['reservAgreement'];
        $userID = $_REQUEST['user_id'];
        $query1 = "insert into checkout_disclaimer(credit_card_disclaimer,cash_disclamer,reservation_agreement,user_id) values('" . $ccDiclaimer . "','" . $checkout_disclaimer . "','" . $reservAgreement . "','" . $userID . "')";
        $resource = operations($query1);
        $result = global_message(200, 1001, $resource);
    } else {

        $result = global_message(201, 1003);
    }


    return $result;
}

/* * *********************************************************
 * Method Name       : setDisclaimer
 * Description       : update disclaimer
 * @Param            : updated id
 * @return           : return the updated data
 * ********************************************************* */

function updateDisclaimer() {

    if ((isset($_REQUEST['updated_id']) && !empty($_REQUEST['updated_id'])) && (isset($_REQUEST['ccDiclaimer']) && !empty($_REQUEST['ccDiclaimer'])) && (isset($_REQUEST['reservAgreement']) && !empty($_REQUEST['reservAgreement']))) {

        $ccDiclaimer = $_REQUEST['ccDiclaimer'];
        $reservAgreement = $_REQUEST['reservAgreement'];
        $updated_id = $_REQUEST['updated_id'];
        $get_cash_disclaimer = $_REQUEST['get_cash_disclaimer'];

        $query1 = "UPDATE `checkout_disclaimer` SET  `credit_card_disclaimer` = '" . $ccDiclaimer . "',`cash_disclamer` = '" . $get_cash_disclaimer . "', `reservation_agreement` = '" . $reservAgreement . "' WHERE `id` ='" . $updated_id . "'";

        $response = operations($query1);
        $result = global_message(200, 1001, $resource);
    } else {

        $result = global_message(201, 1003);
    }


    return $result;
}

/* * *********************************************************
 * Method Name   : setHourlyRate
 * Description       : Insert add rate hourly
 * @Param            : vehicle Code and hour and rate  
 * @return            : employee data array object
 * ********************************************************* */

function getDisclaimer() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $query12 = "select * from checkout_disclaimer where user_id='" . $_REQUEST['user_id'] . "'";
        $queryResult = operations($query12);
        if ((count($queryResult)) > 0) {

            $result = global_message(200, 1002, $queryResult);
        } else {

            $result = global_message(200, 1003);
        }
    } else {

        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setHourlyRate
 * Description       : Insert add rate hourly
 * @Param            : vehicle Code and hour and rate  
 * @return            : employee data array object
 * ********************************************************* */

function setHourlyRate() {
    if ((isset($_REQUEST['cities_hourly']) && !empty($_REQUEST['cities_hourly'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['add_hours']) && !empty($_REQUEST['add_hours'])) && (isset($_REQUEST['occasion']) && !empty($_REQUEST['occasion'])) && (isset($_REQUEST['add_rate']) && !empty($_REQUEST['add_rate']))) {
        $VehicleCode = $_REQUEST['vehicle_code'];
        $addHour = $_REQUEST['add_hours'];
        $addOccasion = $_REQUEST['occasion'];
        $addOccasion = explode(",", $addOccasion);
        $addRate = $_REQUEST['add_rate'];
        $addPeakRate = $_REQUEST['add_peak_rate'];
        $userId = $_REQUEST['user_id'];
        $is_remaining = $_REQUEST['isRemaining'];
        $minimum_hour = (isset($_REQUEST['minimum_hour']) && !empty($_REQUEST['minimum_hour'])) ? $_REQUEST['minimum_hour'] : 0;
        $fromHourly = (isset($_REQUEST['fromHourly']) && !empty($_REQUEST['fromHourly'])) ? $_REQUEST['fromHourly'] : 0;
        $addSma = $_REQUEST['cities_hourly'];
        $addSma = explode(",", $addSma);


        $query = "insert into rate_calculate_hourly(vehicle_code,hourly_rate,peak_rate,from_hour,to_hour,user_id,is_remaining) value('" . $VehicleCode . "','" . $addRate . "','" . $addPeakRate . "','" . $fromHourly . "','" . $addHour . "','" . $userId . "','" . $is_remaining . "')";
        $resource = operations($query);
        $insertId = $resource;
        for ($i = 0; $i < count($addSma); $i++) {
            $getSma = explode("a@a", $addSma[$i]);
            $queryState = "insert into market_hourly(location_type,location_name,location_code,rate_id,minimum_rate,min_peak_hour) values('SMA','" . $getSma[1] . "','" . $getSma[0] . "','" . $insertId . "','" . $minimum_hour . "','" . $_REQUEST['min_peak_hour'] . "')";
            operations($queryState);
        }
        for ($j = 0; $j < count($addOccasion); $j++) {
            //$getSma=explode("a@a",$addSma);				 
            $queryOccasion = "insert into occasion_hourly(rate_id,occasion) values(" . $insertId . ",'" . $addOccasion[$j] . "')";
            operations($queryOccasion);
        }

        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : uploadCsvFile
 * Description       : Insert number of passsenger
 * @Param            : no_of_passenger vehicle_code  add_passenger_rate and add_multiselect_location_passenger  
 * @return            : json data
 * ********************************************************* */

function uploadCsvFile() {

    $csv_file = "csvFile/" . $_FILES["profile_pic"]["name"];
    $user_id = $_REQUEST['user_id'];
    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], "csvFile/" . $_FILES["profile_pic"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile, WP_MEMORY_LIMIT);
        $csv_array = explode(",", $csv_data[$i]);

        if ((isset($csv_array[0]) && !empty($csv_array[0])) && (isset($csv_array[1]) && !empty($csv_array[1])) && (isset($csv_array[2]) && !empty($csv_array[2])) && (isset($csv_array[3]) && !empty($csv_array[3])) && (isset($csv_array[4]) && !empty($csv_array[4])) && (isset($csv_array[5]) && !empty($csv_array[5])) && (isset($csv_array[6]) && !empty($csv_array[6])) && (isset($csv_array[7]) && !empty($csv_array[7])) && (isset($csv_array[8]) && !empty($csv_array[8])) && (isset($csv_array[9]) && !empty($csv_array[9])) && (isset($csv_array[10]) & !empty($csv_array[10])) && (isset($csv_array[11]) && !empty($csv_array[11]))) {

            $vehicle_code = $csv_array[0];
            $hourly_rate = $csv_array[1];
            $service_type = $csv_array[2];
            $from_hour = $csv_array[3];
            $to_hour = $csv_array[4];
            $location_type = $csv_array[5];
            $city_name = $csv_array[6];
            $city_code = $csv_array[7];
            $state_name = $csv_array[8];
            $state_code = $csv_array[9];
            $minimum_hrs = $csv_array[10];
            $zipcode = $csv_array[11];



            $query1 = "insert into rate_calculate_hourly(vehicle_code,hourly_rate,service_type,from_hour,to_hour,minimus_hour,user_id) values('" . $vehicle_code . "','" . $hourly_rate . "','" . $service_type . "','" . $from_hour . "','" . $to_hour . "','" . $minimus_hour . "','" . $user_id . "')";

            $insertId = operations($query1);
            if (strtolower($location_type) == "city") {
                $queryState = "insert into market_hourly(location_type,location_name,location_code,rate_id,minimum_rate) values('" . $location_type . "','" . $city_name . "','" . $city_code . "','" . $insertId . "','" . $minimum_hrs . "')";
                operations($queryState);
            } else {
                $queryState = "insert into market_hourly(location_type,location_name,location_code,rate_id,minimum_rate) values('" . $location_type . "','" . $state_name . "','" . $state_code . "','" . $insertId . "','" . $minimum_hrs . "')";
                operations($queryState);
            }




            $queryCity = "select * from city_information where city_name LIKE '%" . $city_name . "%' or city_abbr = '" . $city_code . "' and user_id= '" . $user_id . "'";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode) values('" . $city_name . "','" . $city_code . "','" . $state_name . "','" . $state_code . "','" . $zipcode . "')";
                print_r($query2);
                operations($query2);
            }
        }
        $i++;
    }

    fclose($csvfile);
    $result = global_message(200, 1003, $queryState);
    return $result;
}

/* * *********************************************************
 * Method Name   : uploadCsvFileMilege
 * Description       : Insert number of passsenger
 * @Param            : no_of_passenger vehicle_code  add_passenger_rate and add_multiselect_location_passenger  
 * @return            : json data
 * ********************************************************* */

function uploadCsvFileMilege() {

    $csv_file = "csvFile/" . $_FILES["profile_pic"]["name"];
    $user_id = $_REQUEST['user_id'];
    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], "csvFile/" . $_FILES["profile_pic"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile, WP_MEMORY_LIMIT);
        $csv_array = explode(",", $csv_data[$i]);

        if ((isset($csv_array[0]) && !empty($csv_array[0])) && (isset($csv_array[1]) && !empty($csv_array[1])) && (isset($csv_array[2]) && !empty($csv_array[2])) && (isset($csv_array[3]) && !empty($csv_array[3])) && (isset($csv_array[4]) && !empty($csv_array[4])) && (isset($csv_array[5]) && !empty($csv_array[5])) && (isset($csv_array[6]) && !empty($csv_array[6])) && (isset($csv_array[7]) && !empty($csv_array[7])) && (isset($csv_array[8]) && !empty($csv_array[8])) && (isset($csv_array[9]) && !empty($csv_array[9])) && (isset($csv_array[10]) & !empty($csv_array[10]))) {

            $vehicle_code = $csv_array[0];
            $rate = $csv_array[1];
            $from_mileage = $csv_array[2];
            $to_mileage = $csv_array[3];
            $location_type = $csv_array[4];
            $city_name = $csv_array[5];
            $city_code = $csv_array[6];
            $state_name = $csv_array[7];
            $state_code = $csv_array[8];
            $minimum_rate = $csv_array[9];
            $zip_code = $csv_array[10];




            $query1 = "insert into rate_calculate_mileage(vehicle_code,rate,from_mileage,to_mileage,minimum_mileage,user_id) values('" . $vehicle_code . "','" . $rate . "','" . $from_mileage . "','" . $to_mileage . "','" . $minimum_mileage . "','" . $user_id . "')";
            $insertId = operations($query1);

            if (strtolower($location_type) == "city") {
                $queryState = "insert into market_milege(location_type,location_name,location_code,rate_id,minimum_base_rate) values('" . $location_type . "','" . $city_name . "','" . $city_code . "','" . $insertId . "','" . $minimum_rate . "')";
                operations($queryState);
            } else {
                $queryState = "insert into market_milege(location_type,location_name,location_code,rate_id,minimum_base_rate) values('" . $location_type . "','" . $state_name . "','" . $state_code . "','" . $insertId . "','" . $minimum_rate . "')";
                operations($queryState);
            }




            $queryCity = "select * from city_information where city_name LIKE '%" . $city_name . "%' or city_abbr = '" . $city_code . "' and user_id= '" . $user_id . "'";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode) values('" . $city_name . "','" . $city_code . "','" . $state_name . "','" . $state_code . "','" . $zip_code . "')";
                print_r($query2);
                operations($query2);
            }
        }
        $i++;
    }


    fclose($csvfile);
    $result = global_message(200, 1003, $queryState);
    return $result;
}

/* * *********************************************************
 * Method Name   : uploadCsvPointToPointAirport
 * Description       : Upload Csv File for airport
 * @Param            : CSV FILE  
 * @return            : json data
 * ********************************************************* */

function uploadCsvPointToPointAirport() {

    $csv_file = "csvFile/" . $_FILES["profile_pic"]["name"];
    $user_id = $_REQUEST['user_id'];
    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], "csvFile/" . $_FILES["profile_pic"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile, WP_MEMORY_LIMIT);
        $csv_array = explode(",", $csv_data[$i]);

        if ((isset($csv_array[0]) && !empty($csv_array[0])) && (isset($csv_array[1]) && !empty($csv_array[1])) && ((isset($csv_array[2]) && !empty($csv_array[2])) || (isset($csv_array[3]) && !empty($csv_array[3]))) && (isset($csv_array[4]) && !empty($csv_array[4])) && (isset($csv_array[5]) && !empty($csv_array[5])) && (isset($csv_array[6]) && !empty($csv_array[6])) && (isset($csv_array[7]) && !empty($csv_array[7])) && (isset($csv_array[8]) && !empty($csv_array[8])) && (isset($csv_array[9]) && !empty($csv_array[9]))) {

            $vehicle_code = $csv_array[0];
            $rate = $csv_array[1];
            $from_airport = $csv_array[2];
            $airport_to = $csv_array[3];
            $state_name = $csv_array[4];
            $state_code = $csv_array[5];
            $city_name = $csv_array[6];
            $city_abbr = $csv_array[7];
            $zone_name = $csv_array[8];
            $zone_abbr = $csv_array[9];
            $zone_zip_code = $csv_array[10];
            $city_zip_code = $csv_array[11];

            $queryCity = "select * from city_information where city_name LIKE '%" . $city_name . "%' or city_abbr = '" . $city_abbr . "' and user_id= '" . $user_id . "' limit 1";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode,user_id) values('" . $city_name . "','" . $city_abbr . "','" . $state_name . "','" . $state_code . "','" . $city_zip_code . "','" . $user_id . "')";
                $lastInsertCityId = operations($query2);
            } else {
                $lastInsertCityId = $getcityResult[0]['id'];
            }

            $queryZone = "select * from zone_to_zone_rate where zone_name LIKE '%" . $zone_name . "%' or zone_abbr = '" . $zone_abbr . "' and user_id= '" . $user_id . "' limit 1";
            $queryZoneResult = operations($queryZone);

            if (gettype($queryZoneResult) == boolean) {
                $query2 = "insert into zone_to_zone_rate(zone_name,zone_abbr,city_name,city_code,user_id,zip_code) values('" . $zone_name . "','" . $zone_abbr . "','" . $city_name . "','" . $city_abbr . "','" . $user_id . "','" . $zone_zip_code . "')";
                $lastInsertZoneId = operations($query2);
            } else {
                $lastInsertZoneId = $queryZoneResult[0]['id'];
            }

            $query2 = "insert into ptp_airport(airport_from,airport_to,city_id,zone_id,rate,user_id,car_code) values('" . $from_airport . "','" . $airport_to . "','" . $lastInsertCityId . "','" . $lastInsertZoneId . "','" . $rate . "','" . $user_id . "','" . $vehicle_code . "')";
            operations($query2);
        }
        $i++;
    }


    fclose($csvfile);
    $result = global_message(200, 1003, $queryState);
    return $result;
}

/* * *********************************************************
 * Method Name   : uploadCsvPointToPointSeaPort
 * Description       : Upload Csv File for seaport
 * @Param            : CSV FILE  
 * @return            : json data
 * ********************************************************* */

function uploadCsvPointToPointSeaPort() {

    $csv_file = "csvFile/" . $_FILES["profile_pic"]["name"];
    $user_id = $_REQUEST['user_id'];
    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], "csvFile/" . $_FILES["profile_pic"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile, WP_MEMORY_LIMIT);
        $csv_array = explode(",", $csv_data[$i]);

        if ((isset($csv_array[0]) && !empty($csv_array[0])) && (isset($csv_array[1]) && !empty($csv_array[1])) && ((isset($csv_array[2]) && !empty($csv_array[2])) || (isset($csv_array[3]) && !empty($csv_array[3]))) && (isset($csv_array[4]) && !empty($csv_array[4])) && (isset($csv_array[5]) && !empty($csv_array[5])) && (isset($csv_array[6]) && !empty($csv_array[6])) && (isset($csv_array[7]) && !empty($csv_array[7])) && (isset($csv_array[8]) && !empty($csv_array[8])) && (isset($csv_array[9]) && !empty($csv_array[9]))) {

            $vehicle_code = $csv_array[0];
            $rate = $csv_array[1];
            $from_airport = $csv_array[2];
            $airport_to = $csv_array[3];
            $state_name = $csv_array[4];
            $state_code = $csv_array[5];
            $city_name = $csv_array[6];
            $city_abbr = $csv_array[7];
            $zone_name = $csv_array[8];
            $zone_abbr = $csv_array[9];
            $zone_zip_code = $csv_array[10];
            $city_zip_code = $csv_array[11];

            $queryCity = "select * from city_information where city_name LIKE '%" . $city_name . "%' or city_abbr = '" . $city_abbr . "' and user_id= '" . $user_id . "' limit 1";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode,user_id) values('" . $city_name . "','" . $city_abbr . "','" . $state_name . "','" . $state_code . "','" . $city_zip_code . "','" . $user_id . "')";
                $lastInsertCityId = operations($query2);
            } else {
                $lastInsertCityId = $getcityResult[0]['id'];
            }

            $queryZone = "select * from zone_to_zone_rate where zone_name LIKE '%" . $zone_name . "%' or zone_abbr = '" . $zone_abbr . "' and user_id= '" . $user_id . "' limit 1";
            $queryZoneResult = operations($queryZone);

            if (gettype($queryZoneResult) == boolean) {
                $query2 = "insert into zone_to_zone_rate(zone_name,zone_abbr,city_name,city_code,user_id,zip_code) values('" . $zone_name . "','" . $zone_abbr . "','" . $city_name . "','" . $city_abbr . "','" . $user_id . "','" . $zone_zip_code . "')";
                $lastInsertZoneId = operations($query2);
            } else {
                $lastInsertZoneId = $queryZoneResult[0]['id'];
            }

            $query2 = "insert into ptp_seaport(seaport_from,seaport_to,city_id,zone_id,rate,user_id,car_code) values('" . $from_airport . "','" . $airport_to . "','" . $lastInsertCityId . "','" . $lastInsertZoneId . "','" . $rate . "','" . $user_id . "','" . $vehicle_code . "')";
            operations($query2);
        }
        $i++;
    }


    fclose($csvfile);
    $result = global_message(200, 1003, $queryState);
    return $result;
}

/* * *********************************************************
 * Method Name   : uploadCsvPointToPointZone
 * Description       : insert zone to zone by csv file
 * @Param            :CSV file

 * ********************************************************* */

function uploadCsvPointToPointZone() {

    $csv_file = "csvFile/" . $_FILES["profile_pic"]["name"];
    $user_id = $_REQUEST['user_id'];
    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], "csvFile/" . $_FILES["profile_pic"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile);
        $csv_array = explode(",", $csv_data[$i]);


        if ((isset($csv_array[0]) && !empty($csv_array[0])) && (isset($csv_array[1]) && !empty($csv_array[1])) && ((isset($csv_array[2]) && !empty($csv_array[2])) || (isset($csv_array[3]) && !empty($csv_array[3]))) && (isset($csv_array[4]) && !empty($csv_array[4])) && (isset($csv_array[5]) && !empty($csv_array[5])) && (isset($csv_array[6]) && !empty($csv_array[6])) && (isset($csv_array[7]) && !empty($csv_array[7])) && (isset($csv_array[8]) && !empty($csv_array[8])) && (isset($csv_array[9]) && !empty($csv_array[9])) && (isset($csv_array[10]) && !empty($csv_array[10])) && (isset($csv_array[11]) && !empty($csv_array[11])) && (isset($csv_array[12]) && !empty($csv_array[12])) && (isset($csv_array[13]) && !empty($csv_array[13])) && (isset($csv_array[14]) && !empty($csv_array[14])) && (isset($csv_array[15]) && !empty($csv_array[15])) && (isset($csv_array[16]) && !empty($csv_array[16])) && (isset($csv_array[17]) && !empty($csv_array[17]))) {

            $vehicle_code = $csv_array[0];
            $rate = $csv_array[1];
            $from_state_name = $csv_array[2];
            $from_state_code = $csv_array[3];
            $from_city_name = $csv_array[4];
            $from_city_code = $csv_array[5];
            $from_city_zip_code = $csv_array[6];
            $from_zone_name = $csv_array[7];
            $from_zone_code = $csv_array[8];
            $from_zone_zip_code = $csv_array[9];
            $to_state_name = $csv_array[10];
            $to_state_code = $csv_array[11];
            $to_city_name = $csv_array[12];
            $to_city_code = $csv_array[13];
            $to_city_zip_code = $csv_array[14];
            $to_zone_name = $csv_array[15];
            $to_zone_code = $csv_array[16];
            $to_zone_zip_code = $csv_array[17];


            $queryCity = "select * from city_information where city_name LIKE '%" . $from_city_name . "%' or city_abbr = '" . $from_city_code . "' and user_id= '" . $user_id . "' limit 1";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode,user_id) values('" . $from_city_name . "','" . $from_city_code . "','" . $from_state_name . "','" . $from_state_code . "','" . $from_city_zip_code . "','" . $user_id . "')";
                $lastInsertFromCityId = operations($query2);
            } else {
                $lastInsertFromCityId = $getcityResult[0]['id'];
            }

            $queryZone = "select * from zone_to_zone_rate where zone_name LIKE '%" . $from_zone_name . "%' or zone_abbr = '" . $from_zone_code . "' and user_id= '" . $user_id . "' limit 1";
            $queryZoneResult = operations($queryZone);

            if (gettype($queryZoneResult) == boolean) {
                $query2 = "insert into zone_to_zone_rate(zone_name,zone_abbr,city_name,city_code,user_id,zip_code) values('" . $from_zone_name . "','" . $from_zone_code . "','" . $from_city_name . "','" . $from_city_code . "','" . $user_id . "','" . $from_zone_zip_code . "')";
                $lastInsertFromZoneId = operations($query2);
            } else {
                $lastInsertFromZoneId = $queryZoneResult[0]['id'];
            }

            /* to State And City And zone Functionality is    */

            $queryCity = "select * from city_information where city_name LIKE '%" . $to_city_name . "%' or city_abbr = '" . $to_city_code . "' and user_id= '" . $user_id . "' limit 1";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode,user_id) values('" . $to_city_name . "','" . $to_city_code . "','" . $to_state_name . "','" . $to_state_code . "','" . $to_city_zip_code . "','" . $user_id . "')";
                $lastInsertToCityId = operations($query2);
            } else {
                $lastInsertToCityId = $getcityResult[0]['id'];
            }

            $queryZone = "select * from zone_to_zone_rate where zone_name LIKE '%" . $to_zone_name . "%' or zone_abbr = '" . $to_zone_code . "' and user_id= '" . $user_id . "' limit 1";
            $queryZoneResult = operations($queryZone);

            if (gettype($queryZoneResult) == boolean) {
                $query2 = "insert into zone_to_zone_rate(zone_name,zone_abbr,city_name,city_code,user_id,zip_code) values('" . $to_zone_name . "','" . $to_zone_code . "','" . $to_city_name . "','" . $to_city_code . "','" . $user_id . "','" . $to_zone_zip_code . "')";
                $lastInsertToZoneId = operations($query2);
            } else {
                $lastInsertToZoneId = $queryZoneResult[0]['id'];
            }






            $query2 = "insert into ptp_zone_to_zone(from_city,from_zone,to_city,to_zone,rate,user_id,car_code) values('" . $lastInsertFromCityId . "','" . $lastInsertFromZoneId . "','" . $lastInsertToCityId . "','" . $lastInsertToZoneId . "','" . $rate . "','" . $user_id . "','" . $vehicle_code . "')";
            operations($query2);
        }
        $i++;
    }


    fclose($csvfile);
    $result = global_message(200, 1003, $queryState);
    return $result;
}

/* * *********************************************************
 * Method Name   : uploadCsvFilePerPeasonger
 * Description       : insert perpesasonger information by csv file
 * @Param            :CSV file

 * ********************************************************* */

function uploadCsvFilePerPeasonger() {

    $csv_file = "csvFile/" . $_FILES["profile_pic"]["name"];
    $user_id = $_REQUEST['user_id'];
    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], "csvFile/" . $_FILES["profile_pic"]["name"]);
    $csvfile = fopen($csv_file, 'r');
    $theData = fgets($csvfile);
    $i = 0;

    while (!feof($csvfile)) {
        $csv_data[] = fgets($csvfile);
        $csv_array = explode(",", $csv_data[$i]);

        if ((isset($csv_array[0]) && !empty($csv_array[0])) && (isset($csv_array[1]) && !empty($csv_array[1])) && ((isset($csv_array[2]) && !empty($csv_array[2])) || (isset($csv_array[3]) && !empty($csv_array[3]))) && (isset($csv_array[4]) && !empty($csv_array[4])) && (isset($csv_array[5]) && !empty($csv_array[5])) && (isset($csv_array[6]) && !empty($csv_array[6]))) {

            $vehicle_code = $csv_array[0];
            $rate = $csv_array[1];
            $location_type = $csv_array[2];
            $state_name = $csv_array[3];
            $state_code = $csv_array[4];
            $city_name = $csv_array[5];
            $city_code = $csv_array[6];
            $city_zip_code = $csv_array[7];


            $queryCity = "select * from city_information where city_name LIKE '%" . $city_name . "%' or city_abbr = '" . $city_code . "' and user_id= '" . $user_id . "' limit 1";
            $getcityResult = operations($queryCity);

            if (gettype($getcityResult) == boolean) {

                $query2 = "insert into city_information(city_name,city_abbr,state_name,state_code,zipcode,user_id) values('" . $city_name . "','" . $city_code . "','" . $state_name . "','" . $state_code . "','" . $city_zip_code . "','" . $user_id . "')";
                operations($query2);
            }


            $query2 = "insert into passenger_rate(no_of_passenger,rate,user_id,vehicle_code) values('1','" . $rate . "','" . $user_id . "','" . $vehicle_code . "')";
            $lastInsertId = operations($query2);



            if (strtolower($location_type) == "city") {
                $queryState = "insert into market_passenger(location_type,location_name,location_code,rate_id) values('" . $location_type . "','" . $city_name . "','" . $city_code . "','" . $lastInsertId . "')";
                operations($queryState);
            } else {
                $queryState = "insert into market_passenger(location_type,location_name,location_code,rate_id) values('" . $location_type . "','" . $state_name . "','" . $state_code . "','" . $lastInsertId . "')";
                operations($queryState);
            }
        }
        $i++;
    }


    fclose($csvfile);
    $result = global_message(200, 1003, $queryState);
    return $result;
}

/* * *********************************************************
 * Method Name   : setPassengerRate
 * Description       : Insert number of passsenger
 * @Param            : no_of_passenger vehicle_code  add_passenger_rate and add_multiselect_location_passenger  
 * @return            : json data
 * ********************************************************* */

function setPassengerRate() {



    if ((isset($_REQUEST['no_of_passenger']) && !empty($_REQUEST['no_of_passenger'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['add_passenger_rate']) && !empty($_REQUEST['add_passenger_rate'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['add_multiselect_location_passenger']) && !empty($_REQUEST['add_multiselect_location_passenger']))) {

        $noOfPassenger = $_REQUEST['no_of_passenger'];

        $addPassengerRate = $_REQUEST['add_passenger_rate'];
        $addMultiselectLocationPassenger = $_REQUEST['add_multiselect_location_passenger'];
        $vehicle_code = $_REQUEST['vehicle_code'];
        $userId = $_REQUEST['user_id'];

        $addCities = explode(",", $addMultiselectLocationPassenger);




        $query = "insert into passenger_rate(no_of_passenger,rate,user_id,vehicle_code) value('" . $noOfPassenger . "','" . $addPassengerRate . "','" . $userId . "','" . $vehicle_code . "')";
        $resource = operations($query);

        $insertId = $resource;

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);

            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);

                $queryState = "insert into market_passenger(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $insertId . "')";
                operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_passenger(location_type,location_name,location_code,rate_id) values('state','" . $getCities[2] . "','" . $getCities[1] . "','" . $insertId . "')";
                operations($queryState);
            }
        }
        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setMilesRate
 * Description       : Insert number of market_milege
 * @Param            : add_miles_cities vehicle_code  user_id and add_miles_rate  
 * @return            : json data
 * ********************************************************* */

function setMilesRate() {



    if ((isset($_REQUEST['add_miles_cities']) && !empty($_REQUEST['add_miles_cities'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['add_miles']) && !empty($_REQUEST['add_miles'])) && (isset($_REQUEST['add_miles_rate']) && !empty($_REQUEST['add_miles_rate']))) {
        $VehicleCode = $_REQUEST['vehicle_code'];
        $addCities = $_REQUEST['add_miles_cities'];
        $addMiles = $_REQUEST['add_miles'];
        $addMilesRate = $_REQUEST['add_miles_rate'];
        $userId = $_REQUEST['user_id'];
        $minimum_base_rate = (isset($_REQUEST['minimum_base_rate']) && !empty($_REQUEST['minimum_base_rate'])) ? $_REQUEST['minimum_base_rate'] : 0;


        $fromMiles = (isset($_REQUEST['fromMiles']) && !empty($_REQUEST['fromMiles'])) ? $_REQUEST['fromMiles'] : 0;
        $addCities = explode(",", $addCities);
        $query = "insert into rate_calculate_mileage(vehicle_code,rate,from_mileage,to_mileage,user_id) value('" . $VehicleCode . "','" . $addMilesRate . "','" . $fromMiles . "','" . $addMiles . "','" . $userId . "')";
        $resource = operations($query);
        $insertId = $resource;

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);
            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);

                $queryState = "insert into market_milege(location_type,location_name,location_code,rate_id,minimum_base_rate) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $insertId . "','" . $minimum_base_rate . "')";
                operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_milege(location_type,location_name,location_code,rate_id,minimum_base_rate) values('state','" . $getCities[2] . "','" . $getCities[1] . "','" . $insertId . "','" . $minimum_base_rate . "')";
                operations($queryState);
            }
        }




        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setMinimumHourly
 * Description       : update rate_calculate_hourly
 * @Param            : 
 * @return            : json data
 * ********************************************************* */

function setMinimumHourly() {



    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['minimum_hour']) && !empty($_REQUEST['minimum_hour']))) {
        $VehicleCode = $_REQUEST['vehicle_code'];
        $userId = $_REQUEST['user_id'];
        $minimumHour = $_REQUEST['minimum_hour'];
        $query = "update rate_calculate_hourly set minimus_hour='" . $minimumHour . "' where  user_id='" . $userId . "' and vehicle_code='" . $VehicleCode . "'";
        $resource = operations($query);
        $result = global_message(200, 1009);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setMinimumMiles
 * Description       : update rate_calculate_mileage
 * @Param            : user_id vehicle_code minimum_mileage
 * @return            : json data
 * ********************************************************* */

function setMinimumMiles() {



    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['minimum_mileage']) && !empty($_REQUEST['minimum_mileage']))) {
        $VehicleCode = $_REQUEST['vehicle_code'];
        $userId = $_REQUEST['user_id'];
        $minimumMileage = $_REQUEST['minimum_mileage'];
        $query = "update rate_calculate_mileage set minimum_mileage='" . $minimumMileage . "' where  user_id='" . $userId . "' and vehicle_code='" . $VehicleCode . "'";
        $resource = operations($query);
        $result = global_message(200, 1009);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPointToPointAirportRate
 * Description       : insert into ptp_airport
 * @Param            : getPointToPointRate getPointToPointToll getPointToPointVehicleCode
 * @return            : json data
 * ********************************************************* */

function setPointToPointAirportRate() {



    if ((isset($_REQUEST['getPointToPointRate']) && !empty($_REQUEST['getPointToPointRate'])) && (isset($_REQUEST['getPointToPointToll']) && !empty($_REQUEST['getPointToPointToll'])) && (isset($_REQUEST['getPointToPointVehicleCode']) && !empty($_REQUEST['getPointToPointVehicleCode'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $vichleCodeArray = array();
        $insertId = '';



        $getPointToPointFrom = preg_replace("/'/", '', $_REQUEST['getPointToPointFrom']);
        $getPointToPointto = preg_replace("/'/", '', $_REQUEST['getPointToPointto']);

        $from = (isset($_REQUEST['getPointToPointFrom']) && !empty($_REQUEST['getPointToPointFrom'])) ? $getPointToPointFrom : '';
        $to = (isset($_REQUEST['getPointToPointto']) && !empty($_REQUEST['getPointToPointto'])) ? $getPointToPointto : '';



        $rate = $_REQUEST['getPointToPointRate'];
        $toll = $_REQUEST['getPointToPointToll'];
        $vehicleCode = $_REQUEST['getPointToPointVehicleCode'];
        $toAirport = $_REQUEST['toAirport'];
        $cityId = $_REQUEST['add_airport_rate_city'];
        $zoneId = $_REQUEST['add_airport_rate_zone'];

        $userId = $_REQUEST['user_id'];
        $vichleCodeArray = explode(",", $vehicleCode);

        for ($i = 0; $i < count($vichleCodeArray); $i++) {
            $query = "insert into ptp_airport(airport_from,airport_to,city_id,zone_id,rate,airport_toll,user_id,car_code,is_to_airport) value('" . $from . "','" . $to . "','" . $cityId . "','" . $zoneId . "','" . $rate . "','" . $toll . "','" . $userId . "','" . $vichleCodeArray[$i] . "','" . $toAirport . "')";

            $resource = operations($query);
            $insertId .= $resource. "__";
        }

        $result = global_message(200, 1008, $insertId);
        return $result;
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPointToPointZone
 * Description       : insert into ptp_zone_to_zone
 * @Param            : getPointRate getPointToll getPointVehicleCode user_id
 * @return            : json data
 * ********************************************************* */

function setPointToPointZone() {


    if ((isset($_REQUEST['getPointRate']) && !empty($_REQUEST['getPointRate'])) && (isset($_REQUEST['getPointToll']) && !empty($_REQUEST['getPointToll'])) && (isset($_REQUEST['getPointVehicleCode']) && !empty($_REQUEST['getPointVehicleCode'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $vichleCodeArray = array();
        $insertId = '';
        $from_city = (isset($_REQUEST['from_city']) && !empty($_REQUEST['from_city'])) ? $_REQUEST['from_city'] : "";
        $from_zone = (isset($_REQUEST['from_zone']) && !empty($_REQUEST['from_zone'])) ? $_REQUEST['from_zone'] : "";



        $to_city = (isset($_REQUEST['to_city']) && !empty($_REQUEST['to_city'])) ? $_REQUEST['to_city'] : "";
        $to_zone = (isset($_REQUEST['to_zone']) && !empty($_REQUEST['to_zone'])) ? $_REQUEST['to_zone'] : "";


        $rate = $_REQUEST['getPointRate'];
        $toll = $_REQUEST['getPointToll'];
        $vehicleCode = $_REQUEST['getPointVehicleCode'];


        $userId = $_REQUEST['user_id'];
        $vichleCodeArray = explode(",", $vehicleCode);

        for ($i = 0; $i < count($vichleCodeArray); $i++) {
            $query = "insert into ptp_zone_to_zone(from_city,from_zone,to_city,to_zone,rate,user_id,car_code,zone_toll) value('" . $from_city . "','" . $from_zone . "','" . $to_city . "','" . $to_zone . "','" . $rate . "','" . $userId . "','" . $vichleCodeArray[$i] . "','" . $toll . "')";
            $resource = operations($query);
            $insertId .= $resource. "__";
        }





        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPointToPointSeaPortRate
 * Description       : insert into ptp_zone_to_zone
 * @Param            : getPointRate getPointToll getPointVehicleCode user_id
 * @return            : json data
 * ********************************************************* */

function setPointToPointSeaPortRate() {


    if ((isset($_REQUEST['getPointToPointRate']) && !empty($_REQUEST['getPointToPointRate'])) && (isset($_REQUEST['getPointToPointToll']) && !empty($_REQUEST['getPointToPointToll'])) && (isset($_REQUEST['getPointToPointVehicleCode']) && !empty($_REQUEST['getPointToPointVehicleCode'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {






        $from = (isset($_REQUEST['getPointToPointFrom']) && !empty($_REQUEST['getPointToPointFrom'])) ? $_REQUEST['getPointToPointFrom'] : "";
        $to = (isset($_REQUEST['getPointToPointto']) && !empty($_REQUEST['getPointToPointto'])) ? $_REQUEST['getPointToPointto'] : "";
        $rate = $_REQUEST['getPointToPointRate'];
        $toll = $_REQUEST['getPointToPointToll'];
        $vehicleCode = $_REQUEST['getPointToPointVehicleCode'];


        $add_seaport_city_to = $_REQUEST['add_seaport_city_to'];

        $add_airport_zone_to = $_REQUEST['add_airport_zone_to'];
        $toSeaport = $_REQUEST['toSeaport'];
        $userId = $_REQUEST['user_id'];
        $insertId = '';
        $vichleCodeArray = explode(",", $vehicleCode);
        for ($i = 0; $i < count($vichleCodeArray); $i++) {
            $query = "insert into ptp_seaport(seaport_from,seaport_to,city_id,zone_id,rate,user_id,car_code,seaport_toll,is_to_seaport) value('" . $from . "','" . $to . "','" . $add_seaport_city_to . "','" . $add_airport_zone_to . "','" . $rate . "','" . $userId . "','" . $vichleCodeArray[$i] . "','" . $toll . "','" . $toSeaport . "')";
            $resource = operations($query);

            $insertId .= $resource. "__";
        }
        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPChildExtraRate
 * Description       : insert into extra_child
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setPChildExtraRate() {


    if ((isset($_REQUEST['applylocationExtraChild']) && !empty($_REQUEST['applylocationExtraChild'])) && (isset($_REQUEST['Numberofseats']) && !empty($_REQUEST['Numberofseats'])) && (isset($_REQUEST['Fixed_rate_child']) && !empty($_REQUEST['Fixed_rate_child'])) && (isset($_REQUEST['applyVehicleExtraChild']) && !empty($_REQUEST['applyVehicleExtraChild'])) && (isset($_REQUEST['applyServiceExtraChild']) && !empty($_REQUEST['applyServiceExtraChild'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {



        $Numberofseats = $_REQUEST['Numberofseats'];
        $FixedRateChild = $_REQUEST['Fixed_rate_child'];
        $applyVehicleExtraChild = $_REQUEST['applyVehicleExtraChild'];
        $applyServiceExtraChild = $_REQUEST['applyServiceExtraChild'];
        $userId = $_REQUEST['user_id'];
        $addCities = $_REQUEST['applylocationExtraChild'];
        $insertId = '';
        $applyVehicleExtraChildArray = explode(",", $applyVehicleExtraChild);
        $applyServiceExtraChildArray = explode(",", $applyServiceExtraChild);
        $addCities = explode(",", $addCities);

        $query2 = "SELECT MAX(state_comman_id) AS article FROM extra_child";

        $resourceGetMaxId = operations($query2);

        $maxValueInsertedInTable = $resourceGetMaxId[0]['article'] + 1;



        for ($i = 0; $i < count($applyVehicleExtraChildArray); $i++) {
            for ($j = 0; $j < count($applyVehicleExtraChildArray); $j++) {
                $query = "insert into extra_child(name,seat,rate,vehicle_code,service_code,state_comman_id,user_id) value('Child Seats','" . $Numberofseats . "','" . $FixedRateChild . "','" . $applyVehicleExtraChildArray[$i] . "','" . $applyServiceExtraChildArray[$j] . "','" . $maxValueInsertedInTable . "','" . $userId . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);

            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);

                $queryState = "insert into market_child_seat(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $maxValueInsertedInTable . "')";

                operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_child_seat(location_type,location_name,location_code,rate_id) values('state','" . $getCities[2] . "','" . $getCities[1] . "','" . $maxValueInsertedInTable . "')";

                operations($queryState);
            }
        }


        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPointToPointFixedRate
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setPointToPointFixedRate() {


    if ((isset($_REQUEST['apply_location_fix_rate']) && !empty($_REQUEST['apply_location_fix_rate'])) && (isset($_REQUEST['addFixRate']) && !empty($_REQUEST['addFixRate'])) && (isset($_REQUEST['amountFixRate']) && !empty($_REQUEST['amountFixRate'])) && (isset($_REQUEST['applyVihecleFixRate']) && !empty($_REQUEST['applyVihecleFixRate'])) && (isset($_REQUEST['applyServiceFixRate']) && !empty($_REQUEST['applyServiceFixRate'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {



        $addFixRate = $_REQUEST['addFixRate'];
        $amountFixRate = $_REQUEST['amountFixRate'];
        $applyVihecleFixRate = $_REQUEST['applyVihecleFixRate'];
        $applyServiceFixRate = $_REQUEST['applyServiceFixRate'];
        $userId = $_REQUEST['user_id'];
        $addCities = $_REQUEST['apply_location_fix_rate'];
        $insertId = '';
        $applyVihecleFixRateArray = explode(",", $applyVihecleFixRate);
        $applyServiceFixRateArray = explode(",", $applyServiceFixRate);
        $addCities = explode(",", $addCities);

        $query2 = "SELECT MAX(state_comman_id) AS article FROM add_fixed_rate;";

        $resourceGetMaxId = operations($query2);
        $maxValueInsertedInTable = $resourceGetMaxId[0]['article'] + 1;
        for ($i = 0; $i < count($applyVihecleFixRateArray); $i++) {
            for ($j = 0; $j < count($applyServiceFixRateArray); $j++) {
                $query = "insert into add_fixed_rate(name,rate,vechicle_code,service_type,state_comman_id,user_id) value('" . $addFixRate . "','" . $amountFixRate . "','" . $applyVihecleFixRateArray[$i] . "','" . $applyServiceFixRateArray[$j] . "','" . $maxValueInsertedInTable . "','" . $userId . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);
            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);

                $queryState = "insert into market_fix_rate(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $maxValueInsertedInTable . "')";
                operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_fix_rate(location_type,location_name,location_code,rate_id) values('state','" . $getCities[2] . "','" . $getCities[1] . "','" . $maxValueInsertedInTable . "')";
                operations($queryState);
            }
        }


        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPointToPointBaseRate
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setPointToPointBaseRate() {


    if ((isset($_REQUEST['addBaseRateName']) && !empty($_REQUEST['addBaseRateName'])) && (isset($_REQUEST['addBaseRateAmount']) && !empty($_REQUEST['addBaseRateAmount'])) && (isset($_REQUEST['addBaseRateVehicleName']) && !empty($_REQUEST['addBaseRateVehicleName'])) && (isset($_REQUEST['addBaseRateServiceName']) && !empty($_REQUEST['addBaseRateServiceName'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {





        $addBaseRateName = $_REQUEST['addBaseRateName'];
        $addBaseRateAmount = $_REQUEST['addBaseRateAmount'];
        $addBaseRateVehicleName = $_REQUEST['addBaseRateVehicleName'];
        $addBaseRateServiceName = $_REQUEST['addBaseRateServiceName'];
        $addCities = $_REQUEST['add_base_rate_Location_name'];
        $userId = $_REQUEST['user_id'];
        $insertId = '';
        $addBaseRateVehicleNameArray = explode(",", $addBaseRateVehicleName);
        $addBaseRateServiceNameArray = explode(",", $addBaseRateServiceName);
        $addCities = explode(",", $addCities);

        $query2 = "SELECT MAX(state_comman_id) AS article FROM add_base_rate;";
        $resourceGetMaxId = operations($query2);

        $maxValueInsertedInTable = $resourceGetMaxId[0]['article'] + 1;
        for ($i = 0; $i < count($addBaseRateVehicleNameArray); $i++) {
            for ($j = 0; $j < count($addBaseRateServiceNameArray); $j++) {
                $query = "insert into add_base_rate(name,rate,vehicle_code,service_type,user_id,state_comman_id) value('" . $addBaseRateName . "','" . $addBaseRateAmount . "','" . $addBaseRateVehicleNameArray[$i] . "','" . $addBaseRateServiceNameArray[$j] . "','" . $userId . "','" . $maxValueInsertedInTable . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }


        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);
            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);

                $queryState = "insert into market_base_rate(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $maxValueInsertedInTable . "')";
                operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_base_rate(location_type,location_name,location_code,rate_id) values('state','" . $getCities[2] . "','" . $getCities[1] . "','" . $maxValueInsertedInTable . "')";
                operations($queryState);
            }
        }



        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setPointToPointIncrementalRate
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setPointToPointIncrementalRate() {




    if ((isset($_REQUEST['incrementalRateName']) && !empty($_REQUEST['incrementalRateName'])) && (isset($_REQUEST['incrementalRateMinAmount']) && !empty($_REQUEST['incrementalRateMinAmount'])) && (isset($_REQUEST['incrementalRateMaxAmount']) && !empty($_REQUEST['incrementalRateMaxAmount'])) && (isset($_REQUEST['incrementalRateVehicle']) && !empty($_REQUEST['incrementalRateVehicle'])) && (isset($_REQUEST['incrementalRateService']) && !empty($_REQUEST['incrementalRateService'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {



        $incrementalRateName = $_REQUEST['incrementalRateName'];
        $incrementalRateMinAmount = $_REQUEST['incrementalRateMinAmount'];
        $incrementalRateMaxAmount = $_REQUEST['incrementalRateMaxAmount'];



        $incrementalRateVehicle = $_REQUEST['incrementalRateVehicle'];
        $incrementalRateService = $_REQUEST['incrementalRateService'];
        $userId = $_REQUEST['user_id'];
        $insertId = '';



        $incrementalRateVehicleArray = explode(",", $incrementalRateVehicle);
        $incrementalRateServiceArray = explode(",", $incrementalRateService);


        for ($i = 0; $i < count($incrementalRateVehicleArray); $i++) {
            for ($j = 0; $j < count($incrementalRateServiceArray); $j++) {
                $query = "insert into add_incremental_rate(name,percentage_minimum,percentage_maximum,vehicle_code,service_type,user_id) value('" . $incrementalRateName . "','" . $incrementalRateMinAmount . "','" . $incrementalRateMaxAmount . "','" . $incrementalRateVehicleArray[$i] . "','" . $incrementalRateServiceArray[$j] . "','" . $userId . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }
        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : setCitiesState
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setCitiesState() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['zipcode']) && !empty($_REQUEST['zipcode'])) && (isset($_REQUEST['cityName']) && !empty($_REQUEST['cityName'])) && (isset($_REQUEST['stateName']) && !empty($_REQUEST['stateName'])) && (isset($_REQUEST['stateCode']) && !empty($_REQUEST['stateCode']))) {



        $cityName = $_REQUEST['cityName'];
        $lat_longName = $_REQUEST['lat_longName'];
        $stateName = $_REQUEST['stateName'];
        $country_id = $_REQUEST['country_id'];

        $stateCode = $_REQUEST['stateCode'];
        $county_id = $_REQUEST['county_id'];
        $user_id = $_REQUEST['user_id'];
        $zipcode = $_REQUEST['zipcode'];

        $zipcode1 = explode(",", $zipcode);


        /*  $query2="select sc.id,ss.sma_country_id from sma_county sc,sma_state ss where  sc.state_id='".$stateCode."' and ss.id='".$stateCode."' and ss.user_id='".$user_id."' limit 1"; */


        //$query2="select sc.id,ss.sma_country_id from sma_county sc,sma_state ss where  sc.state_id='".$stateCode."' and ss.id='".$stateCode."' and ss.user_id='".$user_id."' and sc.user_id='".$user_id."' limit 1";
        //$result2 = operations($query2);  
        //print_r($result2[0]['sma_country_id']);
        //   $query="insert into sma_city(city_name,county_id,postal_code,state_id,country_id,lat_log_name) values('".$cityName."'
        // ,'".$result2[0]['id']."','".$zipcode."','".$stateCode."','".$result2[0]['sma_country_id']."','".$lat_longName."')";
        for ($i = 0; $i < count($zipcode1); $i++) {

            $query = "insert into sma_city(city_name,county_id,postal_code,state_id,country_id,user_id,lat_log_name) values('" . $cityName . "'
		   ,'" . $county_id . "','" . $zipcode1[$i] . "','" . $stateCode . "','" . $country_id . "','" . $user_id . "', '')";

            $resource = operations($query);
        }





        $insertId = $resource;
        $result = global_message(200, 1011, $insertId);
    } else {


        $result = global_message(201, 1003);
    }
    return $result;
}

function setRestrictedCarCode() {



    if ((isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['city_code']) && !empty($_REQUEST['city_code']))) {


        $vehicleCode = $_REQUEST['vehicle_code'];
        $cityCode = $_REQUEST['city_code'];

        $userId = $_REQUEST['user_id'];
        $cityCodeArray = explode(",", $cityCode);
        $cityFinalArray = array();

        if (gettype($cityCodeArray) == "array") {

            for ($i = 0; $i < count($cityCodeArray); $i++) {
                $cityFinalArray = explode("_", $cityCodeArray[$i]);
                if ($cityFinalArray[0] != "city") {
                    $query = "insert into restricted_car_code(vehicle_code,user_id,loc_type,loc_name,loc_code) values('" . $vehicleCode . "','" . $userId . "','" . $cityFinalArray[0] . "','" . $cityFinalArray[2] . "','" . $cityFinalArray[1] . "')";
                } else {
                    $locationName = array();
                    $locationName = explode("(", $cityFinalArray[2]);
                    $locationName = explode(")", $locationName[1]);

                    $query = "insert into restricted_car_code(vehicle_code,user_id,loc_type,loc_name,loc_code) values('" . $vehicleCode . "','" . $userId . "','" . $cityFinalArray[0] . "','" . $locationName[0] . "','" . $cityFinalArray[1] . "')";
                }
                $resource = operations($query);
            }
        }

        $insertId = $resource;
        $result = global_message(200, 1011, $insertId);
    } else {


        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : setZoneRate
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setZoneRate() {



    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['zipcode']) && !empty($_REQUEST['zipcode'])) && (isset($_REQUEST['cityName']) && !empty($_REQUEST['cityName'])) && (isset($_REQUEST['cityAbbr']) && !empty($_REQUEST['cityAbbr'])) && (isset($_REQUEST['stateName']) && !empty($_REQUEST['stateName'])) && (isset($_REQUEST['stateCode']) && !empty($_REQUEST['stateCode']))) {


        $cityName = $_REQUEST['cityName'];
        $cityAbbr = $_REQUEST['cityAbbr'];
        $stateName = $_REQUEST['stateName'];
        $stateCode = $_REQUEST['stateCode'];
        $user_id = $_REQUEST['user_id'];
        $zipcode = $_REQUEST['zipcode'];

        $query = "insert into zone_to_zone_rate(zone_name,zone_abbr,city_name,city_code,user_id,zip_code) values('" . $cityName . "'
		   ,'" . $cityAbbr . "','" . $stateName . "','" . $stateCode . "','" . $user_id . "','" . $zipcode . "')";


        $resource = operations($query);

        $insertId = $resource;
        $result = global_message(200, 1011, $insertId);
    } else {


        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : setApiKey
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setApiKey() {


    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['limoApiKey']) && !empty($_REQUEST['limoApiKey'])) && (isset($_REQUEST['limoApiID']) && !empty($_REQUEST['limoApiID']))) {

        $userID = $_REQUEST['user_id'];

        $limoApiKey = $_REQUEST['limoApiKey'];

        $limoApiID = $_REQUEST['limoApiID'];

        $query2 = "select * from  limoanywhereapikey where user_id='" . $userID . "'";

        $getResult = operations($query2);

        if (count($getResult) <= 1 && gettype($getResult) != "boolean") {


            $query = "update limoanywhereapikey set limo_any_where_api_key='" . $limoApiKey . "',limo_any_where_api_id='" . $limoApiID . "' where user_id='" . $userID . "'";
        } else {
            $url_name_query = "select max(id) as id from limoanywhereapikey";

            $url_name_query_result = operations($url_name_query);
            $lastId = '';

            if (empty($url_name_query_result[0]['id'])) {
                $lastId = 1;
            } else {

                $lastId = $url_name_query_result[0]['id'];
            }

            $finalurl = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $finalurl = $finalurl . $lastId;
            $query = "insert into limoanywhereapikey(limo_any_where_api_key,limo_any_where_api_id,user_id,url_name) value('" . $limoApiKey . "','" . $limoApiID . "','" . $userID . "','" . $finalurl . "')";
        }
        operations($query);


        $resultHold = getApiKey();


        $result = global_message(200, 1015, $resultHold);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}



/* * *********************************************************
 * Method Name   : setRestApiKey
 * Description       : insert into add_fixed_rate
 * @Param            : applylocationExtraChild Fixed_rate_child getPointVehicleCode applyVehicleExtraChild
 * @return            : json data
 * ********************************************************* */

function setRestApiKey() {


    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['client_id']) && !empty($_REQUEST['client_id'])) && (isset($_REQUEST['client_secret']) && !empty($_REQUEST['client_secret']))) {

        $userID = $_REQUEST['user_id'];

        $client_id = $_REQUEST['client_id'];
        // $limoApiKey = $_REQUEST['limoApiKey'];

        // $limoApiID = $_REQUEST['limoApiID'];
        $client_secret = $_REQUEST['client_secret'];
        $access_token = $_REQUEST['access_token'];
        $token_type = $_REQUEST['token_type'];
        $expires_in = $_REQUEST['expires_in'];

        $query2 = "select * from  limoanywhereapikey where user_id='" . $userID . "'";

        $getResult = operations($query2);

        if (count($getResult) <= 1 && gettype($getResult) != "boolean") {


            $query = "update limoanywhereapikey set client_id='" . $client_id . "',access_token='" . $access_token . "',token_type='" . $token_type . "',expires_in='" . $expires_in . "',client_secret='" . $client_secret . "' where user_id='" . $userID . "'";
        } else {
            $url_name_query = "select max(id) as id from limoanywhereapikey";

            $url_name_query_result = operations($url_name_query);
            $lastId = '';

            if (empty($url_name_query_result[0]['id'])) {
                $lastId = 1;
            } else {

                $lastId = $url_name_query_result[0]['id'];
            }

            $finalurl = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $finalurl = $finalurl . $lastId;
            $query = "insert into limoanywhereapikey(client_id,access_token,token_type,expires_in,client_secret,user_id,url_name) value('" . $client_id . "','" . $access_token . "','" . $token_type . "','" . $expires_in . "','" . $client_secret . "','" . $userID . "','" . $finalurl . "')";
            
        }
        operations($query);


        $resultHold = getApiKey();


        $result = global_message(200, 1015, $resultHold);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updateCitiesState() {



    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['cityName']) && !empty($_REQUEST['cityName'])) && (isset($_REQUEST['zipcode']) && !empty($_REQUEST['zipcode'])) && (isset($_REQUEST['stateName']) && !empty($_REQUEST['stateName'])) && (isset($_REQUEST['stateCode']) && !empty($_REQUEST['stateCode']))) {


        $cityName = $_REQUEST['cityName'];
        $cityAbbr = $_REQUEST['cityAbbr'];
        $stateName = $_REQUEST['stateName'];
        $stateCode = $_REQUEST['stateCode'];
        $country_id = $_REQUEST['country_id'];
        $county_id = $_REQUEST['county_id'];
        $zipcode = $_REQUEST['zipcode'];
        $user_id = $_REQUEST['rowId'];
        $lat_long = $_REQUEST['getLatLang'];

        //$zipcode=explode(',', $zipcode);  


        /* $query2="select sma_country_id as country_id from sma_state where id='".$stateCode."' ";
          $resourceResutl = operations($query2); */


        $query = "update sma_city set country_id='" . $country_id . "',postal_code='" . $zipcode . "',city_name='" . $cityName . "',state_id='" . $stateCode . "',county_id='" . $county_id . "' where id='" . $user_id . "'";
        $resource = operations($query);


        $result = global_message(200, 1011, $insertId);
    } else {


        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updateCitiesZone() {



    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['cityName']) && !empty($_REQUEST['cityName'])) && (isset($_REQUEST['cityAbbr']) && !empty($_REQUEST['cityAbbr'])) && (isset($_REQUEST['zipcode']) && !empty($_REQUEST['zipcode'])) && (isset($_REQUEST['stateName']) && !empty($_REQUEST['stateName'])) && (isset($_REQUEST['stateCode']) && !empty($_REQUEST['stateCode']))) {


        $cityName = $_REQUEST['cityName'];
        $cityAbbr = $_REQUEST['cityAbbr'];
        $stateName = $_REQUEST['stateName'];
        $stateCode = $_REQUEST['stateCode'];
        $zipcode = $_REQUEST['zipcode'];
        $user_id = $_REQUEST['rowId'];

        $query = "update zone_to_zone_rate set  zip_code='" . $zipcode . "',zone_name='" . $cityName . "',zone_abbr='" . $cityAbbr . "',city_name='" . $stateName . "',city_code='" . $stateCode . "' where id='" . $user_id . "'";

        $resource = operations($query);
        $result = global_message(200, 1018);
    } else {


        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name    	  : delete_mobile
 * Description       : delete mobile from the list
 * @Param            : all Param  
 * @return           : return json of mobile
 * ********************************************************* */

function delete_mobile() {


    if ((isset($_REQUEST['delete_id']) && !empty($_REQUEST['delete_id'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $update_mobile_id = $_REQUEST['delete_id'];
        $user_id = $_REQUEST['user_id'];
        $mobile_notify = '';

        $query1 = "update email_mobil_notify set mobile_notify='" . $mobile_notify . "'  where id='" . $update_mobile_id . "' and user_id='" . $user_id . "'";
        $resource = operations($query1);

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name    	  : delete_mobile
 * Description       : delete mobile from the list
 * @Param            : all Param  
 * @return           : return json of mobile
 * ********************************************************* */

function delete_email() {


    if ((isset($_REQUEST['delete_id']) && !empty($_REQUEST['delete_id'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $update_email_id = $_REQUEST['delete_id'];
        $user_id = $_REQUEST['user_id'];
        $email_notify = '';

        $query1 = "update email_mobil_notify set email_notify='" . $email_notify . "'  where id='" . $update_email_id . "' and user_id='" . $user_id . "'";
        $resource = operations($query1);

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name    	  : get_notify_email
 * Description       : get all email and mobile for notification
 * @Param            : all Param  
 * @return           : return json of mobile
 * ********************************************************* */

function get_notify_email() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $query12 = "SELECT * from email_mobil_notify where user_id='" . $_REQUEST['user_id'] . "' ORDER BY id ASC";
        //	echo $query12;
        $queryResult = operations($query12);



        if (count($queryResult) >= 1 && gettype($queryResult) != "boolean") {

            $result = global_message(200, 1002, $queryResult);
            //$query="update limoanywhereapikey set limo_any_where_api_key='".$limoApiKey."',limo_any_where_api_id='".$limoApiID."' where user_id='".$userID."'";
        } else {

            $result = global_message(201, 1003);
        }
    } else {


        $result = global_message(201, 1009);
    }

    //print_r($result);

    return $result;
}

/* * *********************************************************
 * Method Name        : add_nitify_email
 * Description        : update mobile data from the table
 * @Param             : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function add_nitify_email() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['email_notify']) && !empty($_REQUEST['email_notify'])) && (isset($_REQUEST['mobile_notify']) && !empty($_REQUEST['mobile_notify']))) {

        $email_address = $_REQUEST['email_notify'];
        $mobile_number = $_REQUEST['mobile_notify'];

        $email_array = explode(",", $email_address);
        $mobile_array = explode(",", $mobile_number);

        //print_r($email_address);
        //print_r($mobile_number);

        $notification_send = 0;
        if (count($email_array) > count($mobile_array)) {

            $notification_send = count($email_array);
        } else if (count($mobile_array) > count($email_array)) {

            $notification_send = count($mobile_array);
        } else {

            $notification_send = count($mobile_array);
        }

        //echo $notification_send;  
        $userId = $_REQUEST['user_id'];

        $queryDelete = "delete from email_mobil_notify where user_id='" . $userId . "'";
        $resource = operations($queryDelete);



        for ($i = 0; $i < $notification_send; $i++) {

            $queryState = "insert into email_mobil_notify(email_notify	,mobile_notify,user_id) values('" . $email_array[$i] . "','" . $mobile_array[$i] . "','" . $userId . "')";
            $resource = operations($queryState);
        }


        $result = global_message(200, 1009, $resource);
    } else if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['email_notify']) && !empty($_REQUEST['email_notify']))) {

        $queryDelete = "delete from email_mobil_notify where user_id='" . $_REQUEST['user_id'] . "'";
        $resource = operations($queryDelete);

        $mobile_value = '';


        $queryState = "insert into email_mobil_notify(email_notify,mobile_notify,user_id) values('" . $_REQUEST['email_notify'] . "','" . $mobile_value . "','" . $_REQUEST['user_id'] . "')";
        $resource = operations($queryState);

        $result = global_message(200, 1009, $resource);
    } else if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['mobile_notify']) && !empty($_REQUEST['mobile_notify']))) {
        $queryDelete = "delete from email_mobil_notify where user_id='" . $_REQUEST['user_id'] . "'";
        $resource = operations($queryDelete);

        $email_value = '';

        $queryState = "insert into email_mobil_notify(email_notify,mobile_notify,user_id) values('" . $email_value . "','" . $_REQUEST['mobile_notify'] . "','" . $_REQUEST['user_id'] . "')";
        $resource = operations($queryState);

        $result = global_message(200, 1009, $resource);
    } else if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (empty($_REQUEST['mobile_notify'])) && empty($_REQUEST['email_notify'])) {
        $queryDelete = "delete from email_mobil_notify where user_id='" . $_REQUEST['user_id'] . "'";
        $resource = operations($queryDelete);
        $result = global_message(200, 1003);
    } else {

        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updateHourlyRate() {
    if ((isset($_REQUEST['cities_hourly']) && !empty($_REQUEST['cities_hourly'])) && (isset($_REQUEST['getIdAddRate']) && !empty($_REQUEST['getIdAddRate'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['add_hours']) && !empty($_REQUEST['add_hours'])) && (isset($_REQUEST['occasion']) && !empty($_REQUEST['occasion'])) && (isset($_REQUEST['add_rate']) && !empty($_REQUEST['add_rate']))) {
        $getIdAddRate = $_REQUEST['getIdAddRate'];
        $vehicleCode = $_REQUEST['vehicle_code'];
        $addHours = $_REQUEST['add_hours'];
        $startHours = $_REQUEST['start_hours'];
        $addOccasion = $_REQUEST['occasion'];
        $addOccasion = explode(",", $addOccasion);
        $addRate = $_REQUEST['add_rate'];
        $addPeakRate = $_REQUEST['add_peak_rate'];
        $addSma = $_REQUEST['cities_hourly'];

        $addSma = explode(",", $addSma);
        $userId = $_REQUEST['user_id'];
        $minimum_hour = (isset($_REQUEST['minimum_hour']) && !empty($_REQUEST['minimum_hour'])) ? $_REQUEST['minimum_hour'] : 0;
        $query1 = "update rate_calculate_hourly  set  hourly_rate='" . $addRate . "',peak_rate='" . $addPeakRate . "',from_hour='" . $startHours . "',to_hour='" . $addHours . "'  where id='" . $getIdAddRate . "'";

        $resource = operations($query1);

        $queryDelete = "delete from market_hourly where rate_id='" . $getIdAddRate . "'";
        $resource = operations($queryDelete);

        $queryDelete1 = "delete from occasion_hourly where rate_id='" . $getIdAddRate . "'";
        $resource1 = operations($queryDelete1);

        for ($i = 0; $i < count($addSma); $i++) {
            $getSma = explode("a@a", $addSma[$i]);
            $queryState = "insert into market_hourly(location_type,location_name,location_code,rate_id,minimum_rate,min_peak_hour) values('SMA','" . $getSma[1] . "','" . $getSma[0] . "','" . $getIdAddRate . "','" . $minimum_hour . "','" . $_REQUEST['min_peak_hour'] . "')";
            operations($queryState);
        }
        for ($j = 0; $j < count($addOccasion); $j++) {
            //$getSma=explode("a@a",$addSma);				 
            $queryOccasion = "insert into occasion_hourly(rate_id,occasion) values(" . $getIdAddRate . ",'" . $addOccasion[$j] . "')";
            operations($queryOccasion);
        }


        $result = global_message(200, 1009, $resource);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePassengerRate() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['addPassengerRate']) && !empty($_REQUEST['addPassengerRate'])) && (isset($_REQUEST['addMultiselectLocation']) && !empty($_REQUEST['addMultiselectLocation']))) {

        $rowId = $_REQUEST['rowId'];
        $addPassengerRate = $_REQUEST['addPassengerRate'];
        $addMultiselectLocation = $_REQUEST['addMultiselectLocation'];



        $addCities = explode(",", $addMultiselectLocation);
        $userId = $_REQUEST['user_id'];

        $query1 = "update passenger_rate  set  rate='" . $addPassengerRate . "'  where id='" . $rowId . "'";

        $resource = operations($query1);

        $queryDelete = "delete from market_passenger where rate_id='" . $rowId . "'";

        $resource = operations($queryDelete);

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);

            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);

                $queryState = "insert into market_passenger(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $rowId . "')";
                operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_passenger(location_type,location_name,location_code,rate_id) values('state','" . $queryStateCitiesArray[0]['state_name'] . "','" . $getCities[1] . "','" . $rowId . "')";
                operations($queryState);
            }
        }



        $result = global_message(200, 1009, $rowId);
    } else {
        $result = global_message(201, 1003);
    }

    //return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updateMilegeRate() {

    if ((isset($_REQUEST['add_miles_cities']) && !empty($_REQUEST['add_miles_cities'])) && (isset($_REQUEST['getIdAddMilegeRate']) && !empty($_REQUEST['getIdAddMilegeRate'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['add_miles']) && !empty($_REQUEST['add_miles'])) && (isset($_REQUEST['add_rate_mile']) && !empty($_REQUEST['add_rate_mile']))) {

        $getIdAddMilegeRate = $_REQUEST['getIdAddMilegeRate'];
        $addCities = $_REQUEST['add_miles_cities'];
        $vehicle_code = $_REQUEST['vehicle_code'];
        $add_miles = $_REQUEST['add_miles'];
        $add_rate_mile = $_REQUEST['add_rate_mile'];
        $userId = $_REQUEST['user_id'];
        $addCities = explode(",", $addCities);
        $minimum_base_rate = (isset($_REQUEST['minimum_base_rate']) && !empty($_REQUEST['minimum_base_rate'])) ? $_REQUEST['minimum_base_rate'] : 0;

        $query1 = "update rate_calculate_mileage  set  rate='" . $add_rate_mile . "',to_mileage='" . $add_miles . "'  where id='" . $getIdAddMilegeRate . "'";
        $resource = operations($query1);

        $queryDelete = "delete  from market_milege where rate_id='" . $getIdAddMilegeRate . "'";
        $resource = operations($queryDelete);


        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);
            if ($getCities[0] == "city") {


                $query = "select city_name from city_information where user_id='" . $userId . "' and city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);
                $queryState = "insert into market_milege(location_type,location_name,location_code,rate_id,minimum_base_rate) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $getIdAddMilegeRate . "','" . $minimum_base_rate . "')";
                $queryStateCitiesArray = operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "' and user_id='" . $userId . "'";
                $queryStateCitiesArray = operations($queryStateCities);
                $queryState = "insert into market_milege(location_type,location_name,location_code,rate_id,minimum_base_rate) values('SMA','" . $queryStateCitiesArray[0]['state_name'] . "','" . $getCities[1] . "','" . $getIdAddMilegeRate . "','" . $minimum_base_rate . "')";
                operations($queryState);
            }
        }





        $result = global_message(200, 1009, $resource);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePointToPointAirportRate() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['getPointToPointToll']) && !empty($_REQUEST['getPointToPointToll'])) && (isset($_REQUEST['getPointToPointVehicleCode']) && !empty($_REQUEST['getPointToPointVehicleCode'])) && (isset($_REQUEST['getPointToPointRate']) && !empty($_REQUEST['getPointToPointRate']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);



        $from = (isset($_REQUEST['getPointToPointFrom']) && !empty($_REQUEST['getPointToPointFrom'])) ? $_REQUEST['getPointToPointFrom'] : "";

        $toAirport = $_REQUEST['toAirport'];
        $add_airport_rate_city = $_REQUEST['add_airport_rate_city'];
        $add_airport_rate_zone = $_REQUEST['add_airport_rate_zone'];

        $toll = $_REQUEST['getPointToPointToll'];
        $vehicleCode = $_REQUEST['getPointToPointVehicleCode'];

        $to = (isset($_REQUEST['getPointToPointto']) && !empty($_REQUEST['getPointToPointto'])) ? $_REQUEST['getPointToPointto'] : "";


        $rate = $_REQUEST['getPointToPointRate'];
        $user_id = $_REQUEST['user_id'];

        $vehicleCodeArray = explode(",", $vehicleCode);

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from ptp_airport where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $insertId = '';
        for ($i = 0; $i < count($vehicleCodeArray); $i++) {
            $query = "insert into ptp_airport(airport_from,airport_to,city_id,zone_id,rate,airport_toll,user_id,car_code,is_to_airport) value('" . $from . "','" . $to . "','" . $add_airport_rate_city . "','" . $add_airport_rate_zone . "','" . $rate . "','" . $toll . "','" . $user_id . "','" . $vehicleCodeArray[$i] . "','" . $toAirport . "')";
            $resource = operations($query);
            $insertId .= $resource. "__";
        }

        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePointToPointZone() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['getPointToll']) && !empty($_REQUEST['getPointToll'])) && (isset($_REQUEST['getPointVehicleCode']) && !empty($_REQUEST['getPointVehicleCode'])) && (isset($_REQUEST['getPointRate']) && !empty($_REQUEST['getPointRate']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        $from_city = (isset($_REQUEST['from_city']) && !empty($_REQUEST['from_city'])) ? $_REQUEST['from_city'] : "";
        $from_zone = (isset($_REQUEST['from_zone']) && !empty($_REQUEST['from_zone'])) ? $_REQUEST['from_zone'] : "";
        $toll = $_REQUEST['getPointToll'];
        $vehicleCode = $_REQUEST['getPointVehicleCode'];
        $to_city = (isset($_REQUEST['to_city']) && !empty($_REQUEST['to_city'])) ? $_REQUEST['to_city'] : "";
        $to_zone = (isset($_REQUEST['to_zone']) && !empty($_REQUEST['to_zone'])) ? $_REQUEST['to_zone'] : "";
        $rate = $_REQUEST['getPointRate'];
        $user_id = $_REQUEST['user_id'];

        $vehicleCodeArray = explode(",", $vehicleCode);

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from ptp_zone_to_zone where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }


        $insertId = '';
        for ($i = 0; $i < count($vehicleCodeArray); $i++) {
            $query = "insert into ptp_zone_to_zone(from_city,from_zone,to_city,to_zone,rate,user_id,car_code,zone_toll) value('" . $from_city . "','" . $from_zone . "','" . $to_city . "','" . $to_zone . "','" . $rate . "','" . $user_id . "','" . $vehicleCodeArray[$i] . "','" . $toll . "')";
            $resource = operations($query);
            $insertId .= $resource. "__";
        }

        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePointToPointFixedRate() {

    if ((isset($_REQUEST['apply_location_fix_rate']) && !empty($_REQUEST['apply_location_fix_rate'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['addFixRate']) && !empty($_REQUEST['addFixRate'])) && (isset($_REQUEST['amountFixRate']) && !empty($_REQUEST['amountFixRate'])) && (isset($_REQUEST['applyVihecleFixRate']) && !empty($_REQUEST['applyVihecleFixRate'])) && (isset($_REQUEST['applyServiceFixRate']) && !empty($_REQUEST['applyServiceFixRate']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        $addFixRate = $_REQUEST['addFixRate'];
        $addCities = $_REQUEST['apply_location_fix_rate'];
        $amountFixRate = $_REQUEST['amountFixRate'];
        $applyVihecleFixRate = $_REQUEST['applyVihecleFixRate'];
        $applyServiceFixRate = $_REQUEST['applyServiceFixRate'];
        $user_id = $_REQUEST['user_id'];
        $applyVihecleFixRateArray = explode(",", $applyVihecleFixRate);
        $applyServiceFixRateArray = explode(",", $applyServiceFixRate);
        $addCities = explode(",", $addCities);

        $query = "select state_comman_id from add_fixed_rate where id='" . $rowIdArray[0] . "'";

        $getFixRateCommanId = operations($query);

        $deleteQuery = "delete from market_fix_rate where rate_id='" . $getFixRateCommanId[0]['state_comman_id'] . "'";
        $resource = operations($deleteQuery);

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);
            if ($getCities[0] == "city") {
                $query = "select city_name from city_information where  city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);
                $queryState = "insert into market_fix_rate(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                $queryStateCitiesArray = operations($queryState);
            } else {



                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_fix_rate(location_type,location_name,location_code,rate_id) values('state','" . $queryStateCitiesArray[0]['state_name'] . "','" . $getCities[1] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                operations($queryState);
            }
        }

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {
            $query = "delete from add_fixed_rate where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        for ($i = 0; $i < count($applyVihecleFixRateArray); $i++) {
            for ($j = 0; $j < count($applyServiceFixRateArray); $j++) {
                $query = "insert into add_fixed_rate(name,rate,vechicle_code,service_type,state_comman_id,user_id) value('" . $addFixRate . "','" . $amountFixRate . "','" . $applyVihecleFixRateArray[$i] . "','" . $applyServiceFixRateArray[$j] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "','" . $user_id . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }







        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updateExtraSeatRate() {

    if ((isset($_REQUEST['Numberofseats']) && !empty($_REQUEST['Numberofseats'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['Fixed_rate_child']) && !empty($_REQUEST['Fixed_rate_child'])) && (isset($_REQUEST['applyVehicleExtraChild']) && !empty($_REQUEST['applyVehicleExtraChild'])) && (isset($_REQUEST['applyServiceExtraChild']) && !empty($_REQUEST['applyServiceExtraChild'])) && (isset($_REQUEST['applylocationExtraChild']) && !empty($_REQUEST['applylocationExtraChild']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        $Numberofseats = $_REQUEST['Numberofseats'];
        $FixedRateChild = $_REQUEST['Fixed_rate_child'];
        $applyVehicleExtraChild = $_REQUEST['applyVehicleExtraChild'];
        $applyServiceExtraChild = $_REQUEST['applyServiceExtraChild'];
        $applylocationExtraChild = $_REQUEST['applylocationExtraChild'];
        $applyVehicleExtraChildArray = explode(",", $applyVehicleExtraChild);
        $applyServiceExtraChildArray = explode(",", $applyServiceExtraChild);
        $addCities = explode(",", $applylocationExtraChild);
        $userId = $_REQUEST['user_id'];
        $query = "select state_comman_id from extra_child where id='" . $rowIdArray[0] . "'";

        $getFixRateCommanId = operations($query);

        $deleteQuery = "delete from market_child_seat where rate_id='" . $getFixRateCommanId[0]['state_comman_id'] . "'";
        $resource = operations($deleteQuery);

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);

            if ($getCities[0] == "city") {
                $query = "select city_name from city_information where  city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);
                $queryState = "insert into market_child_seat(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                $queryStateCitiesArray = operations($queryState);
            } else {



                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_child_seat(location_type,location_name,location_code,rate_id) values('state','" . $queryStateCitiesArray[0]['state_name'] . "','" . $getCities[1] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                operations($queryState);
            }
        }

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {
            $query = "delete from extra_child where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }



        for ($i = 0; $i < count($applyVehicleExtraChildArray); $i++) {
            for ($j = 0; $j < count($applyVehicleExtraChildArray); $j++) {
                $query = "insert into extra_child(name,seat,rate,vehicle_code,service_code,state_comman_id,user_id) value('Child Seats','" . $Numberofseats . "','" . $FixedRateChild . "','" . $applyVehicleExtraChildArray[$i] . "','" . $applyServiceExtraChildArray[$j] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "','" . $userId . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }




        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePointToPointBaseRate() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['addBaseRateName']) && !empty($_REQUEST['addBaseRateName'])) && (isset($_REQUEST['addBaseRateAmount']) && !empty($_REQUEST['addBaseRateAmount'])) && (isset($_REQUEST['addBaseRateVehicleName']) && !empty($_REQUEST['addBaseRateVehicleName'])) && (isset($_REQUEST['addBaseRateServiceName']) && !empty($_REQUEST['addBaseRateServiceName']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        $addBaseRateName = $_REQUEST['addBaseRateName'];
        $addBaseRateAmount = $_REQUEST['addBaseRateAmount'];
        $addBaseRateVehicleName = $_REQUEST['addBaseRateVehicleName'];
        $addBaseRateServiceName = $_REQUEST['addBaseRateServiceName'];
        $user_id = $_REQUEST['user_id'];
        $addCities = $_REQUEST['add_base_rate_Location_name'];
        $addBaseRateVehicleNameArray = explode(",", $addBaseRateVehicleName);
        $addBaseRateServiceNameArray = explode(",", $addBaseRateServiceName);
        $addCities = explode(",", $addCities);

        $query = "select state_comman_id from add_base_rate where id='" . $rowIdArray[0] . "'";
        $getFixRateCommanId = operations($query);

        $deleteQuery = "delete from market_base_rate where rate_id='" . $getFixRateCommanId[0]['state_comman_id'] . "'";
        $resource = operations($deleteQuery);

        for ($i = 0; $i < count($addCities); $i++) {
            $getCities = explode("_", $addCities[$i]);
            if ($getCities[0] == "city") {
                $query = "select city_name from city_information where  city_abbr='" . $getCities[1] . "'";
                $queryArray = operations($query);
                $queryState = "insert into market_base_rate(location_type,location_name,location_code,rate_id) values('city','" . $queryArray[0]['city_name'] . "','" . $getCities[1] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                $queryStateCitiesArray = operations($queryState);
            } else {

                $queryStateCities = "select state_name from city_information where state_code='" . $getCities[1] . "'";
                $queryStateCitiesArray = operations($queryStateCities);

                $queryState = "insert into market_base_rate(location_type,location_name,location_code,rate_id) values('state','" . $queryStateCitiesArray[0]['state_name'] . "','" . $getCities[1] . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                operations($queryState);
            }
        }

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {
            $query = "delete from add_base_rate where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        for ($i = 0; $i < count($addBaseRateVehicleNameArray); $i++) {
            for ($j = 0; $j < count($addBaseRateServiceNameArray); $j++) {
                $query = "insert into add_base_rate(name,rate,vehicle_code,service_type,user_id,state_comman_id) value('" . $addBaseRateName . "','" . $addBaseRateAmount . "','" . $addBaseRateVehicleNameArray[$i] . "','" . $addBaseRateServiceNameArray[$j] . "','" . $user_id . "','" . $getFixRateCommanId[0]['state_comman_id'] . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }



        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePointToPointIncrementalRate() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['incrementalRateName']) && !empty($_REQUEST['incrementalRateName'])) && (isset($_REQUEST['incrementalRateMinAmount']) && !empty($_REQUEST['incrementalRateMinAmount'])) && (isset($_REQUEST['incrementalRateMaxAmount']) && !empty($_REQUEST['incrementalRateMaxAmount'])) && (isset($_REQUEST['incrementalRateVehicle']) && !empty($_REQUEST['incrementalRateVehicle'])) && (isset($_REQUEST['incrementalRateService']) && !empty($_REQUEST['incrementalRateService']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        $incrementalRateName = $_REQUEST['incrementalRateName'];
        $incrementalRateMinAmount = $_REQUEST['incrementalRateMinAmount'];
        $incrementalRateMaxAmount = $_REQUEST['incrementalRateMaxAmount'];
        $incrementalRateVehicle = $_REQUEST['incrementalRateVehicle'];
        $applyServiceFixRate = $_REQUEST['incrementalRateService'];
        $user_id = $_REQUEST['user_id'];
        $incrementalRateVehicleArray = explode(",", $incrementalRateVehicle);
        $incrementalRateServiceArray = explode(",", $incrementalRateService);

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {
            $query = "delete from add_incremental_rate where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        for ($i = 0; $i < count($applyVihecleFixRateArray); $i++) {
            for ($j = 0; $j < count($applyServiceFixRateArray); $j++) {
                $query = "insert into add_incremental_rate(name,percentage_minimum,percentage_maximum,vehicle_code,service_type,user_id) value('" . $incrementalRateName . "','" . $incrementalRateMinAmount . "','" . $incrementalRateMaxAmount . "','" . $incrementalRateVehicleArray[$i] . "','" . $incrementalRateServiceArray[$j] . "','" . $user_id . "')";
                $resource = operations($query);

                $insertId .= $resource. "__";
            }
        }







        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function updatePointToPointSeaPortRate() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['getPointToPointToll']) && !empty($_REQUEST['getPointToPointToll'])) && (isset($_REQUEST['getPointToPointVehicleCode']) && !empty($_REQUEST['getPointToPointVehicleCode'])) && (isset($_REQUEST['getPointToPointRate']) && !empty($_REQUEST['getPointToPointRate']))) {

        $rowId = $_REQUEST['rowId'];
        $from = (isset($_REQUEST['getPointToPointFrom']) && !empty($_REQUEST['getPointToPointFrom'])) ? $_REQUEST['getPointToPointFrom'] : "";





        $toll = $_REQUEST['getPointToPointToll'];
        $vehicleCode = $_REQUEST['getPointToPointVehicleCode'];

        $to = (isset($_REQUEST['getPointToPointto']) && !empty($_REQUEST['getPointToPointto'])) ? $_REQUEST['getPointToPointto'] : "";
        $add_seaport_city_to = (isset($_REQUEST['add_seaport_city_to']) && !empty($_REQUEST['add_seaport_city_to'])) ? $_REQUEST['add_seaport_city_to'] : "";
        $add_airport_zone_to = (isset($_REQUEST['add_airport_zone_to']) && !empty($_REQUEST['add_airport_zone_to'])) ? $_REQUEST['add_airport_zone_to'] : "";
        $rate = $_REQUEST['getPointToPointRate'];
        $toSeaport = $_REQUEST['toSeaport'];
        $user_id = $_REQUEST['user_id'];
        $rowIdArray = explode("__", $rowId);
        $vehicleCodeArray = explode(",", $vehicleCode);

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from ptp_seaport where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }


        $insertId = '';
        for ($i = 0; $i < count($vehicleCodeArray); $i++) {
            $query = "insert into ptp_seaport(seaport_from,seaport_to,city_id,zone_id,rate,user_id,car_code,seaport_toll,is_to_seaport) value('" . $from . "','" . $to . "','" . $add_seaport_city_to . "','" . $add_airport_zone_to . "','" . $rate . "','" . $user_id . "','" . $vehicleCodeArray[$i] . "','" . $toll . "','" . $toSeaport . "')";
            $resource = operations($query);
            $insertId .= $resource. "__";
        }

        $result = global_message(200, 1009, $insertId);
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : deleteHourlyRate
 * Description       : Delete Hour Rate Manualy
 * @Param            : ID
 * @return            : DELETE ITEM SUCCESSFULLY OR NOT json
 * ********************************************************* */

function deleteHourlyRate() {

    if ((isset($_REQUEST['id']) && !empty($_REQUEST['id']))) {
        $rowId = $_REQUEST['id'];

        $query = "delete from rate_calculate_hourly where id='" . $rowId . "'";
        $resource = operations($query);
        $queryDelete = "delete  from market_hourly where rate_id='" . $rowId . "'";
        $resource = operations($queryDelete);
        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deleteMilesRate() {



    if ((isset($_REQUEST['id']) && !empty($_REQUEST['id']))) {
        $rowId = $_REQUEST['id'];

        $query = "delete from rate_calculate_mileage where id='" . $rowId . "'";
        $resource = operations($query);
        $queryDelete = "delete  from market_milege where rate_id='" . $rowId . "'";
        $resource = operations($queryDelete);
        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

function deleteairportPointToPointRate() {



    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $rowIdArray = array();
        $rowId = $_REQUEST['user_id'];
        $rowIdArray = explode("__", $rowId);

        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from ptp_airport where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }


        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deletePointToPointRateSeaPort() {



    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId']))) {

        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from ptp_seaport where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

function deletePointToPointZone() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId']))) {

        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from ptp_zone_to_zone where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deletePointToPointFixedRate() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId']))) {

        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);

        $query = "select state_comman_id from add_fixed_rate where id='" . $rowIdArray[0] . "'";

        $getFixRateCommanId = operations($query);

        $deleteQuery = "delete from market_fix_rate where rate_id='" . $getFixRateCommanId[0]['state_comman_id'] . "'";
        $resource = operations($deleteQuery);





        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from  add_fixed_rate where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deleteExtraChild() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId']))) {

        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);

        $query = "select state_comman_id from extra_child where id='" . $rowIdArray[0] . "'";

        $getFixRateCommanId = operations($query);

        $deleteQuery = "delete from market_child_seat where rate_id='" . $getFixRateCommanId[0]['state_comman_id'] . "'";
        $resource = operations($deleteQuery);





        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from  extra_child where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deletePointToPointBaseRate() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId']))) {

        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from  add_base_rate where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deletePointToPointIncrementalRate() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId']))) {

        $rowId = $_REQUEST['rowId'];
        $rowIdArray = explode("__", $rowId);
        for ($i = 0; $i < count($rowIdArray) - 1; $i++) {

            $query = "delete from  add_incremental_rate where id='" . $rowIdArray[$i] . "'";
            $resource = operations($query);
        }

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deletePassengerRate() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $rowId = $_REQUEST['rowId'];
        $user_id = $_REQUEST['user_id'];



        $query = "delete from  passenger_rate where id='" . $rowId . "' and user_id='" . $user_id . "'";
        $resource = operations($query);

        $query1 = "delete from  market_passenger where rate_id='" . $rowId . "'";
        $resource = operations($query1);

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

function deleteRestrictedCarCode() {


    if ((isset($_REQUEST['rowId']) && !empty($_REQUEST['rowId'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $rowId = $_REQUEST['rowId'];
        $user_id = $_REQUEST['user_id'];



        $query = "delete from  restricted_car_code where id='" . $rowId . "' and user_id='" . $user_id . "'";
        $resource = operations($query);

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deleteZone() {


    if ((isset($_REQUEST['id']) && !empty($_REQUEST['id'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $rowId = $_REQUEST['id'];
        $user_id = $_REQUEST['user_id'];



        $query = "delete from  zone_to_zone_rate where id='" . $rowId . "' and user_id='" . $user_id . "'";
        $resource = operations($query);



        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function deleteCities() {


    if ((isset($_REQUEST['id']) && !empty($_REQUEST['id'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $rowId = $_REQUEST['id'];
        $user_id = $_REQUEST['user_id'];



        $query = "delete from  sma_city where id='" . $rowId . "' ";
        $resource = operations($query);

        // $query1="delete from  zone_to_zone_rate where city_code='".$rowId."' and user_id='".$user_id."'";
        //  		$resource = operations($query1);



        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


    /* * *********************************************************
 * Method Name   : forgetPwd
 * Description       : forgetPwd Company User
 * @Param            : Email and Password
 * @return            : employee data json
 * ********************************************************* */

function forgetPwd() {



    if ((isset($_REQUEST['email']) && !empty($_REQUEST['email']))) {


        $email = $_REQUEST['email'];
        $query = "select * from company_user_info where  email='" . $email . "'";
        $resource = operations($query);


       


        if (count($resource) > 0 && gettype($resource) != "boolean") {


             // $to = 'om@lirctek.com'; // note the comma
             $to = $_REQUEST['email']; // note the comma

$subject = 'Password Recovery';

// Message
$message = '
<html>
<head>
  <title>My Limo Project customer support</title>
</head>
<body>
  Dear '.$resource[0]['full_name'].'
  <br>
  Thank you for being a My Limo Project valued customer
  <br>Below is your account login information require to access your backend management system
  <br>
  User Name:-'.$_REQUEST['email'].'
  <br>
  Password:- '.$resource[0]['password'].'
  <br>
  To access your account, please go to <a href="//mylimoproject.com">www.mylimoproject.com</a>
  <br>
  Should you have any question or concerns, please feel free to contact us at (305) 967-7551 or <a href="mailto:support@mylimoproject.com">support@mylimoproject.com</a>
  <br>
  My Limo Project Customer support 
  <br>
  

    </body>
</html>
';

$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';
$headers[] = 'From: support@mylimoproject.com';
mail($to, $subject, $message, implode("\r\n", $headers));








            $result = global_message(200, 1004, $resource);
        } else {
            $result = global_message(200, 1005);
        }
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}



/* * *********************************************************
 * Method Name   : logIn
 * Description       : logIn Company User
 * @Param            : Email and Password
 * @return            : employee data json
 * ********************************************************* */

function logIn() {



    if ((isset($_REQUEST['email']) && !empty($_REQUEST['email'])) && (isset($_REQUEST['password']) && !empty($_REQUEST['password']))) {

        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        $query = "select * from company_user_info where  email='" . $email . "' and password='" . $password . "'";
        $resource = operations($query);
        if (count($resource) > 0) {

            $result = global_message(200, 1004, $resource);
        } else {
            $result = global_message(200, 1005);
        }
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getHourlyRate() {
    if ((isset($_REQUEST['vihicle_code']) && !empty($_REQUEST['vihicle_code'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query = "select * from rate_calculate_hourly where vehicle_code='" . $_REQUEST['vihicle_code'] . "' and user_id='" . $_REQUEST['user_id'] . "' ";
        $resource = operations($query);


        for ($i = 0; $i < count($resource); $i++) {

            $queryState = "SELECT  occasion FROM occasion_hourly where rate_id='" . $resource[$i]['id'] . "'";
            $queryData = operations($queryState);

            for ($k = 0; $k < count($queryData); $k++) {
                $resource[$i]["occasion"] .= $queryData[$k]['occasion'] . ',';
            }
            $querySma = "SELECT  location_code,location_name,minimum_rate,min_peak_hour FROM market_hourly where rate_id='" . $resource[$i]['id'] . "'";
            $querySmaData = operations($querySma);

            for ($j = 0; $j < count($querySmaData); $j++) {
                $resource[$i]["location_name"] .= $querySmaData[$j]['location_name'] . ',';
                $resource[$i]["location_code"] .= $querySmaData[$j]['location_code'] . ',';
            }

            $resource[$i]["minimus_hour"] = $querySmaData[0]['minimum_rate'];
            $resource[$i]["min_peak_hour"] = $querySmaData[0]['min_peak_hour'];
            //$resource[$i]["peak_rate"]=$resource[0]['peak_rate'];
        }

        if (count($resource) > 0 && gettype($resource) != "boolean") {
            $result = global_message(200, 1007, $resource);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getHourlyMilege() {



    if ((isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query = "select * from rate_calculate_mileage where vehicle_code='" . $_REQUEST['vehicle_code'] . "' and user_id='" . $_REQUEST['user_id'] . "' ";
        $resource = operations($query);



        for ($i = 0; $i < count($resource); $i++) {


            $queryState = "SELECT  location_name,minimum_base_rate FROM market_milege where rate_id='" . $resource[$i]['id'] . "'";
            $queryData = operations($queryState);

            for ($k = 0; $k < count($queryData); $k++) {
                $resource[$i]["city_code"] .= $queryData[$k]['location_name'] . ',';
                $resource[$i]["minimum_base_rate"] = $queryData[$k]['minimum_base_rate'];
            }
        }




        if (count($resource) > 0 && gettype($resource) != "boolean") {
            $result = global_message(200, 1007, $resource);
        } else {
            $result = global_message(200, 1006, $query);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getApiKey() {



    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $user_id = $_REQUEST['user_id'];
        $query1 = "select * from limoanywhereapikey where user_id='" . $user_id . "' ";

        $resource = operations($query1);
        if (count($resource) > 0 && gettype($resource) != "boolean") {

            $result = global_message(201, 1017, $resource);
        } else {
            $result = global_message(201, 1016);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getPointToPointRate() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $query = "SELECT * FROM ptp_airport where user_id='" . $_REQUEST['user_id'] . "' GROUP BY airport_from,airport_to";
        $resource = operations($query);
        $query2 = "SELECT zone_name from zone_to_zone_rate where id='" . $resource[0]["zone_id"] . "'";
        $resource2 = operations($query2);
        $query3 = "SELECT state_code from city_information where id='" . $resource[0]["city_id"] . "' LIMIT 1";
        $resource3 = operations($query3);


        for ($i = 0; $i < count($resource); $i++) {



            $resource[$i]['car_code'] = '';
            $resource[$i]['id'] = '';
            $resource[$i]['zone_name'] = $resource2[0]['zone_name'];
            $resource[$i]['state_code'] = $resource3[0]['state_code'];
            $query1 = "select id,car_code from ptp_airport where airport_from='" . $resource[$i]['airport_from'] . "' and airport_to='" . $resource[$i]['airport_to'] . "'";
            $resource1 = operations($query1);

            for ($j = 0; $j < count($resource1); $j++) {

                $resource[$i]['car_code'] .= $resource1[$j]['car_code'] . ",";
                $resource[$i]['id'] .= $resource1[$j]['id'] . "__";
            }
        }
        if (count($resource) > 0 && gettype($resource) != "boolean") {
            $result = global_message(200, 1007, $resource);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getPointToPointZoneStation() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $query = "SELECT * FROM ptp_zone_to_zone where user_id='" . $_REQUEST['user_id'] . "' GROUP BY from_zone,to_zone";
        $resource = operations($query);
        $query2 = "SELECT zone_name from zone_to_zone_rate where id='" . $resource[0]["to_zone"] . "'";
        $resource2 = operations($query2);

        $fromCityId = "SELECT state_code from city_information where id='" . $resource[0]["from_city"] . "' LIMIT 1";
        $resourceFromCityId = operations($fromCityId);

        $toCityId = "SELECT state_code from city_information where id='" . $resource[0]["to_city"] . "' LIMIT 1";
        $resourceToCityId = operations($fromCityId);



        $query3 = "SELECT zone_name from zone_to_zone_rate where id='" . $resource[0]["from_zone"] . "'";
        $resource3 = operations($query3);


        for ($i = 0; $i < count($resource); $i++) {
            $resource[$i]['car_code'] = '';
            $resource[$i]['id'] = '';
            $resource[$i]['zone_name'] = $resource2[0]['zone_name'];
            $resource[$i]['from_zone_name'] = $resource3[0]['zone_name'];
            $resource[$i]['from_stateCode'] = $resourceFromCityId[0]['state_code'];
            $resource[$i]['to_stateCode'] = $resourceToCityId[0]['state_code'];

            $query1 = "select id,car_code from ptp_zone_to_zone where from_zone='" . $resource[$i]['from_zone'] . "' and to_zone='" . $resource[$i]['to_zone'] . "'";
            $resource1 = operations($query1);
            for ($j = 0; $j < count($resource1); $j++) {

                $resource[$i]['car_code'] .= $resource1[$j]['car_code'] . ",";
                $resource[$i]['id'] .= $resource1[$j]['id'] . "__";
            }
        }
        if (count($resource) > 0 && gettype($resource) != "boolean") {
            $result = global_message(200, 1007, $resource);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getCityState() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $cityArray = array();
        $stateCityArray = array();
        $mixArrayCityState = array();
        $queryState = "SELECT distinct state_name,state_code FROM city_information where user_id='" . $_REQUEST['user_id'] . "'";
        $queryData = operations($queryState);

        if (gettype($queryData) != "boolean") {
            for ($i = 0; $i < count($queryData); $i++) {
                $cityArray[$i]['name'] = $queryData[$i]['state_name'];
                $cityArray[$i]['code'] = $queryData[$i]['state_code'];
                $cityArray[$i]['state_type'] = "state";

                $queryCity = "select city_name,city_abbr from city_information where user_id='" . $_REQUEST['user_id'] . "' and state_name='" . $queryData[$i]['state_name'] . "'";
                $queryCityData = operations($queryCity);


                for ($j = 0; $j < count($queryCityData); $j++) {
                    $stateCityArray[$j]['name'] = $queryData[$i]['state_name'] . "(" . $queryCityData[$j]['city_name'] . ")";
                    //$stateCityArray[$j]['city_name']=$queryCityData[$j]['city_name'];
                    //$stateCityArray[$j]['state_name']=$queryCityData[$i]['state_name'];
                    $stateCityArray[$j]['code'] = $queryCityData[$j]['city_abbr'];
                    $stateCityArray[$j]['state_type'] = "city";
                }
            }
            $mixArrayCityState = array_merge($cityArray, $stateCityArray);

            aasort($mixArrayCityState, "name");

            $result = global_message(200, 1013, $mixArrayCityState);
        } else {
            $result = global_message(200, 1014);
        }
    } else {
        $result = global_message(201, 1003);
    }

    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function aasort(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key => $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getPointToPointFixedRate() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $getFixedRateInfo = "SELECT * FROM add_fixed_rate where user_id='" . $_REQUEST['user_id'] . "' GROUP BY name";
        $getFixedRateInfoHoldData = operations($getFixedRateInfo);

        for ($i = 0; $i < count($getFixedRateInfoHoldData); $i++) {

            $queryState = "SELECT  location_name FROM market_fix_rate where rate_id='" . $getFixedRateInfoHoldData[$i]['state_comman_id'] . "'";
            $getFixedRateInfoHoldDataArray = operations($queryState);

            for ($k = 0; $k < count($getFixedRateInfoHoldDataArray); $k++) {
                $getFixedRateInfoHoldData[$i]["city_code"] .= $getFixedRateInfoHoldDataArray[$k]['location_name'] . ',';
            }

            $getFixedRateInfoHoldData[$i]['vechicle_code'] = '';
            $getFixedRateInfoHoldData[$i]['id'] = '';

            $vehicleInformation = "select vechicle_code from add_fixed_rate where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' GROUP BY vechicle_code";
            $vehicleInformationHoldData = operations($vehicleInformation);


            for ($j = 0; $j < count($vehicleInformationHoldData); $j++) {
                $getFixedRateInfoHoldData[$i]['service_type'] = '';
                $getFixedRateInfoHoldData[$i]['vechicle_code'] .= $vehicleInformationHoldData[$j]['vechicle_code'] . ",";

                $vehicleServiceInformation = "select id,service_type from add_fixed_rate where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' and vechicle_code='" . $vehicleInformationHoldData[$j]['vechicle_code'] . "' ";



                $vehicleServiceInformationHoldData = operations($vehicleServiceInformation);
                for ($k = 0; $k < count($vehicleServiceInformationHoldData); $k++) {

                    $getFixedRateInfoHoldData[$i]['service_type'] .= $vehicleServiceInformationHoldData[$k]['service_type'] . ",";
                    $getFixedRateInfoHoldData[$i]['id'] .= $vehicleServiceInformationHoldData[$k]['id'] . "__";
                }
            }
        }







        if (count($getFixedRateInfoHoldData) > 0 && gettype($getFixedRateInfoHoldData) != "boolean") {
            $result = global_message(200, 1007, $getFixedRateInfoHoldData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getExtraChild() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $getFixedRateInfo = "SELECT * FROM extra_child where user_id='" . $_REQUEST['user_id'] . "' GROUP BY name";
        $getFixedRateInfoHoldData = operations($getFixedRateInfo);

        for ($i = 0; $i < count($getFixedRateInfoHoldData); $i++) {

            $queryState = "SELECT  location_name FROM market_child_seat where rate_id='" . $getFixedRateInfoHoldData[$i]['state_comman_id'] . "'";
            $getFixedRateInfoHoldDataArray = operations($queryState);

            for ($k = 0; $k < count($getFixedRateInfoHoldDataArray); $k++) {
                $getFixedRateInfoHoldData[$i]["city_code"] .= $getFixedRateInfoHoldDataArray[$k]['location_name'] . ',';
            }

            $getFixedRateInfoHoldData[$i]['vehicle_code'] = '';
            $getFixedRateInfoHoldData[$i]['id'] = '';

            $vehicleInformation = "select vehicle_code from extra_child where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' GROUP BY vehicle_code";
            $vehicleInformationHoldData = operations($vehicleInformation);


            for ($j = 0; $j < count($vehicleInformationHoldData); $j++) {
                $getFixedRateInfoHoldData[$i]['service_code'] = '';
                $getFixedRateInfoHoldData[$i]['vehicle_code'] .= $vehicleInformationHoldData[$j]['vehicle_code'] . ",";

                $vehicleServiceInformation = "select id,service_code from extra_child where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' and vehicle_code='" . $vehicleInformationHoldData[$j]['vehicle_code'] . "' ";



                $vehicleServiceInformationHoldData = operations($vehicleServiceInformation);
                for ($k = 0; $k < count($vehicleServiceInformationHoldData); $k++) {

                    $getFixedRateInfoHoldData[$i]['service_code'] .= $vehicleServiceInformationHoldData[$k]['service_code'] . ",";
                    $getFixedRateInfoHoldData[$i]['id'] .= $vehicleServiceInformationHoldData[$k]['id'] . "__";
                }
            }
        }







        if (count($getFixedRateInfoHoldData) > 0 && gettype($getFixedRateInfoHoldData) != "boolean") {
            $result = global_message(200, 1007, $getFixedRateInfoHoldData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getCity() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $cityInformationQuery = "SELECT distinct city_name,city_abbr,id FROM city_information where user_id='" . $_REQUEST['user_id'] . "'";
        $cityInformationData = operations($cityInformationQuery);







        if (count($cityInformationData) > 0 && gettype($cityInformationData) != "boolean") {
            $result = global_message(200, 1007, $cityInformationData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getState() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $limit = 100;
        $pageNo = $_REQUEST['pageNo'];
        $skip = ($pageNo - 1)*$limit;

        $searchQ = '';
        if(isset($_REQUEST['keyword']) && $_REQUEST['keyword'] != ''){
            $searchQ = " and (sc.city_name LIKE '%".$_REQUEST['keyword']."%' or sc.postal_code LIKE '%".$_REQUEST['keyword']."%' or ss.sma_state LIKE '%".$_REQUEST['keyword']."%' or scs.country_name LIKE '%".$_REQUEST['keyword']."%') ";
        }

        $countQ = "select COUNT(sc.id) as total from sma_city as sc,sma_state as ss ,sma_countries as scs where sc.country_id=scs.id and sc.state_id=ss.id and sc.user_id='" . $_REQUEST['user_id']."' ".$searchQ;
        $total = operations($countQ);
        $total = $total[0]['total'];
//        print_r($countQ);
        $cityInformationQuery = "select sc.*,ss.sma_state,scs.country_name from sma_city as sc,sma_state as ss ,sma_countries as scs where sc.country_id=scs.id and sc.state_id=ss.id and sc.user_id='" . $_REQUEST['user_id'] . "' ".$searchQ." LIMIT ".$skip.", ".$limit;
        $cityInformationData = operations($cityInformationQuery);

        if (count($cityInformationData) > 0 && gettype($cityInformationData) != "boolean") {
            $result = global_message(200, 1007, ['data'=>$cityInformationData, 'total' => $total]);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

function getRestrictedCarCode() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code']))) {


        $cityInformationQuery = "SELECT * FROM restricted_car_code where user_id='" . $_REQUEST['user_id'] . "' and vehicle_code='" . $_REQUEST['vehicle_code'] . "'";
        $cityInformationData = operations($cityInformationQuery);

        if (count($cityInformationData) > 0 && gettype($cityInformationData) != "boolean") {
            $result = global_message(200, 1007, $cityInformationData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getCityPoitAirport() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['state_code']) && !empty($_REQUEST['state_code']))) {


        $cityInformationQuery = "SELECT * FROM city_information where user_id='" . $_REQUEST['user_id'] . "' and state_code='" . $_REQUEST['state_code'] . "'";
        $cityInformationData = operations($cityInformationQuery);







        if (count($cityInformationData) > 0 && gettype($cityInformationData) != "boolean") {
            $result = global_message(200, 1007, $cityInformationData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getZonePoitAirport() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['city_id']) && !empty($_REQUEST['city_id']))) {


        $cityInformationQuery = "SELECT * FROM zone_to_zone_rate where user_id='" . $_REQUEST['user_id'] . "' and city_code='" . $_REQUEST['city_id'] . "'";
        $cityInformationData = operations($cityInformationQuery);







        if (count($cityInformationData) > 0 && gettype($cityInformationData) != "boolean") {
            $result = global_message(200, 1007, $cityInformationData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getZoneState() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $cityInformationQuery = "SELECT * FROM zone_to_zone_rate where user_id='" . $_REQUEST['user_id'] . "'";
        $cityInformationData = operations($cityInformationQuery);







        if (count($cityInformationData) > 0 && gettype($cityInformationData) != "boolean") {
            $result = global_message(200, 1007, $cityInformationData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getPointToPointBaseRate() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $getFixedRateInfo = "SELECT * FROM add_base_rate where user_id='" . $_REQUEST['user_id'] . "' GROUP BY name";
        $getFixedRateInfoHoldData = operations($getFixedRateInfo);


        for ($i = 0; $i < count($getFixedRateInfoHoldData); $i++) {



            $queryState = "SELECT  location_name FROM market_base_rate where rate_id='" . $getFixedRateInfoHoldData[$i]['state_comman_id'] . "'";
            $getFixedRateInfoHoldDataArray = operations($queryState);

            for ($k = 0; $k < count($getFixedRateInfoHoldDataArray); $k++) {
                $getFixedRateInfoHoldData[$i]["city_code"] .= $getFixedRateInfoHoldDataArray[$k]['location_name'] . ',';
            }





            $getFixedRateInfoHoldData[$i]['vehicle_code'] = '';
            $getFixedRateInfoHoldData[$i]['id'] = '';

            $vehicleInformation = "select vehicle_code from add_base_rate where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' GROUP BY vehicle_code";
            $vehicleInformationHoldData = operations($vehicleInformation);


            for ($j = 0; $j < count($vehicleInformationHoldData); $j++) {
                $getFixedRateInfoHoldData[$i]['service_type'] = '';
                $getFixedRateInfoHoldData[$i]['vehicle_code'] .= $vehicleInformationHoldData[$j]['vehicle_code'] . ",";

                $vehicleServiceInformation = "select id,service_type from add_base_rate where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' and vehicle_code='" . $vehicleInformationHoldData[$j]['vehicle_code'] . "' ";



                $vehicleServiceInformationHoldData = operations($vehicleServiceInformation);



                for ($k = 0; $k < count($vehicleServiceInformationHoldData); $k++) {

                    $getFixedRateInfoHoldData[$i]['service_type'] .= $vehicleServiceInformationHoldData[$k]['service_type'] . ",";
                    $getFixedRateInfoHoldData[$i]['id'] .= $vehicleServiceInformationHoldData[$k]['id'] . "__";
                }
            }
        }

        if (count($getFixedRateInfoHoldData) > 0 && gettype($getFixedRateInfoHoldData) != "boolean") {
            $result = global_message(200, 1007, $getFixedRateInfoHoldData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : getPassengerRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getPassengerRate() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['vehicle_code']) && !empty($_REQUEST['vehicle_code']))) {

        $vehicle_code = $_REQUEST['vehicle_code'];
        $PassengerRateData = "SELECT * FROM passenger_rate where user_id='" . $_REQUEST['user_id'] . "' and vehicle_code='" . $vehicle_code . "' ";
        $PassengerRateDataAll = operations($PassengerRateData);
        for ($i = 0; $i < count($PassengerRateDataAll); $i++) {

            $PassengerRateAllCityData = "SELECT * FROM market_passenger where rate_id='" . $PassengerRateDataAll[$i]['id'] . "'";
            $PassengerRateAllCityDataArray = operations($PassengerRateAllCityData);

            for ($j = 0; $j < count($PassengerRateAllCityDataArray); $j++) {

                $PassengerRateDataAll[$i]['code'] .= $PassengerRateAllCityDataArray[$j]['location_name'] . ',';
            }
        }

        if (count($PassengerRateDataAll) > 0 && gettype($PassengerRateDataAll) != "boolean") {
            $result = global_message(200, 1007, $PassengerRateDataAll);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* * *********************************************************
 * Method Name   : updateHourlyRate
 * Description       : Update Hourly Rate Manual
 * @Param            : Hour rate or  service or hour
 * @return            : hour rate update msg return in array
 * ********************************************************* */

function getPointToPointIncrementalRate() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $getFixedRateInfo = "SELECT * FROM add_incremental_rate where user_id='" . $_REQUEST['user_id'] . "' GROUP BY name";
        $getFixedRateInfoHoldData = operations($getFixedRateInfo);


        for ($i = 0; $i < count($getFixedRateInfoHoldData); $i++) {

            $getFixedRateInfoHoldData[$i]['vehicle_code'] = '';
            $getFixedRateInfoHoldData[$i]['id'] = '';

            $vehicleInformation = "select vehicle_code from add_incremental_rate where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' GROUP BY vehicle_code";
            $vehicleInformationHoldData = operations($vehicleInformation);


            for ($j = 0; $j < count($vehicleInformationHoldData); $j++) {
                $getFixedRateInfoHoldData[$i]['service_type'] = '';
                $getFixedRateInfoHoldData[$i]['vehicle_code'] .= $vehicleInformationHoldData[$j]['vehicle_code'] . ",";

                $vehicleServiceInformation = "select id,service_type from add_incremental_rate where user_id='" . $_REQUEST['user_id'] . "' and name='" . $getFixedRateInfoHoldData[$i]['name'] . "' and vehicle_code='" . $vehicleInformationHoldData[$j]['vehicle_code'] . "' ";



                $vehicleServiceInformationHoldData = operations($vehicleServiceInformation);



                for ($k = 0; $k < count($vehicleServiceInformationHoldData); $k++) {

                    $getFixedRateInfoHoldData[$i]['service_type'] .= $vehicleServiceInformationHoldData[$k]['service_type'] . ",";
                    $getFixedRateInfoHoldData[$i]['id'] .= $vehicleServiceInformationHoldData[$k]['id'] . "__";
                }
            }
        }







        if (count($getFixedRateInfoHoldData) > 0 && gettype($getFixedRateInfoHoldData) != "boolean") {
            $result = global_message(200, 1007, $getFixedRateInfoHoldData);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

function getPointToPointSeaPortStation() {
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $query = "SELECT * FROM ptp_seaport where user_id='" . $_REQUEST['user_id'] . "' GROUP BY seaport_from,seaport_to";
        $resource = operations($query);

        $query2 = "SELECT zone_name from zone_to_zone_rate where id='" . $resource[0]["zone_id"] . "'";
        $resource2 = operations($query2);
        $query3 = "SELECT state_code from city_information where id='" . $resource[0]["city_id"] . "' LIMIT 1";
        $resource3 = operations($query3);



        for ($i = 0; $i < count($resource); $i++) {
            $resource[$i]['car_code'] = '';
            $resource[$i]['id'] = '';

            $resource[$i]['zone_name'] = $resource2[0]['zone_name'];
            $resource[$i]['state_code'] = $resource3[0]['state_code'];

            $query1 = "select id,car_code from ptp_seaport where seaport_from='" . $resource[$i]['seaport_from'] . "' and seaport_to='" . $resource[$i]['seaport_to'] . "'";
            $resource1 = operations($query1);
            for ($j = 0; $j < count($resource1); $j++) {

                $resource[$i]['car_code'] .= $resource1[$j]['car_code'] . ",";
                $resource[$i]['id'] .= $resource1[$j]['id'] . "__";
            }
        }

        if (count($resource) > 0 && gettype($resource) != "boolean") {
            $result = global_message(200, 1007, $resource);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* Inser System shutdown data start */

function systemshutdowninfo() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {

        $start_date = $_REQUEST['start_date'];
        $start_date = explode('/', $start_date);
        $start_date = $start_date[2] . "-" . $start_date[0] . "-" . $start_date[1];

        $end_date = $_REQUEST['end_date'];
        $end_date = explode('/', $end_date);
        $end_date = $end_date[2] . "-" . $end_date[0] . "-" . $end_date[1];


        $start_time = (isset($_REQUEST['start_time'])) ? $_REQUEST['start_time'] : '12:00 AM';
        $end_time = (isset($_REQUEST['end_time']) ) ? $_REQUEST['end_time'] : '11:59 PM';

        $end_time = explode(' ', $end_time);
        if ($end_time[1] == pm) {
            $end_time1 = explode(':', $end_time[0]);
            $end_time2 = $end_time1[0] + 12;
            $end_time = $end_time2 . ":" . $end_time1[1];
        } else {
            $end_time = $end_time[0];
        }

        $start_time = explode(' ', $start_time);
        if ($start_time[1] == pm) {
            $start_time1 = explode(':', $start_time[0]);
            $start_time2 = $start_time1[0] + 12;
            $start_time = $start_time2 . ":" . $start_time1[1];
        } else {
            $start_time = $start_time[0];
        }

        $query = "delete from system_shutdown_info where user_id='" . $_REQUEST['user_id'] . "'";

        $resource = operations($query);

        $insert_data = 'insert into system_shutdown_info(start_date,end_date,start_time,end_time,message,user_id) values("' . $start_date . '","' . $end_date . '","' . $start_time . '","' . $end_time . '","' . $_REQUEST['system_message'] . '","' . $_REQUEST['user_id'] . '")';


        $system_data = operations($insert_data);

        $result = global_message(200, 1002);
        return $result;
    }
}

/* get system data info */

function getsysteminfo() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query12 = "select * from system_shutdown_info where user_id='" . $_REQUEST['user_id'] . "'";
        $queryResult = operations($query12);

        if (count($queryResult) > 0 && gettype($queryResult) != "boolean") {
            $result = global_message(200, 1007, $queryResult);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* delete system shutdown info */

function deletesysteminfo() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $query = "delete from system_shutdown_info where user_id='" . $_REQUEST['user_id'] . "'";

        $resource = operations($query);

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/* checkSystem shutdown or not */

function checksystem() {

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {


        $isSystemCheck = "select message from system_shutdown_info  where user_id='" . $_REQUEST['user_id'] . "' and (start_date<='" . $_REQUEST['current_date'] . "' and end_date>='" . $_REQUEST['current_date'] . "') and (start_time<='" . $_REQUEST['current_time'] . "' and end_time>='" . $_REQUEST['current_time'] . "')";


        $systemCheck = operations($isSystemCheck);


        if (count($systemCheck) > 0 && gettype($systemCheck) != "boolean") {
            $result = global_message(200, 1007, $systemCheck);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(200, 1006);
    }

    return $result;
}

/* * ***********
  Get Weather Details
 * ************* */

function getWeather() {

    $CountryName = "United States";
    $cityName = $_REQUEST['city'];
    $soapClient = new SoapClient("//www.webservicex.net/globalweather.asmx?WSDL");
    $action = 'GetWeather';
    $result = 'GetWeatherResult';
    $trans_vehicle = $soapClient->$action(array('CountryName' => $CountryName, 'CityName' => $cityName))->$result;
    return $trans_vehicle;
}

?>