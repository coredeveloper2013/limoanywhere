<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');
/*****************************************************************
 * Method:             seo_friendly_url()
 * InputParameter:        string
 * Return:             seo friendly url
 *****************************************************************/
function seo_friendly_url($string)
{
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/'), '-', $string);
    return strtolower(trim($string, '-'));
}

/*****************************************************************
 * Method:             setPaymentGateway()
 * InputParameter:        string
 * Return:             set Holiday
 *****************************************************************/
function setPaymentGateway()
{
    if (!empty($_REQUEST['checkout_option']) && !empty($_REQUEST['stripe_publishable_key']) && !empty($_REQUEST['stripe_secret_key']) && !empty($_REQUEST['amount_type'])) {

        if (empty($_REQUEST['auth_capt_deposit_amt'])) {
            $auth_capt_deposit_amt = 0;
        } else {
            $auth_capt_deposit_amt = $_REQUEST['auth_capt_deposit_amt'];
        }
        $VehicleCode = explode(',', $_REQUEST['vehicle_code']);
        $service_typeObj = explode(',', $_REQUEST['service_typeObj']);
        $addSma = explode(',', $_REQUEST['sma_id']);
        $userId = $_REQUEST['user_id'];


        $apiID = $_REQUEST['apiID'];
        $apiToken = $_REQUEST['apiToken'];
        $payment_gateway = $_REQUEST['payment_gateway'];
        $saveName = $_REQUEST['saveName'];
        $paymentGatewayOptions = $_REQUEST['paymentGatewayOptions'];


        // paymentGatewayOptions    apiID  apiToken

        //          $query1 ='TRUNCATE TABLE payment_gateway_setup';
        // operations($query1);


        $query = 'insert into payment_gateway_setup(checkout_option,stripe_publishable_key,stripe_secret_key,auth_capt_deposit_amt,amount_type,api_id,api_token,payment_gateway_type,user_id,payment_setup_as,is_default,gateway_type) value("' . $_REQUEST["checkout_option"] . '","' . $_REQUEST["stripe_publishable_key"] . '","' . $_REQUEST["stripe_secret_key"] . '","' . $auth_capt_deposit_amt . '","' . $_REQUEST["amount_type"] . '","' . $apiID . '","' . $apiToken . '","' . $payment_gateway . '","' . $userId . '","' . $saveName . '","' . $_REQUEST['is_default'] . '","' . $payment_gateway . '")';

        $setup_id = operations($query);
        for ($i = 0; $i < count($VehicleCode); $i++) {
            $Vehquery = "insert into payment_gateway_vehicle(payment_gateway_setup_id,vehicle_code,user_id) value('" . $setup_id . "','" . $VehicleCode[$i] . "','" . $userId . "')";
            $resource1 = operations($Vehquery);
        }//VehicleCode

        for ($i = 0; $i < count($service_typeObj); $i++) {
            $Vehquery = "insert into payment_gateway_service(payment_gateway_setup_id,service_type,user_id) value('" . $setup_id . "','" . $service_typeObj[$i] . "','" . $userId . "')";
            $resource1 = operations($Vehquery);
        }//VehicleCode


        for ($j = 0; $j < count($addSma); $j++) {
            $Smaquery = "insert into payment_gateway_sma(payment_gateway_setup_id,sma_id,user_id) value('" . $setup_id . "','" . $addSma[$j] . "','" . $userId . "')";
            $resource2 = operations($Smaquery);
        }//addSma


        $result = global_message(200, 1008, $setup_id);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             getPaymentMatrixList()
 * InputParameter:        user_id
 * Return:             get Rate MatrixList
 *****************************************************************/
function getPaymentMatrixList()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query = "Select * from payment_gateway_setup where user_id=" . $_REQUEST['user_id'];
        $resource = operations($query);
        $contents = array();
        if (count($resource) > 0 && gettype($resource) != "boolean") {


            for ($i = 0; $i < count($resource); $i++) {
                $vehicle_code = '';
                $sma_name = '';
                $sma_id = '';
                $value = '';

                $Vehquery = "Select vehicle_code from payment_gateway_vehicle where payment_gateway_setup_id=" . $resource[$i]['id'];
                $resource1 = operations($Vehquery);
                for ($j = 0; $j < count($resource1); $j++) {
                    $vehicle_code .= $resource1[$j]['vehicle_code'] . ',';
                }


                $service_type = "Select service_type from payment_gateway_service where payment_gateway_setup_id=" . $resource[$i]['id'];
                $service_typeResult = operations($service_type);

                $service_typeString = '';
                for ($j = 0; $j < count($service_typeResult); $j++) {
                    $service_typeString .= $service_typeResult[$j]['service_type'] . ',';
                }


                $Smaquery = "Select sma_id,sma_name from payment_gateway_sma,sma where sma.id=payment_gateway_sma.sma_id AND payment_gateway_sma.payment_gateway_setup_id=" . $resource[$i]['id'];
                $resource2 = operations($Smaquery);

                for ($k = 0; $k < count($resource2); $k++) {
                    $sma_name .= $resource2[$k]['sma_name'] . ',';
                    $sma_id .= $resource2[$k]['sma_id'] . ',';
                }


                $contents[$i]['id'] = $resource[$i]['id'];
                $contents[$i]['checkout_option'] = $resource[$i]['checkout_option'];
                $contents[$i]['stripe_publishable_key'] = $resource[$i]['stripe_publishable_key'];
                $contents[$i]['stripe_secret_key'] = $resource[$i]['stripe_secret_key'];
                $contents[$i]['auth_capt_deposit_amt'] = $resource[$i]['auth_capt_deposit_amt'];
                $contents[$i]['amount_type'] = $resource[$i]['amount_type'];
                $contents[$i]['is_default'] = $resource[$i]['is_default'];

                $contents[$i]['api_id'] = $resource[$i]['api_id'];
                $contents[$i]['api_token'] = $resource[$i]['api_token'];
                $contents[$i]['payment_gateway_type'] = $resource[$i]['payment_gateway_type'];


                $contents[$i]['payment_setup_as'] = $resource[$i]['payment_setup_as'];
                $contents[$i]['date_time'] = $resource[$i]['date_time'];
                $contents[$i]['sma_id'] = $sma_id;
                $contents[$i]['sma_name'] = $sma_name;
                $contents[$i]['vehicle_code'] = $vehicle_code;
                $contents[$i]['service_type'] = $service_typeString;
                $getArraySort[$i] = strtoupper($resource[$i]['payment_setup_as'] . '@' . $resource[$i]['id']);


            }


            sort($getArraySort);

            $getSmaId = [];
            $gearraysplit = [];
            for ($i = 0; $i < count($contents); $i++) {
                $gearraysplit = explode("@", $getArraySort[$i]);

                $getSmaId[$i] = $gearraysplit[1];
            }


            $getArrayFullResult = [];
            $m = 0;

            for ($k = 0; $k < count($getSmaId); $k++) {
                for ($l = 0; $l < count($contents); $l++) {
                    if ($contents[$l]['id'] == $getSmaId[$k]) {
                        $getArrayFullResult[$m] = $contents[$l];
                        $m++;

                    }
                }

            }


            // for($i=0; $i<count($resource); $i++)
            // {


            // }
        }
        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


/*****************************************************************
 * Method:             deleteHoliday()
 * InputParameter:        holiday_id
 * Return:             delete Holiday
 *****************************************************************/
function deletePaymentSetup()
{
    if ((isset($_REQUEST['setup_id']) && !empty($_REQUEST['setup_id']))) {
        $rowId = $_REQUEST['setup_id'];
        $query = "delete from payment_gateway_setup where id='" . $rowId . "'";
        $resource = operations($query);

        $query = "delete from payment_gateway_service where payment_gateway_setup_id='" . $rowId . "'";
        $resource = operations($query);

        $query = "delete from payment_gateway_vehicle where payment_gateway_setup_id='" . $rowId . "'";
        $resource = operations($query);

        $query = "delete from payment_gateway_sma where payment_gateway_setup_id='" . $rowId . "'";
        $resource = operations($query);

        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


function editPaymentGateway()
{
    $userId = $_REQUEST['user_id'];
    if (!empty($_REQUEST['checkout_option']) && !empty($_REQUEST['stripe_publishable_key']) && !empty($_REQUEST['stripe_secret_key']) && !empty($_REQUEST['amount_type']) && !empty($_REQUEST['update_Setup_id'])) {

        if (empty($_REQUEST['auth_capt_deposit_amt'])) {
            $auth_capt_deposit_amt = 0;
        } else {
            $auth_capt_deposit_amt = $_REQUEST['auth_capt_deposit_amt'];
        }


        $rowId = $_REQUEST['update_Setup_id'];
        $setup_id = $_REQUEST['update_Setup_id'];

        $VehicleCode = explode(',', $_REQUEST['vehicle_code']);
        $service_typeObj = explode(',', $_REQUEST['service_typeObj']);
        $addSma = explode(',', $_REQUEST['sma_id']);


        // $paymentGatewayOptions=$_REQUEST['paymentGatewayOptions'];

        $apiID = $_REQUEST['apiID'];   //apid
        $apiToken = $_REQUEST['apiToken'];  //token
        $payment_gateway = $_REQUEST['payment_gateway']; //name


        $saveName = $_REQUEST['saveName'];

        $paymentGatewayOptions = $_REQUEST['paymentGatewayOptions'];

        if ($_REQUEST['is_default'] == 1) {
            operations("UPDATE  payment_gateway_setup SET  is_default='0'");
        }

        $query = "UPDATE  payment_gateway_setup SET  checkout_option='" . $_REQUEST['checkout_option'] . "',gateway_type='" . $paymentGatewayOptions . "',stripe_publishable_key='" . $_REQUEST['stripe_publishable_key'] . "',stripe_secret_key='" . $_REQUEST['stripe_secret_key'] . "',auth_capt_deposit_amt='" . $auth_capt_deposit_amt . "',amount_type='" . $_REQUEST['amount_type'] . "',is_default='" . $_REQUEST['is_default'] . "',api_id='" . $apiID . "',api_token='" . $apiToken . "',payment_gateway_type='" . $payment_gateway . "',payment_setup_as='" . $saveName . "'  where id='" . $rowId . "'";
        $resource = operations($query);


        $query = "delete from payment_gateway_service where payment_gateway_setup_id='" . $rowId . "'";
        $resource = operations($query);

        $query = "delete from payment_gateway_vehicle where payment_gateway_setup_id='" . $rowId . "'";
        $resource = operations($query);

        $query = "delete from payment_gateway_sma where payment_gateway_setup_id='" . $rowId . "'";
        $resource = operations($query);


        $resource = operations($query);


        for ($i = 0; $i < count($VehicleCode); $i++) {
            $Vehquery = "insert into payment_gateway_vehicle(payment_gateway_setup_id,vehicle_code,user_id) value('" . $setup_id . "','" . $VehicleCode[$i] . "','" . $userId . "')";
            $resource1 = operations($Vehquery);
        }//VehicleCode

        for ($i = 0; $i < count($service_typeObj); $i++) {
            $Vehquery = "insert into payment_gateway_service(payment_gateway_setup_id,service_type,user_id) value('" . $setup_id . "','" . $service_typeObj[$i] . "','" . $userId . "')";
            $resource1 = operations($Vehquery);
        }//VehicleCode


        for ($j = 0; $j < count($addSma); $j++) {
            $Smaquery = "insert into payment_gateway_sma(payment_gateway_setup_id,sma_id,user_id) value('" . $setup_id . "','" . $addSma[$j] . "','" . $userId . "')";
            $resource2 = operations($Smaquery);
        }//addSma


        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}
