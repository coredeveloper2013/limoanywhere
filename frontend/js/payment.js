var paymentClass={

	_SERVICEPATHServer:_SERVICEPATHSERVICECLIENT,
  _SERVICEPATHServer_email:_SERVICEPATHCLIENT,
  _Serverpath:_SERVICEPATHSERVICECLIENTADDACOUNT,

  getEmailNotifyData:function(){

   var getLocalStorageValue=window.localStorage.getItem("limoanyWhereVerification");

      if(typeof(getLocalStorageValue)=='string'){

        getLocalStorageValue=JSON.parse(getLocalStorageValue);
      }


         var user_id=getLocalStorageValue[0].user_id;
         $('#refresh_overlay').css("display","block");
         $.ajax({

          url: paymentClass._SERVICEPATHServer_email,
          type: 'POST',
          data: "action=get_notify_email&user_id="+user_id,

          success: function(response) {

                    var result=response;
                    if(typeof(result)=='string'){

                      result=JSON.parse(response);

                    }
                  console.log(result);

                  if(result.data.length>0){

                    localStorage.setItem('email_notify_data',JSON.stringify(result.data));
                  }
                 $('#refresh_overlay').css("display","none");
             }

         });

   },

 /*---------------------------------------------
    Function Name: getUserAddress
    Input Parameter:api_id,api_key,account_id
    return:address of the user
  --------------------------------------------*/
    getUserAddress: function () {

        var getLocalStorageValue = window.localStorage.getItem("limoanyWhereVerification");

        if (typeof(getLocalStorageValue) == 'string') {

            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }


        var getLocalUserInfo = window.localStorage.getItem("localUserInfo");
        if (getLocalUserInfo != "guestinfo") {
            getLocalUserInfo = JSON.parse(getLocalUserInfo);
        }
        $.ajax({

            url: paymentClass._Serverpath,
            type: 'POST',
            data: "action=GetAccountAddresses&limoApiID=" + getLocalStorageValue[0].limo_any_where_api_id + "&account_id=" + getLocalUserInfo[0].account_id + "&limoApiKey=" + getLocalStorageValue[0].limo_any_where_api_key,
            success: function (response) {
                var responseHTML = '';
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }


                if (responseObj.ResponseCode == 0) {

                    if (responseObj.Addresses.Address != null) {

                        var address_length = responseObj.Addresses.Address.length;
                        if (address_length > 0) {

                            var recent_rank = parseInt(address_length) - 1;
                            var recent_address = responseObj.Addresses.Address[recent_rank].Address1 + ',' + responseObj.Addresses.Address[recent_rank].City + ',' + responseObj.Addresses.Address[recent_rank].State;

                            var recent_zip = responseObj.Addresses.Address[recent_rank].Zip;
                            var getPaymentdetails = localStorage.getItem('card_details');
                            if (getPaymentdetails == null) {

                                $('#cardHolderAddress').val(recent_address);
                                $('#cardHolderZipCode').val(recent_zip);
                            }
                        } else {

                            var recent_address = responseObj.Addresses.Address.Address1 + ',' + responseObj.Addresses.Address.City + ',' + responseObj.Addresses.Address.State;
                            var recent_zip = responseObj.Addresses.Address.Zip;
                            var getPaymentdetails = localStorage.getItem('card_details');
                            if (getPaymentdetails == null) {

                                $('#cardHolderAddress').val(recent_address);
                                $('#cardHolderZipCode').val(recent_zip);
                            }
                        }
                    }
                }
            }
        });
    },

    /*---------------------------------------------
    Function Name: getUserCreaditCardInfo
    Input Parameter:api_id,api_key,account_id
    return:address of the user
  --------------------------------------------*/
    getUserCreaditCardInfo: function () {

        var getLocalStorageValue = window.localStorage.getItem("limoanyWhereVerification");

        if (typeof(getLocalStorageValue) == 'string') {

            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }


        var getLocalUserInfo = window.localStorage.getItem("localUserInfo");
        if (getLocalUserInfo != "guestinfo") {
            getLocalUserInfo = JSON.parse(getLocalUserInfo);
        }
        $.ajax({

            url: paymentClass._Serverpath,
            type: 'POST',
            data: "action=GetAccountCreditCards&limoApiID=" + getLocalStorageValue[0].limo_any_where_api_id + "&account_id=" + getLocalUserInfo[0].account_id + "&limoApiKey=" + getLocalStorageValue[0].limo_any_where_api_key,
            success: function (response) {
                var responseHTML = '';
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }

                if (responseObj.ResponseCode == 0) {

                    if (responseObj.CreditCards.CreditCard != null) {
                        var cresdit_card_length = responseObj.CreditCards.CreditCard.length;
                        if (cresdit_card_length > 0) {

                            var fetch_credit_card = parseInt(cresdit_card_length) - 1;
                            var exp_date = responseObj.CreditCards.CreditCard[fetch_credit_card].ExpDate;
                            var date_value = exp_date.split('/');
                            var month_value = date_value[0];
                            var year_value = date_value[1];
                            var BillingAddress = responseObj.CreditCards.CreditCard[fetch_credit_card].BillingAddress;
                            var zip_code = responseObj.CreditCards.CreditCard[fetch_credit_card].BillingAddress.ZipCode;

                            var getPaymentdetails = localStorage.getItem('card_details');
                            if (getPaymentdetails == null) {

                                $('#cardMonth').val(month_value);
                                $('#cardYear').val(year_value);
                                $('#cardHolderAddress').val(BillingAddress);
                                $('#cardHolderZipCode').val(zip_code);
                            }
                        } else {

                            var exp_date = responseObj.CreditCards.CreditCard.ExpDate;
                            var date_value = exp_date.split('/');
                            var month_value = date_value[0];
                            var year_value = date_value[1];
                            var BillingAddress = responseObj.CreditCards.CreditCard.BillingAddress;
                            var zip_code = responseObj.CreditCards.CreditCard.ZipCode;
                            var getPaymentdetails = localStorage.getItem('card_details');
                            if (getPaymentdetails == null) {

                                $('#cardMonth').val(month_value);
                                $('#cardYear').val(year_value);
                                $('#cardHolderAddress').val(BillingAddress);
                                $('#cardHolderZipCode').val(zip_code);
                            }
                        }
                    } else {

                        paymentClass.getUserAddress();

                    }
                }
            }

        });
    },


    /*---------------------------------------------
    Function Name: getAirlinename
    Input Parameter:api_id,api_key,account_id
    return:address of the user
  --------------------------------------------*/
    getAirlinename: function () {
        var getLocalUserInfo = window.localStorage.getItem("localUserInfo");
        if (getLocalUserInfo !== undefined && getLocalUserInfo !== null) {
            getLocalUserInfo = JSON.parse(getLocalUserInfo);
            console.log("====================");
            console.log(getLocalUserInfo);
            $.ajax({
                url: paymentClass._Serverpath,
                type: 'POST',
                data: {
                    action: 'GetAirlines',
                },
                success: function (response) {
                    var responseHTML = '';
                    var responseObj = response;
                    var airLineName1 = [];
                    if (typeof(response) == "string") {
                        responseObj = JSON.parse(response);
                    }
                    console.log(responseObj);
                    if (responseObj.ResponseCode == 0) {

                        for (i = 0; i < responseObj.Airlines.Airline.length; i++) {

                            airLineName1.push({
                                "value": responseObj.Airlines.Airline[i].AirlineName,
                                "label": responseObj.Airlines.Airline[i].AirlineName
                            });
                        }

                        var airLineName = airLineName1;
                        console.log(airLineName);
                        setTimeout(function () {
                            $("#service_name").autocomplete({
                                source: airLineName,
                                delay: 0,
                                select: function (event, ui) {
                                    event.preventDefault()
                                    $(this).val(ui.item.label);
                                    var getairline = ui.item.label;
                                    var selectedValue = ui.item.value;
                                    var option = selectedValue;

                                },
                                focus: function () {


                                }

                            });

                        });
                    }

                }

            });
        }
    },

    getSpecialPackageItem: function (id) {
        $('#goldWeddingPackage').modal();
        var getrateSetInformation = window.localStorage.getItem("rateSetInformation");
        if (typeof(getrateSetInformation) == "string") {
            getrateSetInformation = JSON.parse(getrateSetInformation);
        }

        var getLocalBookingInfo = window.localStorage.getItem("bookingInfo");
        if (typeof(getLocalBookingInfo) == 'string') {

            getLocalBookingInfo = JSON.parse(getLocalBookingInfo);

        }
        //var getLocalUserInfo=window.localStorage.getItem("localUserInfo");

        var limoanyWhereVerification = window.localStorage.getItem("limoanyWhereVerification");
        if (typeof(limoanyWhereVerification) == 'string') {

            limoanyWhereVerification = JSON.parse(limoanyWhereVerification);

        }
        var get_service_type = getrateSetInformation.serviceType;
        var pick_date = getrateSetInformation.pickup_date;
        var pick_time = getrateSetInformation.pickup_time;
        var pick_time1 = pick_time.split(' ');
        var pickup_time;
        if (pick_time1[1] == 'PM') {
            var pick_time2 = pick_time1[0].split(':');
            pickup_time = parseInt(pick_time2[0]) + 12 + ":" + pick_time2[1];
        } else {

            pickup_time = pick_time1[0];
        }
        //var rowId = id.split('_');

        var getJsonValueSpecialRequest = {
            "action": "getSpecialPackageItem",
            "rowId": id,
            "user_id": limoanyWhereVerification[0]['user_id'],
            "pickLocation": getLocalBookingInfo.pickuplocation,
            "dropupaddress": getLocalBookingInfo.dropupaddress,
            "vehicle_code": getLocalBookingInfo.vehicle_code,
            "service_type": get_service_type,
            "pick_up_time": pickup_time,
            "pick_up_date": pick_date,

        };
        $.ajax({
            url: paymentClass._SERVICEPATHServer,
            type: 'POST',
            data: getJsonValueSpecialRequest,
            success: function (response) {

                var responseObj = JSON.parse(response);
                responseObj = responseObj.data.newRowId;

                var responseHTML = '';
                $.each(responseObj, function (index, value1) {
                    // console.log(value);
                    $.each(value1, function (index, value2) {
                        responseHTML += '<li style="font-size:14px; line-height:1.5;">' + value2.value + ' </li>';
                        // responseHTML+="<li class='list-group-item'>"+value2.value+'</li>';
                    });


                });

                $('#specialPackageView').html(responseHTML);

            }
        });


    },
    deleteSpecialPackageItem: function (id) {


        $('.specialRequestCheckBox_' + id).attr('checked', false);
        $('#updateSpecialPckBtn').click();

// $('#special_request_div_'+id).remove();


//             var totalGetSpecialAmount=parseFloat($('.specialAmountStart').html());
//             var specialPackageAmount=id.split("_");

//             var totalSpecialAmountAdd=parseFloat(totalGetSpecialAmount)-parseFloat(specialPackageAmount[1]);


//           var grandTotal=parseFloat($('.totalPassengerAmount').html().replace("$",""));
//              var totalAmount1=parseFloat(grandTotal)-parseFloat(specialPackageAmount[1]);
//           // var totalAmount1=parseFloat(grandTotal)+parseFloat(totalSpecialAmountAdd);

//           var totalAmount2= parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
//           //grandTotal=grandTotal+parseInt(newGetRowId[0]);
//           $('.totalPassengerAmount').html("$"+totalAmount2);
//           $('.specialAmount').remove();
//           if(totalSpecialAmountAdd!=0)
//           {
//             $('#passengerSelectVehicleRate').append("<tr class='child specialAmount'><td>special package  Amount</td><td class='specialAmountStart text-right'>"+totalSpecialAmountAdd+"</td></tr>");

//           }


    },
    getSpecialRequestDaTA: function (id) {


        var getrateSetInformation = window.localStorage.getItem("rateSetInformation");
        if (typeof(getrateSetInformation) == "string") {
            getrateSetInformation = JSON.parse(getrateSetInformation);


        }

        var getLocalBookingInfo = window.localStorage.getItem("bookingInfo");
        if (typeof(getLocalBookingInfo) == 'string') {

            getLocalBookingInfo = JSON.parse(getLocalBookingInfo);

        }
        //var getLocalUserInfo=window.localStorage.getItem("localUserInfo");

        var limoanyWhereVerification = window.localStorage.getItem("limoanyWhereVerification");
        if (typeof(limoanyWhereVerification) == 'string') {

            limoanyWhereVerification = JSON.parse(limoanyWhereVerification);

        }
        var get_service_type = getrateSetInformation.serviceType;
        var pick_date = getrateSetInformation.pickup_date;
        var pick_time = getrateSetInformation.pickup_time;
        var pick_time1 = pick_time.split(' ');
        var pickup_time;
        if (pick_time1[1] == 'PM') {
            var pick_time2 = pick_time1[0].split(':');
            pickup_time = parseInt(pick_time2[0]) + 12 + ":" + pick_time2[1];
        } else {

            pickup_time = pick_time1[0];
        }
        //var rowId = id.split('_');

        var getJsonValueSpecialRequest = {
            "action": "specialRequestInfoAllPackage",
            "rowId": id,
            "user_id": limoanyWhereVerification[0]['user_id'],
            "pickLocation": getLocalBookingInfo.pickuplocation,
            "dropupaddress": getLocalBookingInfo.dropupaddress,
            "vehicle_code": getLocalBookingInfo.vehicle_code,
            "service_type": get_service_type,
            "pick_up_time": pickup_time,
            "pick_up_date": pick_date,


        };
        $.ajax({
            url: paymentClass._SERVICEPATHServer,
            type: 'POST',
            data: getJsonValueSpecialRequest,
            success: function (response) {


                var responseObj = response;
                console.log(responseObj);

                if (typeof(response) == "string") {
                    var discountData = true;
                    responseObj = JSON.parse(response);

                    if (responseObj.data) {
                        discountData = responseObj.data.special_code_data[0];
                        responseObj = responseObj.data.newRowId;
                    }


                    console.log(discountData);

                    if (discountData != true && typeof(discountData) != 'undefined') {
                        if (discountData.discount_type == '%') {

                            var special_pkg_discount = parseFloat(Math.round(discountData.discount_value * 100) / 100).toFixed(2);
                            var getPackageAmount = localStorage.getItem('specl_pakage_amount');
                            if (getPackageAmount != null) {
                                var discount_amount = parseFloat(Math.round(getPackageAmount * special_pkg_discount) / 100).toFixed(2);
                            }
                            var ResponseHtml = '<tr class="child specialPackageDiscount "><td>Special Package Discount</td><td class="text-right">-' + discount_amount + '</td></tr>';
                            $('.specialPackageDiscount').remove();
                            $('#passengerSelectVehicleRate').append(ResponseHtml);


                            var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                            var totalAmount1 = parseFloat(grandTotal) - parseFloat(discount_amount);
                            var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
                            //grandTotal=grandTotal+parseInt(newGetRowId[0]);
                            localStorage.setItem('special_package_disc', discount_amount);
                            $('.totalPassengerAmount').html("$" + totalAmount2);
                            //$(

                        } else {


                            var special_pkg_discount = parseFloat(Math.round(discountData.discount_value * 100) / 100).toFixed(2);
                            var ResponseHtml = '<tr class="child specialPackageDiscount "><td>Special Package Discount</td><td class="text-right">-' + special_pkg_discount + '</td></tr>';
                            $('.specialPackageDiscount').remove();
                            $('#passengerSelectVehicleRate').append(ResponseHtml);

                            var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                            var totalAmount1 = parseFloat(grandTotal) - parseFloat(special_pkg_discount);
                            var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
                            //grandTotal=grandTotal+parseInt(newGetRowId[0]);
                            localStorage.setItem('special_package_disc', special_pkg_discount);
                            $('.totalPassengerAmount').html("$" + totalAmount2);
                            //$('.specialPackageDiscount').remove();

                        }

                    } else {


                        $('#special_pkg_promo').show();
                    }


                    var responseHTML = '';
                    $.each(responseObj, function (index, value1) {
                        // console.log(value);
                        $.each(value1, function (index, value2) {

                            responseHTML += '<li style="font-size:14px; line-height:1.5;">' + value2.value + ' </li>';
                            // responseHTML+="<li class='list-group-item'>"+value2.value+'</li>';
                        });


                    });


                    $('#specialPackageView').html(responseHTML);
                    // $('#specialPackage').html(responseHTML);


                }


            }
        });


    }
    ,
    getChildRateFunction: function (numberOfSeat) {

        // alert(numberOfSeat);

        var getVehicleCode = window.localStorage.getItem("bookingInfo");
        var apiInformation = window.localStorage.getItem("limoanyWhereVerification");
        if (typeof(apiInformation == 'string')) {

            apiInformation = JSON.parse(apiInformation);
        }

        var numberOfSeat = numberOfSeat;
        if (typeof(getVehicleCode) == "string") {

            getVehicleCode = JSON.parse(getVehicleCode);
        }
        console.log(getVehicleCode);
        if (typeof(apiInformation) == "string") {
            apiInformation = JSON.parse(apiInformation);


        }


        var pickuplocation = getVehicleCode.pickuplocation;
        var dropoflocation = getVehicleCode.dropupaddress;
        // if(getVehicleCode.updateVehicleInfoservices=="AIRA" || getVehicleCode.updateVehicleInfoservices=="SEAA")
        // {
        //   pickuplocation=getVehicleCode.dropupaddress
        // }


        var fullJson = {
            "action": "getChildSeatRate",
            "numberOfSeat": numberOfSeat,
            "user_id": apiInformation[0].user_id,
            "pickuplocation": pickuplocation,
            "dropoflocation": dropoflocation,
            "vehicle_code": getVehicleCode.vehicle_code,
            "service_type": getVehicleCode.updateVehicleInfoservices
        };


        $.ajax({
            url: paymentClass._SERVICEPATHServer,
            type: 'POST',
            data: fullJson,
            success: function (response) {
                var responseObj = response;
                var responseObjHTML = '';
                if (typeof(response) == "string") {

                    responseObj = JSON.parse(response);


                    if (responseObj.code == 1006) {
                        $('#childInformation').hide();
                        if (responseObj.data == 1) {

                            alert("Please Select Maximum Of One Seat");

                        }
                        else if (responseObj.data = 2) {
                            alert("Please Select Maximum Of Two Seat");


                        }
                        else if (responseObj.data = 3) {
                            alert("Please Select Maximum Of Three Seat");


                        } else if (responseObj.data = 4) {
                            alert("Please Select Maximum Of Four Seat");


                        }

                    }
                    if (responseObj.code == 1007) {

                        $('#childInformation').show();
                        responseObjHTML = "<tr class='child childSeatExtra' ><td >Child Seat Rate</td><td class='childSeatExtraRate text-right' >" + responseObj.data + "</td></tr>";

                    }
                    var totalAmount = $('.totalPassengerAmount').html().replace('$', '');
                    var oldRate = $('.childSeatExtraRate').html();
                    if (oldRate != undefined) {
                        totalAmount = parseFloat(totalAmount) - parseFloat(oldRate);
                    }

                    if (responseObj.code == 1007) {
                        totalAmount = parseFloat(totalAmount) + parseFloat(responseObj.data);
                    }


                    $(".childSeatExtra").remove();
                    $('#passengerSelectVehicleRate').append(responseObjHTML);
                    $('.totalPassengerAmount').html("$" + totalAmount);


                }


            }
        });


    },

    getAutoDispuntPackagePromoCode: function (rowId) {


        /*special request auto applied code start here*/


        var limoanyWhereVerification = window.localStorage.getItem("limoanyWhereVerification");
        if (typeof(limoanyWhereVerification) == 'string') {

            limoanyWhereVerification = JSON.parse(limoanyWhereVerification);

        }


        var fullJsonDisCount;
        if ($.trim(promocode) != '') {
            fullJsonDisCount = {
                "action": "getAutoDispuntPackagePromoCode",
                "specailRequestPackage": rowId,
                "user_id": limoanyWhereVerification[0]['user_id']
            }
        }

        // console.log(fullJsonDisCount);


        $.ajax({
            url: paymentClass._SERVICEPATHServer,
            type: 'POST',
            data: fullJsonDisCount,
            success: function (response) {

                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);


                }


                var discountHTML = '';
                if (responseObj.code == 1006) {
                    $('.specialPackageDiscount').remove();

                    localStorage.setItem('spcl_package_promo', responseObj.data.all_value[0].code);
                    localStorage.setItem('spcl_combined_discount', responseObj.data.all_value[0].is_combine_discount)
                    localStorage.setItem('apply_auto', responseObj.data.all_value[0].promo_pref)
                    var special_pkg_discount = 0;
                    var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                    // alert($('#specialPackageAmount').html());

                    if ($('#specialPackageAmount').html() != undefined) {
                        var specialDiscountAmount = $('#specialPackageAmount').html();
                        special_pkg_discount = parseFloat(specialDiscountAmount);
                        grandTotal = parseFloat(grandTotal) + parseFloat(special_pkg_discount);
                    }

                    var ResponseHtml = '<tr class="child specialPackageDiscount"><td>Add-on Discount</td><td class="text-right ">-<span id="specialPackageAmount">' + parseFloat(Math.round(responseObj.data.discount_amount * 100) / 100).toFixed(2) + '</span></td></tr>';

                    $('#passengerSelectVehicleRate').append(ResponseHtml);


                    // var special_pkg_discount=parseFloat($('.specialAmountStart').html().replace("$",""));


                    var totalAmount1 = parseFloat(grandTotal) - parseFloat(responseObj.data.discount_amount);
                    var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);

                    localStorage.setItem('special_package_disc', special_pkg_discount);
                    $('.totalPassengerAmount').html("$" + totalAmount2);

                    if (responseObj.data.all_value[0].is_combine_discount == '1') {

                        $('#special_promocodeBtn').prop('disabled', false);
                    } else {

                        $('#special_promocodeBtn').prop('disabled', true);

                        //localStorage.removeItem('spcl_package_promo');
                    }

                    $('#special_promocode').val('');

                } else {


                    localStorage.removeItem('spcl_package_promo');
                    localStorage.removeItem('spcl_combined_discount');
                    localStorage.setItem('apply_auto', '0')
                }


            }
        });


        /*special request auto applied code end here*/


    },

    paymentPageOnload: function () {

        var checkCombineDiscount = window.localStorage.getItem("combine_discount");
        var chechPromoCodeStatus = window.localStorage.getItem("promo_prefrence");

        if (checkCombineDiscount != undefined) {


            if (chechPromoCodeStatus != undefined) {


                if (chechPromoCodeStatus == '1' && checkCombineDiscount == '0') {


                    //$('#promocodeBtn').prop("disabled",true);


                }
                else if (chechPromoCodeStatus == '1' && checkCombineDiscount == '1') {

                    // $('#promocodeBtn').prop("disabled",false);


                }

            }


        }


        $('#promocodeBtn').on("click", function () {

            var promocode_value = $('#promocode').val();

            if (promocode_value == '') {

                alert('Promocode value should not be blank');
                return false;
            }


            var localStorageValue = window.localStorage.getItem("bookingInfo");
            var rateSetInformation = window.localStorage.getItem("rateSetInformation");

            if (typeof(localStorageValue) == "string") {

                localStorageValue = JSON.parse(localStorageValue);
                rateSetInformation = JSON.parse(rateSetInformation);


            }

            console.log(localStorageValue.AllRateJson);
            var totalCupponDiscount = 0;
            $.each(localStorageValue.AllRateJson, function (index, discountsum) {

                totalCupponDiscount += parseFloat(discountsum.baseRate);

            });
            console.log(totalCupponDiscount);
            var vehicle_code = localStorageValue.vehicle_code;

            var serviceType = rateSetInformation.serviceType;
            var user_id = rateSetInformation.user_id;


            var chechPromoCodeStatus = window.localStorage.getItem("promo_prefrence");
            var checkCombineDiscount = window.localStorage.getItem("combine_discount");
            var promocode = $("#promocode").val();
            var pickupDate = $(".pickupDate").html();
            var pickupTime = $(".pickupTime").html();

            var pickuplocation = localStorageValue.pickuplocation;

            if (serviceType == "AIRD" || serviceType == "AIRD") {
                pickuplocation = localStorageValue.dropupaddress;
            }
            var fullJsonDisCount;
            if ($.trim(promocode) != '') {
                fullJsonDisCount = {
                    "action": "getDiscountPromoCode",
                    "promocode": promocode,
                    "pickupDate": pickupDate,
                    "pickupTime": pickupTime,
                    "vehicle_code": vehicle_code,
                    "serviceType": serviceType,
                    "pickuplocation": pickuplocation,
                    "user_id": user_id
                }
            }

            // console.log(fullJsonDisCount);


            $.ajax({
                url: paymentClass._SERVICEPATHServer,
                type: 'POST',
                data: fullJsonDisCount,
                success: function (response) {

                    var responseObj = response;
                    if (typeof(response) == "string") {
                        responseObj = JSON.parse(response);


                    }
                    var discountAmount = 0;
                    var discountHTML = '';
                    if (responseObj.code == 1006) {
                        var getPromoId = window.localStorage.getItem("promo_code_id");
                        var get_promo_code = localStorage.getItem('auto_promo_code');
                        if (responseObj.data[0].code == get_promo_code) {
                            alert('Promo code already applied try another promocode');
                            return false;

                        }

                        if ((responseObj.data[0].promo_pref == '1' || responseObj.data[0].promo_pref == '2') && responseObj.data[0].is_combine_discount == '1' && checkCombineDiscount == '1') {
                            if (responseObj.data[0].discount_type == '%') {

                                if (responseObj.data[0].apply_rate_on == 'GRT') {

                                    discountAmount = (parseFloat(totalCupponDiscount) * responseObj.data[0].discount_value) / 100;
                                    discountAmount = parseFloat(Math.round(discountAmount * 100) / 100).toFixed(2);
                                    discountAmount = '-' + discountAmount;
                                } else {

                                    discountAmount = (parseFloat(localStorageValue.AllRateJson[0].baseRate) * responseObj.data[0].discount_value) / 100;
                                    discountAmount = parseFloat(Math.round(discountAmount * 100) / 100).toFixed(2);
                                    discountAmount = '-' + discountAmount;

                                }
                            } else {

                                discountAmount = parseFloat(Math.round(responseObj.data[0].discount_value * 100) / 100).toFixed(2);
                                discountAmount = '-' + discountAmount;
                            }


                            discountHTML = '<tr class="child discountAmount"><td>Promo Code Discount Amount</td><td class="discountAmountStart text-right">' + discountAmount + '</td></tr>';
                            var promoDiscountRate = $('.discountAmountStart').html();
                            var grandTotal = 0;
                            var subtractAmountTotal = 0;


                            if (typeof(promoDiscountRate) != "undefined") {


                                subtractAmountTotal = parseFloat(promoDiscountRate);


                            }

                            $('.discountAmount').remove();
                            $("#passengerSelectVehicleRate").append(discountHTML);

                            var prev_total = parseFloat($('.totalPassengerAmount').html().replace("$", ""));


                            grandTotal = parseFloat(prev_total) + parseFloat(discountAmount) - parseFloat(subtractAmountTotal);
                            grandTotal = parseFloat(Math.round(grandTotal) * 100 / 100).toFixed(2);

                            $('.totalPassengerAmount').html("$" + grandTotal);
                        } else if ((responseObj.data[0].promo_pref == '1' || responseObj.data[0].promo_pref == '2') && checkCombineDiscount == '0') {

                            var get_promo_code = localStorage.getItem('auto_promo_code');
                            alert('The promo -code you have entered cannot be combined with existing promo discount(' + get_promo_code + ").");
                            return false;

                        } else if (responseObj.data[0].promo_pref == '2' && checkCombineDiscount == '0') {

                            if (responseObj.data[0].discount_type == '%') {

                                if (responseObj.data[0].apply_rate_on == 'GRT') {

                                    discountAmount = (parseFloat(totalCupponDiscount) * responseObj.data[0].discount_value) / 100;
                                    discountAmount = parseFloat(Math.round(discountAmount * 100) / 100).toFixed(2);
                                    discountAmount = '-' + discountAmount;
                                } else {

                                    discountAmount = (parseFloat(localStorageValue.AllRateJson[0].baseRate) * responseObj.data[0].discount_value) / 100;
                                    discountAmount = parseFloat(Math.round(discountAmount * 100) / 100).toFixed(2);
                                    discountAmount = '-' + discountAmount;

                                }
                            } else {

                                discountAmount = parseFloat(Math.round(responseObj.data[0].discount_value * 100) / 100).toFixed(2);
                                discountAmount = '-' + discountAmount;
                            }


                            discountHTML = '<tr class="child discountAmount"><td>Promo Code Discount Amount</td><td class="discountAmountStart text-right">' + discountAmount + '</td></tr>';
                            var promoDiscountRate = $('.discountAmountStart').html();
                            var grandTotal = 0;
                            var subtractAmountTotal = 0;


                            if (typeof(promoDiscountRate) != "undefined") {


                                subtractAmountTotal = parseFloat(promoDiscountRate);


                            }

                            $('.discountAmount').remove();
                            $("#passengerSelectVehicleRate").append(discountHTML);

                            var prev_total = parseFloat($('.totalPassengerAmount').html().replace("$", ""));


                            grandTotal = parseFloat(prev_total) + parseFloat(discountAmount) - parseFloat(subtractAmountTotal);
                            grandTotal = parseFloat(Math.round(grandTotal) * 100 / 100).toFixed(2);

                            $('.totalPassengerAmount').html("$" + grandTotal);

                        } else {

                            var responseResult = response;

                            if (responseResult.data == false) {

                                alert('The promo-code you have entered is invalid');
                                return false;
                            }
                            if (typeof(responseResult) == "string") {
                                responseResult = JSON.parse(responseResult);

                            }

                            var pickup_date = new Date(pickupDate);
                            var expire_date = responseResult.data[0].end_date;
                            var expire_date = expire_date.split('-');
                            var expire_date = expire_date[1] + "/" + expire_date[2] + "/" + expire_date[0];
                            var promoExpire_date = new Date(expire_date);
                            if (pickup_date > promoExpire_date) {
                                var msg = "The code you have entered has expired(" + promoExpire_date + ").";
                                alert(msg);
                                return 0;

                            }


                        }

                    }
                    else {

                        var promoDiscountRate = $('.discountAmountStart').html();
                        if (typeof(promoDiscountRate) != "undefined") {
                            // alert("New amount"+subtractAmountTotal);

                            subtractAmountTotal = parseFloat(promoDiscountRate);
                            grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", "")) - parseFloat(subtractAmountTotal) + parseFloat(discountAmount);


                            $('.totalPassengerAmount').html("$" + grandTotal);

                        }


                        $(".discountAmount").remove();

                        var responseResult = response;


                        if (typeof(responseResult) == "string") {
                            responseResult = JSON.parse(responseResult);

                        }
                        console.log(responseResult);
                        if (responseResult.data == false) {

                            alert('The promo-code you have entered is invalid');
                            return false;
                        }


                        var pickup_date = new Date(pickupDate);
                        var expire_date = responseResult.data[0].end_date;
                        var expire_date = expire_date.split('-');
                        var expire_date = expire_date[1] + "/" + expire_date[2] + "/" + expire_date[0];
                        var promoExpire_date = new Date(expire_date);
                        if (pickup_date > promoExpire_date) {
                            var msg = "The code you have entered has expired(" + expire_date + ").";
                            alert(msg);
                            return 0;

                        }

                    }

                    // alert("successfully");


                }
            });


        });

        /* special discount package start */
        $('#special_promocodeBtn').on("click", function () {
            var spcl_promo_cod = $('#special_promocode').val();

            if (spcl_promo_cod == '') {
                alert('Please enter Add-on promo code.');
                return false;
            }

            var add_on_promo = localStorage.getItem('spcl_package_promo');
            var spcl_combo_discount = localStorage.getItem('spcl_combined_discount');
            if (add_on_promo != null) {
                if (add_on_promo == spcl_promo_cod) {
                    alert('You have already Applied this promo code');
                    return false;
                }
            }

            if (spcl_combo_discount == null) {

                spcl_combo_discount = '0';
            }
            var localStorageValue = window.localStorage.getItem("bookingInfo");
            var rateSetInformation = window.localStorage.getItem("rateSetInformation");

            if (typeof(localStorageValue) == "string") {

                localStorageValue = JSON.parse(localStorageValue);
                rateSetInformation = JSON.parse(rateSetInformation);


            }
            var vehicle_code = localStorageValue.vehicle_code;

            var serviceType = rateSetInformation.serviceType;
            var user_id = rateSetInformation.user_id;

            var specailRequestPackage = [];


            $('.SpecialPckData').each(function (index, elementId) {
                var getSeq = $(this).attr("package_id");


                specailRequestPackage.push(getSeq);


            });
            // console.log(specailRequestPackage);


            var promocode = $("#special_promocode").val();
            localStorage.setItem('add_on_promo_code', promocode);
            var pickupDate = $(".pickupTime").html();
            var pickupTime = $(".pickupDate").html();

            var pickuplocation = localStorageValue.pickuplocation;

            if (serviceType == "AIRD" || serviceType == "AIRD") {
                pickuplocation = localStorageValue.dropupaddress;
            }


            var fullJsonDisCount;
            if ($.trim(promocode) != '') {
                fullJsonDisCount = {
                    "action": "getDiscountPackagePromoCode",
                    "specailRequestPackage": specailRequestPackage,
                    "promocode": promocode,
                    "pickupDate": pickupDate,
                    "pickupTime": pickupTime,
                    "vehicle_code": vehicle_code,
                    "serviceType": serviceType,
                    "pickuplocation": pickuplocation,
                    "spcl_combo_discount": spcl_combo_discount,
                    "user_id": user_id
                }
            }

            // console.log(fullJsonDisCount);


            $.ajax({
                url: paymentClass._SERVICEPATHServer,
                type: 'POST',
                data: fullJsonDisCount,
                success: function (response) {

                    var responseObj = response;
                    if (typeof(response) == "string") {
                        responseObj = JSON.parse(response);

                    }

                    var discountHTML = '';
                    if (responseObj.code == 1006) {

                        var promoDiscountRate = $('.specialPackageDiscount').html();
                        var auto_apply = localStorage.getItem('apply_auto');
                        if (typeof(promoDiscountRate) != "undefined" && auto_apply == '1') {

                            var ResponseHtml = '<tr class="child specialPackageDiscount1 "><td>Add-on Discount Ex</td><td class="text-right">-<span id="discountAmount">' + parseFloat(Math.round(responseObj.data * 100) / 100).toFixed(2) + '</span></td></tr>';
                            $('.specialPackageDiscount1').remove();
                            $('#passengerSelectVehicleRate').append(ResponseHtml);

                            var special_pkg_discount = 0;
                            if ($('#discountAmount')) {
                                special_pkg_discount = parseFloat($('#discountAmount').html());
                            }

                            var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                            var totalAmount1 = parseFloat(grandTotal) - parseFloat(special_pkg_discount);
                            var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);

                            localStorage.setItem('special_package_disc', special_pkg_discount);
                            $('.totalPassengerAmount').html("$" + totalAmount2);

                            alert("Add-on Promo Code Applied Successfully");
                            $('#special_promocode').val('');

                        } else {
                            localStorage.removeItem('add_on_promo_code');
                            var ResponseHtml = '<tr class="child specialPackageDiscount "><td>Add-on Discount</td><td class="text-right">-<span id="discountAmount">' + parseFloat(Math.round(responseObj.data * 100) / 100).toFixed(2) + '</span></td></tr>';
                            $('.specialPackageDiscount').remove();
                            $('#passengerSelectVehicleRate').append(ResponseHtml);

                            var special_pkg_discount = 0;
                            if ($('#discountAmount')) {
                                special_pkg_discount = parseFloat($('#discountAmount').html());
                            }

                            var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                            var totalAmount1 = parseFloat(grandTotal) - parseFloat(special_pkg_discount);
                            var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);

                            localStorage.setItem('special_package_disc', special_pkg_discount);
                            $('.totalPassengerAmount').html("$" + totalAmount2);

                            alert("Add-on Promo Code Applied Successfully");
                            $('#special_promocode').val('');

                        }

                    } else {


                        alert("Add-on Promo code Invalid");
                    }


                }
            });


        });

        /* special discount package end */
        /* fetch child seat rate start here */

        $('#childSeatDelete').on("click", function () {

            $('#riding_child_btn').show();
            $('#childValueStorhere').html('');
            $('#childInformation').hide('');
            $('#riding_child_btn').html('Riding with a child?');
            $('#childSeatFrom')[0].reset();

            var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));

            var chiSeatRate = parseFloat($('.childSeatExtraRate').html());

            grandTotal = grandTotal - chiSeatRate;

            $('.totalPassengerAmount').html("$" + grandTotal);
            $('.childSeatExtra').remove();
        })

        $('#updateChildSeat').on("click", function () {
            var getFirstSeat = $('#typeChildSeat1').attr("seq");
            var getSecondSeat = $('#typeChildSeat2').attr("seq");
            var getThirstSeat = $('#typeChildSeat3').attr("seq");
            var totalNumberOfSeat = 0;
            var concanateSeats = '';
            var getSeatFirstNumber = $('#qntSeats1').val();
            var getSeatSecondNumber = $('#qntSeats2').val();
            var getThirdSeatNumber = $('#qntSeats3').val();

            if (getSeatFirstNumber != '') {


                concanateSeats = getFirstSeat + "(" + getSeatFirstNumber + ")";
                totalNumberOfSeat += parseInt(getSeatFirstNumber);

            }
            if (getSeatSecondNumber != '') {

                concanateSeats += "," + getSecondSeat + "(" + getSeatSecondNumber + ")";
                totalNumberOfSeat += parseInt(getSeatSecondNumber);

            }
            if (getThirdSeatNumber != '') {

                concanateSeats += "," + getThirstSeat + "(" + getThirdSeatNumber + ")";


                totalNumberOfSeat += parseInt(getThirdSeatNumber);

            }


            if (totalNumberOfSeat != 0) {

                localStorage.setItem('total_child', totalNumberOfSeat);
                if (totalNumberOfSeat < 4) {


                    $('#myModal1').modal('hide');
                    $('#childValueStorhere').html(concanateSeats);
                    $('#riding_child_btn').html("Child's car seat selection");
                    paymentClass.getChildRateFunction(totalNumberOfSeat);

                }
                else {

                    alert("Total child seat must not exceed than 3 seat.");

                }

            }


        });


        /* fetch child seat rate end here */


        // alert(12);

        var getLocalBookingInfo = window.localStorage.getItem("bookingInfo");
        var getLocalUserInfo = window.localStorage.getItem("localUserInfo");

        var limoanyWhereVerification = window.localStorage.getItem("limoanyWhereVerification");
        if (typeof(limoanyWhereVerification) == 'string') {

            limoanyWhereVerification = JSON.parse(limoanyWhereVerification);

        }

        // console.log(limoanyWhereVerification);


        // alert(getLocalUserInfo);
        if (getLocalUserInfo != "guestinfo") {

            getLocalUserInfo = JSON.parse(getLocalUserInfo);
            // console.log(getLocalUserInfo);

            $('#passengerFirstName').val(getLocalUserInfo[0].f_name)
            $('#passengerMobileNumber').val(getLocalUserInfo[0].cerl_number)
            $('#passengerLastName').val(getLocalUserInfo[0].l_name)
            $('#passengerEmailName').val(getLocalUserInfo[0].eml_address)
        }


        if (typeof(getLocalBookingInfo) == "string") {
            getLocalBookingInfo = JSON.parse(getLocalBookingInfo);


        }

        var getrateSetInformation = window.localStorage.getItem("rateSetInformation");
        if (typeof(getrateSetInformation) == "string") {
            getrateSetInformation = JSON.parse(getrateSetInformation);


        }
        var get_service_type = getrateSetInformation.serviceType;
        var pick_date = getrateSetInformation.pickup_date;
        var pick_time = getrateSetInformation.pickup_time;
        var pick_time1 = pick_time.split(' ');
        var pickup_time;
        if (pick_time1[1] == 'PM') {
            var pick_time2 = pick_time1[0].split(':');
            pickup_time = parseInt(pick_time2[0]) + 12 + ":" + pick_time2[1];
        } else {

            pickup_time = pick_time1[0];
        }
        //var rowId = id.split('_');


        var getJsonValueSpecialRequest = {


            "action": "specialRequestInfo",
            "user_id": limoanyWhereVerification[0]['user_id'],
            "pickLocation": getLocalBookingInfo.pickuplocation,
            "dropupaddress": getLocalBookingInfo.dropupaddress,
            "vehicle_code": getLocalBookingInfo.vehicle_code,
            "service_type": get_service_type,
            "pick_up_time": pickup_time,
            "pick_up_date": pick_date


        };

        // console.log(getJsonValueSpecialRequest);

        $.ajax({
            url: paymentClass._SERVICEPATHServer,
            type: 'POST',
            data: getJsonValueSpecialRequest,
            success: function (response) {
                // console.log(response);


                var responseObj = response;
                if (typeof(response) == "string") {

                    responseObj = JSON.parse(response);


                }

                // console.log(responseObj);


                if (responseObj.code == "1006") {

                    console.log(responseObj.data);
                    var getSelectBox = "";
                    var specialRequestPackageHtml = '';


                    $.each(responseObj.data, function (index, value) {

                        console.log(value);
                        var type = value.id + '_' + value.amount;
                        specialRequestPackageHtml += '<div class="checkbox col-xs-12" > <label style="font-size:14px; line-height:1.5;"><input type="checkbox" class="pck_code_chk specialRequestCheckBox_' + type + '" value="" seq="' + type + '"><span id="package_id_' + type + '">' + value.package_code + '</span> </label> <span style="float:right"><button type="button" class="btn btn-prim"   onclick="paymentClass.getSpecialPackageItem(\'' + type + '\');" style="padding:2px 6px;">View </button></span> </div>';
                        getSelectBox += "<option value='" + value.id + "_" + value.amount + "'>" + value.package_code + "</option>";

                        // console.log(value);

                    })

                    // $.each(responseObj.data,function(index,value){

                    // 		getSelectBox+="<option>"++"</option>";


                    // })


                    $('#showSpecialPackageItem').html(specialRequestPackageHtml);
                    $('#specialRequest').html(getSelectBox);

                    $('#specialRequest').multiselect({
                        maxHeight: 200,
                        buttonWidth: '150px',
                        includeSelectAllOption: true
                    });

                    $('#specialRequestPage').css("display", "block");


                }
                else {
                    $('#specialRequestPage').css("display", "none");


                }


            }
        });


        $('#updateSpecialPckBtn').on('click', function () {
            var selectPackage = [];
            var ischeck = "no";
            var special_request_html = '';


            $('.pck_code_chk').each(function (index, selectValue) {

                var getSeq = $(this).attr('seq');


                if ($(this).is(":checked")) {
                    selectPackage.push(getSeq)

                    special_request_html += '<span id="special_request_div_' + getSeq + ' specialPckDivData" class="SpecialPckData" package_id="' + getSeq + '"><span >' + $('#package_id_' + getSeq).html() + '</span> <span> &nbsp; <i class="fa fa-eye"  onclick="paymentClass.getSpecialPackageItem(\'' + getSeq + '\');" style="cursor:pointer;"></i> &nbsp; <i class="glyphicon glyphicon-trash" onclick="paymentClass.deleteSpecialPackageItem(\'' + getSeq + '\');" style="cursor:pointer;"></i><br><br></span></span>';
                    // special_request_html+=$('#package_id_'+getSeq).html();
                    ischeck = "yes";


                }


            });


            if (ischeck == "no") {

                // return false;
                $('#specialPackageItem').html('');


            }
            else {

                $('#specialPackageItem').html(special_request_html);
                $('#specialPackageRequest').modal("hide");


            }

            var newGetRowId = [];
            var pakageAmount = [];
            var individaul_amt = [];
            var selectvalue = [];
            $(selectPackage).each(function (index, selectedVehicle) {
                selectvalue = selectedVehicle;
                var package_amount = selectedVehicle.split('_');
                pakageAmount.push(package_amount[1]);
                individaul_amt.push(package_amount[0]);
                newGetRowId.push(selectedVehicle);


            });


            var getValueSelectBox = newGetRowId;

            /*   check auto applied code start here*/

            paymentClass.getAutoDispuntPackagePromoCode(getValueSelectBox);


            /*  check auto applied code end here*/


            var package_amount = 0;
            for (i = 0; i < pakageAmount.length; i++) {

                package_amount += parseFloat(pakageAmount[i]);
            }
            localStorage.setItem('specl_pakage_amount', package_amount);
            $('#packageAmount').html(package_amount);

            if (newGetRowId[0] != undefined) {


                $('#packageInfo').css("display", "block");
                paymentClass.getSpecialRequestDaTA(newGetRowId);


                var special_pkg_amount = parseFloat(Math.round(package_amount * 100) / 100).toFixed(2);
                var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));

                var specialAmountCheck = 0;
                if ($('.specialAmountStart').html() != undefined) {
                    specialAmountCheck = parseFloat($('.specialAmountStart').html());

                }


                var specialAmountDiscountCheck = 0;
                if ($('#specialPackageAmount').html() != undefined) {
                    specialAmountDiscountCheck = parseFloat($('#specialPackageAmount').html());

                }
                grandTotal = grandTotal - specialAmountCheck;
                grandTotal = grandTotal + specialAmountDiscountCheck;

                var totalAmount1 = parseFloat(grandTotal) + parseFloat(special_pkg_amount);
                var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);


                $('.totalPassengerAmount').html("$" + totalAmount2);
                $('.specialAmount').remove();

                $('#passengerSelectVehicleRate').append("<tr class='child specialAmount'><td>special package  Amount</td><td class='specialAmountStart text-right'>" + special_pkg_amount + "</td></tr>");


            }
            else {
                $('#packageInfo').css("display", "none");


                var specialAmountPayment = $('.specialAmountStart').html();

                var specialAmount2 = 0;

                if (specialAmountPayment != undefined) {

                    specialAmount2 = parseFloat($('.specialAmountStart').html());

                }


                var specialAmountDiscountDiv = $('#specialPackageAmount').html();

                var specialAmountDiscount = 0;
                if (specialAmountDiscountDiv != undefined) {

                    specialAmountDiscount = parseFloat($('#specialPackageAmount').html());

                }


                var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));

                //var grandTotal


                grandTotal = parseFloat(grandTotal) - parseFloat(specialAmount2);


                grandTotal = parseFloat(grandTotal) + parseFloat(specialAmountDiscount);

                var totalAmount2 = parseFloat(Math.round(grandTotal * 100) / 100).toFixed(2);
                var getSpecialDiscount = localStorage.getItem('special_package_disc');

                //alert(getSpecialDiscount);
                //   if(getSpecialDiscount!=null){

                //     totalAmount2=parseFloat(totalAmount2)+parseFloat(getSpecialDiscount);
                //     totalAmount2=parseFloat(Math.round(totalAmount2 * 100) / 100).toFixed(2);
                //     //totalAmount2=parseFloat(Math.round(totalAmount3 * 100) / 100).toFixed(2);
                //   }
                $('.totalPassengerAmount').html("$" + totalAmount2);

                localStorage.removeItem('special_package_disc')
                $('.specialAmount').remove();
                $('#special_pkg_promo').hide();
                $('.specialPackageDiscount').remove();

                // alert("please select Package");

            }


        })


        //add on concerge fee===========
        var concerse_fees = localStorage.getItem('concierge_fee');
        $('input[type=checkbox][name=add_concerse]').change(function () {

            if ($(this).is(":checked")) {

                var responseObjHTML = "<tr  class='child childSeatExtra' id='concerge_fee' ><td>Concierge Fee</td><td class='concirge_fee text-right'>" + concerse_fees + "</td></tr>"
                $('#passengerSelectVehicleRate').append(responseObjHTML);
                //var totalAmount=$('.totalPassengerAmount').html();
                var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                var totalAmount1 = parseFloat(grandTotal) + parseFloat(concerse_fees);
                var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
                $('.totalPassengerAmount').html("$" + totalAmount2);
            }
            else if ($(this).is(":not(:checked)")) {

                $('#concerge_fee').remove();
                var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                var totalAmount1 = parseFloat(grandTotal) - parseFloat(concerse_fees);
                var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
                $('.totalPassengerAmount').html("$" + totalAmount2);
            }
        });

        //add on concerge fee===========
        var seaport_fee = localStorage.getItem('seaport_fee');
        $('input[type=checkbox][name=add_greeter]').change(function () {

            if ($(this).is(":checked")) {

                var responseObjHTML = "<tr  class='child' id='greeter_fee' ><td>Greeter Fee</td><td class='greeterfee text-right'>" + seaport_fee + "</td></tr>"
                $('#passengerSelectVehicleRate').append(responseObjHTML);
                //var totalAmount=$('.totalPassengerAmount').html();
                var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                var totalAmount1 = parseFloat(grandTotal) + parseFloat(seaport_fee);
                var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
                $('.totalPassengerAmount').html("$" + totalAmount2);
            }
            else if ($(this).is(":not(:checked)")) {

                $('#greeter_fee').remove();
                var grandTotal = parseFloat($('.totalPassengerAmount').html().replace("$", ""));
                var totalAmount1 = parseFloat(grandTotal) - parseFloat(seaport_fee);
                var totalAmount2 = parseFloat(Math.round(totalAmount1 * 100) / 100).toFixed(2);
                $('.totalPassengerAmount').html("$" + totalAmount2);
            }
        });
        /*  special request code end here */


        // console.log(getLocalBookingInfo);
        var vehicleRateStart = '';

        $.each(getLocalBookingInfo.AllRateJson, function (index, result) {


            console.log(result.vehicle_title);
            vehicleRateStart += '<tr class="child "><td>' + result.vehicle_title + '</td><td class="text-right">' + result.baseRate + '</td></tr>'


        });


        $('#passengerSelectVehicleRate').html(vehicleRateStart);
        $('.totalPassengerAmount').html(getLocalBookingInfo.fullTotalAmount);


        var rateSetInformationPayment = window.localStorage.getItem("rateSetInformation");
        if (typeof(rateSetInformationPayment) == "string") {

            rateSetInformationPayment = JSON.parse(rateSetInformationPayment);
        }


        /* stops add here start here */

        if (rateSetInformationPayment.isLocationPut == "yes") {
            // alert(getLocalStorageValue.isLocationPut);
            // $('#add_stop').prop("checked",true);
            $('#add_stop').click();
            $('#vehicleStops').show();
            var stopArray = rateSetInformationPayment.stopAddtress.split('@');

            var stopsShowLocationBetween = '';

            // console.log(stopArray);

            $('#stopLocation_1').val(stopArray[0]);
            if (stopArray[0] != '') {

                // stopsShowLocationBetween="<h4>Stop(s)</h4><br><span class='glyphicon glyphicon-flag' style='color: red;''></span><span style='font-size: 106%;'>Stop</span>&nbsp;&nbsp;&nbsp;#<span>"+stopArray[0]+"</span><br>";
                stopsShowLocationBetween = '<h5 style="margin-bottom:20px;"><img src="images/green-location-icon.png" height="25" style="float:left"><span class="">' + stopArray[0] + '</span></h5>';


            }


            for (var i = 0; i < stopArray.length; i++) {


                if (i != 0 && stopArray[i] != '') {
                    // stopsShowLocationBetween+="<br><span class='glyphicon glyphicon-flag' style='color: red;''></span><span style='font-size: 106%;'>Stop</span>&nbsp;&nbsp;&nbsp;#<span>"+stopArray[i]+"</span><br>";
                    stopsShowLocationBetween += '<h5 style="margin-bottom:20px;"><img src="images/green-location-icon.png" height="25" style="float:left"><span class="">' + stopArray[i] + '</span></h5>';
                }

            }
            // alert(stopsShowLocationBetween);

            $(".passengerStopsPayment").html(stopsShowLocationBetween);


        }

        var getVehicle_url = localStorage.getItem('selected_image');
        if (getVehicle_url != null) {

            $("#selected_vehicle").attr("src", getVehicle_url);

        }

        $('.passengerPickupLocation').html(getLocalBookingInfo.pickuplocation)
        $('.passengerDropOffLocation').html(getLocalBookingInfo.dropupaddress)
        $('.numberOfPassenger').html(getLocalBookingInfo.numberOfPasenger);

        getLocalBookingInfo.numberOfPasenger = parseInt(getLocalBookingInfo.numberOfPasenger);

        if (getLocalBookingInfo.numberOfPasenger == 1) {
            $('#extraSeatDiv').hide();


        }
        else {

            var numberofPassengerHtml = '';
            for (var i = 1; i < getLocalBookingInfo.numberOfPasenger; i++) {


                numberofPassengerHtml += '<h4> &nbsp;&nbsp;&nbspPassenger Number #' + i + '</h4><br> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerFirstName' + i + '" name="passengerFirstName" tabindex="2" placeholder="First Name" > </div> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerLastName' + i + '" name="passengerLastName" tabindex="2" placeholder="Last Name"> </div> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerMobileNumber' + i + '" name="passengerMobileNumber" tabindex="2" placeholder="Enter Mobile Number" > </div> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerEmailName' + i + '" name="passengerEmailName" tabindex="2" placeholder="Enter Email" ><br> </div>'

            }

            $('#extrapassengerDetail').html(numberofPassengerHtml);


            $('#extraSeatDiv').show();


        }


        $('#numberOfPassengerInput').val(getLocalBookingInfo.numberOfPasenger);


        $('.vehicleCapacity').html(getLocalBookingInfo.vehiclePassengerCapacity)


        console.log(getLocalBookingInfo);
        $('.vehicleLuggageCapacity').html(getLocalBookingInfo.numberOfLuggage)
        $('.pickupDate').html(getLocalBookingInfo.jurneyDate)
        $('.pickupTime').html(getLocalBookingInfo.jurneyTime)

        $('.pessangerVehicleTitle').html(getLocalBookingInfo.vehicleTitle)


        // $('.pessangerVehicleTitle').html(getLocalBookingInfo.vehicleTitle)


    }

}


/*add booker details function*/

$('#update_booker').on('click', function (event) {
    event.preventDefault();
//var firstname[] = $('#extrapassengerDetail').find('input[name="passengerFirstName[]"]').val();

    var firstname = [];
    var lastname = [];
    var mobile_no = [];
    var email_name = [];


    var first_name = $('#bookerModel #passengerFirstName').val();
    //first_name=$("input[id=passengerFirstName]").val();

    firstname.push(first_name);

    var last_name = $("#bookerModel #passengerLastName").val();

    lastname.push(last_name);


    var mobile_name = $("#bookerModel #passengerMobileNumber").val();

    mobile_no.push(mobile_name);

    var email_id = $("#bookerModel #passengerEmailName").val();
    email_name.push(email_id);


    console.log(firstname);
    var bookDetails = '';
    for (i = 0; i < firstname.length; i++) {
        if (firstname[i] != '') {
            bookDetails += '<h5 class="booker_info_div"> <span style="padding:10px" class="firstname_' + i + '">' + firstname[i] + '</span><span style="padding:10px" class="lastname_' + i + '">' + lastname[i] + '</span><span style="padding:10px" class="mobilenumber_' + i + '">' + mobile_no[i] + '</span><span style="padding:10px" style="display:none;" class="email_id_' + i + '">' + email_name[i] + '</span></h5>'
        }
    }
    console.log(bookDetails);
    $('#booker_details').html(bookDetails);

//$("#extra_passenger_details")[0].reset();
    $('#bookerModel').modal('hide');
    $("#delete_data").attr("id", "delete_booker");
    $('#add_booker').text('Edit Booker');
    $('#add_booker').css('width', '45%');
    $('#delete_booker').show();
});

/*delete booker details start*/

$('#delete_booker').on('click', function () {

    $('#booker_details').html('');
    $("#booker_passenger_details")[0].reset();
    $('#add_booker').text('Add Booking Contact');
    $('#add_booker').css('width', '100%');
    $('#delete_booker').hide();
});

/*delete booker details end*/

/*add booker details function end here*/
$('#extra_passenger_details').on('submit', function (event) {
    event.preventDefault();
//var firstname[] = $('#extrapassengerDetail').find('input[name="passengerFirstName[]"]').val();

    var firstname = [];
    var lastname = [];
    var mobile_no = [];
    var email_name = [];

    var i = 1;
    $("input[name=passengerFirstName]").each(function () {

        var first_name = '';
        //alert($("input[id=passengerFirstName"+i+"]").val());

        if ($("input[id=passengerFirstName" + i + "]").val() == undefined) {
            first_name = '';
        } else {

            first_name = $("input[id=passengerFirstName" + i + "]").val();
        }


        firstname.push(first_name);

        i++;
    });

    var l = 1;
    $("input[name=passengerLastName]").each(function () {
        var last_name = '';

        if ($("input[id=passengerLastName" + l + "]").val() == undefined) {
            last_name = '';
        } else {

            last_name = $("input[id=passengerLastName" + l + "]").val();
        }
        lastname.push(last_name);
        l++;
    });

    var k = 1;
    $("input[name=passengerMobileNumber]").each(function () {
        var mobile_name = '';

        if ($("input[id=passengerMobileNumber" + k + "]").val() == undefined) {
            mobile_name = '';
        } else {

            mobile_name = $("input[id=passengerMobileNumber" + k + "]").val();
        }

        mobile_no.push(mobile_name);
        k++;
    });

    var j = 1;
    $("input[name=passengerEmailName]").each(function () {

        var email_id = '';
        if ($("input[id=passengerEmailName" + j + "]").val() == undefined) {
            email_id = '';
        } else {

            email_id = $("input[id=passengerEmailName" + j + "]").val();
        }

        email_name.push(email_id);
        j++;

    });


    var pasDetails = '';
    for (i = 0; i < firstname.length; i++) {
        if (firstname[i] != '') {
            pasDetails += '<h5 class="add_passenger_info_div"> <span style="padding:10px" class="firstname_' + i + '">' + firstname[i] + '</span><span style="padding:10px" class="lastname_' + i + '">' + lastname[i] + '</span><span style="padding:10px" class="mobilenumber_' + i + '">' + mobile_no[i] + '</span><span style="padding:10px" style="display:none;" class="email_id_' + i + '">' + email_name[i] + '</span></h5>'
        }
    }

    $('#passenger_details').html(pasDetails);

//$("#extra_passenger_details")[0].reset();
    $('#extraPassengerModel').modal('hide');
    $("#delete_data").attr("id", "delete_passenger");
    $('#add_passenger').text('Edit Passenger');
    $('#add_passenger').css('width', '45%');
    $('#delete_passenger').show();
});

$('#delete_passenger').on('click', function () {

    $('#passenger_details').html('');
    $("#extra_passenger_details")[0].reset();
    $('#add_passenger').text('Add More Passenges');
    $('#add_passenger').css('width', '100%');
    $('#delete_passenger').hide();
});

/*Handle customer information*/
var getLocalBookingInfoDetails = window.localStorage.getItem("bookingInfo");
if (typeof(getLocalBookingInfoDetails) == 'string') {
    getLocalBookingInfoDetails = JSON.parse(getLocalBookingInfoDetails);
}

var passenger_info_length = getLocalBookingInfoDetails.numberOfPasenger;

var return_passenger_details = localStorage.getItem('extra_customer_info');
return_passenger_details = $.parseJSON(return_passenger_details);
var get_return_trip = localStorage.getItem('return_trip');

var get_first_pasenger = localStorage.getItem('first_customer_info');
get_first_pasenger = $.parseJSON(get_first_pasenger);


if (get_return_trip == 'Yes') {
    if (get_first_pasenger != null && get_first_pasenger != '') {
        $('#passengerFirstName').val(get_first_pasenger.first_name);
        $('#passengerLastName').val(get_first_pasenger.last_name);
        $('#passengerMobileNumber').val(get_first_pasenger.mobile_no);
        $('#passengerEmailName').val(get_first_pasenger.email_address);
    }

    if (return_passenger_details != null && return_passenger_details != '') {


        var pasDetails = '';
        var return_pass_len_dtl = 0;
        if (return_passenger_details.length >= passenger_info_length) {

            return_pass_len_dtl = passenger_info_length - 1;

        } else {

            return_pass_len_dtl = return_passenger_details.length;
        }

        for (j = 0; j < return_pass_len_dtl; j++) {

            pasDetails += '<h5 class="add_passenger_info_div"> <span style="padding:10px" class="firstname_' + j + '">' + return_passenger_details[j].firstname + '</span><span style="padding:10px" class="lastname_' + j + '">' + return_passenger_details[j].lastname + '</span><span style="padding:10px" class="mobilenumber_' + j + '">' + return_passenger_details[j].MNumber + '</span><span style="padding:10px" style="display:none;" class="email_id_' + j + '">' + return_passenger_details[j].Email_id + '</span></h5>'

        }

        $('#passenger_details').html(pasDetails);
        $('#add_passenger').text('Edit Passenger');
        $('#add_passenger').css('width', '45%');
        $("#add_passenger").attr("id", "show_passenger");
        $("#delete_passenger").attr("id", "delete_data");
        $('#delete_data').show();

        $('#show_passenger').on('click', function () {

            var extara_passenger = '';
            var input_passenger_count = passenger_info_length - 1;


            for (var i = 0; i < input_passenger_count; i++) {
                var divseq = i + 1;
                extara_passenger += '<h4> &nbsp;&nbsp;&nbspPassenger Number #' + divseq + '</h4><br> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerFirstName' + divseq + '" name="passengerFirstName" tabindex="2" placeholder="First Name" value="" > </div> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerLastName' + divseq + '" name="passengerLastName" tabindex="2" value="" placeholder="Last Name"> </div> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerMobileNumber' + divseq + '" name="passengerMobileNumber" tabindex="2" value="" placeholder="Enter Mobile Number" > </div> <div class="col-xs-6 col-sm-6 col-md-6"> <input type="text" class="form-control" id="passengerEmailName' + divseq + '" name="passengerEmailName" value="" tabindex="2" placeholder="Enter Email" ><br> </div>'

            }

            var return_pass_len = return_passenger_details.length;
            if (return_pass_len > input_passenger_count) {
                return_pass_len = input_passenger_count;

            }
            $('#extrapassengerDetail').html(extara_passenger);
            for (var k = 0; k < return_pass_len; k++) {

                var div_id = k + 1;

                $('#passengerFirstName' + div_id).val(return_passenger_details[k].firstname);
                $('#passengerLastName' + div_id).val(return_passenger_details[k].lastname);
                $('#passengerMobileNumber' + div_id).val(return_passenger_details[k].MNumber);
                $('#passengerEmailName' + div_id).val(return_passenger_details[k].Email_id);

            }


//$('#extraPassengerModel').modal('show');

        });

        $('#delete_data').on('click', function () {
            localStorage.removeItem('extra_customer_info');
            $('#passenger_details').html('');

            $("#show_passenger").attr("id", "add_passenger");
            $("#extra_passenger_details")[0].reset();
            $('#add_passenger').text('Add More Passenges');
            $('#add_passenger').css('width', '100%');
            $('#delete_passenger').hide();
        });

        /*$('#add_passenger').on('click',function(){

$("#extra_passenger_details")[0].reset();

});
*/
    }
}
var get_account_id = localStorage.getItem('localUserInfo');
if (get_account_id != 'guestinfo') {
    paymentClass.getUserCreaditCardInfo();

}
paymentClass.getAirlinename();
paymentClass.paymentPageOnload();
paymentClass.getEmailNotifyData();
