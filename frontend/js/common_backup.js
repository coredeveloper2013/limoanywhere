 //code for google map start===========
var Demo = {
  // HTML Nodes
  mapContainer: document.getElementById('map-container'),
  dirContainer: document.getElementById('dir-container'),
  fromInput: document.getElementById('from-input'),
  toInput: document.getElementById('to-input'),
  travelModeInput: document.getElementById('travel-mode-input'),
  unitInput: document.getElementById('unit-input'),

  // API Objects
  dirService: new google.maps.DirectionsService(),
  dirRenderer: new google.maps.DirectionsRenderer(),
  map: null,

  showDirections: function(dirResult, dirStatus) {
    if (dirStatus != google.maps.DirectionsStatus.OK) {
      //alert('Directions failed: ' + dirStatus);
      return;
    }

    // Show directions
    Demo.dirRenderer.setMap(Demo.map);
    Demo.dirRenderer.setPanel(Demo.dirContainer);
    Demo.dirRenderer.setDirections(dirResult);
  },

  getSelectedTravelMode: function() {
    var value ="DRIVING";
    return value;
  },

  getSelectedUnitSystem: function() {
    return 0;
  },
  getDirections: function(startpoint,endpoint) {

      alert(startpoint);
      var fromStr = startpoint;
      var toStr =  endpoint;
      var dirRequest = {
      origin: fromStr,
      destination: toStr,
      travelMode: Demo.getSelectedTravelMode(),
      unitSystem: Demo.getSelectedUnitSystem(),
      provideRouteAlternatives: true
    };
    Demo.dirService.route(dirRequest, Demo.showDirections);
  },

  init: function() {
    var param=parseURLParams(window.location.href);
   
    var latLng = new google.maps.LatLng(37.0902, 95.7129);
    Demo.map = new google.maps.Map(Demo.mapContainer, {
      zoom: 25,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Show directions onload
    // Demo.getDirections(startloc,endloc);
  }
};

  function parseURLParams(url) {
      var queryStart = url.indexOf("?") + 1;
      var queryEnd   = url.indexOf("#") + 1 || url.length + 1;
      var query      = url.slice(queryStart, queryEnd - 1);

      if (query === url || query === "") return;

      var params  = {};
      var nvPairs = query.replace(/\+/g, " ").split("&");

      for (var i=0; i<nvPairs.length; i++) {
        var nv = nvPairs[i].split("=");
        var n  = decodeURIComponent(nv[0]);
        var v  = decodeURIComponent(nv[1]);
        if ( !(n in params) ) {
          params[n] = [];
        }
        params[n].push(nv.length === 2 ? v : null);
      }
      return params;
    }

//google.maps.event.addDomListener(window, 'load', Demo.init);


 //code for google map end============



//global variable declare for map ====

var option;
var startlocation;
var endlocation;

//global variable declare for map end ====

//stoplocation value on yes and no radi button ====

 $('input[type=radio][name=add_stop]').change(function() {

           alert(this.value);
        if (this.value == 'yes') {
           
           //$('.form-control2').show();
            
            $('#stop_points').show();
            $('.aadMore').html('<div class="stopLocationfield"><input type="text" id="stopLocation" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>');
             
              var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation'),options);
             

        }
        else if (this.value == 'no') {
           $('#stop_points').hide();
             $('.aadMore').html(' ');
        }
    });
  

  var options = {
    types: ['(cities)'],
     componentRestrictions: {country: "us"}
     
   };   
     
    $('#services').change(function()
    {


    option = $(this).find('option:selected').val();
        if(option=='AIRA'){

            $('#intFlt').show();    
        }
        else{
            $('#intFlt').hide();
        }



        //reset the all form value=======

          $('.stopLocationfield').remove();

          $('#searchRateForm')[0].reset();
          $('#services').val(option);
          $('#stop_points').hide();
          $("#location_map").hide();
      /*again reset the passenger and luggage quantity to zero start===*/
           /*$("input[name='luggage_quantity']").TouchSpin({
                initval: 0
              });*/
               $('#luggage_quantity').val("0");
               $('#total_passenger').val("0");
           /*  $("input[name='total_passenger']").TouchSpin({
                initval: 0
            });*/ 
       /*again reset the passenger and luggage quantity to zero end===*/


         option = $(this).find('option:selected').val();
         if(option=='PTP'){
        
            $('#map_loaction').show();
            $('#stop_point').show();
            $('#triphr').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#pick_hourly_loc').hide();
            $('#pick_charter_loc').hide();
            //$('#drop_charte_loc').hide();
            $('#drop_point_loc').show();
            $('#from_pont_to_point').show();
            $('#pick_hourly').hide();
            $('#from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_seapick').hide();
            //$('#drop_hourly_loc').hide();
            $('#to_seadrop').hide();  
        }else if(option=='CH'){
/*
            google.maps.event.addDomListener(window, 'load', function () {
               var places = new google.maps.places.Autocomplete(document.getElementById('pick_charter'),options);
             });*/
            $('#map_loaction').hide();
            $('#stop_point').hide();
            $('#triphr').show();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#pick_hourly_loc').hide();
            
            $('#pick_charter_loc').show();
           //$('#drop_charte_loc').show();
            $('#from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_seapick').hide();
            //$('#drop_hourly_loc').hide();
            $('#to_seadrop').hide();  
        
         }else if(option=='HRLY'){

            /* google.maps.event.addDomListener(window, 'load', function () {
               var places = new google.maps.places.Autocomplete(document.getElementById('pick_hourly'),options);
             });*/
            $('#map_loaction').hide();
            $('#stop_point').hide();
            $('#pick_charter_loc').hide();
            //$('#drop_charte_loc').hide(); 
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();

             //$('#drop_hourly_loc').show();
            $('#triphr').show();
            $('#pick_hourly_loc').show();
            $('#pick_hourly').show();
            $('#from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_seapick').hide();
           
            $('#to_seadrop').hide();  
        }
        else if(option=='SEAA'){

            $('#map_loaction').show();
            $('#stop_point').show();
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();

            $('#from_seapick').show();
            $('#from_seadrop').show();
            $('#to_seapick').hide();
           $('#pick_charter_loc').hide();
            //$('#drop_charte_loc').hide();
            $('#to_seadrop').hide();
            $('#triphr').hide();

            }else if(option=='SEAD'){
             $('#map_loaction').show();
             $('#stop_point').show();
            
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#pick_charter_loc').hide();
           // $('#drop_charte_loc').hide();
            $('#from_seapick').hide();
            $('#to_seapick').show();
            $('#pick_hourly_loc').hide();
            $('#to_seadrop').show();
            $('#from_seadrop').hide();
            //$('#drop_from_seaport').hide();
            $('#triphr').hide();

        }
         else if(option=='AIRD'){
            $('#map_loaction').show();
            $('#stop_point').show();
           // $('#drop_hourly_loc').hide();
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#pick_charter_loc').hide();
           // $('#drop_charte_loc').hide();
            $('#triphr').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#from_seapick').hide();
            //$('#droploc').show();
            
            $('from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_airport_pickup').show();
            $('#to_airport_drop').show();
            $('#to_seapick').hide();
            $('#to_seadrop').hide();
           
        }else if(option=='AIRA'){

            $('#map_loaction').show();
            $('#stop_point').show();
           // $('#drop_hourly_loc').hide();
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#to_seapick').hide();
            $('#to_seadrop').hide();
            $('#triphr').hide();
            $('#from_airport_drop').show();
            $('#from_seadrop').hide();
            $('#from_airport_pickup').show();
            $('#from_seadrop').hide();
            $('#pick_charter_loc').hide();
           // $('#drop_charte_loc').hide();
        }
    });
        

       $('#to_airport_droploc').change(function(){

            $('#no_map').prop("checked", true);
            $("#map_icon").show();
            $("#location_map").hide();

       });

        $('#from_airport_pickloc').change(function(){

             alert('changed');

            $('#no_map').prop("checked", true);
            $("#map_icon").show();
            $("#location_map").hide();

       });

        $('input[type=radio][name=map]').change(function() {
        if (this.value == 'yes') {
            
            if(option=='AIRA'){
              //$("#yourdropdownid option:selected").text();
              startlocation = $('#from_airport_pickloc option:selected').text();
              endlocation  =  $('#from_airport_droploc').val();
             

            }else if(option=='AIRD'){
              
              endlocation = $('#to_airport_droploc option:selected').text();
              startlocation =  $('#to_airport_pickloc').val();
              

            }
            else if(option=='PTP'){
              
              startlocation = $('#pick_point').val();
              endlocation  =  $('#drop_point').val();
               
            }
            else if(option=='CH'){
              
              endlocation = $('#slect_hour option:selected').val();
              startlocation  =  $('#pick_charter').val();
            
            }
            else if(option=='SEAA'){
              
              startlocation = $('#pick_from_seaport option:selected').text();
              endlocation  =  $('#drop_from_seaport').val();
             

            }
            else if(option=='SEAD'){
              
              endlocation = $('#drop_to_seaport option:selected').text();
              startlocation =  $('#pick_to_seaport').val();
             

            }
             else if(option=='HRLY'){
              
              endlocation = $('#slect_hour option:selected').val();
              startlocation  =  $('#pick_charter').val();
             
            }
            // Demo.init(startlocation,endlocation);
            if(option==undefined){
               alert("Please Select service");
                 $('#no_map').prop("checked", true);
              }else{

                if(startlocation=='Select' || startlocation=='' ){
                  alert('Plese Provide Start Point');
                  $('#no_map').prop("checked", true);
                 
                }else if(endlocation=='Select' || endlocation=='' ){
                    alert('Plese Provide End Point');
                    $('#no_map').prop("checked", true);
                }else{
                Demo.getDirections(startlocation,endlocation)
                $("#map_icon").hide();
                $("#location_map").show();
                }
           }           
        }
        if (this.value == 'no'){

            $("#map_icon").show();
            $("#location_map").hide();          
        }

   }); 
            google.maps.event.addDomListener(window, 'load', function () {
                  var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('from_airport_droploc'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('to_airport_pickloc'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('drop_from_seaport'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_to_seaport'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_charter'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_point'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('drop_point'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('stoplocation'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_hourly'),options);
             });
       
           
         

getServiceTypes();
function getServiceTypes()
          {
            var serviceTypeData = [];
    $.ajax({
            url : "webservice/service.php",
            type : 'post',
            data : 'action=GetServiceTypes',
            dataType : 'json',
            success : function(data){
                //console.log(data);
                if(data.ResponseText == 'OK'){

                    var ResponseHtml='<option value="">Select</option>';
                    $.each(data.ServiceTypes.ServiceType, function( index, result){
                     ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                     console.log(result);
                                    });
                 
                 
                  
                 $('#services').html(ResponseHtml);
                   
                }
      }});
 }

var getinfo={
       _Serverpath:"phpfile/client.php",

getAirportName:function(){
 
 var getUserId=window.localStorage.getItem('companyInfo');

 var locationdata1 = [];

         if(typeof(getUserId)=="string")
                {   

                    getUserId=JSON.parse(getUserId);

                }

              var fd = new FormData();
                fd.append("action","getAirportName");
                fd.append("user_id",getUserId[0].id)

    $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData:false,
            contentType:false,
            data:fd
    }).done(function(result){

           
            var response=JSON.parse(result);
     
         var ResponseHtml='<option value="">Select</option>';
        for(i=0;i<response.data.length;i++){
            ResponseHtml+="<option value='"+response.data[i].airport_code+"' seq='"+response.data[i].is_int_flight+"' insidemeet_msg='"+response.data[i].insidemeet_msg+"' curbside_msg='"+response.data[i].curbside_msg+"' int_flight_msg='"+response.data[i].int_flight_msg+"' meatandGreet='"+response.data[i].is_insidemeet_greet+"' curbsideSeq='"+response.data[i].is_curbside+"'>"+response.data[i].airport_name+"</option>";
        }
            $('#from_airport_pickloc').html(ResponseHtml);
            $('#from_airport_pickloc').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               });  




            $('#from_airport_pickloc').off();
           $('#from_airport_pickloc').on("change",function(){
                    var option = $(this).find('option:selected').attr('seq');
                    var meatandGreet = $(this).find('option:selected').attr('meatandGreet');
                        
                    var curbsideSeq = $(this).find('option:selected').attr('curbsideSeq');
                    var insidemeet_msg = $(this).find('option:selected').attr('insidemeet_msg');
                    var curbside_msg = $(this).find('option:selected').attr('curbside_msg');
                    var int_flight_msg = $(this).find('option:selected').attr('int_flight_msg');
                    if(meatandGreet!='0'){
                      //var msg ="this is the content";
                        $('#meetAndGreet_disclaimer').attr("data-content",insidemeet_msg); 
                         //var popover = $('#meetAndGreet').data('bs.popover');
                          // popover.a.content = "YOUR NEW TEXT";
                        //$(".popover-content")[0].innerHTML = 'something else';
                        $('#meetAndGreet').show();
                    }
                    else
                    {

                         $('#meetAndGreet').hide();
                    }
                    if(curbsideSeq!='0'){
                        $('#curbside_disclaimer').attr("data-content",curbside_msg);
                        $('#curbSide').show();
                    }
                    else
                    {
                        $('#curbSide').hide();


                    }



                    if(option!='0')
                    {

                        $('#international_disclaimer').attr("data-content",int_flight_msg);
                        $('.interNationFlight').show();
                    }
                    else
                    {
                          $('.interNationFlight').hide();


                    }
           })
           //alert(result);

          $('#to_airport_droploc').html(ResponseHtml);
          $('#to_airport_droploc').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               });  

   });

},

getSeaPortName:function(){
 
 var getUserId=window.localStorage.getItem('companyInfo');

 var locationdata1 = [];

         if(typeof(getUserId)=="string")
                {   

                    getUserId=JSON.parse(getUserId);

                }

                var fd = new FormData();
                fd.append("action","getSeaPortName");
                fd.append("user_id",getUserId[0].id)

    $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData:false,
            contentType:false,
            data:fd
    }).done(function(result){

           
        var response=JSON.parse(result);
      /*$.each(response, function(index, locdata){
        locationdata1.push(locdata.airport_code+' ('+locdata.airport_name+')');
        });*/
           var ResponseHtml='<option value="">Select</option>';
        for(i=0;i<response.data.length;i++){
            ResponseHtml+="<option value='"+response.data[i].seaport_code+"'>"+response.data[i].seaport_name+"</option>";
        }
          

           $('#pick_from_seaport').html(ResponseHtml);
            //$('#from_airport_pickloc').distroy();
             $('#pick_from_seaport').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               });  
           //alert(result);
           
           $('#drop_to_seaport').html(ResponseHtml);
           $('#drop_to_seaport').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               }); 
           //console.log(locationData);
       
          
           //alert(result);

   });

}


};
Demo.init();
//google.maps.event.addDomListener(window, 'load', mapLoc.init);
getinfo.getAirportName();
getinfo.getSeaPortName();
