var getinfo = {
    _Serverpath: "phpfile/client.php",
    _ServerpathVehicle: _SERVICEPATHSERVICECLIENT,

    getAirportNameUpdateFunction: function(inputBoxFieldValue) {
        var getLocalStorageValue = localStorage.getItem("rateSetInformation");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var inputBoxFieldValue = inputBoxFieldValue;
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        inputBoxFieldValue = inputBoxFieldValue.split("(");
        var fd = new FormData();
        fd.append("action", "getAirportName");
        fd.append("inputValue", inputBoxFieldValue[0]);
        $.ajax({
            url: getinfo._Serverpath,
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {
            var locationdata1 = [];
            var response = JSON.parse(result);
            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {
                if (inputBoxFieldValue[0] == response.data[i].airport_name) {
                    locationdata1.push({ "value": response.data[i].is_int_flight + "@@" + response.data[i].insidemeet_msg + "@@" + response.data[i].curbside_msg + "@@" + response.data[i].int_flight_msg + "@@" + response.data[i].is_insidemeet_greet + "@@" + response.data[i].is_curbside, "label": response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' });
                    ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";
                }
            }
            var airportLocation = locationdata1;
            setTimeout(function() {
                if(locationdata1.length > 0){
                    var selectedValue = locationdata1[0].value;
                    var selectedAirportValue = selectedValue.split('@@');
                    var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[4];
                    var curbsideSeq = selectedAirportValue[5];
                    var insidemeet_msg = selectedAirportValue[1];
                    var curbside_msg = selectedAirportValue[2];
                    var int_flight_msg = selectedAirportValue[3];
                    if (meatandGreet != '0' && getLocalStorageValue.ismeetGreetUpdateChecked == "yes") {

                        $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                        $('#intFlt').show();
                        $('#meetGreetUpdate').prop('checked', true);
                        $('#meetAndGreet').show();

                    }
                    else if (meatandGreet != '0') {
                        $('#intFlt').show();

                        $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                        $('#meetAndGreet').show();
                    } else {

                        $('#meetAndGreet').hide();
                    }
                    if (curbsideSeq != '0' && getLocalStorageValue.iscurbsideUpdateChecked == "yes") {
                        $('#curbside_disclaimer').attr("data-content", curbside_msg);
                        $('#intFlt').show();
                        $('#curbsideUpdate').prop('checked', true);
                        $('#curbSide').show();


                    }
                    else if (curbsideSeq != '0') {
                        $('#intFlt').show();
                        $('#curbside_disclaimer').attr("data-content", curbside_msg);
                        $('#curbSide').show();


                    }
                    else {
                        $('#curbSide').hide();
                    }
                    if (option != '0' && getLocalStorageValue.interNationFlightChecked == "yes") {
                        $('#international_disclaimer').attr("data-content", int_flight_msg);
                        $('#intFlt').show();
                        $('#interNationFlightUpdate').prop("checked", true);
                        $('.interNationFlight').show();
                    }
                    else if (option != '0') {

                        $('#international_disclaimer').attr("data-content", int_flight_msg);
                        $('#intFlt').show();

                        $('.interNationFlight').show();

                    }
                    else {
                        $('.interNationFlight').hide();
                    }
                }
            }, 1000);
        });

    },
    getAirportToNameUpdateFunction: function(inputBoxFieldValue) {
        //updateVehicleInfoservices
        var inputBoxFieldValue = inputBoxFieldValue;
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }

        inputBoxFieldValue = inputBoxFieldValue.split("(");

        var fd = new FormData();
        fd.append("action", "getAirportName");
        fd.append("inputValue", inputBoxFieldValue[0]);
        //fd.append("user_id",getUserId.user_id);

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {
            var locationdata1 = [];
            var response = JSON.parse(result);
            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {
                locationdata1.push({ "value": response.data[i].is_int_flight + "@@" + response.data[i].insidemeet_msg + "@@" + response.data[i].curbside_msg + "@@" + response.data[i].int_flight_msg + "@@" + response.data[i].is_insidemeet_greet + "@@" + response.data[i].is_curbside, "label": response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' });
                ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";

            }
            var airportLocation = locationdata1;
            setTimeout(function() {

                var selectedValue = locationdata1.length > 0 ? locationdata1[0].value : '';
                var selectedAirportValue = selectedValue.split('@@');
                var option = selectedAirportValue[0];
                var meatandGreet = selectedAirportValue[4];
                var curbsideSeq = selectedAirportValue[5];
                var insidemeet_msg = selectedAirportValue[1];
                var curbside_msg = selectedAirportValue[2];
                var int_flight_msg = selectedAirportValue[3];

            }, 1000);
        });
    },
    getTrainName: function() {
        var inputBoxFieldValue = inputBoxFieldValue;
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;


        var fd = new FormData();
        fd.append("action", "getTrainName");
        // fd.append("inputValue",inputBoxFieldValue);
        fd.append("user_id", user_id);

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {

            var locationdata1 = [];
            var response = JSON.parse(result);


            var ResponseHtml = '<option value="">Select</option>';

            for (i = 0; i < response.data.length; i++) {

                locationdata1.push({ "value": response.data[i].train_name + '(' + response.data[i].train_code + ')' + '(' + response.data[i].city_name + ')', "label": response.data[i].train_name + '(' + response.data[i].train_code + ')' + '(' + response.data[i].city_name + ')' });
                ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";

            }
            var airportLocation = locationdata1;
            setTimeout(function() {


                /* from airport service autocomplete start here */



                $("#to_train_dropoff_location_input").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function(event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);


                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        var getLocation = ui.item.label;

                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {

                            checkBlackOutDate(getDate, getTime, getLocation);
                        }




                    },
                    focus: function() {



                        //$(this).val('asdfasdfasd');
                    }



                });




                $("#from_train_pickup_location_input").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function(event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);


                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        var getLocation = ui.item.label;

                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {

                            checkBlackOutDate(getDate, getTime, getLocation);
                        }




                    },
                    focus: function() {



                        //$(this).val('asdfasdfasd');
                    }
                }).data("uiAutocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append('<a><i class="fa fa-subway"></i>' + ' &nbsp; ' + item.label)
                        .appendTo(ul);
                };



                /* from airport service autocomplete end here */

                /*$("#to_train_dropof_location_input").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function(event, ui) {


                    },
                    focus: function() {



                        //$(this).val('asdfasdfasd');
                    }
                }).data("uiAutocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append('<a><i class="fa fa-subway"></i>' + ' &nbsp; ' + item.label)
                        .appendTo(ul);
                };*/


            }, 2000);














        });

    },

    getAirportName: function(inputBoxFieldValue) {

        var inputBoxFieldValue = inputBoxFieldValue;
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");

        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        var fd = new FormData();
        fd.append("action", "getAirportName");
        fd.append("inputValue", inputBoxFieldValue);
        fd.append("user_id", user_id);

        //fd.append("user_id",getUserId.user_id);

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {
            var locationdata1 = [];
            var response = JSON.parse(result);


            var ResponseHtml = '<option value="">Select</option>';

            for (i = 0; i < response.data.length; i++) {

                locationdata1.push({ "value": response.data[i].is_int_flight + "@@" + response.data[i].insidemeet_msg + "@@" + response.data[i].curbside_msg + "@@" + response.data[i].int_flight_msg + "@@" + response.data[i].is_insidemeet_greet + "@@" + response.data[i].is_curbside, "label": response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' });
                ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";

            }
            var airportLocation = locationdata1;

            setTimeout(function() {

                $("#airport_from_dropof_select_vehicle").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function(event, ui) {

                        event.preventDefault()
                        $(this).val(ui.item.label);
                        var selectedValue = ui.item.value;
                        var selectedAirportValue = selectedValue.split('@@');
                        var option = selectedAirportValue[0];
                        var meatandGreet = selectedAirportValue[4];
                        var curbsideSeq = selectedAirportValue[5];
                        var insidemeet_msg = selectedAirportValue[1];
                        var curbside_msg = selectedAirportValue[2];
                        var int_flight_msg = selectedAirportValue[3];

                        /*   
                         if(meatandGreet!='0'){
                         
                         $('#meetAndGreet_disclaimer').attr("data-content",insidemeet_msg); 
                         $('#intFlt').show();
                         $('#meetAndGreet').show();
                         }
                         else
                         {
                         $('#meetAndGreet').hide();
                         }
                         if(curbsideSeq!='0'){
                         $('#curbside_disclaimer').attr("data-content",curbside_msg);
                         $('#intFlt').show();
                         $('#curbSide').show();
                         }
                         else
                         {
                         $('#curbSide').hide();
                         
                         
                         }
                         
                         
                         
                         if(option!='0')
                         {
                         
                         $('#international_disclaimer').attr("data-content",int_flight_msg);
                         $('#intFlt').show();  
                         $('.interNationFlight').show();
                         }
                         else
                         {
                         $('.interNationFlight').hide();
                         
                         
                         }
                         */
                    },
                    focus: function() {



                        //$(this).val('asdfasdfasd');
                    }



                });
                $("#airport_from_pickup_select_vehicle").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function(event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);
                        var getDate = $('#datePickerComman').val();
                        var getTime = $('#timePickerComman').val();
                        var getLocation = ui.item.label;

                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {

                            vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);
                        }



                        var selectedValue = ui.item.value;
                        var selectedAirportValue = selectedValue.split('@@');
                        var option = selectedAirportValue[0];
                        var meatandGreet = selectedAirportValue[4];
                        var curbsideSeq = selectedAirportValue[5];
                        var insidemeet_msg = selectedAirportValue[1];
                        var curbside_msg = selectedAirportValue[2];
                        var int_flight_msg = selectedAirportValue[3];


                        if (meatandGreet != '0') {

                            $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                            $('#intFlt').show();
                            $('#meetAndGreet').show();
                            $('#curbsideUpdate').prop("checked", true);
                        } else {
                            $('#meetAndGreet').hide();
                        }
                        if (curbsideSeq != '0') {
                            $('#curbside_disclaimer').attr("data-content", curbside_msg);
                            $('#intFlt').show();
                            $('#curbSide').show();
                            $('#curbsideUpdate').prop("checked", true);
                        } else {
                            $('#curbSide').hide();


                        }



                        if (option != '0') {

                            $('#international_disclaimer').attr("data-content", int_flight_msg);
                            $('#intFlt').show();
                            $('.interNationFlight').show();




                        } else {
                            $('.interNationFlight').hide();


                        }

                    },
                    focus: function() {
                        //$(this).val('asdfasdfasd');
                    }
                }).data("uiAutocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append('<a><i class="fa fa-plane"></i>' + ' &nbsp; ' + item.label)
                        .appendTo(ul);
                };


            }, 1000);






        });

    },
    getSeaPortNameUpdateVehicle: function(inputBoxFieldValue, serviceType) {

        var getLocalStorageValue = localStorage.getItem("rateSetInformation");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }


        var locationdata1 = [];
        inputBoxFieldValue = inputBoxFieldValue.split('(');
        var inputBoxFieldValue = inputBoxFieldValue[0];
        var fd = new FormData();
        fd.append("action", "getSeaPortName");
        fd.append("inputValue", inputBoxFieldValue);
        $.ajax({
            url: getinfo._Serverpath,
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {
            var response = JSON.parse(result);
            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {
                if (inputBoxFieldValue == response.data[i].seaport_name) {
                    locationdata1.push({ "value": response.data[i].inside_meet_text + "@@" + response.data[i].curbside_text + "@@" + response.data[i].is_inside_meet_check + "@@" + response.data[i].is_curbside_check, "label": response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')' });
                    ResponseHtml += "<option value='" + response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')' + "'  insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "'  meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";
                }

            }


            if (locationdata1.length > 0) {
                var selectedValue = locationdata1[0].value;
                var selectedAirportValue = selectedValue.split('@@');

                // var option = selectedAirportValue[0];
                var meatandGreet = selectedAirportValue[2];

                var curbsideSeq = selectedAirportValue[3];
                var insidemeet_msg = selectedAirportValue[0];
                var curbside_msg = selectedAirportValue[1];



            }


            if (meatandGreet != 0 && serviceType == "SEAA" && getLocalStorageValue.ismeetGreetUpdateChecked == "yes") {

                $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                $('#intFlt').show();
                $('#meetGreetUpdate').prop("checked", true);
                $('#meetAndGreet').show();



            } else if (meatandGreet != 0 && serviceType == "SEAA") {

                $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                $('#intFlt').show();

                $('#meetAndGreet').show();
            } else {

                $('#meetAndGreet').hide();
            }
            if (curbsideSeq != '0' && serviceType == "SEAA" && getLocalStorageValue.iscurbsideUpdateChecked == "yes") {
                $('#curbside_disclaimer').attr("data-content", curbside_msg);
                $('#intFlt').show();
                $('#curbSide').show();
                $('#curbsideUpdate').prop('checked', true);
            } else if (curbsideSeq != '0' && serviceType == "SEAA") {
                $('#curbside_disclaimer').attr("data-content", curbside_msg);
                $('#intFlt').show();
                $('#curbSide').show();

            } else {
                $('#curbSide').hide();
            }





        });




    },

    getSeaPortName: function(inputBoxFieldValue) {
        var locationdata1 = [];
        var inputBoxFieldValue = inputBoxFieldValue;
        var fd = new FormData();
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        fd.append("action", "getSeaPortName");
        fd.append("inputValue", inputBoxFieldValue);
        fd.append("user_id", user_id);

        $.ajax({
            url: getinfo._Serverpath,
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {
            var response = JSON.parse(result);
            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {
                locationdata1.push({ "value": response.data[i].inside_meet_text + "@@" + response.data[i].curbside_text + "@@" + response.data[i].is_inside_meet_check + "@@" + response.data[i].is_curbside_check, "label": response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')' });
                ResponseHtml += "<option value='" + response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')' + "'  insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "'  meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";


            }
            var airportLocation = locationdata1;



            $("#seaport_to_dropof_select_vehicle").autocomplete({
                source: airportLocation,
                select: function(event, ui) {
                    event.preventDefault()
                    $(this).val(ui.item.label);
                    var selectedValue = ui.item.value;
                    var selectedAirportValue = selectedValue.split('@@');

                    // var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[2];

                    var curbsideSeq = selectedAirportValue[3];
                    var insidemeet_msg = selectedAirportValue[0];
                    var curbside_msg = selectedAirportValue[1];
                    // if(meatandGreet!=0){

                    //      $('#meetAndGreet_disclaimer').attr("data-content",insidemeet_msg); 
                    //      $('#intFlt').show();
                    //      $('#meetAndGreet').show();


                    //  }
                    //  else
                    //  {
                    //       $('#meetAndGreet').hide();
                    //  }
                    // if(curbsideSeq!='0'){
                    //     $('#curbside_disclaimer').attr("data-content",curbside_msg);
                    //     $('#intFlt').show();
                    //     $('#curbSide').show();
                    // }
                    // else
                    // {
                    //     $('#curbSide').hide();
                    // }




                }
            }).data("uiAutocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                    .append('<a><i class="fa fa-ship"></i>' + ' &nbsp; ' + item.label + '</a>')
                    .appendTo(ul);
            };



            $("#seaport_from_pickup_select_vehicle").autocomplete({
                source: airportLocation,
                select: function(event, ui) {
                    event.preventDefault()
                    $(this).val(ui.item.label);



                    var getDate = $('#datePickerComman').val();
                    var getTime = $('#timePickerComman').val();
                    var getLocation = ui.item.label;

                    if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {

                        vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);
                    }




                    $('.interNationFlight').hide();

                    var selectedValue = ui.item.value;
                    var selectedAirportValue = selectedValue.split('@@');

                    // var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[2];

                    var curbsideSeq = selectedAirportValue[3];
                    var insidemeet_msg = selectedAirportValue[0];
                    var curbside_msg = selectedAirportValue[1];
                    if (meatandGreet != 0) {

                        $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                        $('#intFlt').show();
                        $('#meetAndGreet').show();
                        $('#meetGreetUpdate').prop("checked", true);

                    } else {
                        $('#meetAndGreet').hide();
                    }
                    if (curbsideSeq != '0') {
                        $('#curbside_disclaimer').attr("data-content", curbside_msg);
                        $('#intFlt').show();
                        $('#curbSide').show();
                        $('#curbsideUpdate').prop("checked", true);
                    } else {
                        $('#curbSide').hide();
                    }




                }
            }).data("uiAutocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                    .append('<a><i class="fa fa-ship"></i>' + ' &nbsp; ' + item.label + '</a>')
                    .appendTo(ul);
            };




        });




    }

    /*function end for getting the seaportname list */
};
var options = {};
google.maps.event.addDomListener(window, 'load', function() {



    var places12 = new google.maps.places.Autocomplete(document.getElementById('from_train_dropoff_location_input'), options);
    var places12 = new google.maps.places.Autocomplete(document.getElementById('to_train_pickup_location_input'), options);



    var places = new google.maps.places.Autocomplete(document.getElementById('airport_from_location_select_vehicle'), options);
    var toAirportPickupLocation = new google.maps.places.Autocomplete(document.getElementById('airport_to_location_select_vehicle'), options);

    google.maps.event.addListener(toAirportPickupLocation, 'place_changed', function() {
        var getDate = $('#datePickerComman').val();
        var getTime = $('#timePickerComman').val();
        var getLocation = $('#airport_to_location_select_vehicle').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
            vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);
        }


    });


    var point_to_point_dropoff = new google.maps.places.Autocomplete(document.getElementById('point_to_point_dropof_location_select_vehicle'), options);



    google.maps.event.addListener(point_to_point_dropoff, 'place_changed', function() {
        var place = point_to_point_dropoff.getPlace();
        if (place.name.toLowerCase().indexOf("airport") >= 0) {
            $('#airportDisclaimerMsg').html("please select the Airport Departure service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else if (place.name.toLowerCase().indexOf("port") >= 0) {
            $('#airportDisclaimerMsg').html("please select the To Seaport service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else {
            $('#commanDisclaimerMsg').hide();
        }


    });






    var point_to_point_pickup_location_select = new google.maps.places.Autocomplete(document.getElementById('point_to_point_pickup_location_select_vehicle'), options);








    google.maps.event.addListener(point_to_point_pickup_location_select, 'place_changed', function() {
        // planSub.focus();
        var place = point_to_point_pickup_location_select.getPlace();


        if (place.name.toLowerCase().indexOf("airport") >= 0) {
            $('#airportDisclaimerMsg').html("please select the Airport Arrival service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();

        } else if (place.name.toLowerCase().indexOf("port") >= 0) {
            $('#airportDisclaimerMsg').html("please select the From Seaport service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else {


            var getDate = $('#datePickerComman').val();
            var getTime = $('#timePickerComman').val();
            var getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
                vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);
            }







            $('#commanDisclaimerMsg').hide();
        }

    });


    var places = new google.maps.places.Autocomplete(document.getElementById('seaport_dropof_location_select_vehicle'), options);
    var seaport_pickup_location_select_vehicle = new google.maps.places.Autocomplete(document.getElementById('seaport_pickup_location_select_vehicle'), options);

    google.maps.event.addListener(seaport_pickup_location_select_vehicle, 'place_changed', function() {
        var getDate = $('#datePickerComman').val();
        var getTime = $('#timePickerComman').val();
        var getLocation = $('#seaport_pickup_location_select_vehicle').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
            checkBlackOutDate(getDate, getTime, getLocation);
        }


    });



    var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation_1'), options);
});
var vehicleSelectService = {
    _Serverpath: "phpfile/client.php",

    perpassengerDropBoxValue: function() {

        var getUserId = window.localStorage.getItem('limoanyWhereVerification');

        if (typeof(getUserId) == "string") {

            getUserId = JSON.parse(getUserId);

        }


        var fd = new FormData();
        fd.append("action", "getPerpassengerName");
        fd.append("user_id", getUserId[0].user_id);

        $.ajax({
            url: vehicleSelectService._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function(result) {
            var resultObj = result;

            var pickup_locationdata = '<option value="">Select Location</option>';
            var dropof_locationdata = '<option value="">Select Location</option>';

            if (typeof(result) == "string") {
                resultObj = JSON.parse(result);


                if (resultObj.code == 1007) {

                    $.each(resultObj.data['pickupLocation'], function(index, result) {
                        pickup_locationdata += "<option value='" + result.pickup_location + "'>" + result.pickup_location + "</option>";

                    });

                    $.each(resultObj.data['dropLocation'], function(index, result) {
                        dropof_locationdata += "<option value='" + result.drop_off_location + "'>" + result.drop_off_location + "</option>";

                    });
                    /*   code start here */

                    $('#perpassenger_pickup_location_input').html(pickup_locationdata);
                    $('#perpassenger_dropof_location_input').html(dropof_locationdata);



                    $('#from_perpassenger_pickup_location_input').on("change", function() {
                        var getLocation = $(this).val();
                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
                            vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);
                        }
                    });
                    /* code end here*/






                }



            }



        });








    },

    vehicleLoadPage: function() {

        /*  set all default value in select vehicle popup start here */



        var getLocalStorageValue = localStorage.getItem("rateSetInformation");
        //totalLuggage

        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        setTimeout(function() {

            $('#updateVehicleInfoservices').val(getLocalStorageValue.serviceType);

        }, 2000);



        $('#datePickerComman').val(getLocalStorageValue.pickup_date);
        $('#timePickerComman').val(getLocalStorageValue.pickup_time);
        $('#numberOfPasenger').val(getLocalStorageValue.total_passenger);
        $('#numberOfLuggage').val(getLocalStorageValue.totalLuggage);


        var tagetInp = $('#lugSelector').closest('.mrgn-btm').find('input[type="text"]');
        var tagetBtn = $('#lugSelector').closest('.mrgn-btm').find('button');
        if(getLocalStorageValue.totalLuggage == 0){
            $('#lugSelector').prop('checked', true);
        } else {
            $('#lugSelector').prop('checked', false);
        }

        var luggageJson1 = localStorage.getItem("luggageJson");
        luggageJson1Value = JSON.parse(luggageJson1);
        $('#small_luggage').val(luggageJson1Value.small);
        $('#medium_luggage').val(luggageJson1Value.medium);
        $('#large_luggage').val(luggageJson1Value.large);



        if (getLocalStorageValue.isLocationPut == "yes") {

            // $('#add_stop').prop("checked",true);
            $('#add_stop').click();
            $('#vehicleStops').show();
            var stopArray = getLocalStorageValue.stopAddtress.split('@');

            var stopsShowLocationBetween = '';

            $('#stopLocation_1').val(stopArray[0]);
            if (stopArray[0] != '') {


                stopsShowLocationBetween = "<span class='glyphicon glyphicon-flag' style='color: red;'></span>(" + stopArray[0] + ")<br>";
            }


            for (var i = 0; i < stopArray.length; i++) {


                if (i != 0 && stopArray[i] != '') {

                    $('.stopLocationfield').append('<div class="stopLocation_comman_main" id="stopLocation_main_' + (i + 1) + '"><br><input type="text"  class="form-control2 form-control1 stopLocation_' + (i + 1) + '" id="stopLocation_' + (i + 1) + '" name="mytext[]" placeholder="Enter a location" value="' + stopArray[i] + '" autocomplete="off" style="width:32%;margin-left:27%;"><a href="#" class="btn btn-primary3 remove_field" seq=' + (i + 1) + '>X</a></div>');
                    stopsShowLocationBetween += "<span class='glyphicon glyphicon-flag' style='color: red;'></span>(" + stopArray[i] + ")<br>";

                    var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation_' + (i + 1)), options);

                    $('.remove_field').off();
                    $('.remove_field').on("click", function() {
                        alert('ok');
                        var getSeq = $(this).attr("seq");
                        alert(getSeq);
                        $("#stopLocation_main_" + getSeq).remove();
                    });


                }


            }

            $("#passengerStops").html(stopsShowLocationBetween);




        }


        if (getLocalStorageValue.serviceType == "PTP") {
            $('#stop_point').show();
            $('#point_to_point_pickup_location_select_vehicle').val(getLocalStorageValue.pickuplocation);
            $('#point_to_point_dropof_location_select_vehicle').val(getLocalStorageValue.dropoff_location);

            vehicleSelectService.pointToVehicle();
        } else if (getLocalStorageValue.serviceType == "PPS") {
            // aert(lgetLocalStorageValue.pickuplocation);
            $('#stop_point').hide();
            setTimeout(function() {
                $('#perpassenger_pickup_location_input').val(getLocalStorageValue.pickuplocation);
                $('#perpassenger_dropof_location_input').val(getLocalStorageValue.dropoff_location);
            }, 1000)

            vehicleSelectService.perpassengerToVehicle();




        } else if (getLocalStorageValue.serviceType == "FTS") {
            $('#stop_point').show();
            $('#from_train_pickup_location_input').val(getLocalStorageValue.pickuplocation);
            $('#from_train_dropoff_location_input').val(getLocalStorageValue.dropoff_location);

            vehicleSelectService.fromTrainVehicle();
        } else if (getLocalStorageValue.serviceType == "TTS") {
            $('#stop_point').show();
            $('#to_train_pickup_location_input').val(getLocalStorageValue.pickuplocation);
            $('#to_train_dropoff_location_input').val(getLocalStorageValue.dropoff_location);


            vehicleSelectService.toTrainVehicle();
        } else if (getLocalStorageValue.serviceType == "AIRA") {
            $('#stop_point').show();
            $('#airport_from_pickup_select_vehicle').val(getLocalStorageValue.pickuplocation);
            $('#airport_from_location_select_vehicle').val(getLocalStorageValue.dropoff_location);
            getinfo.getAirportNameUpdateFunction(getLocalStorageValue.pickuplocation);
            vehicleSelectService.airportFromVehicle();


        } else if (getLocalStorageValue.serviceType == "AIRD") {



            $('#stop_point').show();

            $('#airport_to_location_select_vehicle').val(getLocalStorageValue.pickuplocation);
            $('#airport_from_dropof_select_vehicle').val(getLocalStorageValue.dropoff_location);
            getinfo.getAirportToNameUpdateFunction(getLocalStorageValue.dropoff_location);



            vehicleSelectService.airportToVehicle();


        } else if (getLocalStorageValue.serviceType == "CH" || getLocalStorageValue.serviceType == "HRLY") {
            $('#stop_point').show();
            $('#point_to_point_pickup_location_select_vehicle').val(getLocalStorageValue.pickuplocation);
            $('#point_to_point_dropof_location_select_vehicle').val(getLocalStorageValue.dropoff_location);


            $('#hourlyRateHour').val(getLocalStorageValue.jurney_hour);
            vehicleSelectService.hourlyRateVehicle();

        } else if (getLocalStorageValue.serviceType == "SEAA") {
            $('#stop_point').show();
            $('#seaport_from_pickup_select_vehicle').val(getLocalStorageValue.pickuplocation);
            $('#seaport_dropof_location_select_vehicle').val(getLocalStorageValue.dropoff_location);
            getinfo.getSeaPortNameUpdateVehicle(getLocalStorageValue.pickuplocation, "SEAA");
            vehicleSelectService.seaportFromVehicle();

        } else if (getLocalStorageValue.serviceType == "SEAD") {
            //  if(getLocalStorageValue.interNationFlightChecked=="yes")
            // {
            //  // $('#interNationFlightUpdate').prop("checked",true);
            //  // $('#intFlt').show();
            //  // $('#interNationFlight').show();

            // }else{


            // }
            $('#intFlt').hide();


            $('#stop_point').show();
            $('#seaport_pickup_location_select_vehicle').val(getLocalStorageValue.pickuplocation);
            $('#seaport_to_dropof_select_vehicle').val(getLocalStorageValue.dropoff_location);
            getinfo.getSeaPortNameUpdateVehicle(getLocalStorageValue.dropoff_location, "SEAD");

            vehicleSelectService.seaportToVehicle();

        }













        /* set all default value in select vehicle popup end here */


        $('#updateVehicleInfoservices').on("change", function() {
            option = $(this).find('option:selected').val();

            /* $('.stopLocation_comman_main').remove();
             $('#vehicleStops').css("display","none");*/

            $('#commanDisclaimerMsg').css("display", "none");
            $('#submitUpdateForm').css("display", "block");

            $('#vehicleUpdateForm')[0].reset();
            $(this).val(option);
            $('#intFlt').hide();
            if (option == "AIRA") {
                $('#stop_point').show();
                getinfo.getAirportName("k");
                vehicleSelectService.airportFromVehicle();

            }
            if (option == "AIRD") {
                $('#stop_point').show();
                getinfo.getAirportName("k");
                vehicleSelectService.airportToVehicle();

            }
            if (option == "PTP") {
                $('#stop_point').show();
                vehicleSelectService.pointToVehicle();

            }
            if (option == "PPS") {
                $('#stop_point').hide();
                vehicleSelectService.perpassengerToVehicle();


            }
            if (option == "FTS") {


                $('#stop_point').show();
                vehicleSelectService.fromTrainVehicle();


            }
            if (option == "TTS") {


                $('#stop_point').show();
                vehicleSelectService.toTrainVehicle();


            }


            // if(option=="CH")
            // {
            //  vehicleSelectService.hourlyRateVehicle();
            // }
            if (option == "SEAA") {
                $('#stop_point').show();
                getinfo.getSeaPortName('k');
                vehicleSelectService.seaportFromVehicle();
            }
            if (option == "SEAD") {
                $('#stop_point').show();
                getinfo.getSeaPortName('k');
                vehicleSelectService.seaportToVehicle();
            }
            if (option == "HRLY" || option == 'CH') {
                $('#stop_point').show();
                vehicleSelectService.hourlyRateVehicle();
            }
            if (option == '') {
                $('#vehicleUpdateForm')[0].reset();
                $('#vehicleUpdateForm').find('input, button').attr('disabled', 'disabled');
                $('#to_Point_down_droploc').show();
                $('#to_Point_To_droploc').show();
                $('#airport_pickloc').hide();
                $('#down_droploc').hide();
                $('#to_airport_down_droploc').hide();
                $('#to_airport_pickloc').hide();
                $('#triphr').hide();
                $('#seaport_dropof').hide();
                $('#to_Seaport_droploc').hide();
                $('#to_seaport_down_droploc').hide();
                $('#seaport_pickloc').hide();


            }



        });


    },

    perpassengerToVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_Point_down_droploc').hide();
        $('#to_Point_To_droploc').hide();
        $('#airport_pickloc').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();
        $('#triphr').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();


        $('#perpassenger_pickup_location').show();
        $('#perpassenger_dropof_location').show();



        /* validation start here */

        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);

        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);



        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);



        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);

        $('#perpassenger_pickup_location_input').prop("required", true);
        $('#perpassenger_dropof_location_input').prop("required", true);





        /* validation end here */


    },

    pointToVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_Point_down_droploc').show();
        $('#to_Point_To_droploc').show();
        $('#airport_pickloc').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();
        $('#triphr').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();

        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();


        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();

        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();


        /* validation start here */

        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);

        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);



        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);

        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);

        $('#point_to_point_pickup_location_select_vehicle').prop("required", true);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", true);





        /* validation end here */


    },

    fromTrainVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_Point_down_droploc').hide();
        $('#to_Point_To_droploc').hide();
        $('#airport_pickloc').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();
        $('#triphr').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();
        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();


        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();

        $('#from_train_pickup_location').show();
        $('#from_train_dropoff_location').show();


        /* validation start here */

        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);

        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);



        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);

        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);

        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);



        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);


        $('#from_train_pickup_location_input').prop("required", true);
        $('#from_train_dropoff_location_input').prop("required", true);




        /* validation end here */


    },
    toTrainVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_Point_down_droploc').hide();
        $('#to_Point_To_droploc').hide();
        $('#airport_pickloc').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();
        $('#triphr').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();
        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();




        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();
        $('#to_train_pickup_location').show();
        $('#to_train_dropoff_location').show();


        /* validation start here */

        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);

        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);



        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);

        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);

        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);






        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);


        $('#to_train_pickup_location_input').prop("required", true);
        $('#to_train_dropoff_location_input').prop("required", true);





        /* validation end here */


    },

    seaportFromVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_seaport_down_droploc').show();
        $('#seaport_pickloc').show();
        $('#triphr').hide();
        $('#to_Point_down_droploc').hide();
        $('#to_Point_To_droploc').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#airport_pickloc').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();

        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();


        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();

        /*  validation start here*/
        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);
        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);


        $('#seaport_from_pickup_select_vehicle').prop("required", true);
        $('#seaport_dropof_location_select_vehicle').prop("required", true);
        /* validatiion end here */



    },
    seaportToVehicle: function() {
        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#seaport_dropof').show();
        $('#to_Seaport_droploc').show();
        $('#triphr').hide();
        $('#to_Point_down_droploc').hide();
        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();
        $('#to_Point_To_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();
        $('#airport_pickloc').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();

        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();

        /*  validation start here*/



        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);

        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);
        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);
        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);


        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);


        $('#seaport_pickup_location_select_vehicle').prop("required", true);
        $('#seaport_to_dropof_select_vehicle').prop("required", true);



        /* validatiion end here */


    },
    hourlyRateVehicle: function() {


        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_Point_down_droploc').show();
        $('#to_Point_To_droploc').show();
        $('#triphr').show();
        $('#airport_pickloc').hide();
        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();
        $('#down_droploc').hide();
        $('#to_airport_down_droploc').hide();

        $('#to_airport_pickloc').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();


        /*  validation start here*/



        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);

        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);


        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);


        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);

        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);

        $('#point_to_point_pickup_location_select_vehicle').prop("required", true);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", true);





        /* validatiion end here */
    },
    airportFromVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#airport_pickloc').show();
        $('#down_droploc').show();
        $('#triphr').hide();
        $('#to_Point_down_droploc').hide();
        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();
        $('#to_Point_To_droploc').hide();
        $('#to_airport_down_droploc').hide();
        $('#to_airport_pickloc').hide();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();
        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();

        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();


        /*  validation start here*/
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", false);
        $('#airport_from_dropof_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);
        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);

        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#airport_from_pickup_select_vehicle').prop("required", true);
        $('#airport_from_location_select_vehicle').prop("required", true);
        /* validatiion end here */

    },
    airportToVehicle: function() {

        $('#vehicleUpdateForm').find('input, button').removeAttr("disabled");
        $('#to_Point_down_droploc').hide();
        $('#triphr').hide();


        $('#intFlt').hide();
        $('#to_Point_To_droploc').hide();
        $('#airport_pickloc').hide();
        $('#down_droploc').hide();
        $('#perpassenger_pickup_location').hide();
        $('#perpassenger_dropof_location').hide();
        $('#to_airport_down_droploc').show();
        $('#to_airport_pickloc').show();
        $('#seaport_dropof').hide();
        $('#to_Seaport_droploc').hide();

        $('#to_seaport_down_droploc').hide();
        $('#seaport_pickloc').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropoff_location').hide();

        $('#from_train_pickup_location').hide();
        $('#from_train_dropoff_location').hide();


        /*  validation start here*/

        $('#airport_from_pickup_select_vehicle').prop("required", false);
        $('#airport_from_location_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_from_pickup_select_vehicle').prop("required", false);
        $('#seaport_dropof_location_select_vehicle').prop("required", false);
        $('#seaport_pickup_location_select_vehicle').prop("required", false);
        $('#perpassenger_pickup_location_input').prop("required", false);
        $('#perpassenger_dropof_location_input').prop("required", false);
        $('#seaport_to_dropof_select_vehicle').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropoff_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropoff_location_input').prop("required", false);


        $('#point_to_point_pickup_location_select_vehicle').prop("required", false);
        $('#point_to_point_dropof_location_select_vehicle').prop("required", false);
        $('#airport_to_location_select_vehicle').prop("required", true);
        $('#airport_from_dropof_select_vehicle').prop("required", true);
        /* validatiion end here */


    },

    checkBlackOutDate: function(getDate, getTime, getLocation) {


        $('#refresh_overlay').show()
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
            var getJson = { "action": "isBlackOutDate", "pick_date": getDate, "pick_time": getTime, "pick_up_location": getLocation, "user_id": getLocalStorageValue[0].user_id, "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key, "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id };
            $.ajax({

                url: getinfo._ServerpathVehicle,

                type: 'POST',

                data: getJson,

                success: function(response) {


                    var responseObj = response;
                    if (typeof(response) == "string") {

                        responseObj = JSON.parse(response);

                        $('#refresh_overlay').hide();

                        if (responseObj.code == 1007) {
                            var isCheckAllBlackOut = 'Value Found';
                            var isAllVehicleExist = 0;
                            var totalVehicleLoop = 0;
                            var totalVehicleNotExit = 0;
                            var vichleExists = [];
                            var allVehicleArray = [];
                            var disclamer_message = 'no';

                            // return 0;
                            $.each(responseObj.data, function(index, result) {

                                totalVehicleLoop = parseInt(index) + 1;

                                if (typeof(disclamer_message = result.blackOutDateInformation) != "undefined") {
                                    disclamer_message = result.blackOutDateInformation[0].msg

                                }


                                if (result.vehicle == "No Value Found") {
                                    // isCheckAllBlackOut='No Value Found';
                                    totalVehicleNotExit++;

                                    allVehicleArray.push({ "vehicle_info": result.vehicleNotExist, "black_out_info": disclamer_message });
                                    vichleExists.push({ "vehicle_code": '0' });


                                } else {
                                    vichleExists.push({ "vehicle_code": result.blackOutDateInformation[0].vehicle_code });
                                    isAllVehicleExist++;
                                    disclamer_message = result.blackOutDateInformation[0].msg;


                                    // allVehicleArray.push({"vehicle_info":result.vehicle,"black_out_info":result.blackOutDateInformation});

                                }


                            });
                            if (totalVehicleLoop == isAllVehicleExist) {


                                //  localStorage.removeItem("blackOutDateInformation");

                                var servic_type = $('#updateVehicleInfoservices').val();

                                if (servic_type != 'HRLY') {
                                    alert(disclamer_message);
                                    $('#submitUpdateForm').prop("disabled", true);
                                } else {

                                    $('#submitUpdateForm').prop("disabled", false);

                                }


                                //edited by infograins


                            } else if (isAllVehicleExist > 0) {
                                var getJsonFullBlackOut = { "allVehicle": allVehicleArray, "checkBlackOutDate": "blackout" };

                                getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)
                                var getJsonVehicleName = { "vehicleExists": vichleExists };
                                getJsonVehicleName = JSON.stringify(getJsonVehicleName);
                                localStorage.setItem("vehicle_name", getJsonVehicleName);
                                localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                                $('#submitUpdateForm').prop("disabled", false);


                            }
                            if (totalVehicleLoop == totalVehicleNotExit) {

                                localStorage.removeItem("blackOutDateInformation");
                                localStorage.removeItem('vehicle_name');
                                $('#submitUpdateForm').prop("disabled", false);

                            }




                        } else {

                            localStorage.removeItem("blackOutDateInformation");
                            localStorage.removeItem('vehicle_name');
                            $('#submitUpdateForm').prop("disabled", false);
                        }


                    }



                }
            });


        }









    },

    formSubmitSelectService: function() {



        $('#timePickerComman').focusout(function() {



            var getLocation = '';

            if ($.trim($('#airport_from_pickup_select_vehicle').val()) != '') {

                getLocation = $('#airport_from_pickup_select_vehicle').val();
            }
            if ($.trim($('#airport_to_location_select_vehicle').val()) != '') {
                getLocation = $('#airport_to_location_select_vehicle').val();
            }
            if ($.trim($('#point_to_point_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            }
            if ($.trim($('#point_to_point_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            }
            if ($.trim($('#seaport_from_pickup_select_vehicle').val()) != '') {
                getLocation = $('#seaport_from_pickup_select_vehicle').val();
            }
            if ($.trim($('#seaport_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#seaport_pickup_location_select_vehicle').val();
            }
            if ($.trim($('#point_to_point_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            }





            var getDate = $('#datePickerComman').val();
            var getTime = $('#timePickerComman').val();


            if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '')

            {


                /* comman file function call here */
                // checkBlackOutDate();
                vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);



            }


        });



        $('#datePickerComman').change(function() {


            var getLocation = '';

            if ($.trim($('#airport_from_pickup_select_vehicle').val()) != '') {

                getLocation = $('#airport_from_pickup_select_vehicle').val();
            }
            if ($.trim($('#airport_to_location_select_vehicle').val()) != '') {
                getLocation = $('#airport_to_location_select_vehicle').val();
            }
            if ($.trim($('#point_to_point_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            }
            if ($.trim($('#point_to_point_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            }
            if ($.trim($('#seaport_from_pickup_select_vehicle').val()) != '') {
                getLocation = $('#seaport_from_pickup_select_vehicle').val();
            }
            if ($.trim($('#seaport_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#seaport_pickup_location_select_vehicle').val();
            }
            if ($.trim($('#point_to_point_pickup_location_select_vehicle').val()) != '') {
                getLocation = $('#point_to_point_pickup_location_select_vehicle').val();
            }





            var getDate = $('#datePickerComman').val();
            var getTime = $('#timePickerComman').val();


            if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '')

            {


                /* comman file function call here */
                // checkBlackOutDate();
                vehicleSelectService.checkBlackOutDate(getDate, getTime, getLocation);



            }




        });












        $('#vehicleUpdateForm').on("submit", function(event) {
            event.preventDefault();


            var lugSelector = $('#lugSelector').prop('checked');
            var smallLuggage = $('#small_luggage').val();
            var mediumLuggage = $('#medium_luggage').val();
            var largeLuggage = $('#large_luggage').val();
            var totalLuggage = 0;

            if(lugSelector == true){
                var luggageJson = {
                    "small": 0,
                    "medium": 0,
                    "large": 0
                };
                window.localStorage.setItem("luggageJson", JSON.stringify(luggageJson));
                totalLuggage = 0;
            } else {
                if (smallLuggage == 0 && mediumLuggage == 0 && largeLuggage == 0) {
                    alert("Please Enter Atleat one Small or Medium or Lagre Luggage");
                    return 0
                } else {
                    var luggageJson = {
                        "small": smallLuggage,
                        "medium": mediumLuggage,
                        "large": largeLuggage
                    }
                }

                window.localStorage.setItem("luggageJson", JSON.stringify(luggageJson));
                totalLuggage = parseInt(smallLuggage) + parseInt(mediumLuggage) + parseInt(largeLuggage);
            }
            var numberOfPasenger = $('#numberOfPasenger').val();
            if (numberOfPasenger == '0') {
                alert('Number Of Passenger Should Be Atleast One');
                return 0;

            }


            var interNationFlightChecked = "no";
            var iscurbsideUpdateChecked = "no";
            var ismeetGreetUpdateChecked = "no";
            if ($('#interNationFlightUpdate').is(":checked")) {

                interNationFlightChecked = "yes";
            }



            if ($('#curbsideUpdate').prop("checked")) {

                iscurbsideUpdateChecked = "yes";
            }


            if ($('#meetGreetUpdate').prop("checked")) {

                ismeetGreetUpdateChecked = "yes";
            }


            var getVehicleInfo = window.localStorage.getItem("limoanyWhereVerification");

            if (typeof(getVehicleInfo) == "string") {

                getVehicleInfo = JSON.parse(getVehicleInfo);

            }

            var isLocationPut = "no";
            var stopAddtress = '';

            if ($('#add_stop').prop('checked')) {

                isLocationPut = "yes";
            }

            var stoploaction = 0;
            if (isLocationPut == "yes") {

                $('.remove_field').each(function() {

                    var getSeq = $(this).attr("seq");
                    var getInputValue = $('.stopLocation_' + getSeq).val();
                    if (getInputValue == '') {

                        alert('Stop Address Should not be blank');
                        stoploaction = 1;
                        return 0;
                    }
                    if (getSeq == 1) {

                        stopAddtress += getInputValue;
                    } else {

                        stopAddtress += "@" + getInputValue;
                    }

                });



            }

            if (stoploaction == 1) {
                return 0;

            }

            var fullDate = new Date()

            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

            var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear();
            var localUserInfo = window.localStorage.getItem("localUserInfo");
            var passenger_id = '';
            if (localUserInfo != "guestinfo") {
                localUserInfo = JSON.parse(localUserInfo);
                passenger_id = localUserInfo[0].ac_number;
            }
            var getServiceName = $('#updateVehicleInfoservices').val();


            if ($('#curbSide').is(':visible') && $('#meetAndGreet').is(':visible')) {

                if ($('#curbsideUpdate').prop("checked") || $('#meetGreetUpdate').prop("checked")) {

                } else {
                    alert('Please select Curbside or Meet & Greet');
                    return 0;
                }

            }

            if (getServiceName == "PTP") {


                var getPickLocation = $('#point_to_point_pickup_location_select_vehicle').val();
                var drop_point = $('#point_to_point_dropof_location_select_vehicle').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var getJson = {
                    "action": "getPointToPointRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                };
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";

            } else if (getServiceName == "PPS") {


                var getPickLocation = $('#perpassenger_pickup_location_input').val();
                var drop_point = $('#perpassenger_dropof_location_input').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var getJson = {
                    "action": "getPerPassengerRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                };
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";

            } else if (getServiceName == "HRLY" || getServiceName == "CH") {
                var getPickLocation = $('#point_to_point_pickup_location_select_vehicle').val();
                var drop_point = $('#point_to_point_dropof_location_select_vehicle').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#hourlyRateHour').val();

                var getJson = {
                    "action": "getHourlyRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }



                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                window.location.href = "select-vehicle.html";



            } else if (getServiceName == "AIRA") {

                var getPickLocation = $('#airport_from_pickup_select_vehicle').val();
                var drop_point = $('#airport_from_location_select_vehicle').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#slect_hour').val();
                var getJson = {
                    "action": "getFromAirportRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));


                window.location.href = "select-vehicle.html";



            } else if (getServiceName == "SEAA") {
                var getPickLocation = $('#seaport_from_pickup_select_vehicle').val();
                var drop_point = $('#seaport_dropof_location_select_vehicle').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#slect_hour').val();
                var getJson = {
                    "action": "getFromSeaportRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";
            } else if (getServiceName == "AIRD") {



                // var getServiceName=$('#services').val();
                var getPickLocation = $('#airport_to_location_select_vehicle').val();
                var drop_point = $('#airport_from_dropof_select_vehicle').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#slect_hour').val();
                var getJson = {
                    "action": "getToAirportRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";
            } else if (getServiceName == "SEAD") {



                // var getServiceName=$('#services').val();
                var getPickLocation = $('#seaport_pickup_location_select_vehicle').val();
                var drop_point = $('#seaport_to_dropof_select_vehicle').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#slect_hour').val();
                var getJson = {
                    "action": "getToSeaportRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";
            } else if (getServiceName == "FTS") {



                // var getServiceName=$('#services').val();
                var getPickLocation = $('#from_train_pickup_location_input').val();
                var drop_point = $('#from_train_dropoff_location_input').val();


                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#slect_hour').val();

                var getJson = {
                    "action": "getFromTrainRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";
            } else if (getServiceName == "TTS") {



                // var getServiceName=$('#services').val();
                var getPickLocation = $('#to_train_pickup_location_input').val();
                var drop_point = $('#to_train_dropoff_location_input').val();
                var selected_date = $('#datePickerComman').val();
                var selected_time = $('#timePickerComman').val();
                var total_passenger = $('#numberOfPasenger').val();
                var luggage_quantity = totalLuggage;
                var slect_hour = $('#slect_hour').val();
                var getJson = {
                    "action": "getToTrainRate",
                    "pickuplocation": getPickLocation,
                    "dropoff_location": drop_point,
                    "pickup_date": selected_date,
                    "pickup_time": selected_time,
                    "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                    "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                    "user_id": getVehicleInfo[0].user_id,
                    "totalLuggage": luggage_quantity,
                    "total_passenger": total_passenger,
                    "serviceType": getServiceName,
                    "jurney_hour": slect_hour,
                    "isLocationPut": isLocationPut,
                    "stopAddtress": stopAddtress,
                    "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                    "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                    "interNationFlightChecked": interNationFlightChecked,
                    "current_date": currentDate,
                    "passenger_id": passenger_id
                }
                window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                window.location.href = "select-vehicle.html";
            }










        });


    }



}

getinfo.getTrainName();
vehicleSelectService.vehicleLoadPage();
vehicleSelectService.formSubmitSelectService();
vehicleSelectService.perpassengerDropBoxValue();


getinfo.getAirportName();
getinfo.getSeaPortName();

function changeLugSelector(trigger){
    var trigger = $('#lugSelector');
    var targets = trigger.closest('.mrgn-btm').find('input[type="text"]');
    var btns = trigger.closest('.mrgn-btm').find('button');
    var v = trigger.prop('checked');

    if(v === true){
        targets.prop("disabled", true);
        btns.prop("disabled", true);
        $('#small_luggage').val(0);
        $('#medium_luggage').val(0);
        $('#large_luggage').val(0);
    } else {
        targets.prop("disabled", false);
        btns.prop("disabled", false);
    }
}