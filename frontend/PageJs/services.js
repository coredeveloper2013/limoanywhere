var getParams = function (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};

if(localStorage.localUserInfo === undefined){
    window.location.href = './';
}

/* twitter login */
var get_params = getParams(window.location.href);
if(get_params['oauth_token'] != undefined) {

    //var twitter_data = [{ 'f_name': 'User' }];
    //var user_info = JSON.stringify(twitter_data);
    //window.localStorage.setItem("localUserInfo", user_info);
    window.localStorage.setItem('localUserInfo', 'guestinfo');

}
/* ./twitter login */

var localUserInfo = window.localStorage.getItem("localUserInfo");
if (localUserInfo != "guestinfo") {
    localUserInfo = JSON.parse(localUserInfo);
    var f_name = localUserInfo[0].f_name;
    $('.ufname').text(f_name);
    $('.glyphicon-log-in').addClass('glyphicon-log-out').removeClass('glyphicon-log-in');
} else {
    $('.ufname').text('User');
    $('.glyphicon-log-out').addClass('glyphicon-log-in').removeClass('glyphicon-log-out');
}

function checkHourlyBlackOutDate(getDate, getTime, getLocation) {
    $('#refresh_overlay').show()
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    if (typeof(getLocalStorageValue) == "string") {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
        var getJson = { "action": "isHrlyBlackOutDate", "pick_date": getDate, "pick_time": getTime, "pick_up_location": getLocation, "user_id": getLocalStorageValue[0].user_id, "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key, "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id };
        $.ajax({
            url: _SERVICEPATHServer,
            type: 'POST',
            data: getJson,
            success: function(response) {
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                    console.log(responseObj);
                    $('#refresh_overlay').hide()
                    if (responseObj.code == 1007) {
                        var isCheckAllBlackOut = 'Value Found';
                        var isAllVehicleExist = 0;
                        var totalVehicleLoop = 0;
                        var totalVehicleNotExit = 0;
                        var allVehicleArray = [];
                        var disclamer_message = 'no';
                        if (responseObj.data[0].hourlyInfo !== undefined && responseObj.data[0].hourlyInfo[0].cut_off_time > 0) {
                            var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
                            if (typeof(getLocalStorageValue) == "string") {
                                getLocalStorageValue = JSON.parse(getLocalStorageValue);
                            }
                            var user_id = getLocalStorageValue[0].user_id;
                            var serviceTypeData = [];
                            var currentDateTime = new Date();
                            var getCurrentDtate = currentDateTime.getDate();
                            if (getCurrentDtate < 10) {
                                getCurrentDtate = '0' + getCurrentDtate;

                            } else {
                                getCurrentDtate = getCurrentDtate;
                            }
                            var getCurrentMonth = currentDateTime.getMonth() + 1;

                            if (getCurrentMonth < 10) {

                                getCurrentMonth = '0' + getCurrentMonth;

                            } else {
                                getCurrentMonth = getCurrentMonth;
                            }
                            var getCurrentyear = currentDateTime.getFullYear();

                            var currentFullDtae = getCurrentMonth + '/' + getCurrentDtate + '/' + getCurrentyear;


                            var getcurrentHour = currentDateTime.getHours();
                            var getCurrentMinute = currentDateTime.getMinutes();

                            var currentTime = getcurrentHour + ':' + getCurrentMinute;



                            $.ajax({
                                url: _SERVICEPATHServer,
                                type: 'post',
                                data: 'action=checkHourlyCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&current_date=" + currentFullDtae + "&current_time=" + currentTime + "&getTime=" + getTime + "&servicesType=HRLY",
                                dataType: 'json',
                                success: function(data) {

                                    var dataObj = data;

                                    if (typeof(data) == "string") {

                                        dataObj = JSON.parse(data);

                                    }

                                    if (dataObj.code == "1006") {
                                        var cut_off_hr = dataObj.data / 60;

                                        var a = parseInt(cut_off_hr);
                                        var b = parseInt(responseObj.data[0].hourlyInfo[0].cut_off_time);

                                        if (b > a) {
                                            $('#CutoffTimeMain').css("display", "block").html('Cut off hour : ' + responseObj.data[0].hourlyInfo[0].cut_off_time);
                                            $('#submit_button').css("display", "none");
                                        }


                                    } else {
                                        $('#CutoffTimeMain').css("display", "none").html('');
                                        // $('#submit_button').prop("disabled",false);
                                        $('#submit_button').css("display", "block");





                                    }




                                }
                            });

                        }



                    } else {

                        localStorage.removeItem("blackOutDateInformation");
                        $('#submit_button').prop("disabled", false);
                    }


                }



            }
        });


    }

}









var registrationPageClass = {

    _Serverpath: "phpfile/client.php",
    _SERVICEPATHServer: _SERVICEPATHSERVICECLIENT,


    checkCutoffTime: function(getDate, getTime, servicesType) {
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        var serviceTypeData = [];
        var currentDateTime = new Date();


        var getCurrentDtate = currentDateTime.getDate();
        if (getCurrentDtate < 10) {

            getCurrentDtate = '0' + getCurrentDtate;

        } else {

            getCurrentDtate = getCurrentDtate;

        }
        var getCurrentMonth = currentDateTime.getMonth() + 1;

        if (getCurrentMonth < 10) {

            getCurrentMonth = '0' + getCurrentMonth;

        } else {
            getCurrentMonth = getCurrentMonth;
        }
        var getCurrentyear = currentDateTime.getFullYear();

        var currentFullDtae = getCurrentMonth + '/' + getCurrentDtate + '/' + getCurrentyear;


        var getcurrentHour = currentDateTime.getHours();
        var getCurrentMinute = currentDateTime.getMinutes();

        var currentTime = getcurrentHour + ':' + getCurrentMinute;



        $.ajax({
            url: _SERVICEPATHServer,
            type: 'post',
            data: 'action=checkCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&current_date=" + currentFullDtae + "&current_time=" + currentTime + "&getTime=" + getTime + "&servicesType=" + servicesType,
            dataType: 'json',
            success: function(data) {

                var dataObj = data;
                if (typeof(data) == "string") {

                    dataObj = JSON.parse(data);

                }

                if (dataObj.code == "1006") {


                    $('#CutoffTimeMain').css("display", "block").html(dataObj.data.cutoff_message);
                    $('#submit_button').css("display", "none");
                } else {
                    $('#CutoffTimeMain').css("display", "none").html('');
                    // $('#submit_button').prop("disabled",false);
                    $('#submit_button').css("display", "block");





                }




            }
        });


    },

    passengerLogin: function() {




        /* black out date check functionality start here */

        $('#selected_time').focusout(function() {



            var getLocation = '';
            var selectService = $('#services').val();


            if ($.trim($('#pick_point').val()) != '') {

                getLocation = $('#pick_point').val();
            }
            if ($.trim($('#pick_hourly').val()) != '') {
                getLocation = $('#pick_hourly').val();
            }

            if (selectService == "HRLY") {
                var getDate = $('#selected_date').val();
                var getTime = $('#selected_time').val();
                if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {
                    checkHourlyBlackOutDate(getDate, getTime, getLocation);
                    return 0;
                }
            }

            if ($.trim($('#from_airport_pickloc').val()) != '') {
                getLocation = $('#from_airport_pickloc').val();
            }
            if ($.trim($('#to_airport_pickloc').val()) != '') {
                getLocation = $('#to_airport_pickloc').val();
            }
            if ($.trim($('#pick_charter').val()) != '') {
                getLocation = $('#pick_charter').val();
            }
            if ($.trim($('#pick_from_seaport').val()) != '') {
                getLocation = $('#pick_from_seaport').val();
            }
            if ($.trim($('#pick_to_seaport').val()) != '') {
                getLocation = $('#pick_to_seaport').val();
            }
            if ($.trim($('#pick_hourly').val()) != '') {
                getLocation = $('#pick_hourly').val();
            }





            var getDate = $('#selected_date').val();
            var getTime = $('#selected_time').val();




            if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '')

            {


                /* comman file function call here */
                // checkBlackOutDate();
                checkBlackOutDate(getDate, getTime, getLocation);



            }


        })




        $('#selected_date').change(function() {
            var selectService = $('#services').val();
            var getLocation = '';
            if ($.trim($('#pick_point').val()) != '') {

                getLocation = $('#pick_point').val();
            }
            if ($.trim($('#pick_hourly').val()) != '') {
                getLocation = $('#pick_hourly').val();
            }

            if (selectService == "HRLY") {


                var getDate = $('#selected_date').val();
                var getTime = $('#selected_time').val();
                if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {

                    checkHourlyBlackOutDate(getDate, getTime, getLocation);
                    return 0;
                }



            }



            if ($.trim($('#from_airport_pickloc').val()) != '') {
                getLocation = $('#from_airport_pickloc').val();
            }
            if ($.trim($('#to_airport_pickloc').val()) != '') {
                getLocation = $('#to_airport_pickloc').val();
            }
            if ($.trim($('#pick_charter').val()) != '') {
                getLocation = $('#pick_charter').val();
            }
            if ($.trim($('#pick_from_seaport').val()) != '') {
                getLocation = $('#pick_from_seaport').val();
            }
            if ($.trim($('#pick_to_seaport').val()) != '') {
                getLocation = $('#pick_to_seaport').val();
            }









            var getDate = $('#selected_date').val();
            var getTime = $('#selected_time').val();


            if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {


                /* comman file function call here */

                checkBlackOutDate(getDate, getTime, getLocation);



            }
            var servicesType = $('#services').val();
            if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '')

            {
                /* comman file function call here */
                registrationPageClass.checkCutoffTime(getDate, getTime, servicesType);
            }
        });
        /* black out edatye check functionality end here */
        $('#searchRateForm').on("submit", function(event) {
            event.preventDefault();
            var lugSelector = $('#lugSelector').prop('checked');
            var smallLuggage = $('#luggage_quantity_small').val();
            var mediumLuggage = $('#luggage_quantity_medium').val();
            var largeLuggage = $('#luggage_quantity_large').val();
            var totalLuggage = 0;

            if(lugSelector == true){
                var luggageJson = {
                    "small": 0,
                    "medium": 0,
                    "large": 0
                };
                window.localStorage.setItem("luggageJson", JSON.stringify(luggageJson));
                totalLuggage = 0;
            } else {
                if (smallLuggage == 0 && mediumLuggage == 0 && largeLuggage == 0) {
                    alert("Please Enter Atleat one Small or Medium or Lagre Luggage");
                    return 0
                } else {
                    var luggageJson = {
                        "small": smallLuggage,
                        "medium": mediumLuggage,
                        "large": largeLuggage
                    }
                }

                window.localStorage.setItem("luggageJson", JSON.stringify(luggageJson));
                totalLuggage = parseInt(smallLuggage) + parseInt(mediumLuggage) + parseInt(largeLuggage);
            }


            var getPessangerQuntity = parseInt($('#total_passenger').val());
            if (getPessangerQuntity == '0') {
                alert("Number Of Passenger Should Be Atleast One");
                $('#refresh_overlay').css('display', 'none');
                return 0;
            }
            $('#refresh_overlay').css('display', 'block');
            var interNationFlightChecked = "no";
            var iscurbsideUpdateChecked = "no";
            var ismeetGreetUpdateChecked = "no";

            if ($('#interNationFlightUpdateService').is(":checked")) {
                interNationFlightChecked = "yes";

            }

            if ($('#curbsideUpdateService').prop("checked")) {
                iscurbsideUpdateChecked = "yes";

            }

            if ($('#meetGreetUpdateService').prop("checked")) {
                ismeetGreetUpdateChecked = "yes";

            }
            // alert(ismeetGreetUpdateChecked);         



            var isCheckRadioBtn = $("#add_stop").is(":checked");
            var isLocationPut = 'no';
            var isSubmitServiceVehicleRate = "yes";
            // alert(isCheckRadioBtn);
            if (isCheckRadioBtn) {
                var i = 0;
                var isEmptyTextBox = 'no';
                var stopAddtress = '';

                $('.stopLocationfield').each(function(index, value) {
                    if (i == 0) {
                        if ($('#stopLocation').val() == '') {
                            isEmptyTextBox = 'yes';
                            isLocationPut = 'no';
                        } else {
                            stopAddtress = $('#stopLocation').val();
                            isLocationPut = 'yes';
                        }
                    } else {
                        if ($('#stopeLocation' + i).val() == '') {
                            isLocationPut = 'no';
                            isEmptyTextBox = "yes";

                        }

                        stopAddtress += '@' + $('#stopeLocation' + i).val();



                    }

                    i++;
                });

                if (isLocationPut == "no") {

                    alert("Please enter Stop location");
                    window.localStorage.setItem("isStop", "no");

                } else {

                    window.localStorage.setItem("isStop", "yes");
                    window.localStorage.setItem("StopsAll", stopAddtress);


                }







            }




            if (!isCheckRadioBtn || isEmptyTextBox == "no") {


                var getVehicleInfo = window.localStorage.getItem("limoanyWhereVerification");

                if (typeof(getVehicleInfo) == "string") {

                    getVehicleInfo = JSON.parse(getVehicleInfo);

                }
                var getServiceName = $('#services').val();

                var fullDate = new Date()

                var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

                var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear();
                var localUserInfo = window.localStorage.getItem("localUserInfo");
                var passenger_id = '';
                // alert(localUserInfo);
                if (localUserInfo != "guestinfo") {

                    localUserInfo = JSON.parse(localUserInfo);
                    // console.log();
                    passenger_id = localUserInfo[0].ac_number;
                }





                if ($('#curbsideUpdateService').is(':visible') && $('#meetAndGreet').is(':visible')) {

                    if ($('#curbsideUpdateService').prop("checked") || $('#meetGreetUpdateService').prop("checked")) {

                    } else {
                        alert('Please select Curbside or Meet & Greet');
                        $('#refresh_overlay').css('display', 'none');
                        return 0;
                    }

                }

                if (getServiceName == "PTP") {

                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#pick_point').val();
                    var drop_point = $('#drop_point').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();



                    var getJson = {
                        "action": "getPointToPointRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    };
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";

                } else if (getServiceName == "PPS") {
                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#perpassenger_pickup_location').val();
                    var drop_point = $('#perpassenger_dropof_location').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var getJson = {
                        "action": "getPerPassengerRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    };
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                    window.location.href = "select-vehicle.html";

                } else if (getServiceName == "CH") {
                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#pick_charter').val();
                    var drop_point = $('#drop_charter').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getHourlyRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }



                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";



                } else if (getServiceName == "HRLY") {
                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#pick_hourly').val();
                    var drop_point = $('#drop_hourly').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getHourlyRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }



                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";

                } else if (getServiceName == "AIRA") {



                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#from_airport_pickloc').val();
                    var drop_point = $('#from_airport_droploc').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getFromAirportRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }




                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";



                } else if (getServiceName == "SEAA") {



                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#pick_from_seaport').val();
                    var drop_point = $('#drop_from_seaport').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getFromSeaportRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }

                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));
                    window.location.href = "select-vehicle.html";

                } else if (getServiceName == "AIRD") {



                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#to_airport_pickloc').val();
                    var drop_point = $('#to_airport_droploc').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getToAirportRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }




                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";




                } else if (getServiceName == "SEAD") {



                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#pick_to_seaport').val();
                    var drop_point = $('#drop_to_seaport').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getToSeaportRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }


                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";




                } else if (getServiceName == "FTS") {



                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#from_train_pickup_location_input').val();
                    var drop_point = $('#from_train_dropof_location_input').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getFromTrainRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }


                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";




                } else if (getServiceName == "TTS") {



                    var getServiceName = $('#services').val();
                    var getPickLocation = $('#to_train_pickup_location_input').val();
                    var drop_point = $('#to_train_dropof_location_input').val();
                    var selected_date = $('#selected_date').val();
                    var selected_time = $('#selected_time').val();
                    var total_passenger = $('#total_passenger').val();
                    var luggage_quantity = $('#luggage_quantity').val();
                    var slect_hour = $('#slect_hour').val();
                    var getJson = {
                        "action": "getToTrainRate",
                        "pickuplocation": getPickLocation,
                        "dropoff_location": drop_point,
                        "pickup_date": selected_date,
                        "pickup_time": selected_time,
                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,
                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,
                        "user_id": getVehicleInfo[0].user_id,
                        "luggage_quantity": luggage_quantity,
                        "total_passenger": total_passenger,
                        "serviceType": getServiceName,
                        "jurney_hour": slect_hour,
                        "stopAddtress": stopAddtress,
                        "isLocationPut": isLocationPut,
                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,
                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,
                        "interNationFlightChecked": interNationFlightChecked,
                        "current_date": currentDate,
                        "passenger_id": passenger_id,
                        "totalLuggage": totalLuggage
                    }


                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    window.location.href = "select-vehicle.html";




                }



            }

            //$('#refresh_overlay').css('display','none');

        });






    }




}


$("#selected_time").blur(function() {
    var servicesType = $('#services').val();
    var getDate = $('#selected_date').val();
    var getTime = $('#selected_time').val();

    if((new Date() > new Date(getDate+' '+getTime))){
        $('#selected_time').val('');
        alert("Please select a valid date. You can't choose past date & time.");
    } else {
        if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {
            /* comman file function call here */
            registrationPageClass.checkCutoffTime(getDate, getTime, servicesType);
            //CutoffTimeMain
            $.ajax({
                url : _SERVICEPATHServer,
                type: 'post',
                data : {
                    action: 'getHolidaySurcharge',
                    pick_date: getDate,
                    pick_time: getTime
                },
                success : function (response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if(res !== 0){
                        $('#CutoffTimeMain').css("display", "block").html(res[0].blackout_Msg);
                        $('#selected_time').val('');
                    }
                }
            });
        }
    }

});


$('#selected_date,#services').on("change", function() {


    var servicesType = $('#services').val();
    var getDate = $('#selected_date').val();
    var getTime = $('#selected_time').val();
    if((new Date() > new Date(getDate+' '+getTime))){
        if(getTime != ''){
            $('#selected_time').val('');
            alert("Please select a valid date. You can't choose past date & time.");
        }
    } else {
        if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {
            /* comman file function call here */
            registrationPageClass.checkCutoffTime(getDate, getTime, servicesType);
            $.ajax({
                url : _SERVICEPATHServer,
                type: 'post',
                data : {
                    action: 'getHolidaySurcharge',
                    pick_date: getDate,
                    pick_time: getTime
                },
                success : function (response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if(res !== 0){
                        $('#CutoffTimeMain').css("display", "block").html(res[0].blackout_Msg);
                        $('#selected_time').val('');
                    }
                }
            });
        }
    }
});



registrationPageClass.passengerLogin();


function changeLugSelector(trigger){
    var trigger = $(trigger);
    var targets = trigger.closest('.section-border').find('input[type="text"]');
    var btns = trigger.closest('.section-border').find('button');
    var v = trigger.prop('checked');

    if(v === true){
        targets.prop("disabled", true);
        btns.prop("disabled", true);
    } else {
        targets.prop("disabled", false);
        btns.prop("disabled", false);
    }
}