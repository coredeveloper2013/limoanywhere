<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');
// date_default_timezone_set('Australia/Melbourne');

function timeDiffrence($time,$date,$current_date,$current_time)
{

 
  $datetime1 = new DateTime($current_date.' '.$current_time);  
  $time_in_24_hour_format  = date("H:i", strtotime($time));
  $datetime2 = new DateTime($date.' '.$time_in_24_hour_format);
  $interval = $datetime1->diff($datetime2);
  $totalTime= $interval->y*365+$interval->y+$interval->m*30+$interval->d*24+$interval->h;
  $totalTime=$totalTime*60+$interval->i;
  return $totalTime;

}
function checkCutoffTime()
{

    $user_id=$_REQUEST['user_id'];
    $getDate=$_REQUEST['getDate'];
    $getTime=$_REQUEST['getTime'];
    $servicesType=$_REQUEST['servicesType'];
    $current_date=$_REQUEST['current_date'];
    // echo $current_date;
    $current_time=$_REQUEST['current_time'];
    $timeReturn=timeDiffrence($getTime,$getDate,$current_date,$current_time);
   
    // $timeReturn=explode('_',$timeReturn);

    $hourFindQuery="select a.* from cutoff_time_table a inner join cutoff_service_type b on a.id=b.parent_id where a.user_id='".$user_id."' and b.service_type='".$servicesType."'";

    $hourFindQueryResult=operations($hourFindQuery);
    

   
    if(count($hourFindQueryResult)>=1 && getType($hourFindQueryResult)!="boolean")
     {
     for($i=0; $i<count($hourFindQueryResult); $i++)
       {

        $totalTimeMin=$hourFindQueryResult[$i]['hh_time']*60+$hourFindQueryResult[$i]['mm_time'];
     
        if($totalTimeMin>$timeReturn)
          {
              $result=global_message(200,1006,$hourFindQueryResult[$i]);
              return $result;
          } 
       }
              $result=global_message(200,1007);

      } 
     


      return $result;




}


function getAutoAppliedDiscountPromoCode($user_id,$current_date,$reservation_date,$serviceType,$smaId,$vehicle_code,$passenger_id=true)
{ 

    $pickupDate=$reservation_date;
    $pickupDate=explode("/",$pickupDate);
    $pickupDate=$pickupDate[2]."-".$pickupDate[0]."-".$pickupDate[1];
    $current_date=$current_date;
    $current_date=explode("/",$current_date);
    $current_date=$current_date[2]."-".$current_date[0]."-".$current_date[1];
    $vehicle_code=$vehicle_code;
    $serviceType=$serviceType;
    $user_id=$user_id;
    $passenger_id=$passenger_id;
    $getPicklocationSmaInfo=$smaId;
    if($passenger_id)
    { 
        // $query="select a.*,GROUP_CONCAT(DISTINCT  g.apply_rate) applyrate from vehicle_discount_table a inner join vehicle_dicount_extra_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id inner join discount_user_associate f on a.id=f.parent_id inner join vehicle_discount_apply_rate_table g on a.id=g.parent_id   where  (a.is_all_user='All' or (a.is_all_user='AllUser' and f.passenger_id='".$passenger_id."')) and  b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and (start_time<='".$current_date."' and end_time>='".$current_date."') and e.apply_service_type='".$serviceType."' group by a.id";
        $query="select a.*,GROUP_CONCAT(DISTINCT  g.apply_rate) applyrate from vehicle_discount_table a inner join vehicle_dicount_extra_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id inner join discount_user_associate f on a.id=f.parent_id inner join vehicle_discount_apply_rate_table g on a.id=g.parent_id   where  (a.is_all_user='All' or  b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and (start_time<='".$current_date."' and end_time>='".$current_date."') and e.apply_service_type='".$serviceType."' group by a.id";
    
    }
    else
    {      
        $query="select a.*,GROUP_CONCAT(DISTINCT  g.apply_rate) applyrate from vehicle_discount_table a inner join vehicle_dicount_extra_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id inner join discount_user_associate f on a.id=f.parent_id inner join vehicle_discount_apply_rate_table g on a.id=g.parent_id   where  b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and (start_time<='".$current_date."' and end_time>='".$current_date."') and e.apply_service_type='".$serviceType."' group by a.id";
        // $query="select a.*,GROUP_CONCAT(DISTINCT  g.apply_rate) applyrate from vehicle_discount_table a inner join vehicle_dicount_extra_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id inner join discount_user_associate f on a.id=f.parent_id inner join vehicle_discount_apply_rate_table g on a.id=g.parent_id   where  a.is_all_user='All' and  b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and (start_time<='".$current_date."' and end_time>='".$current_date."') and e.apply_service_type='".$serviceType."' group by a.id";
          
    }

      


  $resultStore=operations($query);

  return $resultStore;

}
function getDiscountPromoCode()
{



  $promocode=$_REQUEST['promocode'];
  $pickupDate=$_REQUEST['pickupDate'];
    $pickupDate=explode("/",$pickupDate);
    $pickupDate=$pickupDate[2]."-".$pickupDate[0]."-".$pickupDate[1];


 
  $pickupTime=$_REQUEST['pickupTime'];
  $pickupTime=explode(" ",$pickupTime);
  if($pickupTime[1]=="AM")
  {
    $pickupTime=$pickupTime[0];
  }
  else
  {
    $pickupTime=$pickupTime[0]+12;
  }
  $pickupTime=$pickupTime.":00:00";
  $vehicle_code=$_REQUEST['vehicle_code'];
  $serviceType=$_REQUEST['serviceType'];
  $user_id=$_REQUEST['user_id'];
  $pickuplocation=explode("(",$_REQUEST['pickuplocation']);
    $pickuplocation=$pickuplocation[0];
    /* sma code start here */
    $PickLocationZone= getZoneCity($pickuplocation);
    if($PickLocationZone)
        {
           $pickuplocation=$PickLocationZone[0]['type_name'];
        }


        $getPicklocationSmaInfo=getSmaInformation($pickuplocation,$user_id);





    /* sma code end here */
        $query="select a.* from vehicle_discount_table a inner join vehicle_dicount_extra_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id  where a.`code`='".$promocode."'  and b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and e.apply_service_type='".$serviceType."' group by a.id";
       

        $resultStore=operations($query);

        if(count($resultStore)>=1 && getType($resultStore)!=boolean)
        {
            $result=global_message(200,1006,$resultStore);
            return $result;
        }
            $result=global_message(200,1007);
            return $result;



}

function getAutoDispuntPackagePromoCode()
{ 



    /* sma code end here */
    $totalAmount=0;
    $specailRequestPackage=$_REQUEST['specailRequestPackage'];
    for($i=0; $i<count($specailRequestPackage); $i++)
    {
      $rowId=explode("_",$specailRequestPackage[$i]);


        $query="select a.* from vehicle_special_discount_table a inner join special_discount_package b on a.id=b.parent_id   where  a.promo_pref=1 and b.apply_package_id='".$rowId[0]."' ";
      
        $resultStore=operations($query);
        if(count($resultStore)>=1 && gettype($resultStore)!="boolean")
        {

            if($resultStore[0]['discount_type']=="%")
            {
             $totalAmount=$totalAmount+(($rowId[1]*$resultStore[0]['discount_value'])/100);
            }
            else
            {
              $totalAmount=$totalAmount+$resultStore[0]['discount_value'];
            }
        }
        

    }
        

         

        if($totalAmount!=0)
        {
            $result=global_message(200,1006,$totalAmount);
            return $result;
        }
        $totalAmount=0;
            $result=global_message(200,1007,$totalAmount);
            return $result;



}

function getDiscountPackagePromoCode()
{



    $promocode=$_REQUEST['promocode'];
    $pickupDate=$_REQUEST['pickupDate'];
    $pickupDate=explode("/",$pickupDate);
    $pickupDate=$pickupDate[2]."-".$pickupDate[0]."-".$pickupDate[1];


 
  $pickupTime=$_REQUEST['pickupTime'];
  $pickupTime=explode(" ",$pickupTime);
  if($pickupTime[1]=="AM")
  {
    $pickupTime=$pickupTime[0];
  }
  else
  {
    $pickupTime=$pickupTime[0]+12;
  }
  $pickupTime=$pickupTime.":00:00";
  $vehicle_code=$_REQUEST['vehicle_code'];
  $serviceType=$_REQUEST['serviceType'];
  $user_id=$_REQUEST['user_id'];
  $pickuplocation=explode("(",$_REQUEST['pickuplocation']);
    $pickuplocation=$pickuplocation[0];
    /* sma code start here */
    $PickLocationZone= getZoneCity($pickuplocation);
    if($PickLocationZone)
        {
           $pickuplocation=$PickLocationZone[0]['type_name'];
        }


        $getPicklocationSmaInfo=getSmaInformation($pickuplocation,$user_id);





    /* sma code end here */
    $totalAmount=0;
    $specailRequestPackage=$_REQUEST['specailRequestPackage'];
    for($i=0; $i<count($specailRequestPackage); $i++)
    {
      $rowId=explode("_",$specailRequestPackage[$i]);


        $query="select a.* from vehicle_special_discount_table a inner join special_discount_package b on a.id=b.parent_id   where a.`code`='".$promocode."' and a.promo_pref=2 and b.apply_package_id='".$rowId[0]."' ";
   
        $resultStore=operations($query);
        if(count($resultStore)>=1 && gettype($resultStore)!="boolean")
        {

            if($resultStore[0]['discount_type']=="%")
            {
             $totalAmount=$totalAmount+(($rowId[1]*$resultStore[0]['discount_value'])/100);
            }
            else
            {
              $totalAmount=$totalAmount+$resultStore[0]['discount_value'];
            }
        }
        

    }
        

         

        if($totalAmount!=0)
        {
            $result=global_message(200,1006,$totalAmount);
            return $result;
        }
            $result=global_message(200,1007);
            return $result;



}


function passengerReservation()
{



  $offercode=$_REQUEST['offercode'];
  $promocodeType='';
  $vouchercodeType='';


  if($offercode['offertype']==1)
  {

    $promocodeType=$offercode['promocode'];

  }
  else
  {

    $vouchercodeType=$offercode['promocode'];
  }




  $jurneyDetail=json_decode($_REQUEST['jurneyDetail']);
  $bookingInfo=json_decode($_REQUEST['bookingInfo']);
  $carTotalValue=$_REQUEST['cardTotalValue'];
  $cardNumber='';
  $cardExpiryMonth=0;
  $cardExpiryYear=0;
  $cardHolderName=0;
  $cardHolderAddress=0;
  $cardHolderZipCode=0;
  if(getType($carTotalValue)!="undefined")
    {
      $cardNumber=$carTotalValue['cardNumber'];
      $cardExpiryMonth=$carTotalValue['cardMonth'];
      $cardExpiryYear=$carTotalValue['cardYear'];
      $cardHolderName=$carTotalValue['cardHolderName'];
      $cardHolderAddress=$carTotalValue['cardHolderAddress'];
      $cardHolderZipCode=$carTotalValue['cardHolderZipCode'];


    }





  

 

  $fistName=$_REQUEST['firstName'];
  $lastName=$_REQUEST['lastName'];
  $passengerMobileNumber=$_REQUEST['passengerMobileNumber'];
  $passengerEmailName=$_REQUEST['passengerEmailName'];
  $numberOfPassengerInput=$_REQUEST['numberOfPassengerInput'];
  $additionalPassengerDetail=$_REQUEST['additionalPassengerDetail'];






  $apiKey=$jurneyDetail->limo_any_where_api_key;
  $apiid=$jurneyDetail->limo_any_where_api_id;
  $luggage_quantity=$jurneyDetail->luggage_quantity;
  $service=$jurneyDetail->serviceType;
  $from=$jurneyDetail->pickuplocation;
  $to=$jurneyDetail->dropoff_location;

  $vehicle_code=$bookingInfo->vehicle_code;
/* booking info  start here*/
	$baseRate=$bookingInfo->AllRateJson[0]->baseRate;
	$driverGratuty=$bookingInfo->AllRateJson[1]->baseRate;
	// $driverGratuty=$bookingInfo->AllRateJson[1]->baseRate;
	$grandTotal=$_REQUEST['grandTotal'];

	


/* booking info end here */
	


  $date=$jurneyDetail->pickup_date;
  $date=explode("/",$date);
  $date=$date[2]."-".$date[0]."-".$date[1];
  

  
  $time=$jurneyDetail->pickup_time;
  $time_in_24_hour_format  = date("H:i", strtotime($time));
/*  limo any code start here */

  $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
    // Prepare SoapHeader parameters
        $sh_param = array(
                'apiId'    =>$apiid,
                   'apiKey'    => $apiKey);
        $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL','GetReservation', $sh_param);
        // Prepare Soap Client
    //$action = $_REQUEST['action'];
       $service =$service;
    //$ride = $_REQUEST['ride'];
    //$RideSource = $_REQUEST['RideSource'];
      $from =$from;
      $to =$to;
      $date=$date;
      $time =$time_in_24_hour_format;
      $pass_number=$numberOfPassengerInput;
      $luggage=$luggage_quantity;
      $pass_first1=$fistName;
      $pass_last1=$lastName;
      $pass_email=$passengerEmailName;
      $pass_phone=$passengerMobileNumber;
      $bill_contact=0;
      $bill_company=0;
      $specialRequests=0;
      $veh_type=$vehicle_code;
      $notes=0;
      $airline_name=" ";
      $pay_mode=" ";
      $promo='';
      $pay_status="0";
      $child_details=" ";
        $handicap=0;
      $number_of_hours=0;
      $add_pass_details=0;
      $card_number=$cardNumber;
      $expDate="08/25";
  
     


      $bill_address='';//
      $bill_city='';
      $bill_state='';
      $billing_zip='';
     // $special_child_seat=$_REQUEST['special_seat'];
      $number_of_stop=0;
      $stop_address='';
     // $dropup=$_REQUEST['dropup'];
     // $pickup=$_REQUEST['pickup'];
      $grandtotal=$grandTotal;
      $base_rate=$baseRate;
      $gratuity=$driverGratuty;
      $surcharge=10;
      $sales_tax=0;
      $extra_rate=0;
      $base_rate_unit=0;
      $base_rate_rate=0;
      $gratuity_unit=0;
      $gratuity_rate=0;
      $surcharge_unit=0;
      $surcharge_rate=0;
      $sales_tax_unit=0;
      $sales_tax_rate=0;
      $hour_rate=0;
      $hour_rate_unit=0;
      $hour_rate_rate=0;
      $pass_rate=0;
      $pass_rate_unit=0;
      $pass_rate_rate=0;
      $mileage_rate=0;
      $mileage_rate_unit=0;
      $mileage_rate_rate=0;
      $stop_rate=0;
      $stop_rate_unit=0;
      $stop_rate_rate=0;
      $greetingSign="abc";

       $pickup_airline_name='';
       $pickupLocationType='ADDR';
       $dropoffLocationType='ADDR';
       if($service=='AIRA')
       {
        $pickupLocationType='AIR';
        $pickup_airline_name=$airline_name;
       }
       if($service=='AIRD')
       {
        $dropoffLocationType='AIR';
       }
       if($service=='SEAA')
       {
        $pickupLocationType='SEA';
       }

       if($service=='SEAD')
       {
        $dropoffLocationType='SEA';
       }

//****************************************** Getting Routing Item's Info *********************************************************
    
          
    $RoutingItem = array();
    
    $pickupArray = array('RIId'=>1,
               'RIType'=>'PU',
               'LocationType'=>$pickupLocationType,
               'RIPosition'=>'1',
               'RIAddr1'=>$from,
               'RIIdAirportInstr'=>'2',
               'RIMisc1'=>$pickup_airline_name);
    array_push($RoutingItem,$pickupArray);                
        if($stop_address!="")
        {
            $stop_address=(explode("@",$stop_address));
          
            for($i=1;$i<count($stop_address); $i++)
            { 
              
            $pos = 50+intval($i);
             $stopArray = array('RIId'=>1,
               'RIType'=>'ST',
               'LocationType'=>'ADDR',
               'RIPosition'=>$pos,
               'RIAddr1'=>$stop_address[$i-1],
               'RIIdAirportInstr'=>'2');
               
               array_push($RoutingItem,$stopArray);
            
            }
        }
          $additionalpassenger =array();
          $additionalpassengerArray=array();

        for($i=0; $i<count($additionalPassengerDetail); $i++)
        {

            $additionalpassenger=array('additionalpaxfirstname'=>$additionalPassengerDetail[$i]['firstname'],'additionalpaxlastname'=>$additionalPassengerDetail[$i]['lastname'],'additionalpaxphonenum'=>$additionalPassengerDetail[$i]['MNumber']);       
            array_push($additionalpassengerArray,$additionalpassenger);    

        }



    $dropOffArray = array('RIId'=>1,
               'RIType'=>'DO',
               'LocationType'=>$dropoffLocationType,
               'RIPosition'=>'99',
               'RIAddr1'=>$to,
               'RIIdAirportInstr'=>'2');
               
      array_push($RoutingItem,$dropOffArray);        
        
//***************************************************** Getting Child Seat's Info *********************************************************
      $AdditionalChildSeats = array();
      
      
      if( $child_details!='' && $child_details!='null')
      {
      $child_details=json_decode($child_details, true);
        $AdditionalChildSeats[]=$child_details; 
      }
   
        /*Converting stdclass to array*/
    //$AdditionalChildSeats=array(array("ChildSeatType"=>"Infant","ChildSeatCount"=>2),array("ChildSeatType"=>"Booster","ChildSeatCount"=>3));

//***************************************************** Getting Additional Passenger's Info *********************************************************
        $AdditionalPassengers='';
      
      if( $add_pass_details!='' && $add_pass_details!='null')
      {
      $add_pass_details=json_decode($add_pass_details, true);
        $AdditionalPassengers=$add_pass_details;  
      }
//***************************************************** Getting Ride Note's Info *********************************************************
      $rideNotes = array(
                'Value'=>$notes,
                'AddToTripSheet'=>'False',
                'HideFromCustomer'=>'False'
                  );
    
  
//**************************************************  Getting All the Rate Values **********************************************************                  
        // Base Rate
      $baserate = array(
                'Rate'=>$grandtotal
                  );
      //Baserate Close
            // Per Mileage Rate
      $permileagerate = array(
                'RatePerUnit'=>$mileage_rate_rate,
                'Units'=>$mileage_rate_unit,
                'Total'=>$mileage_rate
                  );
          //Per Mileage rate Close
            // Per Passenger Rate
      $perpassrate = array(
                'RatePerUnit'=>$pass_rate_rate,
                'Units'=>$pass_rate_unit,
                'Total'=>$pass_rate
                  );
          //Per Passenger rate Close
            // Per Hour Rate
      $perhourrate = array(
                'RatePerUnit'=>$hour_rate_rate,
                'Units'=>$hour_rate_unit,
                'Total'=>$hour_rate
                  );
          //Per Hour rate Close
     // Gratuity and all other additional Rates       
      $additional_rate = array(
                'PercentageAmount'=>$gratuity_rate,
                'Total'=>$gratuity
                  );
    //Gratuity  Close
    
         // Fuel Rates      
      $Fuel_rate = array(
                'PercentageAmount'=>$surcharge_rate,
                'Total'=>$surcharge
                  );
    //Fuel  Close

    
        // Fixed Discount Rate
      $discount = array(
                'Rate'=>0
                  );
      //Fixed Discount Close
        // Fixed Extra Stop Rate
      $extrafixedrate = array(
                'Rate'=>0
                  );
                
      $extraStoprate = array(
                'Rate'=>$stop_rate
                  );          
      //Fixed Extra Stop Rate Close
//*********************************Mapping of All The Rates *********************************************     
      
      $RatesMapping = array('BaseRate'=>$baserate,
               'PerMileRate'=>$permileagerate,
               'PerHourRate'=>$perhourrate,
               'PerPassengerRate'=>$perpassrate,
               'ExtraStops'=>$extraStoprate,
               'OvertimeWaitTime'=>$extrafixedrate,
               'Gratuity'=>$additional_rate,
               'FuelSurcharge'=>$additional_rate,
               'STCSurcharge'=>$additional_rate,
               'Discount2'=>$discount,
               'Misc1'=>'',
               'Misc2'=>'',
               'Misc3'=>'',
               'Misc4'=>'',
               'Total' => $grandtotal
                    );

//******************************************** Ride (Array) parameter For Import Reservation Method ******************************************                
            $ride = array('IdTrip'=>1,
               'TripConfNumber'=>'PU',
               'TripCode'=>'trp1',
               'RideSource'=>'123corp',
               'RideType'=>'RES',
               'RideNumber'=>'5',
               'ServiceType'=>$service,
               'RideDuration'=>$number_of_hours,
               'PickUpDate'=>$date,
               'PickUpTime'=>$time,
               'DropOffTime'=>'',
               'SpotTime'=>'',
               'IsAcceptedByDriver'=>'',
               'IdCard'=>'',
               'AdditionalPassengers'=>$additionalpassengerArray,
               'NumberOfPax'=>$pass_number,
               'NumberOfLuggage'=>$luggage,
               'PassengerFirstName'=>$pass_first1  ,
               'PassengerLastName'=>$pass_last1,
               'PassengerEmail'=>$pass_email,
               'PassengerPhone'=>$pass_phone,
               'SpecialRequests'=>$specialRequests,
               'RideRouteBlock'=>$RoutingItem,
               'BillingContact'=>$bill_contact,
               'BillingAccountNumber'=>'',
               'BillingCompany'=>$bill_company,
               'AdditionalPassengers'=>$AdditionalPassengers,
               'GroupName'=>$_REQUEST['grpname'],
               'Occasion'=>'',
               'VehicleType'=>$veh_type,
               'PaymentMethod'=>$pay_mode,
              'CreditCardNumber'=>$card_number  ,    
              'CCExpDate'=>$expDate ,
              'CCName'=>$pass_first1   ,
              'CCBillingAddr'=>$bill_address,      
              'CCCity'=>$bill_city  , 
              'CCState'=>$bill_state  ,
              'CCZip'=>$billing_zip   ,   
               'PromoCode'=>$promocodeType,
               'PaymentStatus'=>$pay_status,
               'SpecialChildSeat'=>'',
               'VoucherNumber'=>$vouchercodeType,




               'SpecialChildSeatCount'=>'',
               'AdditionalChildSeats'=>$AdditionalChildSeats[0],
               'SpecialHandicap' => $handicap,
               'IdAgent'=> '',
               'IdAgent2'=>'',
               'IdAffiliate' =>'',
               'RideNotes'=>$rideNotes,
               'RatesMapping'=>$RatesMapping,
               'GreetingSignNotes'=>$greetingSign
                    );
                    
    $trans = $soapClient->ImportReservation(array('apiId' => "UySGaySkGxFjA28", 'apiKey' =>"Ypk882t5QfWEgqrorQRJ", 'ride' => $ride))->ImportReservationResult;
        
 

        $result=global_message(200,1006,$trans);
       return $result;
/* limo any code End here */







}


function getQuoteToBackOffice()
{
$jurneyDetail=json_decode($_REQUEST['jurneyDetail']);
$bookingInfo=json_decode($_REQUEST['bookingInfo']);
 


  $fistName=$_REQUEST['firstName'];
  $lastName=$_REQUEST['lastName'];
  $passengerMobileNumber=$_REQUEST['passengerMobileNumber'];
  $passengerEmailName=$_REQUEST['passengerEmailName'];
  $numberOfPassengerInput=$_REQUEST['numberOfPassengerInput'];
  $apiKey=$jurneyDetail->limo_any_where_api_key;
  $apiid=$jurneyDetail->limo_any_where_api_id;
  $luggage_quantity=$jurneyDetail->luggage_quantity;
  $service=$jurneyDetail->serviceType;
  $from=$jurneyDetail->pickuplocation;
  $to=$jurneyDetail->dropoff_location;
  $vehicle_code=$bookingInfo->vehicle_code;


  $date=$jurneyDetail->pickup_date;
  $date=explode("/",$date);
  $date=$date[2]."-".$date[0]."-".$date[1];
  

  
  $time=$jurneyDetail->pickup_time;
  $time_in_24_hour_format  = date("H:i", strtotime($time));
/*  limo any code start here */

  $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
    // Prepare SoapHeader parameters
        $sh_param = array(
                'apiId'    =>$apiid,
                   'apiKey'    => $apiKey);
        $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL','GetReservation', $sh_param);
        // Prepare Soap Client
    //$action = $_REQUEST['action'];
       $service =$service;
    //$ride = $_REQUEST['ride'];
    //$RideSource = $_REQUEST['RideSource'];
      $from =$from;
      $to =$to;
      $date=$date;
      $time =$time_in_24_hour_format;
      $pass_number=$numberOfPassengerInput;
      $luggage=$luggage_quantity;
      $pass_first1=$fistName;
      $pass_last1=$lastName;
      $pass_email=$passengerEmailName;
      $pass_phone=$passengerMobileNumber;
      $bill_contact=0;
      $bill_company=0;
      $specialRequests=0;
      $veh_type=$vehicle_code;
      $notes=0;
      $airline_name=" ";
      $pay_mode=" ";
      $promo='';
      $pay_status="0";
      $child_details=" ";
        $handicap=0;
      $number_of_hours=0;
      $add_pass_details=0;
      $card_number='';
      $expDate='';
      $bill_address='';//
      $bill_city='';
      $bill_state='';
      $billing_zip='';
     // $special_child_seat=$_REQUEST['special_seat'];
      $number_of_stop=0;
      $stop_address='';
     // $dropup=$_REQUEST['dropup'];
     // $pickup=$_REQUEST['pickup'];
      $grandtotal=0;
      $base_rate=0;
      $gratuity=0;
      $surcharge=10;
      $sales_tax=0;
      $extra_rate=0;
      $base_rate_unit=0;
      $base_rate_rate=0;
      $gratuity_unit=0;
      $gratuity_rate=0;
      $surcharge_unit=0;
      $surcharge_rate=0;
      $sales_tax_unit=0;
      $sales_tax_rate=0;
      $hour_rate=0;
      $hour_rate_unit=0;
      $hour_rate_rate=0;
      $pass_rate=0;
      $pass_rate_unit=0;
      $pass_rate_rate=0;
      $mileage_rate=0;
      $mileage_rate_unit=0;
      $mileage_rate_rate=0;
      $stop_rate=0;
      $stop_rate_unit=0;
      $stop_rate_rate=0;
      $greetingSign="abc";

       $pickup_airline_name='';
       $pickupLocationType='ADDR';
       $dropoffLocationType='ADDR';
       if($service=='AIRA')
       {
        $pickupLocationType='AIR';
        $pickup_airline_name=$airline_name;
       }
       if($service=='AIRD')
       {
        $dropoffLocationType='AIR';
       }
       if($service=='SEAA')
       {
        $pickupLocationType='SEA';
       }

       if($service=='SEAD')
       {
        $dropoffLocationType='SEA';
       }

//****************************************** Getting Routing Item's Info *********************************************************
    
          
    $RoutingItem = array();
    
    $pickupArray = array('RIId'=>1,
               'RIType'=>'PU',
               'LocationType'=>$pickupLocationType,
               'RIPosition'=>'1',
               'RIAddr1'=>$from,
               'RIIdAirportInstr'=>'2',
               'RIMisc1'=>$pickup_airline_name);
    array_push($RoutingItem,$pickupArray);                
        if($stop_address!="")
        {
            $stop_address=(explode("@",$stop_address));
      
            for($i=1;$i<count($stop_address); $i++)
            { 
              
            $pos = 50+intval($i);
             $stopArray = array('RIId'=>1,
               'RIType'=>'ST',
               'LocationType'=>'ADDR',
               'RIPosition'=>$pos,
               'RIAddr1'=>$stop_address[$i-1],
               'RIIdAirportInstr'=>'2');
               
               array_push($RoutingItem,$stopArray);
            
            }
        }
    $dropOffArray = array('RIId'=>1,
               'RIType'=>'DO',
               'LocationType'=>$dropoffLocationType,
               'RIPosition'=>'99',
               'RIAddr1'=>$to,
               'RIIdAirportInstr'=>'2');
               
      array_push($RoutingItem,$dropOffArray);        
        
//***************************************************** Getting Child Seat's Info *********************************************************
      $AdditionalChildSeats = array();
      
      
      if( $child_details!='' && $child_details!='null')
      {
      $child_details=json_decode($child_details, true);
        $AdditionalChildSeats[]=$child_details; 
      }
   
        /*Converting stdclass to array*/
    //$AdditionalChildSeats=array(array("ChildSeatType"=>"Infant","ChildSeatCount"=>2),array("ChildSeatType"=>"Booster","ChildSeatCount"=>3));

//***************************************************** Getting Additional Passenger's Info *********************************************************
        $AdditionalPassengers='';
      
      if( $add_pass_details!='' && $add_pass_details!='null')
      {
      $add_pass_details=json_decode($add_pass_details, true);
        $AdditionalPassengers=$add_pass_details;  
      }
//***************************************************** Getting Ride Note's Info *********************************************************
      $rideNotes = array(
                'Value'=>$notes,
                'AddToTripSheet'=>'False',
                'HideFromCustomer'=>'False'
                  );
    
  
//**************************************************  Getting All the Rate Values **********************************************************                  
        // Base Rate
      $baserate = array(
                'Rate'=>$grandtotal
                  );
      //Baserate Close
            // Per Mileage Rate
      $permileagerate = array(
                'RatePerUnit'=>$mileage_rate_rate,
                'Units'=>$mileage_rate_unit,
                'Total'=>$mileage_rate
                  );
          //Per Mileage rate Close
            // Per Passenger Rate
      $perpassrate = array(
                'RatePerUnit'=>$pass_rate_rate,
                'Units'=>$pass_rate_unit,
                'Total'=>$pass_rate
                  );
          //Per Passenger rate Close
            // Per Hour Rate
      $perhourrate = array(
                'RatePerUnit'=>$hour_rate_rate,
                'Units'=>$hour_rate_unit,
                'Total'=>$hour_rate
                  );
          //Per Hour rate Close
     // Gratuity and all other additional Rates       
      $additional_rate = array(
                'PercentageAmount'=>$gratuity_rate,
                'Total'=>$gratuity
                  );
    //Gratuity  Close
    
         // Fuel Rates      
      $Fuel_rate = array(
                'PercentageAmount'=>$surcharge_rate,
                'Total'=>$surcharge
                  );
    //Fuel  Close

    
        // Fixed Discount Rate
      $discount = array(
                'Rate'=>0
                  );
      //Fixed Discount Close
        // Fixed Extra Stop Rate
      $extrafixedrate = array(
                'Rate'=>0
                  );
                
      $extraStoprate = array(
                'Rate'=>$stop_rate
                  );          
      //Fixed Extra Stop Rate Close
//*********************************Mapping of All The Rates *********************************************     
      
      $RatesMapping = array('BaseRate'=>$baserate,
               'PerMileRate'=>$permileagerate,
               'PerHourRate'=>$perhourrate,
               'PerPassengerRate'=>$perpassrate,
               'ExtraStops'=>$extraStoprate,
               'OvertimeWaitTime'=>$extrafixedrate,
               'Gratuity'=>$additional_rate,
               'FuelSurcharge'=>$additional_rate,
               'STCSurcharge'=>$additional_rate,
               'Discount2'=>$discount,
               'Misc1'=>'',
               'Misc2'=>'',
               'Misc3'=>'',
               'Misc4'=>'',
               'Total' => $grandtotal
                    );

//******************************************** Ride (Array) parameter For Import Reservation Method ******************************************                
            $ride = array('IdTrip'=>1,
               'TripConfNumber'=>'PU',
               'TripCode'=>'trp1',
               'RideSource'=>'123corp',
               'RideType'=>'QUOTE',
               'RideNumber'=>'5',
               'ServiceType'=>$service,
               'RideDuration'=>$number_of_hours,
               'PickUpDate'=>$date,
               'PickUpTime'=>$time,
               'DropOffTime'=>'',
               'SpotTime'=>'',
               'IsAcceptedByDriver'=>'',
               'IdCard'=>'',
               'NumberOfPax'=>$pass_number,
               'NumberOfLuggage'=>$luggage,
               'PassengerFirstName'=>$pass_first1  ,
               'PassengerLastName'=>$pass_last1,
               'PassengerEmail'=>$pass_email,
               'PassengerPhone'=>$pass_phone,
               'SpecialRequests'=>$specialRequests,
               'RideRouteBlock'=>$RoutingItem,
               'BillingContact'=>$bill_contact,
               'BillingAccountNumber'=>'',
               'BillingCompany'=>$bill_company,
               'AdditionalPassengers'=>$AdditionalPassengers,
               'GroupName'=>'',
               'Occasion'=>'',
               'VehicleType'=>$veh_type,
               'PaymentMethod'=>$pay_mode,
              'CreditCardNumber'=>$card_number  ,    
              'CCExpDate'=>$expDate ,
              'CCName'=>$pass_first1   ,
              'CCBillingAddr'=>$bill_address,      
              'CCCity'=>$bill_city  , 
              'CCState'=>$bill_state  ,
              'CCZip'=>$billing_zip   ,   
               'PromoCode'=>$promo,
               'PaymentStatus'=>$pay_status,
               'SpecialChildSeat'=>'',
               'SpecialChildSeatCount'=>'',
               'AdditionalChildSeats'=>$AdditionalChildSeats[0],
               'SpecialHandicap' => $handicap,
               'IdAgent'=> '',
               'IdAgent2'=>'',
               'IdAffiliate' =>'',
               'RideNotes'=>$rideNotes,
               'RatesMapping'=>$RatesMapping,
               'GreetingSignNotes'=>$greetingSign
                    );
                    
    $trans = $soapClient->ImportReservation(array('apiId' => "UySGaySkGxFjA28", 'apiKey' =>"Ypk882t5QfWEgqrorQRJ", 'ride' => $ride))->ImportReservationResult;
        $result=global_message(200,1006,$trans);
       return $result;
/* limo any code End here */






}

function getSpecialPackageItem()
{


  $rowId=$_REQUEST['rowId'];


   $newRowId=[];

   $pickupDate=$_REQUEST['pick_up_date'];
   $pickupDate=explode("/",$pickupDate);
   $pickupDate=$pickupDate[2]."-".$pickupDate[0]."-".$pickupDate[1];
   $user_id=$_REQUEST['user_id'];

 
    $pickupTime=$_REQUEST['pick_up_time'].":00";
    $serviceType=$_REQUEST['service_type'];
    $vehicle_code=$_REQUEST['vehicle_code'];
    $pickuplocation=explode("(",$_REQUEST['pickLocation']);
    $pickuplocation=$pickuplocation[0];
    /* sma code start here */
   
    $PickLocationZone= getZoneCity($pickuplocation);
   
    if($PickLocationZone)
        {
           $pickuplocation=$PickLocationZone[0]['type_name'];
        }


      $getPicklocationSmaInfo=getSmaInformation($pickuplocation,$user_id);
   
      $query12="select * from vehicle_special_discount_table a inner join vehicle_special_discount_service_info b  on a.id=b.parent_id inner join vehicle_special_dicount_sma_info c on a.id=c.parent_id inner join vehicle_special_dicount_extra_info d on a.id=d.parent_id where 
      a.promo_pref=1 and d.vehicle_code='".$vehicle_code."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' and b.apply_service_type='".$serviceType."' and   (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') group by a.id";
      

  $query12Result = operations($query12);




       // $query="select a.* from vehicle_special_discount_table a inner join vehicle_special_discount_service_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id  where a.`code`='".$promocode."' and b.vehicle_code='".$vehicle_code."' and b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and (start_time<='".$pickupTime."' and end_time>='".$pickupTime."') and e.apply_service_type='".$serviceType."' group by a.id";

  
  $row_id = explode("-", $rowId);  
  $query="select * from sr_value where sr_id='".$row_id[0]."'";


  $queryResult = operations($query);


  $newRowId[]=$queryResult;
  

  $fullResult=[];

  if(count($queryResult)>=1 && gettype($queryResult))
  {

    $fullResult['newRowId']=$newRowId;
    $fullResult['special_code_data']=$query12Result;

        $result=global_message(200,1006,$fullResult);

  }
  else
  {

    $result=global_message(200,1007);

  }

  return $result;


}




function specialRequestInfoAllPackage()
{


  $rowId=$_REQUEST['rowId'];
   $newRowId=[];

   $pickupDate=$_REQUEST['pick_up_date'];
   $pickupDate=explode("/",$pickupDate);
   $pickupDate=$pickupDate[2]."-".$pickupDate[0]."-".$pickupDate[1];
   $user_id=$_REQUEST['user_id'];

 
    $pickupTime=$_REQUEST['pick_up_time'].":00";
    $serviceType=$_REQUEST['service_type'];
    $vehicle_code=$_REQUEST['vehicle_code'];
    $pickuplocation=explode("(",$_REQUEST['pickLocation']);
    $pickuplocation=$pickuplocation[0];
    /* sma code start here */
   
    $PickLocationZone= getZoneCity($pickuplocation);
   
    if($PickLocationZone)
        {
           $pickuplocation=$PickLocationZone[0]['type_name'];
        }


      $getPicklocationSmaInfo=getSmaInformation($pickuplocation,$user_id);
   
      $query12="select * from vehicle_special_discount_table a inner join vehicle_special_discount_service_info b  on a.id=b.parent_id inner join vehicle_special_dicount_sma_info c on a.id=c.parent_id inner join vehicle_special_dicount_extra_info d on a.id=d.parent_id where 
      a.promo_pref=1 and d.vehicle_code='".$vehicle_code."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' and b.apply_service_type='".$serviceType."' and   (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') group by a.id";
    

  $query12Result = operations($query12);




       // $query="select a.* from vehicle_special_discount_table a inner join vehicle_special_discount_service_info b  on a.id=b.parent_id inner join discount_extra_service_table c on a.id=c.parent_id inner join vehicle_discount_apply_rate_table d on a.id=d.parent_id  inner join vehicle_discount_apply_vehicle_type_table e on a.id=e.parent_id  where a.`code`='".$promocode."' and b.vehicle_code='".$vehicle_code."' and b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' and (a.start_date<='".$pickupDate."' and a.end_date>='".$pickupDate."') and (start_time<='".$pickupTime."' and end_time>='".$pickupTime."') and e.apply_service_type='".$serviceType."' group by a.id";

  for($i=0;$i<count($rowId);$i++)
  {

  $row_id = explode("-", $rowId[$i]);  
  $query="select * from sr_value where sr_id='".$row_id[0]."'";
  $queryResult = operations($query);
  $newRowId[]=$queryResult;
  
  }
  $fullResult=[];

  if(count($queryResult)>=1 && gettype($queryResult))
  {

    $fullResult['newRowId']=$newRowId;
    $fullResult['special_code_data']=$query12Result;

        $result=global_message(200,1006,$fullResult);

  }
  else
  {

    $result=global_message(200,1007);

  }

  return $result;


}

function specialRequestInfo()
{


  $user_id=$_REQUEST['user_id'];
  $vehicleCode=$_REQUEST['vehicle_code'];


    $pickLocation=explode("(",$_REQUEST['pickLocation']);
    $pickLocation=$pickLocation[0];

      $dropupaddress=explode("(",$_REQUEST['dropupaddress']);
    $dropupaddress=$dropupaddress[0];


  $pickLocationZone=getZoneCity($pickLocation);


  

  if($pickLocationZone)
  {


    $pickLocation=$pickLocationZone[0]['type_name'];




  }



   

  $getSmaInformationArray=[];
  $getSmaInformationArray=getSmaInformation($pickLocation,$user_id);


  
  if(count($getSmaInformationArray)==1 &&  getType($getSmaInformationArray)==boolean)
  {


  $getSmaInformationArray=getSmaInformation($dropupaddress,$user_id);

  }

  




  $SmaIdInfo=$getSmaInformationArray[0]['sma_id'];

  //echo 'select a.* from special_request a inner join sr_vehicle b on a.id=b.sr_id inner join sr_sma c on a.id=c.sr_id inner join sr_value d on a.id=d.sr_id  where a.user_id="'.$user_id.'" and b.vehicle_code="'.$vehicleCode.'" and c.sma_id="'.$SmaIdInfo.'" group by a.id';

  $querySelectVehicleCode='select a.* from special_request a inner join sr_vehicle b on a.id=b.sr_id inner join sr_sma c on a.id=c.sr_id inner join sr_value d on a.id=d.sr_id  where a.user_id="'.$user_id.'" and b.vehicle_code="'.$vehicleCode.'" and c.sma_id="'.$SmaIdInfo.'" group by a.id';

  $querySelectVehicleCodeResult = operations($querySelectVehicleCode);
  if(count($querySelectVehicleCodeResult)>=1 && gettype($querySelectVehicleCodeResult)!=boolean)
  {

      $result=global_message(200,1006,$querySelectVehicleCodeResult);
  }
  else
  {
     $result=global_message(200,1007);

  }


   return $result;

}




function getHourlyRate()
{

      $pickLocation=$_REQUEST['pickuplocation'];
      $dropLocation=$_REQUEST['dropoff_location'];
      $PickLocationZone= getZoneCityHrly($_REQUEST['pickuplocation']);
      $dropLocationZone= getZoneCityHrly($_REQUEST['dropoff_location']);


        

      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $serviceType="HRLY";
      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];


      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];

       $isStopSet=$_REQUEST['isLocationPut'];
       $allStops=$_REQUEST['stopAddtress'];




      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
      //$jurney_hour=$_REQUEST['jurney_hour'];
            $pickTimeSecond=explode(" ",$picktime);

        $picktime=explode(" ",$picktime);
  

      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      {   if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    

          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }

     
      if($PickLocationZone)
       {
           // $pickLocation=$PickLocationZone[0]['type_name'];
        $getPicklocationSmaInfo=$PickLocationZone;
       }
       if(!$PickLocationZone && $dropLocationZone)
       {
          // $dropLocation=$dropLocationZone[0]['type_name'];

          $getPicklocationSmaInfo=$dropLocationZone;


       }

        $isLawChecked=getCompanyInfo($user_id);
   
        

        if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }

           $vehicleResult=[];
           $vehicleFinalResult=[];
          $i=0;
          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
          $driverGratuty=0;
           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {

               $dataVehicleInfo=false;
               
                if($isStopSet=="yes")
                    {

                     $allStopsArray=explode('@',$allStops);
                     $lastLocation=$allStopsArray[0];
                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
                     $totalLengthStops=count($allStopsArray)-1;
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);

                      if($totalDistanceBetweenTwoPlace!=0)
                      { 
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                              
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {

                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {
                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }

                                              $lastLocation=$allStopsArray[$location_i];

                                   }


                             }

                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                                 $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$_REQUEST['dropoff_location']);
                            }
                      
                      }
                          
                        $vehicleResult[]=$value;
                        //$getDistanceForMilesRadius=getDistanceBetweenTwoPlace($pickLocation,$dropLocation);
                        $querySelectVehicleCode="select a.*,b.* from master_hour_setup a inner join hourly_vehicle b on a.id=b.parent_id inner join hourly_setup_sma  c on a.id=c.parent_id  where vehicle_code='".$value->VehTypeCode."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo."' and   a.radius_miles>=".$totalDistanceBetweenTwoPlace."";
                        $dataVehicleInfo = operations($querySelectVehicleCode);
                       //print_r($dataVehicleInfo);
                  
                }
                else
                {

                        $vehicleResult[]=$value;
                        $getDistanceForMilesRadius=getDistanceBetweenTwoPlace($pickLocation,$dropLocation);
                        
                        $querySelectVehicleCode="select a.*,b.* from master_hour_setup a inner join hourly_vehicle b on a.id=b.parent_id inner join hourly_setup_sma  c on a.id=c.parent_id  where vehicle_code='".$value->VehTypeCode."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo."' and  a.radius_miles>=".$getDistanceForMilesRadius."";
                        $dataVehicleInfo = operations($querySelectVehicleCode);
                 
                }


         
               if(count($dataVehicleInfo)>=1 && gettype($dataVehicleInfo)!="boolean")
               {  

                    //done
                    $mandatoryFees=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                    $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id);

                    $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                    $getMandatoryFessFinalResult=0;
                    if($mandatoryFees)
                    {
                      $getMandatoryFessFinalResult=$mandatoryFees;
                
                    }
                    $driverGratuty=0;
                   if($driverGratutyArray)
                    {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                    }
              
                    $conditionalSurchargeResultFinalResult=0;
                    $conditionalSurchargeDateTimeFinalResult=0;

                  if($conditionalSurchargeResult)
                    {
                      $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                    }
                    if($conditionalSurchargeDateTime)
                    {
                      $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                    }

                   $getHoliSurchargeFessResultFinal=0;
                   if($getHoliSurchargeFessResult)
                    {
                      $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;
                    }

                        $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);
                        $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                        $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                        $vehicleFinalResult[$i]["vehicle_info"]=$value;
                        $vehicleFinalResult[$i]["vehicle_rate"]=$dataVehicleInfo;
                        $vehicleFinalResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                        $vehicleFinalResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                        $vehicleFinalResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                        $vehicleFinalResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                        $vehicleFinalResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                        $vehicleFinalResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                        $vehicleFinalResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                        $vehicleFinalResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                          
                   $i++;
                }
                 else
                { 
                        
                        $querySelectVehicleCode="select a.*,b.* from master_hour_setup a inner join hourly_vehicle b on a.id=b.parent_id inner join hourly_setup_sma  c on a.id=c.parent_id  where vehicle_code='".$value->VehTypeCode."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo."' and (a.is_miles_set <>1) ";
                        $querySelectVehicleCodeResult = operations($querySelectVehicleCode);

                        $querySelectVehicleResult="select a.*,b.* from master_hour_setup a inner join hourly_vehicle b on a.id=b.parent_id inner join hourly_setup_sma  c on a.id=c.parent_id  where vehicle_code='".$value->VehTypeCode."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo."'";
                        $querySelectVehicleCodeDefault = operations($querySelectVehicleResult);
                      if(count($querySelectVehicleCodeResult)>=1 && gettype($querySelectVehicleCodeResult)!="boolean")
                       { 

                             $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                             $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                             $vehicleFinalResult[$i]["radius_msg_data"]=$querySelectVehicleCodeResult;
                             $vehicleFinalResult[$i]["total_rate"]="mileageProblem";
         
                       }else if(count($querySelectVehicleCodeDefault)>=1 && gettype($querySelectVehicleCodeDefault)!="boolean"){

                        $mandatoryFees=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                        $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                        $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                        $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id);

                        $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                        $getMandatoryFessFinalResult=0;
                        if($mandatoryFees)
                        {
                          $getMandatoryFessFinalResult=$mandatoryFees;
                    
                        }
                        $driverGratuty=0;
                       if($driverGratutyArray)
                        {
                          $driverGratuty=$driverGratutyArray[0]['value'];
                        }
                  
                        $conditionalSurchargeResultFinalResult=0;
                        $conditionalSurchargeDateTimeFinalResult=0;

                       if($conditionalSurchargeResult)
                        {
                          $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                        }
                        if($conditionalSurchargeDateTime)
                        {
                          $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                        }

                        $getHoliSurchargeFessResultFinal=0;
                       if($getHoliSurchargeFessResult)
                        {
                          $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;
                        }

                        $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);
                        $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                        $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                        $vehicleFinalResult[$i]["vehicle_info"]=$value;
                        $vehicleFinalResult[$i]["vehicle_rate"]=$querySelectVehicleCodeDefault;
                        $vehicleFinalResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                        $vehicleFinalResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                        $vehicleFinalResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                        $vehicleFinalResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                        $vehicleFinalResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                        $vehicleFinalResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                        $vehicleFinalResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                        $vehicleFinalResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                          
                       }
                       else
                       {
                            
                            $vehicleResult[]=$value;
                            $getDistanceForMilesRadius=getDistanceBetweenTwoPlace($pickLocation,$dropLocation);
                        
                            $querySelectVehicleCode="select a.*,b.* from master_hour_setup a inner join hourly_vehicle b on a.id=b.parent_id inner join hourly_setup_sma  c on a.id=c.parent_id  where vehicle_code='".$value->VehTypeCode."' and a.user_id='".$user_id."' and c.sma_id='".$getPicklocationSmaInfo."' and  a.radius_miles>=".$getDistanceForMilesRadius."";
                            $dataVehicleInfo = operations($querySelectVehicleCode);
                            

                              $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                              $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                              
                              $vehicleFinalResult[$i]["vehicle_info"]=$value;
                              $vehicleFinalResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                              $vehicleFinalResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                              $vehicleFinalResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                              $vehicleFinalResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                              $vehicleFinalResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                              $vehicleFinalResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                              $vehicleFinalResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                              $vehicleFinalResult[$i]["total_rate"]="GET QUOTE";
                              $vehicleFinalResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                              $vehicleFinalResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                              $vehicleFinalResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                              $vehicleFinalResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;

                       }

                    $i++;
                  
                }
            }  
          };

         $result=global_message(200,1006,$vehicleFinalResult);

         return $result;

}




function getChildSeatRate()
{


  $pickLocation=explode("(",$_REQUEST['pickuplocation']);
  $pickLocation=$pickLocation[0];
  $vehicle_code=$_REQUEST['vehicle_code'];
  $numberOfSeat=$_REQUEST['numberOfSeat'];
  $service_type=$_REQUEST['service_type'];
  $user_id=$_REQUEST['user_id'];
  $PickLocationZone= getZoneCity($pickLocation);
  if($PickLocationZone)
   {
      $pickLocation=$PickLocationZone[0]['type_name'];
   }
  
    $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);

    $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];








    $query="select * from carseat_surcharge a inner join carseat_vehicle b on a.id=b.carseat_id inner join carseat_sma c on a.id=c.carseat_id inner join carseat_service d on a.id=d.carseat_id where b.vehicle_code='".$vehicle_code."' and c.sma_id='".$getPicklocationSmaInfo."' and  a.user_id='".$user_id."' and d.service_type='".$service_type."'";

    $queryResult=operations($query);
    if(count($queryResult)>=1 && gettype($queryResult)!=boolean)
            {
              $baseRate=0;
              if($numberOfSeat==1)
              {
                $baseRate=$queryResult[0]['seat_1'];
              }
              else if($numberOfSeat==2)
              {
                $baseRate=$queryResult[0]['seat_2'];
              } else if($numberOfSeat==3)
              {
                $baseRate=$queryResult[0]['seat_3'];
              } else if($numberOfSeat==4)
              {
                $baseRate=$queryResult[0]['seat_4'];
              }
              $minimumNumberOfSeats=0;
              if($baseRate==0)
              {
                if($queryResult[0]['seat_1']!=0)
                {
                  $minimumNumberOfSeats=1;
                }
                if($queryResult[0]['seat_2']!=0)
                {

                  $minimumNumberOfSeats=2;
                }
                if($queryResult[0]['seat_3']!=0)
                {
                  $minimumNumberOfSeats=3;
                }
                if($queryResult[0]['seat_4']!=0)
                { 
                    $minimumNumberOfSeats=4;
                }
               $result=global_message(200,1006,$minimumNumberOfSeats);
               return $result;
              }
              $result=global_message(200,1007,$baseRate);
              return $result;
            }
          return "NoExists";  




}


function isCheckChildSeat($vehicleCode,$smaId,$user_id,$serviceType)
{


      $query="select * from carseat_surcharge a inner join carseat_vehicle b on a.id=b.carseat_id inner join carseat_sma c on a.id=c.carseat_id inner join carseat_service d on a.id=d.carseat_id where b.vehicle_code='".$vehicleCode."' and c.sma_id='".$smaId."' and d.service_type='".$serviceType."' and  a.user_id='".$user_id."' ";
   

            $queryResult = operations($query);


            if(count($queryResult)>=1 && gettype($queryResult)!=boolean)
            {


              return "YesExists";
            }
          return "NoExists";  


}


/* special request start here */

  function isCheckSpecialRequest($vehicleCode,$SmaInfo,$user_id,$serviceType)
  {


     $querySelectVehicleCode='select a.* from special_request a inner join sr_vehicle b on a.id=b.sr_id inner join sr_sma c on a.id=c.sr_id inner join sr_value d on a.id=d.sr_id inner join sr_service e on a.id= e.sr_id where a.user_id="'.$user_id.'" and b.vehicle_code="'.$vehicleCode.'" and c.sma_id="'.$SmaInfo.'" and e.service_type="'.$serviceType.'" group by a.id';




   






    $querySelectVehicleCodeResult = operations($querySelectVehicleCode);
  if(count($querySelectVehicleCodeResult)>=1 && gettype($querySelectVehicleCodeResult)!=boolean)
  {

      $result="YesExists";
  }
  else
  {
     $result="NoExists";

  }


   return $result;






  }


/* special request end here */



/* hourly black out function start here */

function isHrlyBlackOutDate()
{

  $user_id=$_REQUEST['user_id'];
    $isLawChecked=getCompanyInfo($user_id);
     $pick_up_location=$_REQUEST['pick_up_location'];
     $pick_date=$_REQUEST['pick_date'];



    $limoanywhereID=$_REQUEST['limoanywhereID'];
    $limoanywhereKey=$_REQUEST['limoanywhereKey'];

     $pickLocation=explode("(",$_REQUEST['pick_up_location']);
     $pickLocation=$pickLocation[0];


     $pickLocation= getZoneCity($pickLocation);

     if($pickLocation)
       {
          $pickLocation=$pickLocation[0]['type_name'];
       }

   

        $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);
  
        if(count($getPicklocationSmaInfo)>=1 && gettype($getPicklocationSmaInfo)!=boolean)
        {
            $pickup_date=$_REQUEST['pickup_date'];  
            $vehicleList=[];
            $vehicleListArray=[];
            if(gettype($isLawChecked)!="boolean")
              {
                if($isLawChecked[0]['isLAWCheck']=='0')
                  {
                    $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
                  }
                else
                 {
                    $vehicleList=getVehicleBackOffice();
                 }
              }
              $i=0;
          foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
         
            $query="select * from blackout_date a inner join bd_vehicle b on a.id=b.bd_id inner join bd_sma c on a.id=c.bd_id where  b.vehicle_code='".$value->VehTypeCode."' and  a.calender<='".$pick_date."' && a.end_calender>='".$pick_date."' && c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' group by a.id";
           
     
           $resource = operations($query);


            if(count($resource)>=1 && gettype($resource)!=boolean)
            {

                      $query2="select * from master_hour_setup i inner join hourly_vehicle j on i.id=j.parent_id inner join hourly_setup_sma k on i.id=k.parent_id inner join hourly_event l on i.id=l.parent_id inner join blackout_date m on l.event_id=m.id   where j.vehicle_code='".$value->VehTypeCode."' and k.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."'";
                    
                     $query2Result = operations($query2);    


  
                     if(count($query2Result)>=1 && gettype($query2Result)!=boolean)
                     {
                         $vehicleListArray[$i]['vehicle']=$value;
                        $vehicleListArray[$i]['blackOutDateInformation']=$resource;
                        $vehicleListArray[$i]['hourlyInfo']=$query2Result;

                     }
                     else
                     {
                        $vehicleListArray[$i]['vehicle']=$value;
                        $vehicleListArray[$i]['blackOutDateInformation']=$resource;
                        $vehicleListArray[$i]['hourlyInfo']="Hourly Not Found";


                     }

                



            }
            else
            {

                $vehicleListArray[$i]['vehicle']="No Value Found";
                $vehicleListArray[$i]['vehicleNotExist']=$value;

            }
            $i++;





            }


     
            $result=global_message(200,1007,$vehicleListArray);



        }
        else
        {

                $result=global_message(200,1006);


        }





     


   

        return $result;

    



}




/* hourly black out function end here */



/* blackOut date check */

function isBlackOutDate()
{


 
  $user_id=$_REQUEST['user_id'];

    $isLawChecked=getCompanyInfo($user_id);
     $pick_up_location=$_REQUEST['pick_up_location'];
     $pick_date=$_REQUEST['pick_date'];



    $limoanywhereID=$_REQUEST['limoanywhereID'];
    $limoanywhereKey=$_REQUEST['limoanywhereKey'];

     $pickLocation=explode("(",$_REQUEST['pick_up_location']);
     $pickLocation=$pickLocation[0];
     
   
     $pickLocation= getZoneCity($pickLocation);
      
      
     if($pickLocation)
       {
          $pickLocation=$pickLocation[0]['type_name'];
       }

     

        $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);
  
        if(count($getPicklocationSmaInfo)>=1 && gettype($getPicklocationSmaInfo)!=boolean)
        {
           
            $pickup_date=$_REQUEST['pick_date'];

              $pickup_date=explode("/",$pickup_date);
              $pickup_date=$pickup_date[2]."-".$pickup_date[0]."-".$pickup_date[1];

              $pickupTime=$_REQUEST['pick_time'];
              $pickupTime=explode(" ",$pickupTime);
              if($pickupTime[1]=="AM")
              {
                $pickupTime=$pickupTime[0];
              }
              else
              {
                $pickupTime=$pickupTime[0]+12;
              }
              $pickupTime=$pickupTime.":00";  
            
              $pickupDateTime= $pickup_date." ".$pickupTime;  
              //$timeReturn=timeDiffrence($pickupTime,$getDate,$current_date,$current_time);

              $vehicleList=[];
              $vehicleListArray=[];
            if(gettype($isLawChecked)!="boolean")
              {
                if($isLawChecked[0]['isLAWCheck']=='0')
                  {
                    $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
                  }
                else
                 {
                    $vehicleList=getVehicleBackOffice();
                 }
              }
              $i=0;
          foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
           
            $query="select * from blackout_date a inner join bd_vehicle b on a.id=b.bd_id inner join bd_sma c on a.id=c.bd_id where  b.vehicle_code='".$value->VehTypeCode."' and CONCAT(a.calender,' ',a.start_time) <='".$pickupDateTime."' && CONCAT(a.end_calender,' ',a.end_time)>='".$pickupDateTime."' && c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."'  group by a.id";
            //$query="select * from blackout_date a inner join bd_vehicle b on a.id=b.bd_id inner join bd_sma c on a.id=c.bd_id where  b.vehicle_code='".$value->VehTypeCode."' and  a.calender<='".$pickup_date."' && a.end_calender>='".$pickup_date."' && c.sma_id='".$getPicklocationSmaInfo[0]['sma_id']."' and a.start_time<='".$pickupTime."' && a.end_time>='".$pickupTime."' group by a.id";
            echo $query;
            $resource = operations($query);
            if(count($resource)>=1 && gettype($resource)!=boolean)
            {

       
                $vehicleListArray[$i]['vehicle']=$value;
                $vehicleListArray[$i]['blackOutDateInformation']=$resource;

            }
            else
            {

                $vehicleListArray[$i]['vehicle']="No Value Found";
                $vehicleListArray[$i]['vehicleNotExist']=$value;

            }
            $i++;





            }


     
            $result=global_message(200,1007,$vehicleListArray);



        }
        else
        {

                $result=global_message(200,1006);


        }





     


   

        return $result;

    



}




/* get train rate code start here */



function getFromTrainRate()
{

   
      $pickLocation=explode("(",$_REQUEST['pickuplocation']);
        $pickLocation2=$pickLocation[0];
      $pickLocation=$pickLocation[0];


    $serviceType="FTS";
      $dropLocation=explode("(",$_REQUEST['dropoff_location']);
      $dropLocation=$dropLocation[0];



      $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];

      $pickLocation1= getZoneCityType($pickLocation,"train");

      $dropLocationZone= getZoneCityType($dropLocation,"city");
      
      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];
      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];
            $pickTimeSecond=explode(" ",$picktime);
        $picktime=explode(" ",$picktime);
 
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 
        if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    
          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }


      // $picktime=explode(" ",$picktime);
      // $picktimeTime='';
     

      // if($picktime[1]=="AM")
      // {
      //     $picktimeTime=$picktime[0];

      // }
      // else
      // {

      //   $picktimeTime=$picktime[0]+12;

      // } 



       if($dropLocationZone)
       {
          $dropLocation=$dropLocationZone[0]['type_name'];
       }
      



       /* get Sma Information  */
       
//done


         $getPicklocationSmaInfo=getSmaInformation($pickLocation2,$user_id);


      

       if(!$getPicklocationSmaInfo)
       {
       $getPicklocationSmaInfo=getSmaInformation($dropLocation,$user_id);

         



       }
  

     
       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
                $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
                $resource = operations($query);

               if(count($resource)>=1 && getType($resource)=="boolean")
              {
       
               $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
               $resource = operations($query);
          
               } 
            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];
           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);
          if($return_rate)
          {
            $peak_increase_rate=$resource[0]['peak_increase_rate']; 
            $peak_rate_type=$resource[0]['currency_type']; 
          }
          else
          {
            $peak_increase_rate=0;  

          }
            /*  is peak hour exist in database end herer  */


           

          $vehicleResult=[];
          $i=0;
          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
          $driverGratuty=0;
          /*third */
           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {

            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {
              $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
                  $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                  $getVehicleQueryResult = operations($getVehicleQuery);

                }
              

               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {

             

                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  
               

                 
                  //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  
            
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  


                  $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                                     

                   $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);




                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }
                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }


                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                    
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
             
                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }

                    $querySelectDbZone="select id from sma_zone_data  where type_name='".$pickLocation."'";
                    $querySelectDbZoneResult = operations($querySelectDbZone);
                    $getAirportExtraRate="select * from train_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    $getAirportExtraRateResult = operations($getAirportExtraRate);

                       $getAirportExtraRateResultFull=0;
                     

                      if(count($getAirportExtraRateResult)>=1 && gettype($getAirportExtraRateResult)!=boolean)
                      {
                        $getAirportExtraRateResultFull=$getAirportExtraRateResult;

                      }
                   /* mandatory fees get End here */
                   
                  //done
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                  $vehicleResult[$i]["vehicle_toll_amount"]=$getVehicleQueryResult[0]['toll_amt'];


                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];
                  // $vehicleResult[$i]["airportRateVehicle"]=$getHoliSurchargeFessResultFull;
                  $vehicleResult[$i]["trainRateExtraResult"]=$getAirportExtraRateResultFull[0]['train_pickup_schg'];
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                 
                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type; 
                
                  $i++;
                }
                else
                { // mileage rate start here

                      

                  if($isStopSet=="yes")
                  {
                     $allStopsArray=explode('@',$allStops);
                     $lastLocation=$allStopsArray[0];

                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
                        //done

                      $totalLengthStops=count($allStopsArray)-1;
                     
             
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);


                      if($totalDistanceBetweenTwoPlace!=0)
                      {
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                              
                             
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }

                                                         
                            

                           }

         
                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                                         $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$dropLocation);
                            }
                      

                      }
                   

                  }
                  else
                  {

                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$dropLocation);


                  }




                if($totalDistanceBetweenTwoPlace!=0){



                  //done
                   $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickLocation,$dropLocation);


                   $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);
                    $rateMileagePeakHour=$getMilegeBaseRate[2];
                  $peakHourDb=$getMilegeBaseRate[1];
                $getMilegeBaseRate=$getMilegeBaseRate[0];
                $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

                 if($return_rate)
                      {
                    
                        // $peak_increase_rate=($rateMileagePeakHour*$getMilegeBaseRate)/100;  
                        $peak_increase_rate=$rateMileagePeakHour;


                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }

                }



                   if($getMilegeBaseRate)
                   {
                    

                     /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   


                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  /* done*/
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    

                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);

                       $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                      
                   $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);

                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                   
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }
                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }


                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }


                       $querySelectDbZone="select id from sma_zone_data  where type_name='".$pickLocation."'";
                    $querySelectDbZoneResult = operations($querySelectDbZone);
                    $getAirportExtraRate="select * from train_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    $getAirportExtraRateResult = operations($getAirportExtraRate);


                              $getAirportExtraRateResultFull=0;
                     

                      if(count($getAirportExtraRateResult)>=1 && gettype($getAirportExtraRateResult)!=boolean)
                      {
                        $getAirportExtraRateResultFull=$getAirportExtraRateResult;

                      }


                    // $querySelectDbZone="select id from sma_zone_data  where type_name='".$pickLocation."'";
                

                    

                    // $querySelectDbZoneResult = operations($querySelectDbZone);
                    // $getAirportVehicleRateRate="select * from airport_vehicle  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    // $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);

                    
                    // $getAirportExtraRate="select * from airport_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                     

                    // $getAirportExtraRateResult = operations($getAirportExtraRate);

          





                    $getMilegeBaseRate=round($getMilegeBaseRate,2);




                      //done
                     $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["trainRateExtraResult"]=$getAirportExtraRateResultFull[0]['train_pickup_schg'];
                    
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";

                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                    
                        if($stopCalculationStart!=0)
                    {

                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                      
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";


                    }

                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;





                      
                    
                    

                      $i++;



                   }
                   else
                   {





                       $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  
                  //done    
                     $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];

                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]="GET QUOTE";
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;





                    $i++;

                   }
                 


                }
                /* aa */
            }

              }

          
         

              // return $vehicleResult;
        

          $result=global_message(200,1006,$vehicleResult);
          

          
       
       return $result;




}




/* get train rate code end here */






/* get Rate for from airport  */

function getFromAirportRate()
{


      $pickLocation=explode("(",$_REQUEST['pickuplocation']);
      $pickLocation=$pickLocation[0];
  
      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];
      
      
      $dropLocation=explode("(",$_REQUEST['dropoff_location']);
      $dropLocation=$dropLocation[0];
      $dropLocationLocal=$_REQUEST['dropoff_location'];



      $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];

      $pickLocation1= getZoneCityType($pickLocation,"airport");

      


      $dropLocationZone= getZoneCityType($dropLocation,"city");
      





      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];
      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];

      $serviceType="AIRA";
          $pickTimeSecond=explode(" ",$picktime);
        $picktime=explode(" ",$picktime);
        
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 

          if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    
          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }
     



        if($pickLocation1)
        {
           $pickLocation1=$pickLocation1[0]['type_name'];
        }
      

       if($dropLocationZone)
       {
          $dropLocation=$dropLocationZone[0]['type_name'];
       }
      

           $getPicklocationSmaInfo=getSmaInformation($pickLocation1,$user_id);







           if(!$getPicklocationSmaInfo)
            {
                $getPicklocationSmaInfo=getSmaInformation($dropLocation,$user_id);
            }
        


          

       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
          
                $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
          
              
          
                $resource = operations($query);
       
   
            if(count($resource)>=1 && getType($resource)=="boolean")
            {
       
           $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
           $resource = operations($query);
          
            }
    
            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];


           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

        

          if($return_rate)
          {

            $peak_increase_rate=$resource[0]['peak_increase_rate'];  
            $peak_rate_type=$resource[0]['currency_type'];
          }
          else
          {
            $peak_increase_rate=0;  

          }
         



            /*  is peak hour exist in database end herer  */


           

          $vehicleResult=[];
          $i=0;

      
          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];

          $driverGratuty=0;
          /*third */
           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {

            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {
              $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
                  $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                  
                    
                  $getVehicleQueryResult = operations($getVehicleQuery);

                }





               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {
                
                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  /*done*/
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);


                  
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);



                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                 
                       $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       

                       $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);



                       $conditionalSurchargeResultFinalResult=0;
                       $conditionalSurchargeDateTimeFinalResult=0;

                       $getHoliSurchargeFessResultFinal=0;
                      if($conditionalSurchargeResult)
                      {
                          $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                      }
                      if($conditionalSurchargeDateTime)
                      {
                          $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                      }


                      if($getHoliSurchargeFessResult)
                      {
                          $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                      }
                      if($driverGratutyArray)
                      {
                         $driverGratuty=$driverGratutyArray[0]['value'];
                      }

              
                      $getMandatoryFessFinalResult=0;
                      if($getMandatoryFessArray)
                      {
                        $getMandatoryFessFinalResult=$getMandatoryFessArray;
                      }
                        $querySelectDbZone="select id from sma_zone_data  where type_name='".$pickLocation."'";
                        $querySelectDbZoneResult = operations($querySelectDbZone);
                        $getAirportVehicleRateRate="select * from airport_vehicle  where  parent_id='".$querySelectDbZoneResult[0]['id']."' and vehicle_code='".$value->VehTypeCode."'";
                        $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);
                        $getHoliSurchargeFessResultFull=0;
                        if(count($getAirportVehicleRateRateResult)>=1 && gettype($getAirportVehicleRateRateResult)!=boolean)
                        {

                        $getHoliSurchargeFessResultFull=$getAirportVehicleRateRateResult;
                    }

                    
                    $getAirportExtraRate="select * from airport_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    $getAirportExtraRateResult = operations($getAirportExtraRate);

                       $getAirportExtraRateResultFull=0;
                     

                      if(count($getAirportExtraRateResult)>=1 && gettype($getAirportExtraRateResult)!=boolean)
                      {
                        $getAirportExtraRateResultFull=$getAirportExtraRateResult;

                      }
                   /* mandatory fees get End here */
                   
                    //done
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];
                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["airportRateVehicle"]=$getHoliSurchargeFessResultFull;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResultFull;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type;
                  




                  $i++;
                }
                else
                { 

                  // $pickLocationDistance=explode("(",);
                  //   print_r($pickLocationDistance);
      // $pickLocationDistance=$pickLocation[0];






                  if($isStopSet=="yes")
                  {
                     $allStopsArray=explode('@',$allStops);
                     $lastLocation=$allStopsArray[0];
                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
                     $totalLengthStops=count($allStopsArray)-1;
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);

                      if($totalDistanceBetweenTwoPlace!=0)
                      {
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                              
                                
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }

                                                               
                            

                           }

                           if($totalDistanceBetweenTwoPlace!=0)
                            {
                                         $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$dropLocation);
                            }
                      

                      }
                 

                  }
                  else
                  {
                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$dropLocationLocal);
                

                  }

              



                if($totalDistanceBetweenTwoPlace!=0){
                

                   $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickLocation,$dropLocationLocal);
                }
                $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);

                $rateMileagePeakHour=$getMilegeBaseRate[2];
                $peakHourDb=$getMilegeBaseRate[1];
                $getMilegeBaseRate=$getMilegeBaseRate[0];
                $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

             
                   if($return_rate)
                      {
                    
                        $peak_increase_rate=$rateMileagePeakHour;
                        // $peak_increase_rate=($rateMileagePeakHour*$getMilegeBaseRate)/100;  
                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }


                   if($getMilegeBaseRate)
                   {
                    

                     /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   


                   /* driver gratuty fetch from database start here */

                  //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    

                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);

                       $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                      


                       $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);



                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                   
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }
                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }


                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }

                    $querySelectDbZone="select id from sma_zone_data  where type_name='".$pickLocation."'";
                    

                    $querySelectDbZoneResult = operations($querySelectDbZone);
                    $getAirportVehicleRateRate="select * from airport_vehicle  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);                    



                    $getAirportExtraRate="select * from airport_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                   
                     $getAirportExtraRateResult = operations($getAirportExtraRate);

          
         




                    $getMilegeBaseRate=round($getMilegeBaseRate,2);



                    
                    //done
                     $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];
                  
                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;


                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";


                        if($stopCalculationStart!=0)
                    {

                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                      
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";


                    }

                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                  $i++;

                   }
                   else
                   {





                       $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                      $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                      //done
                      $vehicleResult[$i]["vehicle_info"]=$value;
                      $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];

                      $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];
                      $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                      $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                      $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                      $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                      $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                      $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                      $vehicleResult[$i]["total_rate"]="GET QUOTE";
                      $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;





                    $i++;

                   }
                 


                }
               
            }

              }

          
         

      
          //prtint_r($totalDistanceBetweenTwoPlace);
            
          $result=global_message(200,1006,$vehicleResult);
          

          
       
       return $result;




}


function getToTrainRate()
{


      $pickLocation=explode("(",$_REQUEST['pickuplocation']);
      $pickLocation=$pickLocation[0];
      $dropLocation=explode("(",$_REQUEST['dropoff_location']);
      $dropLocation=$dropLocation[0];
      $PickLocationZone= getZoneCityType($pickLocation,"city");
      $DropLocationZone= getZoneCityType($dropLocation,"train");
  $serviceType="TTS";
      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];

          $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];



      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];



      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];

          $pickTimeSecond=explode(" ",$picktime);
        $picktime=explode(" ",$picktime);
      
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 

          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }

      // $picktime=explode(" ",$picktime);



      // $picktimeTime='';
      // if($picktime[1]=="AM")
      // {
      //     $picktimeTime=$picktime[0];

      // }
      // else
      // {

      //   $picktimeTime=$picktime[0]+12;

      // }


        if($PickLocationZone)
        {
           $pickLocation=$PickLocationZone[0]['type_name'];;
        }


  
      if($DropLocationZone)
        {
           $dropLocation1=$DropLocationZone[0]['type_name'];;
        }




       /* get Sma Information  */
       $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);

             if(!$getPicklocationSmaInfo)
         {
            $getPicklocationSmaInfo=getSmaInformation($dropLocation1,$user_id);
         }


     
       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
       $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
  
       $resource = operations($query);

       if(count($resource)>=1 && getType($resource)=="boolean")
         {
       
           $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
           $resource = operations($query);
          
         }
  

            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];
           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

          

          if($return_rate)
          {

            $peak_increase_rate=$resource[0]['peak_increase_rate'];
            $peak_rate_type=$resource[0]['currency_type'];  
          }
          else
          {
           $peak_increase_rate=0; 
          }



            /*  is peak hour exist in database end herer  */


           

          $vehicleResult=[];
          $i=0;
          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
          $driverGratuty=0;

           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
             


            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {
              

                $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
                  $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                  $getVehicleQueryResult = operations($getVehicleQuery);
                }



              
               
               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {


                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);

                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   

                  $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);




                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }
                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }

                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }




                    $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                    $querySelectDbZoneResult = operations($querySelectDbZone);
                   
                    
                    $getAirportExtraRate="select * from train_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
           
                    $getAirportExtraRateResult = operations($getAirportExtraRate);

          


                    /* mandatory fees get End here */

                    //done
                      $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];
                  // $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;

                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;

                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type;

                


                


                  $i++;
                }

                else 
               {

                 if($isStopSet=="yes")
                    {


                      $allStopsArray=explode('@',$allStops);
                     $lastLocation=$allStopsArray[0];

                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($pickLocation,$allStopsArray[0]);
                   
                     $totalLengthStops=count($allStopsArray)-1;
                     
             
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);

                      if($totalDistanceBetweenTwoPlace!=0)
                      {
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                              
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }
                          
                            

                           }

                   
                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                                         $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$dropLocation);
                            }
                      

                      }



          


                  }
                  else
                  {

                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($pickLocation,$dropLocation);


                  }


                //done
                $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);
                 

                   $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);
                   $rateMileagePeakHour=$getMilegeBaseRate[2];
                   $peakHourDb=$getMilegeBaseRate[1];
                   $getMilegeBaseRate=$getMilegeBaseRate[0];
                   $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

                 if($return_rate)
                      {
                    
                        // $peak_increase_rate=($rateMileagePeakHour*$getMilegeBaseRate)/100;  
                        $peak_increase_rate=$rateMileagePeakHour;  
                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }


                   if($getMilegeBaseRate)
                   {




                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);

                     $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  } 

                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }



                  
                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }



                    $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                    $querySelectDbZoneResult = operations($querySelectDbZone);
                   
                    
                    $getAirportExtraRate="select * from train_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
           
                    $getAirportExtraRateResult = operations($getAirportExtraRate);




                    // $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                    // $querySelectDbZoneResult = operations($querySelectDbZone);
                    // $getAirportVehicleRateRate="select * from seaport_vehicle   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    // $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);

                    
                    // $getAirportExtraRate="select * from seaport_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
           



                    // $getAirportExtraRateResult = operations($getAirportExtraRate);
                    $getMilegeBaseRate=round($getMilegeBaseRate,2);
                    $vehicleResult[$i]["vehicle_info"]=$value;
                    $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                    $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                    $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                    $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                      $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;

                    $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                    $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                    $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";
                    $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                    $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;
                          if($stopCalculationStart!=0)
                    {

                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                      
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";


                    }
                     $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;

                    $i++;



                   }
                   else
                   {


                    $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                    
                    $vehicleResult[$i]["vehicle_info"]=$value;
                    $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                    $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                    $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                    $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                    $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                    $vehicleResult[$i]["total_rate"]="GET QUOTE";
                    $i++;

                   }
                 


               }

            }

              }

          
         

        

          $result=global_message(200,1006,$vehicleResult);
          

    

       return $result;










}

function getToSeaportRate()
{


    

      $pickLocation=explode("(",$_REQUEST['pickuplocation']);
      $pickLocation=$pickLocation[0];
      $dropLocation=explode("(",$_REQUEST['dropoff_location']);
      $dropLocation2=$dropLocation[0];

      $dropLocation=$dropLocation[0];
       $serviceType="SEAD";
      $PickLocationZone= getZoneCityType($pickLocation,"city");
      $DropLocationZone= getZoneCityType($dropLocation,"seaport");


      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];
      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];

          $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];



      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
          $pickTimeSecond=explode(" ",$picktime);
       $picktime=explode(" ",$picktime);
       
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 

        if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    
          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }


        if($PickLocationZone)
        {
           $pickLocation=$PickLocationZone[0]['type_name'];;
        }


         if($DropLocationZone)
        {
           $dropLocation1=$DropLocationZone[0]['type_name'];;
        }


        

       /* get Sma Information  */
       $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);


             if(!$getPicklocationSmaInfo)
         {
            $getPicklocationSmaInfo=getSmaInformation($dropLocation2,$user_id);
         }


       


       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
       $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
       
       $resource = operations($query);

       if(count($resource)>=1 && getType($resource)=="boolean")
        {
       
           $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
           $resource = operations($query);
          
        }
       
            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];
           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);


          if($return_rate)
          {

            $peak_increase_rate=$resource[0]['peak_increase_rate'];
            $peak_rate_type=$resource[0]['currency_type'];  
          }
          else
          {
           $peak_increase_rate=0; 
          }
          /*  is peak hour exist in database end herer  */
          $vehicleResult=[];
          $i=0;
          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
          $driverGratuty=0;

           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
             


            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {
              

                $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
                  $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                  $getVehicleQueryResult = operations($getVehicleQuery);
                }



                
               
               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {


                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  /* done*/
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   
                  $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);


                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;

                     if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  } 




                  $conditionalSurchargeDateTimeFinalResult=0;


                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }


  

                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }



                    $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                    $querySelectDbZoneResult = operations($querySelectDbZone);
                    $getAirportVehicleRateRate="select * from seaport_vehicle   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                      $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);

                    
                    $getAirportExtraRate="select * from seaport_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
           
                    $getAirportExtraRateResult = operations($getAirportExtraRate);

          


                    /* mandatory fees get End here */

                    //done
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;



                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;

                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;

                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type;


                


                  $i++;
                }

                else 
               {


                // $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                //   $getVehicleQueryResult = operations($getVehicleQuery);




                 if($isStopSet=="yes")
                    {


                      $allStopsArray=explode('@',$allStops);
                     $lastLocation=$allStopsArray[0];

                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
            

                      $totalLengthStops=count($allStopsArray)-1;
                     
             
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);


                      if($totalDistanceBetweenTwoPlace!=0)
                      {
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                              
                            
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }

                                              
                            

                           }

                    
                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                                         $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$_REQUEST['dropoff_location']);
                            }
                      

                      }





                  }
                  else
                  {

                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);


                  }


              //done
                $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);
                 
                 

                 $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);
                 $rateMileagePeakHour=$getMilegeBaseRate[2];
                $peakHourDb=$getMilegeBaseRate[1];
                $getMilegeBaseRate=$getMilegeBaseRate[0];
                $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

                 if($return_rate)
                      {
                    
                        $peak_increase_rate=$rateMileagePeakHour;
                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }

                   if($getMilegeBaseRate)
                   {


                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);



                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);
                      
                     $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }

                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }

                   
                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }


                   
                   // $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                   // $querySelectDbZoneResult = operations($querySelectDbZone);
                   // $getAirportVehicleRateRate="select * from seaport_vehicle   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                   // $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);
                   // $getAirportExtraRate="select * from seaport_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                   // $getAirportExtraRateResult = operations($getAirportExtraRate);


                   $getAirportExtraRate="select * from sma_zone_data a inner join seaport_vehicle b on a.id=b.parent_id inner join seaport_extra_info c on a.id=c.parent_id where  a.type_name='".$dropLocation."' and b.vehicle_code='".$value->VehTypeCode."'";
                   
                  

                   $getAirportExtraRateResult = operations($getAirportExtraRate);
  



                   $getMilegeBaseRate=round($getMilegeBaseRate,2);
                    // $value=$getMilegeBaseRate;


                   $vehicleResult[$i]["airportRateVehicle"]=$getAirportExtraRateResult;
                   $vehicleResult[$i]["vehicle_info"]=$value;
                   $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                   $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                   $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                   $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                   $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                   $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                   $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                   $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                   $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";
                   $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                   $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;
                   if($stopCalculationStart!=0)
                    {
                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";
                    }
                    $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;
                    $i++;
                   }
                   else
                   {


                    $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  //done
                    $vehicleResult[$i]["vehicle_info"]=$value;
                    $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];

                    $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                    $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                    $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                    $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                    $vehicleResult[$i]["total_rate"]="GET QUOTE";
                    $i++;

                   }
                 


               }

            }

              }

          
         

              // return $vehicleResult;
        

          $result=global_message(200,1006,$vehicleResult);
          


       return $result;










  
}




function getToAirportRate()
{
    

      $pickLocation=explode("(",$_REQUEST['pickuplocation']);
      $pickLocation=$pickLocation[0];
      $serviceType="AIRD";
      $dropLocation=explode("(",$_REQUEST['dropoff_location']);
      $dropLocation=$dropLocation[0];
   
      $PickLocationZone= getZoneCityType($pickLocation,'city');
      $dropLocation1= getZoneCityType($dropLocation,'airport');
    ;
       
      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];




       $isStopSet=$_REQUEST['isLocationPut'];
       $allStops=$_REQUEST['stopAddtress'];


      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];
      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
    
        $pickTimeSecond=explode(" ",$picktime);
        $picktime=explode(" ",$picktime);
        
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 

        if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    
          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }




         
         
        if($PickLocationZone)
        {
           $pickLocation=$PickLocationZone[0]['type_name'];
        }


       /* get Sma Information  */
       //done
       $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);
         
       
         if(!$getPicklocationSmaInfo)
         {
          
            $getPicklocationSmaInfo=getSmaInformation($dropLocation1[0]['type_name'],$user_id);
         }
     
      
       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
                $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
                $resource = operations($query);

              if(count($resource)>=1 && getType($resource)=="boolean")
            {
       
           $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
           $resource = operations($query);
          
            }
            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];
           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);
      

          if($return_rate)
          {

            $peak_increase_rate=$resource[0]['peak_increase_rate'];
            $peak_rate_type=$resource[0]['currency_type'];  
          }
          else
          {
            $peak_increase_rate=0;  

          }



            /*  is peak hour exist in database end herer  */


           

          $vehicleResult=[];
          $i=0;

          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
        
          $driverGratuty=0;
          /*third */
           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {

            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {
              

              $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
            
                   $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                
                   $getVehicleQueryResult = operations($getVehicleQuery);
                }



              
           


               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {


                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  


                   /* new change start */
                  
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  




                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);



                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  

                  $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);



                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;


                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }
                    if( $conditionalSurchargeDateTime)
                  {
                     $conditionalSurchargeDateTimeFinalResult= $conditionalSurchargeDateTime;
                  }
                   
                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }
                  $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                  $querySelectDbZoneResult = operations($querySelectDbZone);
                  // $getAirportVehicleRateRate="select * from airport_vehicle  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                  // $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);
                  // $getAirportExtraRate="select * from airport_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                  // $getAirportExtraRateResult = operations($getAirportExtraRate);




                    /*  new code start here n */

                    $getAirportVehicleRateRate="select * from airport_vehicle  where  parent_id='".$querySelectDbZoneResult[0]['id']."' and vehicle_code='".$value->VehTypeCode."'";
                    $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);
                    





                    $getHoliSurchargeFessResultFull=0;
                    if(count($getAirportVehicleRateRateResult)>=1 && gettype($getAirportVehicleRateRateResult)!=boolean)
                    {

                        $getHoliSurchargeFessResultFull=$getAirportVehicleRateRateResult;
                    }

                    
                    $getAirportExtraRate="select * from airport_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    $getAirportExtraRateResult = operations($getAirportExtraRate);

                       $getAirportExtraRateResultFull=0;
                     

                      if(count($getAirportExtraRateResult)>=1 && gettype($getAirportExtraRateResult)!=boolean)
                      {
                        $getAirportExtraRateResultFull=$getAirportExtraRateResult;

                      }





                    /* new code end here */







                    /* mandatory fees get End here */
                    //done
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;

                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;



                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["airportRateVehicle"]=$getHoliSurchargeFessResultFull;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResultFull;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;

                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type;

                  
                  $i++;
                }
                else
                { // mileage rate start here


                   if($isStopSet=="yes")
                    {


                      $allStopsArray=explode('@',$allStops);
                      $lastLocation=$allStopsArray[0]; 
                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
                      //done
                     $totalLengthStops=count($allStopsArray)-1;
                     
             
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);


                     if($totalDistanceBetweenTwoPlace!=0)
                      {
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }

                                             
                            

                           }

                         
                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                                         $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$dropLocation);
                            }
                      

                      }



                     //    $allStopsArray=explode('@',$allStops);
                     //    $lastLocation=$allStopsArray[0];
                     //    $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($pickLocation,$allStopsArray[0]);
                     // for($location_i=1; $location_i<=count($allStopsArray); $location_i++)
                     // {

                     // $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                     //  $lastLocation=$allStopsArray[$location_i];

                     // }
                  
                     //   $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$dropLocation);
                    




                  }
                  else
                  {

                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$dropLocation);


                  }




                  

                      // $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($pickLocation,$dropLocation);
            
                if($totalDistanceBetweenTwoPlace!=0){

                

                  //done
                   $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickLocation,$dropLocation);

                

                   $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);
                  $rateMileagePeakHour=$getMilegeBaseRate[2];
                $peakHourDb=$getMilegeBaseRate[1];
                $getMilegeBaseRate=$getMilegeBaseRate[0];
                $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

                 if($return_rate)
                      {
                    
                          $peak_increase_rate=$rateMileagePeakHour;
                        // $peak_increase_rate=($rateMileagePeakHour*$getMilegeBaseRate)/100;  
                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }
                }





                 

                   if($getMilegeBaseRate)
                   {
                    

                     /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  /*done*/
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);

                       $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);


                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);



                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  


                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;



                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }

                    if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }
                   
                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }
                  $querySelectDbZone="select id from sma_zone_data  where type_name='".$dropLocation."'";
                  $querySelectDbZoneResult = operations($querySelectDbZone);
                  $getAirportVehicleRateRate="select * from airport_vehicle  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                  $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);
                  $getAirportExtraRate="select * from airport_extra_info  where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                  $getAirportExtraRateResult = operations($getAirportExtraRate);





                    $getMilegeBaseRate=round($getMilegeBaseRate,2);








                    //done
                     $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;



                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;

                        if($stopCalculationStart!=0)
                    {

                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                      
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";


                    }
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;



                      
                    
                    

                      $i++;



                   }
                   else
                   {



                    $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;

                  //done
                     $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehiceResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];

                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;



                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]="GET QUOTE";
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;



                    $i++;

                   }
                 


                }
                /* aa */
            }

              }

          
         

              // return $vehicleResult;
        

          $result=global_message(200,1006,$vehicleResult);
          

          
       
       return $result;




}

  /*new mileage function start here*/


function getMileageRateType($distancePerMiles,$vehicleCode,$smaId,$user_id,$pickuplocation,$dropoflocation)
{

              $queryGetMilegrate="select a.* from rate_calculate_mileage a inner join  mileage_rate_matrix b on a.mileage_matrix_id=b.id  inner join mileage_sma c on b.id=c.mileage_matrix_id inner join mileage_vehicle d on b.id=d.mileage_matrix_id where a.user_id='".$user_id."' and c.sma_id='".$smaId."' and d.vehicle_code='".$vehicleCode."'   group by a.id";
            
              $queryGetMilegrateRateResult = operations($queryGetMilegrate);
              // print_r($queryGetMilegrateRateResult);

              $queryGetMilegrate="select a.* from rate_calculate_mileage a inner join  mileage_rate_matrix b on a.mileage_matrix_id=b.id  inner join mileage_sma c on b.id=c.mileage_matrix_id inner join mileage_vehicle d on b.id=d.mileage_matrix_id where  d.mileage_matrix_id='".$queryGetMilegrateRateResult[0]['mileage_matrix_id']."' and a.user_id='".$user_id."' and c.sma_id='".$smaId."' and d.vehicle_code='".$vehicleCode."'   group by a.id";
            
              $queryGetMilegrateRate = operations($queryGetMilegrate);
              // print_r($queryGetMilegrateRate);
             

              if($queryGetMilegrateRate!=1)
              {
                $mileageRateQuery="select * from mileage_rate_matrix where id='".$queryGetMilegrateRate[0]['mileage_matrix_id']."'";
                $mileageRateQueryRate = operations($mileageRateQuery);
                


         

                if($mileageRateQueryRate[0]['garage_to_garage']==1)
                 {

                     $getDistanceGarageToPickupLocation=getDistanceBetweenTwoPlace($mileageRateQueryRate[0]['garage_location'],$pickuplocation);
                    
                    
                   

                     $getDropoffToDistanceGarage=getDistanceBetweenTwoPlace($mileageRateQueryRate[0]['garage_location'],$dropoflocation);
                     
                   
                    


                     if($getDistanceGarageToPickupLocation==0)
                     {
                          return false;
                     }
                     if($getDropoffToDistanceGarage==0)
                     {

                        return false;
                     }

                      $distancePerMiles+=$getDistanceGarageToPickupLocation+$getDropoffToDistanceGarage;



 
                  }



              }
           
         
              $distancePerMiles=(int)$distancePerMiles;


              $baseAmount=0;
              if($distancePerMiles!=0)
              {
                for($i=0; $i<count($queryGetMilegrateRate); $i++)
              {
                if($i==0)
                {   
                  if($distancePerMiles<$queryGetMilegrateRate[$i]['to_mileage'] )
                   {
                     $baseAmount=$queryGetMilegrateRate[$i]['fixed_amount']+$distancePerMiles;
                     return $baseAmount;
                    }
                    else
                    {
                       $baseAmount=$queryGetMilegrateRate[$i]['fixed_amount'];
                       $distancePerMiles=$distancePerMiles-$queryGetMilegrateRate[$i]['to_mileage'];
                    }
                }
                else
                {                                      
                  // $totalMileage=$queryGetMilegrateRate[$i]['to_mileage']-$queryGetMilegrateRate[$i]['from_mileage'];
                  $totalMileage=$queryGetMilegrateRate[$i]['to_mileage'];
                
                  if($distancePerMiles<$totalMileage)
                  { 
                    

                     $ratePrice= $distancePerMiles*$queryGetMilegrateRate[$i]['rate'];
                     $baseAmount+=$ratePrice;
                     break;
                  }
                  else
                  {
                      $ratePrice=$queryGetMilegrateRate[$i]['to_mileage']*$queryGetMilegrateRate[$i]['rate'];
                      $baseAmount+=$ratePrice;
                     
                      $distancePerMiles=$distancePerMiles-$queryGetMilegrateRate[$i]['to_mileage'];
                     
                     

                  }
                }
              }

              }
              




              if(count($queryGetMilegrateRate)>=1 && gettype($queryGetMilegrateRate)!="boolean")
              {
                return $baseAmount."@@".$mileageRateQueryRate[0]['peak_hour_id']."@@".$mileageRateQueryRate[0]['miles_increase_percent'];
              }
              return false;


}





  /* new mileage function end here*/







function getMileageRate($distancePerMiles,$vehicleCode,$smaId,$user_id,$pickuplocation,$dropoflocation)
{



              $queryGetMilegrate="select a.* from rate_calculate_mileage a inner join  mileage_rate_matrix b on a.mileage_matrix_id=b.id  inner join mileage_sma c on b.id=c.mileage_matrix_id inner join mileage_vehicle d on b.id=d.mileage_matrix_id where a.user_id='".$user_id."' and c.sma_id='".$smaId."' and d.vehicle_code='".$vehicleCode."'   group by a.id";
             

              $queryGetMilegrateRate = operations($queryGetMilegrate);
                

              if($queryGetMilegrateRate!=1)
              {
                $mileageRateQuery="select * from mileage_rate_matrix where id='".$queryGetMilegrateRate[0]['mileage_matrix_id']."'";
                $mileageRateQueryRate = operations($mileageRateQuery);
                




                if($mileageRateQueryRate[0]['garage_to_garage']==1)
                 {

                     $getDistanceGarageToPickupLocation=getDistanceBetweenTwoPlace($mileageRateQueryRate[0]['garage_location'],$pickuplocation);
                     $getDropoffToDistanceGarage=getDistanceBetweenTwoPlace($mileageRateQueryRate[0]['garage_location'],$dropoflocation);
                     if($getDistanceGarageToPickupLocation==0)
                     {
                          return false;
                     }
                     if($getDropoffToDistanceGarage==0)
                     {

                        return false;
                     }

                      $distancePerMiles+=$getDistanceGarage+$getDropoffToDistanceGarage;



 
                  }



              }
  
        
              $distancePerMiles=(int)$distancePerMiles;

        
              $baseAmount=0;
              for($i=0; $i<count($queryGetMilegrateRate); $i++)
              {
                

                if($i==0)
                {

                  
                    if($distancePerMiles<$queryGetMilegrateRate[$i]['to_mileage'])
                    {
                

                      $baseAmount=$queryGetMilegrateRate[$i]['fixed_amount']+$distancePerMiles;
                     
                      
                      return $baseAmount;

                    }
                    else
                    {

               
                       $baseAmount=$queryGetMilegrateRate[$i]['fixed_amount'];
                  
                       $distancePerMiles=$distancePerMiles-$queryGetMilegrateRate[$i]['to_mileage'];



                    }




                }
                else
                {
                    
                  $totalMileage=$queryGetMilegrateRate[$i]['to_mileage']-$queryGetMilegrateRate[$i]['from_mileage'];
                
                  
                  if($distancePerMiles<$totalMileage)
                    {

                      $ratePrice= $distancePerMiles*$queryGetMilegrateRate[$i]['rate'];
                      


                    
                      $baseAmount+=$ratePrice;
                      


                      break;
                    }
                    else
                    {
                      $ratePrice=$queryGetMilegrateRate[$i]['to_mileage']*$queryGetMilegrateRate[$i]['rate'];
                      

                      $baseAmount+=$ratePrice;

                      $distancePerMiles=$distancePerMiles-$queryGetMilegrateRate[$i]['to_mileage'];
                    }


                }
              }
          


              if(count($queryGetMilegrateRate)>=1 && gettype($queryGetMilegrateRate)!="boolean")
              {

                return $baseAmount;

              }
              
              return false;






}


/* get rate for from seaport */


function getFromSeaportRate()
{

      $pickLocation=explode("(",$_REQUEST['pickuplocation']);
        $pickLocation1=$pickLocation[0];
      $pickLocation=$pickLocation[0];


      $dropLocation=explode("(",$_REQUEST['dropoff_location']);
      $dropLocation=$dropLocation[0];

      $dropLocationZone= getZoneCityType($dropLocation,"city");
      $pickupLocationZone= getZoneCityType($pickLocation,"seaport");
      $serviceType="SEAA";
      $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];


      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];


      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];

      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];


      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
       $pickTimeSecond=explode(" ",$picktime);
      $picktime=explode(" ",$picktime);

     
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 
        if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    

          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }



        if($pickupLocationZone)
        {
           $pickupLocationZone1=$pickupLocationZone[0]['save_as'];;
        }
      
          
       if($dropLocationZone)
       {
          $dropLocation=$dropLocationZone[0]['type_name'];
       }




       /* get Sma Information  */
       //done
      
       $getPicklocationSmaInfo=getSmaInformation($pickLocation1,$user_id);


            if(!$getPicklocationSmaInfo)
         {
             $getPicklocationSmaInfo=getSmaInformation($dropLocation,$user_id);  
         }
             $isLawChecked=getCompanyInfo($user_id);
             $vehicleList=[];
            if(gettype($isLawChecked)!="boolean")
            {
              if($isLawChecked[0]['isLAWCheck']=='0')
                {
                  $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
                }
                else
                {
                  $vehicleList=getVehicleBackOffice();
                }
          }
       $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
          
       $resource = operations($query);

       
        if(count($resource)>=1 && getType($resource)=="boolean")
        {
       
           $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
           $resource = operations($query);
          
        }
   

      
            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];
           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);


          if($return_rate)
          {

            $peak_increase_rate=$resource[0]['peak_increase_rate']; 
            $peak_rate_type=$resource[0]['currency_type']; 
          }
          else
          {
           $peak_increase_rate=0; 
          }


       
            /*  is peak hour exist in database end herer  */


           

          $vehicleResult=[];
          $i=0;
          $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
          $driverGratuty=0;

           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
             


            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {
              

              $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
                   $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                  

            
                    $getVehicleQueryResult = operations($getVehicleQuery);

                }



              
              
               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {


                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  /*done*/
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                 


                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  
                  $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);




                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;

                  

                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;



                  }

                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;



                  }
                  


                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }




                    $querySelectDbZone="select id from sma_zone_data  where save_as='".$pickLocation."'";
                      

                    $querySelectDbZoneResult = operations($querySelectDbZone);
                    $getAirportVehicleRateRate="select * from seaport_vehicle   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                     

                      $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);

                
                    $getAirportExtraRate="select * from seaport_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";

                    $getAirportExtraRateResult = operations($getAirportExtraRate);

          


                    /* mandatory fees get End here */

                    //done
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;

                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];
                  $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;

                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type;
                   
                



                  // $vehicleResult[$i]["vehicle_info"]=$value;
                  // $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  // $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  // $vehicleResult[$i]["driver_gratuty_rate"]=$peak_increase_rate+$driverGratuty[0]['value'];
                  // $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  // $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  // $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  // $vehicleResult[$i]["total_rate"]=$peak_increase_rate+$getVehicleQueryResult[0]['amount'];
                  // $vehicleResult[$i]["airportRateVehicle"]=$getAirportVehicleRateRateResult;
                  // $vehicleResult[$i]["airportRateExtraResult"]=$getAirportExtraRateResult;
                


                  $i++;
                }

                else 
               {


                if($isStopSet=="yes")
                    {

                      $allStopsArray=explode('@',$allStops);
                     $lastLocation=$allStopsArray[0];

                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
                      //done
                      $totalLengthStops=count($allStopsArray)-1;
                     
             
                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);


                      if($totalDistanceBetweenTwoPlace!=0)
                      {
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                 
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }
                         
                            

                           }

                
                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                              $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$_REQUEST['dropoff_location']);
                            }
                      

                      }



                



                  }
                  else
                  {

                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);


                  }







                  //done

                   $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);
         
         
                   $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);
                    $rateMileagePeakHour=$getMilegeBaseRate[2];
                $peakHourDb=$getMilegeBaseRate[1];
                $getMilegeBaseRate=$getMilegeBaseRate[0];
                $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

                 if($return_rate)
                      {
                        $peak_increase_rate=$rateMileagePeakHour;
                    
                        // $peak_increase_rate=($rateMileagePeakHour*$getMilegeBaseRate)/100;  
                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }


                   if($getMilegeBaseRate)
                   {



                     /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */

                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  
                  /*done */
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  




                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                   $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                       $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);
                      
                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;


                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;


                  }

                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;


                  }

                  
                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }




        

                    // $querySelectDbZone="select id from sma_zone_data  where type_name='".$pickLocation."'";
                    // $querySelectDbZoneResult = operations($querySelectDbZone);
                    // $getAirportVehicleRateRate="select * from seaport_vehicle   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
                    //   $getAirportVehicleRateRateResult = operations($getAirportVehicleRateRate);

                    
                    // $getAirportExtraRate="select * from seaport_extra_info   where  parent_id='".$querySelectDbZoneResult[0]['id']."'";
           
                    // $getAirportExtraRateResult = operations($getAirportExtraRate);

                  $getAirportExtraRate="select * from sma_zone_data a inner join seaport_vehicle b on a.id=b.parent_id inner join seaport_extra_info c on a.id=c.parent_id where  a.type_name='".$pickLocation."' and b.vehicle_code='".$value->VehTypeCode."'";
                   
                    

                   $getAirportExtraRateResult = operations($getAirportExtraRate);
                   $getMilegeBaseRate=round($getMilegeBaseRate,2);
                   $vehicleResult[$i]["airportRateVehicle"]=$getAirportExtraRateResult;
                   $vehicleResult[$i]["vehicle_info"]=$value;
                   $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                   $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                   $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                   $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                  $vehicleResult[$i]["airportRateExtraResult"]=undefined;


                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;

                  $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;

                        if($stopCalculationStart!=0)
                    {

                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                      
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";


                    }
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;


                  $i++;



                   }
                   else
                   {

                    $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;


                    $vehicleResult[$i]["vehicle_info"]=$value;
                    $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                    $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                    $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                    $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;



                    $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                    $vehicleResult[$i]["total_rate"]="GET QUOTE";
                    $i++;

                   }
                 


               }

            }

              }

          
         

              // return $vehicleResult;
        

          $result=global_message(200,1006,$vehicleResult);
          

          
       // }
       // else
       // {

       //    $result=global_message(200,1007,$vehicleResult);


       // }

       return $result;




}





 /*  get distance between two places from google api */

function getDistanceBetweenTwoPlace($from,$to)
{
 



    $from = urlencode($from);
    $to = urlencode($to);
  


    $data = file_get_contents("//maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&sensor=false");
    $data = json_decode($data);
    $time = 0;
    $distance = 0;
    foreach($data->rows[0]->elements as $road) {
    $time += $road->duration->value;
    $distance += $road->distance->value;
    }

    $distance=($distance*0.00062137);


  
    return $distance;










}



function getPerPassengerRate()
{


      $pickLocation=$_REQUEST['pickuplocation'];
      $dropLocation=$_REQUEST['dropoff_location'];
      $serviceType="PPS";
      $PickLocationZone= getZoneCity($_REQUEST['pickuplocation']);
      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];
      $dropLocationZone= getZoneCity($_REQUEST['dropoff_location']);
      $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];


    $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];


      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
          $pickTimeSecond=explode(" ",$picktime);

        $picktime=explode(" ",$picktime);
         $pickTimeSecond=explode(":",$picktime[0]);
      $picktime=explode(":",$picktime[0]);
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 

        if($picktime[0]==12)
          {
            $picktime[0]="00";
          }    
          $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }

      // $picktime=explode(" ",$picktime);
      // $picktimeTime='';
     

      // if($picktime[1]=="AM")
      // {
      //     $picktimeTime=$picktime[0];

      // }
      // else
      // {

      //   $picktimeTime=$picktime[0]+12;

      // }






       $getPicklocationSmaInfo=getSmaInformationPerPassenger($pickLocation,$dropLocation,$user_id);

 

       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
          $query="select * from passenger_rate_matrix where pickup_location='".$pickLocation."' and drop_off_location='".$dropLocation."' and user_id='".$user_id."'";
          $resource = operations($query);
        


            /*  is peak hour exist in database start here  */
             $peakHourDb=$resource[0]['peak_hour_db'];
           $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);


          if($return_rate)
          {

            $peak_increase_rate=$resource[0]['peak_increase_rate'];
            $peak_rate_type=$resource[0]['currency_type'];  
          }
          else
          {
            $peak_increase_rate=0;  

          }



            /*  is peak hour exist in database end herer  */


             $vehicleResult=[];
          $i=0;
          $getPicklocationSmaInfo=$getPicklocationSmaInfo;
          $driverGratuty=0;
           foreach ($vehicleList->VehicleTypes->VehicleType as $value) {

            if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
            {

                $getVehicleQueryResult=false;
          
                if($isStopSet=="yes")
                {

                  $getVehicleQueryResult=false;
                }
                else
                {
                   // $getVehicleQuery="select * from passenger_vehicle where vehicle_code='".$value->VehTypeCode."' and passenger_matrix_id='".$resource[0]['id']."'";
                   

                   $getVehicleQuery="select c.* from passenger_rate_matrix a inner join passenger_vehicle b on a.id= b.passenger_matrix_id inner join rate_calculate_passenger c on a.id=c.passenger_matrix_id where  b.vehicle_code='".$value->VehTypeCode."' and a.id='".$resource[0]['id']."'";
                


                   
                    $getVehicleQueryResult = operations($getVehicleQuery);

                }
           
                

            

                if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {

                      





                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */
                   //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
               


                  /* */
                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  

                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  


                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                

                       $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);


                  $conditionalSurchargeResultFinalResult=0;
                  $conditionalSurchargeDateTimeFinalResult=0;


                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }


                   if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }

                  
                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }


                 
                  $totalAmountPerPassanger=0;


                  
                  $psgCapacityCopy=$_REQUEST['total_passenger'];
                  $totalAmountPerPassanger=0;
                  
                  for($iCount=0; $iCount<count($getVehicleQueryResult); $iCount++)
                  {

                    $nextPassengerPrice=$getVehicleQueryResult[$iCount]['to_passenger']-$getVehicleQueryResult[$iCount]['from_passenger'];
                    if($iCount==0)
                    {
                          $psgCapacityCopy=$psgCapacityCopy- $nextPassengerPrice;

                          $totalAmountPerPassanger=$getVehicleQueryResult[$iCount]['rate'];
                   




                    }
                    else
                      {
                          if($psgCapacityCopy>=$nextPassengerPrice)
                          {
                          $psgCapacityCopy=$psgCapacityCopy- $nextPassengerPrice;
                          $totalAmountPerPassanger+=$nextPassengerPrice*$getVehicleQueryResult[$iCount]['rate'];
                          
                        


                          }
                          else if($psgCapacityCopy<$nextPassengerPrice && $psgCapacityCopy>0)
                          {

                        

                          $totalAmountPerPassanger+=$psgCapacityCopy*$getVehicleQueryResult[$iCount]['rate'];
                              $psgCapacityCopy=$psgCapacityCopy- $nextPassengerPrice;
                          


                          }
                    }

                    

                      // if($psgCapacity>=$getVehicleQueryResult[$iCount]['from_passenger'])
                      // {




                      //    if($psgCapacityCopy>$getVehicleQueryResult[$iCount]['to_passenger'])
                      //     {
                         
                      //       $numberofPassenger=$getVehicleQueryResult[$iCount]['to_passenger'];
                      //     }
                      //     else
                      //     {
                      //       $numberofPassenger= $psgCapacityCopy-$getVehicleQueryResult[$iCount]['from_passenger'];
                      //       if($numberofPassenger==0)
                      //       {
                      //           $numberofPassenger=1;
                      //       }


                      //     }
                         


                      //       $totalAmountPerPassanger+=$getVehicleQueryResult[$iCount]['rate'];


                      // }










                  }
                      
                  /* mandatory fees get End here */
                  //done
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$totalAmountPerPassanger;
                  // $vehicleResult[$i]["vehicle_toll_amount"]=$getVehicleQueryResult[0]['toll_amt'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;



                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["total_rate"]=$totalAmountPerPassanger;

                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;



                  $i++;
                

               }
               else
               {



                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);


                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;


                    $vehicleResult[$i]["vehicle_info"]=$value;
                    $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                    $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                    $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                    $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                    


                    $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                    $vehicleResult[$i]["total_rate"]="GET QUOTE";
                    $i++;
               }




              }

            }

              


            $result=global_message(200,1006,$vehicleResult);
          
         

          
    

       return $result;





}



function getSmaInformationPerPassenger($pickLocation,$dropLocation,$user_id)
{
  $query="select b.* from passenger_rate_matrix a inner join passenger_sma b on a.id=b.passenger_matrix_id where a.pickup_location='".$pickLocation."' and a.drop_off_location='".$dropLocation."' and a.user_id='".$user_id."'";


  $resource = operations($query);
 
  return $resource;



}
function getPointToPointRate(){

      $pickLocation=$_REQUEST['pickuplocation'];
      $dropLocation=$_REQUEST['dropoff_location'];
      $PickLocationZone= getZoneCityType($_REQUEST['pickuplocation'],"city");

     
      $serviceType="PTP";
      $dropLocationZone= getZoneCityType($_REQUEST['dropoff_location'],"city");
   
      $isStopSet=$_REQUEST['isLocationPut'];
      $allStops=$_REQUEST['stopAddtress'];
      $passenger_id=$_REQUEST['passenger_id'];
      $current_date=$_REQUEST['current_date'];
      $pickDate=$_REQUEST['pickup_date'];
      $picktime=$_REQUEST['pickup_time'];
      $user_id=$_REQUEST['user_id'];
      $limoanywhereKey=$_REQUEST['limo_any_where_api_key'];
      $limoanywhereID=$_REQUEST['limo_any_where_api_id'];
      $luggCapacity=$_REQUEST['luggage_quantity'];
      $psgCapacity=$_REQUEST['total_passenger'];
            $pickTimeSecond=explode(" ",$picktime);
        $picktime=explode(" ",$picktime);
      $picktime=explode(":",$picktime[0]);
      
      $pickTimeLast=$picktime[1];
      $picktimeTime='';

 
      if($pickTimeSecond[1]=="AM")
      { 

        
         if($picktime[0]==12)
          {
            $picktime[0]="00";
          }     
      

            $picktimeTime=$picktime[0].":".$pickTimeLast;

      }
      else
      {


        $picktimeTime=$picktime[0]+12;
              $picktimeTime=$picktimeTime.":".$pickTimeLast;
              
              

      }
      
   


        if($PickLocationZone)
        {
           $pickLocation=$PickLocationZone[0]['type_name'];
        }


       if($dropLocationZone)
       {
         
          $dropLocation=$dropLocationZone[0]['type_name'];

       }




       /* get Sma Information  */
       //done
       $getPicklocationSmaInfo=getSmaInformation($pickLocation,$user_id);
       
        if(!$getPicklocationSmaInfo)
         {
          
            $getPicklocationSmaInfo=getSmaInformation($dropLocation,$user_id);
         }


          /*sma found for mileage code start here*/

                 $pickuplocationMileageSma= getZoneCityHrly($_REQUEST['pickuplocation']);
                   $dropLocationMileageSma= getZoneCityHrly($_REQUEST['dropoff_location']);

          /*sma found for mileage code end here*/
     


       $isLawChecked=getCompanyInfo($user_id);
       $vehicleList=[];
          if(gettype($isLawChecked)!="boolean")
          {
            if($isLawChecked[0]['isLAWCheck']=='0')
            {
              $vehicleList=getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID);
            }
            else
            {
              $vehicleList=getVehicleBackOffice();
            }
          }
       $query="select * from point_to_point_rate where pickup_zone_id='".$pickLocation."' and drop_off_zone='".$dropLocation."' and user_id='".$user_id."'";
  
       $resource = operations($query);
     
      if(count($resource)>=1 && getType($resource)=="boolean")
      {
       
           $query="select * from point_to_point_rate where pickup_zone_id='".$dropLocation."' and drop_off_zone='".$pickLocation."' and user_id='".$user_id."'";
           $resource = operations($query);
          
      }
      
      /*  is peak hour exist in database start here  */
       $peakHourDb=$resource[0]['peak_hour_db'];
       $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);
       if($return_rate)
        {
           $peak_increase_rate=$resource[0]['peak_increase_rate']; 
           $peak_rate_type=$resource[0]['currency_type']; 
        }
       else
        {
          $peak_increase_rate=0;  
        }

            /*  is peak hour exist in database end herer  */
        $vehicleResult=[];
        $i=0;
        $getPicklocationSmaInfo=$getPicklocationSmaInfo[0]['sma_id'];
        $driverGratuty=0;
        foreach ($vehicleList->VehicleTypes->VehicleType as $value) {
         if($psgCapacity <= $value->PassengerCapacity &&  $luggCapacity <= $value->LuggageCapacity )
          {
            $getVehicleQueryResult=false;
            if($isStopSet=="yes")
               {
                  $getVehicleQueryResult=false;
                }
                else
                {
                   $getVehicleQuery="select * from point_to_point_vehicle_rate where vehicle_id='".$value->VehTypeCode."' and point_parent_id='".$resource[0]['id']."'";
                    

                    $getVehicleQueryResult = operations($getVehicleQuery);


                      


                }


              



               if(count($getVehicleQueryResult)>=1 && gettype($getVehicleQueryResult)!="boolean")
               {



                  /* driver gratuty fetch from database start here */
                  $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */
                   //done
                  $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  
                  /* */
                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                  $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  //2
                  $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);

                  

                  $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                 
                  $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);

                   $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                  $conditionalSurchargeResultFinalResult=0;
                  $getHoliSurchargeFessResultFinal=0;
                  if($conditionalSurchargeResult)
                  {
                    $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                  }
                  if($conditionalSurchargeDateTime)
                  {
                    $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                  }



                  if($getHoliSurchargeFessResult)
                  {

                    $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;

                  }

                  if($driverGratutyArray)
                  {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                  }
              

                  $getMandatoryFessFinalResult=0;
                  if($getMandatoryFessArray)
                  {
                    $getMandatoryFessFinalResult=$getMandatoryFessArray;
                
                  }

                  /* mandatory fees get End here */
                  $vehicleResult[$i]["vehicle_info"]=$value;
                  $vehicleResult[$i]["vehicle_rate"]=$getVehicleQueryResult[0]['amount'];
                  // $vehicleResult[$i]["vehicle_toll_amount"]=$getVehicleQueryResult[0]['toll_amt'];
                  $vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];

                  $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                  $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                  $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                  $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                    

                  $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                  $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                  $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                  $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;

                  $vehicleResult[$i]["total_rate"]=$getVehicleQueryResult[0]['amount'];

                  $vehicleResult[$i]["peak_rate_type"]=$peak_rate_type;
                   

                 // $vehicleResult[$i]["mandatoryAdministrativeFees"]=$mandatoryAdministrativeFees;
                 // $vehicleResult[$i]["mandatoryCreditCardSurchargefees"]=$mandatoryCreditCardSurchargefees;
                 // $vehicleResult[$i]["mandatoryFuelFees"]=$mandatoryFuelFees;
                 // $vehicleResult[$i]["mandatoryReservationProcessingfees"]=$mandatoryReservationProcessingfees;
                 // $vehicleResult[$i]["mandatoryBlackCardFund"]=$mandatoryBlackCardFund;
                 
                $i++;
                // $vehicleInfo

               }
               else 
               {
                /*copy */



                  



            


                   if($pickuplocationMileageSma)
                   {

                      $getPicklocationSmaInfo=$pickuplocationMileageSma[0]['sma_id'];
                   }
                   if(!$pickuplocationMileageSma && $dropLocationMileageSma)
                   {
                      $getPicklocationSmaInfo=$dropLocationMileageSma[0]['sma_id'];


                   }
                

                  if($isStopSet=="yes")
                    {

                      $allStopsArray=explode('@',$allStops);

                     $lastLocation=$allStopsArray[0];



     
// $pickLocation=$_REQUEST['pickuplocation'];
//       $dropLocation=$_REQUEST['dropoff_location'];

                     $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$allStopsArray[0]);
                             //done
                    

                     $totalLengthStops=count($allStopsArray)-1;
                     

                     $stopCalculationStart=stopCalculationStartfunction($value->VehTypeCode,$getPicklocationSmaInfo,$serviceType,$user_id,$totalLengthStops);


             


                      if($totalDistanceBetweenTwoPlace!=0)
                      { 
                        for($location_i=0; $location_i<count($allStopsArray); $location_i++)
                          {
                                $distancePrice=0;
                              
                           
                                  if($location_i>0 && $allStopsArray[$location_i]!='')
                                  {
                                    
                                        $distancePrice=getDistanceBetweenTwoPlace($allStopsArray[$location_i-1],$allStopsArray[$location_i]);
                                            if($distancePrice==0)
                                            {

                                                $totalDistanceBetweenTwoPlace=0;
                                                break;
                                            }
                                            else
                                            {
                                                $totalDistanceBetweenTwoPlace+=$distancePrice;
                                            }
                                              $lastLocation=$allStopsArray[$location_i];



                                  }

                                                       
                            

                           }

                   
                            if($totalDistanceBetweenTwoPlace!=0)
                            {
                                         $totalDistanceBetweenTwoPlace+=getDistanceBetweenTwoPlace($lastLocation,$_REQUEST['dropoff_location']);
                            }
                      

                      }



                  




                  }
                  else
                  {
                      //om
                      $totalDistanceBetweenTwoPlace=getDistanceBetweenTwoPlace($_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);

                  }




                if($totalDistanceBetweenTwoPlace!=0){


                  


                   $getMilegeBaseRate=getMileageRateType($totalDistanceBetweenTwoPlace,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$_REQUEST['pickuplocation'],$_REQUEST['dropoff_location']);
                   


                                  
                   $getMilegeBaseRate=explode("@@", $getMilegeBaseRate);
                $rateMileagePeakHour=$getMilegeBaseRate[2];
                $peakHourDb=$getMilegeBaseRate[1];
                $getMilegeBaseRate=$getMilegeBaseRate[0];
                $return_rate= isPeakHour($pickDate,$picktimeTime,$peakHourDb);

                 if($return_rate)
                      {
                    
                        $peak_increase_rate=$rateMileagePeakHour;
                        // $peak_increase_rate=($rateMileagePeakHour*$getMilegeBaseRate)/100;  
                      }
                      else
                      {
                       $peak_increase_rate=0; 
                      }


               


                }

                // print_r($getMilegeBaseRate);
                   

                   if($getMilegeBaseRate)
                   {
                    /* driver gratuty fetch from database start here */
                    $driverGratutyArray=getdriverGratuty($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                   /* driver gratuty fetch from database start here */
                   //done
                    $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                  
                  /* */
                  // $getMandatoryFessArray=getMandatoryFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                    $conditionalSurchargeResult=conditionalSurcharge($value->VehTypeCode,$getPicklocationSmaInfo,$user_id);
                   
                    $conditionalSurchargeDateTime=conditionalSurchargeDateTime($picktimeTime,$value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);

                    $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    //1
                    $getAutoPliedDiscountPromoCode= getAutoAppliedDiscountPromoCode($user_id,$current_date,$pickDate,$serviceType,$getPicklocationSmaInfo,$value->VehTypeCode,$passenger_id);        
                    $getHoliSurchargeFessResult=getHoliSurchargeFess($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$pickDate,$serviceType);
                    $conditionalSurchargeResultFinalResult=0;

                    
                    $conditionalSurchargeDateTimeFinalResult=0;
                    $getHoliSurchargeFessResultFinal=0;
                    if($conditionalSurchargeResult)
                    {
                      $conditionalSurchargeResultFinalResult=$conditionalSurchargeResult;
                    }
                    if($conditionalSurchargeDateTime)
                    {
                      $conditionalSurchargeDateTimeFinalResult=$conditionalSurchargeDateTime;
                    }


                    if($getHoliSurchargeFessResult)
                    {
                      $getHoliSurchargeFessResultFinal=$getHoliSurchargeFessResult;
                    }
                    if($driverGratutyArray)
                    {
                      $driverGratuty=$driverGratutyArray[0]['value'];
                    }
                      $getMandatoryFessFinalResult=0;
                    if($getMandatoryFessArray)
                    {
                      $getMandatoryFessFinalResult=$getMandatoryFessArray;
                    }
                      $getMilegeBaseRate=round($getMilegeBaseRate,2);
                      $vehicleResult[$i]["vehicle_info"]=$value;
                      $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];
                      //$vehicleResult[$i]["tollAmount"]=$getVehicleQueryResult[0]['toll_amt'];



                      $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                      $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                      $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                      $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                      $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                        



                      $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                      $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                      $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                      $vehicleResult[$i]["ismileageBaseRate"]="yesMileageExist";
                      $vehicleResult[$i]["total_rate"]=$getMilegeBaseRate;
                      $vehicleResult[$i]["promoCodeArray"]=$getAutoPliedDiscountPromoCode;



                       if($stopCalculationStart!=0)
                    {

                      $vehicleResult[$i]["stopRate"]=$stopCalculationStart;
                      
                    }
                    else
                    {
                      $vehicleResult[$i]["stopRate"]="No";


                    }
                      $i++;

                      
                   }
                   else
                   {
                    $isCheckSpecialRequest=isCheckSpecialRequest($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    $isCheckChildSeat=isCheckChildSeat($value->VehTypeCode,$getPicklocationSmaInfo,$user_id,$serviceType);
                    $vehicleResult[$i]["isCheckSpecialRequest"]=$isCheckSpecialRequest;
                    $vehicleResult[$i]["isCheckChildSeat"]=$isCheckChildSeat;
                    $vehicleResult[$i]["vehicle_info"]=$value;
                    $vehicleResult[$i]["vehicle_rate"]=$getMilegeBaseRate[0]['fixed_amount'];

                    $vehicleResult[$i]["peak_hour_rate"]=$peak_increase_rate;
                    $vehicleResult[$i]["driver_gratuty_rate"]=$driverGratuty;
                    $vehicleResult[$i]["mandatoryfees"]=$getMandatoryFessFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeResultFinalResult"]=$conditionalSurchargeResultFinalResult;
                    $vehicleResult[$i]["conditionalSurchargeDateTime"]=$conditionalSurchargeDateTimeFinalResult;
                   


                    $vehicleResult[$i]["getHoliSurchargeFessResultFinal"]=$getHoliSurchargeFessResultFinal;
                    $vehicleResult[$i]["total_rate"]="GET QUOTE";
                    $i++;
                   }
                 


               }

            }

          }

          
         

              // return $vehicleResult;
        

          $result=global_message(200,1006,$vehicleResult);
          
         

          
//     exit;

       return $result;




}



function stopCalculationStartfunction($vehicle_code,$sma_info,$serviceType,$user_id,$toalNumberOfstops)
{


  $query="select a.* from stop_cal_db a inner join stop_cal_services b on a.parent_id=b.parent_id inner join master_stop_cal c on c.id=b.parent_id where b.vehicle_code='".$vehicle_code."' and b.sma_name='".$sma_info."' and b.service_type='".$serviceType."' and c.user_id='".$user_id."' group by a.id order by a.id";
 
  $resource = operations($query);

  
$finalAmount=0;

  for($i=0; $i<count($resource); $i++)
  {
      $TotalAmount=0;
        

    if(trim($resource[$i]['fixed_amt'])=='')
    {
      

      //omstart
      $query="select b.std_price from master_hour_setup a inner join hourly_vehicle b on a.id=b.parent_id inner join hourly_setup_sma  c on a.id=c.parent_id  where vehicle_code='".$vehicle_code."' and a.user_id='".$user_id."' and c.sma_id='".$sma_info."'";
    
      $resourceResult = operations($query);

      
        if(count($resourceResult)>=1 && getType($resourceResult)!=boolean)
        {
          
            $TotalAmount=$resourceResult[0]['std_price']*$resource[$i]['hourly_rate']/100;    

        }

      


    }
    else
    {
      $TotalAmount=$resource[$i]['fixed_amt'];
    }

    if($toalNumberOfstops>$resource[$i]['stops'])
    {
 
      $toalNumberOfstops=$toalNumberOfstops-$resource[$i]['stops'];
      $finalAmount=$finalAmount+$TotalAmount;
 
    }
    else
    {
      
      $finalAmount=$finalAmount+$TotalAmount;


      return $finalAmount;
    }

  }
  return $finalAmount;



}




  function getCompanyInfo($userId)
  {


     $query="select isLAWCheck from company_user_info where id='".$userId."' ";
      
     $userLogin = operations($query);
     if(count($userLogin)>=1 && gettype($userLogin)!="boolean")
     {
          return $userLogin;

     }
     return false;
     

  }




/* get vehicle from limoany where start here */

function getVehicleLimoAnyWhere($limoanywhereKey,$limoanywhereID)
{



   // if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])&&isset($_REQUEST['limoApiKey']) && !empty($_REQUEST['limoApiKey'])&&isset($_REQUEST['limoApiID']) && !empty($_REQUEST['limoApiID'])){
   
         $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
         $sh_param = array(
         'apiId'    =>    $limoanywhereID,
          'apiKey'    =>  $limoanywhereKey);
          $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL','GetStates',$sh_param);
          $action ='GetVehicleTypes';
          $result = $action.'Result';
          $trans_vehicle = $soapClient->$action(array('apiId' => $limoanywhereID, 'apiKey' =>$limoanywhereKey))->$result;
   
            // $vehicle_code_array=[];
            // for($i=0; $i<count($trans_vehicle->VehicleTypes->VehicleType); $i++)
            // {
              
            //   array_push($vehicle_code_array,$trans_vehicle->VehicleTypes->VehicleType[$i]->PassengerCapacity."_".$trans_vehicle->VehicleTypes->VehicleType[$i]->VehTypeId);

            // }
            // sort($trans_vehicle->VehicleTypes->VehicleType[$j]->PassengerCapacity);

              
            // print_r($vehicle_code_array);
              
              $final_vehicle_array=[];
              $parrentArray=$trans_vehicle->VehicleTypes->VehicleType;
              $childArray=$trans_vehicle->VehicleTypes->VehicleType;


              // print_r($parrentArray);
              for($i=0; $i<count($parrentArray); $i++)
              {
                  // $vehicle_code=explode("_",$vehicle_code_array[$i]);

                    // print_r($vehicle_code_array);


                  for($j=$i+1; $j<count($parrentArray); $j++)
                  {
                    
                    if($parrentArray[$i]->PassengerCapacity>$parrentArray[$j]->PassengerCapacity)
                    {
                      $finalVehicleResult=$parrentArray[$i];
                      $parrentArray[$i]=$parrentArray[$j];
                      $parrentArray[$j]=$finalVehicleResult;
                      // $final_vehicle_array[]=$trans_vehicle->VehicleTypes->VehicleType[$j];


                    }

                  }




              }

               $trans_vehicle->VehicleTypes->VehicleType=$parrentArray;
          return $trans_vehicle;





}
/* get vehicle from limoany where end here */






/*  get Vehicle from our back office start here*/
function getVehicleBackOffice()
{


    $querySelectVehicle="select * from vehicle_information order by passenger_capacity";
        $querySelectVehicleResult = operations($querySelectVehicle);
        
        
        $finalResult=[];
        $alternateArray=[];
        $finalVehicleResultArray=[];
        for($i=0; $i<count($querySelectVehicleResult); $i++)
        {
          $querySelectImage="select img_name from vehicle_extra_info where parent_id='".$querySelectVehicleResult[$i]['id']."' ORDER BY image_seq asc LIMIT 1";

          $querySelectImageResult=operations($querySelectImage);
          if(count($querySelectImageResult)>0 && gettype($querySelectImageResult)!="boolean")
          {

            $alternateArray['VehTypeImg1']=$vehicle_image_path.$querySelectImageResult[0]['img_name'];

          }
          else
          {
            $alternateArray['VehTypeImg1']=$vehicle_image_path.'noimage.jpg';


          }

          

            
            $alternateArray['VehTypeId']=$querySelectVehicleResult[$i]['id'];
            $alternateArray['VehTypeCode']=$querySelectVehicleResult[$i]['code'];
            $alternateArray['VehTypeTitle']=$querySelectVehicleResult[$i]['vehicle_title'];
            $alternateArray['PassengerCapacity']=$querySelectVehicleResult[$i]['passenger_capacity'];
            $alternateArray['LuggageCapacity']=$querySelectVehicleResult[$i]['luggage_capacity'];


          

          $finalResult['VehicleType'][$i]=(object)$alternateArray;
            
          
        }   
        $finalVehicleResultArray=array("ResponseCode"=>0,"ResponseText"=>"ok","VehicleTypes"=>(object)$finalResult);  
        $finalVehicleResultArray=(object)$finalVehicleResultArray;

        return $finalVehicleResultArray;

   
}


/* get mandatory fees start here */
function getMandatoryFess($vehicleCode,$smaId,$user_id ,$serviceType)
{


   $query="select a.*,GROUP_CONCAT(d.custom_fee_name) custom_fee_name,group_concat(d.is_subtamount_check) is_subtamount_check , group_concat(d.custom_fee) custom_fee ,group_concat(d.type_rate) type_rate from mandatory_fees  a INNER JOIN mf_sma b on a.id=b.mf_id INNER JOIN mf_vehicle c ON a.id=c.mf_id left join mandatory_fees_extra d on a.id=d.parent_id inner join mandatory_service_type e on  a.id=e.parent_id where  a.user_id='".$user_id."' and c.vehicle_code='".$vehicleCode."' and b.sma_id='".$smaId."' and e.service_type='".$serviceType."'";


   $resource = operations($query);  


    if(count($resource)>=1 && gettype($resource)!="boolean")
       {

          return $resource;
       }
      return false;


 }






function  conditionalSurchargeTimeDate($vehicleCode,$smaId,$user_id)
{
     $query="select a.* from cond_surcharge   a INNER JOIN cs_sma b on a.id=b.cs_id INNER JOIN cs_vehicle c ON a.id=c.cs_id where  a.user_id='".$user_id."' and c.vehicle_code='".$vehicleCode."' and b.sma_id='".$smaId."'";
   

   $resource = operations($query);  


    if(count($resource)>=1 && gettype($resource)!="boolean")
       {

          return $resource;
       }
      return false;

  
  
}


function conditionalSurchargeDateTime($startTime,$vehicleCode,$smaID,$user_id,$serviceType)
{
    $startTime=$startTime.":00";
    $query="select * from conditional_date a inner join cd_vehicle b on a.id=b.cd_id inner join cd_sma c on a.id=c.cd_id inner join con_date_time_service d on a.id=d.parent_id  where  start_time<='".$startTime."' and end_time>='".$startTime."' and b.vehicle_code='".$vehicleCode."' and c.sma_id='".$smaID."' and a.user_id='".$user_id."' and d.service_type='".$serviceType."'";
    
   
      $resource = operations($query);  


    if(count($resource)>=1 && gettype($resource)!="boolean")
       {

          return $resource;
       }
      return false;


}

function conditionalSurcharge($vehicleCode,$smaId,$user_id)
{

   $query="select a.* from cond_surcharge   a INNER JOIN cs_sma b on a.id=b.cs_id INNER JOIN cs_vehicle c ON a.id=c.cs_id where  a.user_id='".$user_id."' and c.vehicle_code='".$vehicleCode."' and b.sma_id='".$smaId."'";
   

   $resource = operations($query);  


    if(count($resource)>=1 && gettype($resource)!="boolean")
       {

          return $resource;
       }
      return false;


}



// under working

/* get mandatory fees start here */
function getHoliSurchargeFess($vehicleCode,$smaId,$user_id,$pickDate,$serviceType)
{

   
   $query="select c.* from holidays  a INNER JOIN holiday_sma b on a.id=b.holiday_id INNER JOIN holiday_vehicle c ON a.id=c.holiday_id INNER JOIN holiday_service_type d on a.id=d.parent_id where  a.user_id='".$user_id."' and c.vehicle_code='".$vehicleCode."' and b.sma_id='".$smaId."' and a.calender='".$pickDate."' and d.service_type='".$serviceType."'";
    

   $resource = operations($query);  


    if(count($resource)>=1 && gettype($resource)!="boolean")
       {

          return $resource;
       }
      return false;


 }





/*  get Vehicle from our back office end here */








function getdriverGratuty($vehicleCode,$smaId,$userID,$serviceType)
{


  $query="select a.* from driver_gratuity a INNER JOIN dg_sma b on a.id=b.dg_id INNER JOIN dg_vehicle c ON a.id=c.dg_id inner join drive_gratuity_service d on a.id=d.parent_id where  a.user_id='".$userID."' and c.vehicle_code='".$vehicleCode."' and b.sma_id='".$smaId."' and d.service_type='".$serviceType."'";
     


           $resource = operations($query);  
           

if(count($resource)>=1 && gettype($resource)!="boolean")
       {

      return $resource;

    }
    return false;


}





/* car rate seat code start here */

  function getCarSeatRate()
  {

        $user_id=$_REQUEST['user_id'];
        $vehicleCode=$_REQUEST['vehicle_code'];
        

  }


/* car rate seat code end here */


/*  check peak hour function start here */




function getSmaInformation($location_name,$userID)
{

    $query="select sma_zone_data.sma_id from sma_zone_data where type_name='".$location_name."' and user_id='".$userID."'";

    $resource = operations($query);
if(count($resource)>=1 && gettype($resource)!="boolean")
       {

      return $resource;

    }
    return false;



}

function isPeakHour($pickupDate,$pickupTime,$pickupHourId)
{ 

  
  if($pickupTime<12){
      $pickupTime=$pickupTime.":00";
  $pickupDay=date('l', strtotime($pickupDate));  
  $query="select * from peak_hour where day_name='".$pickupDay."' and (start_time<='".$pickupTime."' and end_time >= '".$pickupTime."') and parent_id='".$pickupHourId."'";
  }else{
    
   $pickupDay=date('l', strtotime($pickupDate)); 
   $pickupTime =$pickupTime.":00";
   $query="select * from peak_hour where day_name='".$pickupDay."' and (evening_start_time<='".$pickupTime."' and evening_end_time >= '".$pickupTime."') and parent_id='".$pickupHourId."'";  

 }



  $resource = operations($query);



if(count($resource)>=1 && gettype($resource)!="boolean")
       {

      return $resource;

    }
    return false;
}


/* google api use by lat and long start here */


  function reverse_geocode($lat,$lng) {
    $url = "//maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=false";
      

    $result = file_get_contents("$url");
    $json = json_decode($result);
    $postal_code='';
    foreach ($json->results as $result) {
        foreach($result->address_components as $addressPart) {
            
            if((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types))) {
                $city = $addressPart->long_name;
            }
            else if((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types))) {
                $state = $addressPart->long_name;
            }
            else if((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types))) {
                $country = $addressPart->long_name;
            }
            else if(in_array('route', $addressPart->types)) {
                $street = $addressPart->long_name;
            }
            else if(in_array('street_number', $addressPart->types)) {
                $street_number = $addressPart->long_name;
            }
            else if(in_array('postal_code', $addressPart->types)) {
                $postal_code = $addressPart->long_name;
                break;
            }
        }
        
        if($postal_code!='')
        {
          break;
        }

    }
    
    //return $address;
    return array('country' => $country, 'state' => $state, 'city' => $city, 'street' => $street_number . " " . $street, 'zip' => $postal_code);
    
}






/* google api use by lat and long end here */




function getCityAndStateNameGoogleAPI($location)
{ 
  


   $search_code = urlencode($location);
   $url = '//maps.googleapis.com/maps/api/geocode/json?address=' . $search_code . '&sensor=true';
      
   $json = json_decode(file_get_contents($url));

   $lat = $json->results[0]->geometry->location->lat;
   $lng = $json->results[0]->geometry->location->lng;



    $totalResult=reverse_geocode($lat,$lng);



              $locationArray=[];
          
          
              array_push($locationArray, $totalResult['zip']);
              array_push($locationArray, $totalResult['country']);
           
            
              return $locationArray;

      











}








function getZoneCityType($location ,$type)
{
  $locationArray=getCityAndStateNameGoogleAPI($location);
  
  if($type=="city")
  {
    $query="select sma_zone_data.type_name from  sma_location,sma_zone_data,sma_zone_data_city  where sma_zone_data.type='".$type."' and  sma_zone_data_city.postel_code='".$locationArray[0]."' and sma_location.country_name='".$locationArray[1]."'  and  sma_zone_data_city.zone_parent_id=sma_zone_data.id group by sma_zone_data_city.id limit 0,1";
    
  }
  else
  {
  // $query="select sma_zone_data.type_name from  sma_location,sma_zone_data  where sma_zone_data.type='".$type."' and sma_zone_data.type_zip='".$locationArray[0]."' and sma_location.country_name='".$locationArray[1]."'  and sma_zone_data.sma_id=sma_location.sma_id group by sma_zone_data.id limit 0,1";
    $query="select sma_zone_data.type_name from  sma_location,sma_zone_data  where sma_zone_data.type='".$type."' and sma_zone_data.type_name='".$location."' and sma_zone_data.sma_id=sma_location.sma_id group by sma_zone_data.id limit 0,1";
  

  }

$resource = operations($query);

  if(count($resource)>=1 && gettype($resource)!="boolean")
   {
     return $resource;
   }
  return false;

}


  function getZoneCityHrly($location)
  {

    $locationArray=getCityAndStateNameGoogleAPI($location);
 
     // $query="select sma_zone_data.type_name from  sma_location,sma_zone_data  where sma_zone_data.type='city' and sma_location.postal_code='".$locationArray[0]."' and sma_location.country_name='".$locationArray[1]."'  and sma_zone_data.sma_id=sma_location.sma_id group by sma_zone_data.id limit 0,1";
     $query="select sma_location.sma_id from  sma_location  where  sma_location.postal_code='".$locationArray[0]."' and sma_location.country_name='".$locationArray[1]."' limit 0,1";
     $resource = operations($query);
    if(count($resource)>=1 && gettype($resource)!="boolean")
    {
      return $resource;
    }
    return false;
  }






/*  check peak hour function end here */


function getZoneCity($location)
{

 
  $locationArray=getCityAndStateNameGoogleAPI($location);




  $query="select sma_zone_data.type_name from  sma_location,sma_zone_data  where sma_zone_data.type='city' and sma_location.postal_code='".$locationArray[0]."' and sma_location.country_name='".$locationArray[1]."'  and sma_zone_data.sma_id=sma_location.sma_id group by sma_zone_data.id limit 0,1";
 
 $resource = operations($query);
  
if(count($resource)>=1 && gettype($resource)!="boolean")
{
  return $resource;
}
  return false;
}

function getPointToPointService(){



}

?>