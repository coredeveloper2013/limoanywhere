//code for google map start===========
var localUserInfo = window.localStorage.getItem("localUserInfo");
if (localUserInfo != "guestinfo") {

    localUserInfo = JSON.parse(localUserInfo);


    var f_name = localUserInfo[0].f_name;


    $('.ufname').text(f_name);
    $('.glyphicon-log-in').addClass('glyphicon-log-out').removeClass('glyphicon-log-in');

}
else {

    $('.ufname').text('User');

    $('.glyphicon-log-out').addClass('glyphicon-log-in').removeClass('glyphicon-log-out');
}
var getLocalStorageValue = window.localStorage.getItem("rateSetInformation");
var getJsonValue = JSON.parse(getLocalStorageValue);
var getDate = getJsonValue.pickup_date;
var getTime = getJsonValue.pickup_time;
var getLocation = getJsonValue.pickuplocation;
var _SERVICEPATHServer = _SERVICEPATHSERVICECLIENT;
var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
getLocalStorageValue = JSON.parse(getLocalStorageValue);
var getJson = {
    "action": "isHrlyBlackOutDate",
    "pick_date": getDate,
    "pick_time": getTime,
    "pick_up_location": getLocation,
    "user_id": getLocalStorageValue[0].user_id,
    "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key,
    "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id
};
$('#refresh_overlay').show();
$.ajax({
    url: _SERVICEPATHServer,
    type: 'POST',
    data: getJson,
    success: function (response_blk) {
        $('#refresh_overlay').hide();
        responseObj = JSON.parse(response_blk);
        if (responseObj.data !== undefined) {
            // $('#blkout_found').val(responseObj.data[0].blackOutDateInformation);
            // $('#blkout_found').val(responseObj.data[0].blackOutDateInformation);
        }
    }
});
var selectVehicleClass = {
    _SERVICEPATHServer: _SERVICEPATHSERVICECLIENT,


    checkCutoffTime: function (getDate, getTime, servicesType) {

        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        var serviceTypeData = [];


        $('#refresh_overlay').show();
        $.ajax({
            url: selectVehicleClass._SERVICEPATHServer,
            type: 'post',
            data: 'action=checkCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&getTime=" + getTime + "&servicesType=" + servicesType,
            dataType: 'json',
            success: function (data) {
                $('#refresh_overlay').hide();

                var dataObj = data;
                if (typeof(data) == "string") {

                    dataObj = JSON.parse(data);

                }

                if (dataObj.code == "1006") {


                    $('#commanDisclaimerMsg').css("display", "block").html(dataObj.data.cutoff_message);
                    $('#submitUpdateForm').css("display", "none");
                } else {
                    $('#commanDisclaimerMsg').css("display", "none").html('');
                    // $('#submit_button').prop("disabled",false);
                    $('#submitUpdateForm').css("display", "block");


                }


            }
        });


    },

    checkSurchargeCutoffTime: function (getDate, getTime, servicesType) {

        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        var serviceTypeData = [];


        $('#refresh_overlay').show();
        $.ajax({
            url: selectVehicleClass._SERVICEPATHServer,
            type: 'post',
            data: 'action=checkSurchargeCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&getTime=" + getTime + "&servicesType=" + servicesType,
            dataType: 'json',
            success: function (data) {
                $('#refresh_overlay').hide();

                var dataObj = data;
                if (typeof(data) == "string") {

                    dataObj = JSON.parse(data);

                }

                if (dataObj.code == "1006") {


                    $('#commanDisclaimerMsg').css("display", "block").html(dataObj.data.blackout_Msg);
                    $('#submitUpdateForm').css("display", "none");
                } else {
                    $('#commanDisclaimerMsg').css("display", "none").html('');
                    // $('#submit_button').prop("disabled",false);
                    $('#submitUpdateForm').css("display", "block");


                }


            }
        });


    },

    checkBlackOutDate: function (getDate, getTime, getLocation) {
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
            var getJson = {
                "action": "isHrlyBlackOutDate",
                "pick_date": getDate,
                "pick_time": getTime,
                "pick_up_location": getLocation,
                "user_id": getLocalStorageValue[0].user_id,
                "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key,
                "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id
            };
            $('#refresh_overlay').show();
            $.ajax({

                url: _SERVICEPATHServer,

                type: 'POST',

                data: getJson,

                success: function (response) {
                    $('#refresh_overlay').hide();

                    var responseObj = response;
                    if (typeof(response) == "string") {

                        responseObj = JSON.parse(response);
                        if (responseObj.code == 1007) {
                            var isCheckAllBlackOut = 'Value Found';
                            var isAllVehicleExist = 0;
                            var totalVehicleLoop = 0;
                            var totalVehicleNotExit = 0;

                            var allVehicleArray = [];
                            var disclamer_message = 'no';
                            // console.log(responseObj.data);
                            // return 0;

                            if (responseObj.data[0].hourlyInfo[0].cut_off_time > 0) {


                                var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
                                if (typeof(getLocalStorageValue) == "string") {
                                    getLocalStorageValue = JSON.parse(getLocalStorageValue);
                                }
                                var user_id = getLocalStorageValue[0].user_id;
                                var serviceTypeData = [];
                                var currentDateTime = new Date();


                                var getCurrentDtate = currentDateTime.getDate();
                                if (getCurrentDtate < 10) {

                                    getCurrentDtate = '0' + getCurrentDtate;

                                } else {

                                    getCurrentDtate = getCurrentDtate;

                                }
                                var getCurrentMonth = currentDateTime.getMonth() + 1;

                                if (getCurrentMonth < 10) {

                                    getCurrentMonth = '0' + getCurrentMonth;

                                } else {
                                    getCurrentMonth = getCurrentMonth;
                                }
                                var getCurrentyear = currentDateTime.getFullYear();

                                var currentFullDtae = getCurrentMonth + '/' + getCurrentDtate + '/' + getCurrentyear;


                                var getcurrentHour = currentDateTime.getHours();
                                var getCurrentMinute = currentDateTime.getMinutes();

                                var currentTime = getcurrentHour + ':' + getCurrentMinute;


                                $('#refresh_overlay').show();
                                $.ajax({
                                    url: _SERVICEPATHServer,
                                    type: 'post',
                                    data: 'action=checkHourlyCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&current_date=" + currentFullDtae + "&current_time=" + currentTime + "&getTime=" + getTime + "&servicesType=HRLY",
                                    dataType: 'json',
                                    success: function (data) {
                                        $('#refresh_overlay').hide();

                                        var dataObj = data;

                                        if (typeof(data) == "string") {

                                            dataObj = JSON.parse(data);

                                        }

                                        if (dataObj.code == "1006") {
                                            var cut_off_hr = dataObj.data / 60;

                                            var a = parseInt(cut_off_hr);
                                            var b = parseInt(responseObj.data[0].hourlyInfo[0].cut_off_time);

                                            if (b > a) {
                                                $('#commanDisclaimerMsg').css("display", "block").html('Cut off hour : ' + responseObj.data[0].hourlyInfo[0].cut_off_time);
                                                $('#submitUpdateForm').css("display", "none");
                                            }


                                        } else {
                                            $('#commanDisclaimerMsg').css("display", "none").html('');
                                            // $('#submit_button').prop("disabled",false);
                                            $('#submitUpdateForm').css("display", "block");


                                        }


                                    }
                                });

                            }


                        } else {

                            localStorage.removeItem("blackOutDateInformation");
                            $('#submit_button').prop("disabled", false);
                        }


                    }


                }
            });


        }


    },
    getServiceTypes: function () {

        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        var serviceTypeData = [];
        $('#refresh_overlay').show();
        $.ajax({
            url: SERVICEPATHSERVICETYPE,
            type: 'post',
            data: 'action=GetServiceTypes&user_id=' + user_id,
            dataType: 'json',
            success: function (data) {
                $('#refresh_overlay').hide();

                if (data.ResponseText == 'OK') {

                    var ResponseHtml = '<option value="">Select</option>';
                    $.each(data.ServiceTypes.ServiceType, function (index, result) {
                        ResponseHtml += "<option value='" + result.SvcTypeCode + "'>" + result.SvcTypeDescription + "</option>";

                    });


                    $('#updateVehicleInfoservices').html(ResponseHtml);

                }
            }
        });
    },


    selectVehicleOnLoad: function () {
        $('input[type=radio][name=optradio]').change(function () {
            if (this.value == 'yes') {
                $('#stopLoc').show();
            } else if (this.value == 'no') {
                $('#stopLoc').hide();
            }
        });
        $('#services').change(function () {

            var option = $(this).find('option:selected').val();
            if (option == 2) {
                $('#intFlt').show();
            } else {
                $('#intFlt').hide();
            }
        });
        $('#services').change(function () {

            var option = $(this).find('option:selected').val();
            if (option == 4 || option == 5) {
                $('#triphr').show();
                $('#droploc').hide();
                $('#seadrop').hide();
            } else if (option == 7 || option == 8) {
                $('#seapick').show();
                $('#pickloc').hide();
                $('#seadrop').show();
                $('#droploc').hide();
                $('#triphr').hide();
            } else {
                $('#triphr').hide();
                $('#droploc').show();
                $('#seapick').hide();
                $('#pickloc').show();
                $('#seadrop').hide();
            }
        });

        $('#qntSeats1').change(function () {
            if ($('#qntSeats1').val() > 0) {
                $('#childSeatDiv2').show();
            }
        });

        $('#qntSeats2').change(function () {
            if ($('#qntSeats2').val() > 0) {
                $('#childSeatDiv3').show();
            }
        });


    },
    getUpdateVehicleInfo: function () {
        $('#updateVehicleCode').on("click", function (event) {
            // event.preventdefault();
            var getServiceName = $('#updateVehicleInfoservices').val();


        });


    },
    getBlackOutFunction: function (blackOutLocalValue) {
        var getLocalStorageValue = window.localStorage.getItem("rateSetInformation");
        var getJsonValue = getLocalStorageValue;

        if (typeof(getLocalStorageValue) == "string") {

            getJsonValue = JSON.parse(getLocalStorageValue);
        }


        $("#pickupaddress").html(getJsonValue.pickuplocation);
        $("#dropupaddress").html(getJsonValue.dropoff_location);
        var getHTML = '';

        blackOutLocalValue = JSON.parse(blackOutLocalValue);

        $.each(blackOutLocalValue.allVehicle, function (index, result) {


            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">GET QUOTE</button></h4></div></div></div>';
        });

        $('#showAllVehicle').html(getHTML);


    },
    pessangerBooking: function () {
        /*show modal while click on select hour or booking in hourly rate service code start here*/
        $('.pessangerBookHrly').on('click', function (event) {


            /*fetch all rate for specific vehicle variable start here */

            var std_hour = $(this).attr('std_hrs');
            var std_rate = $(this).attr('std_rate');
            var dsc_hour = $(this).attr('dsc_hrs');
            var dsc_rate = $(this).attr('dsc_rate');
            var sdc_hour = $(this).attr('sdc_hrs');
            var sdc_rate = $(this).attr('sdc_rate');
            var vehicle_code = $(this).attr("seq");

            var get_image_url = $(this).attr('vehicle_image');
            localStorage.setItem('selected_image', get_image_url);

            var getCreaditCharge = $(this).attr("creadit_surcharg");

            if (getCreaditCharge != 0) {

                localStorage.setItem('creadit_card_surcharg', getCreaditCharge);
            } else {

                localStorage.removeItem('creadit_card_surcharg');
            }


            /*fetch all rate for specific vahicle variable end here*/

            $('#hourly_rate_calculation').modal('show');

            $('.horlyratebook').attr({
                'vehicle_code': vehicle_code,
                'std_hour': std_hour,
                'std_rate': std_rate,
                'dsc_hour': dsc_hour,
                'dsc_rate': dsc_rate,
                'sdc_hour': sdc_hour,
                'sdc_rate': sdc_rate
            });
            var selectBoxHTml = '';
            var i = 0;
            for (i = std_hour; i <= 16; i++) {


                selectBoxHTml += "<option value='" + i + "'>" + i + "</option>";


            }

            $('#hourlyRateHour').html(selectBoxHTml);


            $('.horlyratebook').off();
            $('.horlyratebook').on('click', function () {


                var std_hour = $(this).attr('std_hour');
                var std_rate = $(this).attr('std_rate');
                var dsc_hour = $(this).attr('dsc_hour');
                var dsc_rate = $(this).attr('dsc_rate');
                var sdc_hour = $(this).attr('sdc_hour');
                var sdc_rate = $(this).attr('sdc_rate');
                var vehicle_code = $(this).attr("vehicle_code");

                var jurney_hour = $('#hourlyRateHour').val();
                var baserate = 0;
                var isServiceType = "";
                var getHidenFieldValue = '';


                if (parseFloat(jurney_hour) < parseFloat(dsc_hour)) {
                    baserate = parseFloat(jurney_hour) * parseFloat(std_rate);
                    isServiceType = "std_hidenClass_";
                } else if (parseFloat(jurney_hour) < parseFloat(sdc_hour)) {
                    baseText = "DSC Price";
                    baserate = parseFloat(jurney_hour) * parseFloat(dsc_rate);


                    isServiceType = "dsc_hidenClass_";

                } else {
                    baseText = "SDC Price";
                    baserate = parseFloat(jurney_hour) * parseFloat(sdc_rate);
                    isServiceType = "sdc_hidenClass_";

                }

                getHidenFieldValue = $('.' + isServiceType + vehicle_code).val();


                var getSeq = vehicle_code;
                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();
                if (getChildSeatAvailable != undefined) {

                    window.localStorage.setItem("isChildRide", "exist");
                } else {

                    window.localStorage.removeItem('isChildRide');
                }
                var finalJsonValue = [];
                var rateDetail = [];
                var i = 0;
                var newVariable = '';
                getHidenFieldValue = JSON.parse(getHidenFieldValue);
                console.log(getHidenFieldValue);
                $(getHidenFieldValue).each(function (index, result) {


                    if (result.vehicle_title == 'Hourly base rate' || result.vehicle_title == 'Driver Gratuty' || result.vehicle_title == 'Tax' || result.vehicle_title == 'Black Car Fund' || result.vehicle_title == 'Fuel Surcharge' || result.vehicle_title == 'STC') {


                        rateDetail.push({
                            "vehicle_title": result.vehicle_title + '($' + parseFloat(result.baseRate).toFixed(2) + ' x ' + parseFloat(jurney_hour) + ')',
                            "baseRate": ((parseFloat(result.baseRate) * parseFloat(jurney_hour)).toFixed(2))
                        });

                    }
                    else {


                        var getCustomfee = localStorage.getItem("isSubTotal");
                        if (typeof(getCustomfee) == "string") {
                            getCustomfee = JSON.parse(getCustomfee);

                            $(getCustomfee).each(function (index, customeresult) {
                                if (customeresult.vehicle_title == result.vehicle_title) {
                                    baserate = baserate - result.baseRate;
                                    result.baseRate = result.baseRate * jurney_hour;
                                    baserate = baserate + result.baseRate;

                                }

                            });

                        }

                        var isTotalItem = localStorage.getItem("isTotalItem");
                        if (typeof(isTotalItem) == "string") {
                            isTotalItem = JSON.parse(isTotalItem);
                            $(isTotalItem).each(function (index, customeresult1) {
                                if (customeresult1.vehicle_title == result.vehicle_title) {
                                    // console.log("result.vehicle_title....", result);
                                    // console.log("baseRate....", baserate);
                                    result.baseRate = (baserate * parseFloat(result.baseRate)) / 100;
                                    baserate = baserate + result.baseRate;
                                }
                            });
                        }

                        // localStorage.removeItem('isTotalItem');
                        // localStorage.removeItem('isSubTotal');
                        rateDetail.push({
                            "vehicle_title": result.vehicle_title,
                            "baseRate": parseFloat(result.baseRate).toFixed(2)
                        });

                    }


                })


                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                var fullTotalAmount = parseFloat(baserate).toFixed(2);
                var pickuplocation = $('#pickupaddress').html();
                var dropupaddress = $('#dropupaddress').html();
                var numberOfPasenger = $('#numberOfPasenger').val();

                var numberOfLuggage = $('#numberOfLuggage').val();
                var childValueStorhere = $('#childValueStorhere').html();
                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                var jurneyDate = $('#jurneyDate').html();
                var jurneyTime = $('#jurneyTime').html();


                finalJsonValue = {
                    "AllRateJson": rateDetail,
                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                    "vehicleTitle": vehicleTitle,
                    "fullTotalAmount": fullTotalAmount,
                    "pickuplocation": pickuplocation,
                    "dropupaddress": dropupaddress,
                    "numberOfPasenger": numberOfPasenger,
                    "numberOfLuggage": numberOfLuggage,
                    "childValueStorhere": childValueStorhere,
                    "jurneyDate": jurneyDate,
                    "jurneyTime": jurneyTime,
                    "vehicle_code": getSeq,
                    "updateVehicleInfoservices": updateVehicleInfoservices
                }

                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                window.location.href = "payment.html";


            });


        });


        /*show modal while click on select hour or booking in hourly rate service code End here*/

        /*show modal while click on select hour or booking in All service type excepting hourly only start here*/


        $('.pessangerBook').on("click", function (event) {

            var getSeq = $(this).attr("seq");
            var get_image_url = $(this).attr('vehicle_image');
            localStorage.setItem('selected_image', get_image_url);
            var getCreaditCharge = $(this).attr("creadit_surcharg");

            if (getCreaditCharge != 0) {

                localStorage.setItem('creadit_card_surcharg', getCreaditCharge);
            } else {

                localStorage.removeItem('creadit_card_surcharg');
            }

            var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

            if (getChildSeatAvailable != undefined) {

                window.localStorage.setItem("isChildRide", "exist");
            } else {

                window.localStorage.removeItem('isChildRide');
            }
            var finalJsonValue = [];

            // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
            var rateDetail = [];
            var i = 0;
            var newVariable = '';
            $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                if (i == 0) {
                    newVariable = $(this).html();
                    i = 1;
                } else {
                    rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                    // rateDetail[k][1]=$(this).html();

                    i = 0
                }


            });

            var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
            var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
            var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

            var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
            var pickuplocation = $('#pickupaddress').html();
            var dropupaddress = $('#dropupaddress').html();
            var numberOfPasenger = $('#numberOfPasenger').val();

            var numberOfLuggage = $('#numberOfLuggage').val();
            var childValueStorhere = $('#childValueStorhere').html();
            var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
            var jurneyDate = $('#jurneyDate').html();
            var jurneyTime = $('#jurneyTime').html();


            finalJsonValue = {
                "AllRateJson": rateDetail,
                "vehicleLuggageCapacity": vehicleLuggageCapacity,
                "vehiclePassengerCapacity": vehiclePassengerCapacity,
                "vehicleTitle": vehicleTitle,
                "fullTotalAmount": fullTotalAmount,
                "pickuplocation": pickuplocation,
                "dropupaddress": dropupaddress,
                "numberOfPasenger": numberOfPasenger,
                "numberOfLuggage": numberOfLuggage,
                "childValueStorhere": childValueStorhere,
                "jurneyDate": jurneyDate,
                "jurneyTime": jurneyTime,
                "vehicle_code": getSeq,
                "updateVehicleInfoservices": updateVehicleInfoservices
            }

            window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
            window.location.href = "payment.html";


        });

        /*show modal while click on select hour or booking in All service type excepting hourly only End here*/


    },
    getBlackHourlyFunction: function (getBlackOutDateInfo) {
        var getLocalStorageValue = window.localStorage.getItem("rateSetInformation");
        var getJsonValue = JSON.parse(getLocalStorageValue);
        getBlackOutDateInfo = JSON.parse(getBlackOutDateInfo);
        $('#refresh_overlay').show();
        $.ajax({
            url: selectVehicleClass._SERVICEPATHServer,
            type: 'POST',
            data: getJsonValue,
            success: function (response) {
                $('#refresh_overlay').hide();
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }
                if (responseObj.code == 1006) {
                    var getHTML = '';
                    var k = 0;
                    $.each(responseObj.data, function (index, result) {
                        $.each(getBlackOutDateInfo.hourlyInfoArray, function (index2, result2) {
                            if (result2[0].vehicle_code == result.vehicle_info.VehTypeCode) {
                                if (result.total_rate == "GET QUOTE") {
                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }
                                    var getSpecial2RequestHTML = '';
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }
                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';
                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';
                                }
                                else {
                                    var totalAmount = parseFloat(0);
                                    var tableHtml = '';
                                    var sdc_hrs = parseFloat(result.vehicle_rate[0].sdc_hrs) + parseFloat(result.vehicle_rate[0].increase_hrs);
                                    var dsc_hrs = parseFloat(result.vehicle_rate[0].dsc_hrs) + parseFloat(result.vehicle_rate[0].increase_hrs);
                                    var std_hrs = parseFloat(result.vehicle_rate[0].std_hrs) + parseFloat(result.vehicle_rate[0].increase_hrs);
                                    var jurney_hour = parseFloat(getJsonValue.jurney_hour);
                                    var stcCalCulationRate = 0;
                                    // if(jurney_hour>)
                                    if (jurney_hour < std_hrs) {

                                        stcCalCulationRate = jurney_hour * (result.vehicle_rate[0].std_price + result.vehicle_rate[0].increase_rate);
                                    } else if (jurney_hour < dsc_hrs && jurney_hour > std_hrs) {
                                        stcCalCulationRate = jurney_hour * (result.vehicle_rate[0].dsc_price + result.vehicle_rate[0].increase_rate);


                                    } else {
                                        stcCalCulationRate = jurney_hour * (result.vehicle_rate[0].sdc_price + result.vehicle_rate[0].increase_rate);
                                    }
                                    if (stcCalCulationRate != 0) {


                                        var stcCalCulationRate1 = parseFloat(Math.round(stcCalCulationRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stcCalCulationRate1);
                                        tableHtml += '<tr><td>Per hour Rate </td><td>' + stcCalCulationRate1 + '</td></tr>';

                                    }
                                    if (typeof(result.vehicle_rate) == 'number') {
                                        var vehicle_rate = parseFloat(Math.round(result.vehicle_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(vehicle_rate);
                                        tableHtml += '<tr><td>Base Rate</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                    }
                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    // if(typeof(result.vehicle_rate)!='number' && result.vehicle_rate[0].std_price!=0)
                                    //  {
                                    //        totalAmount+=parseFloat(result.vehicle_rate[0].std_price);
                                    //        tableHtml+='<tr><td>STD Price</td><td>'+result.vehicle_rate[0].std_price+'</td></tr>';
                                    //  }
                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);

                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }
                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }
                                    if (result.mandatoryfees != 0) {
                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            var admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            var calculated_tax = (parseFloat(result.total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }

                                        // if(result.mandatoryfees[0].admin!=0)
                                        //     {
                                        //       totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //       tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //     }


                                        //done
                                        if (result.mandatoryfees[0].custom_fee_name != null) {
                                            var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                            var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                            var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                            for (var l = 0; l < customfeesNameArray.length; l++) {
                                                var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                                if (customerFee != 0) {

                                                    if (type_rateArray[l] == "%") {

                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';

                                                    } else {

                                                        // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                        totalAmount += parseFloat(customerFee);
                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                    }


                                                }


                                            }
                                        }
                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(result.total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(totalAmout2);
                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(result.total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(totalAmout2);
                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(result.total_rate) * result.mandatoryfees[0].stc) / 100;
                                            var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(totalAmout2);
                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //      {
                                        //        totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //        tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //      }
                                        //  if(result.mandatoryfees[0].fuel!=0)
                                        //      {
                                        //         totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //         tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //       }


                                        // if(result.mandatoryfees[0].stc!=0)
                                        // {
                                        //   totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //   tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        // }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            var reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }


                                    }
                                    var getHolidaySurcharge = 0;
                                    var getHolidaySurchargefee = 0;
                                    var getHolidaySurchargefee_type = 'fxdAMT';
                                    var getJson = {
                                        "action": "getHolidaySurcharge",
                                        "pick_date": getDate,
                                        "pick_time": getTime
                                    };
                                    $('#refresh_overlay').show();
                                    $.ajax({

                                        url: _SERVICEPATHServer,

                                        type: 'POST',


                                        data: getJson,

                                        success: function (response) {
                                            $('#refresh_overlay').hide();


                                            if (response != 0) {
                                                getHolidaySurcharge = 1;
                                                responseObj = JSON.parse(response);

                                                getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                            } else {

                                                getHolidaySurcharge = 0;
                                            }

                                        }

                                    });
                                    if (getHolidaySurcharge != 0) {
                                        var numberOfPasenger = $('#numberOfPasenger').val();

                                        if (getHolidaySurchargefee_type == 'fxdAMT') {
                                            var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                        } else {
                                            var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                            var total_holi_Surch_fee = per_amount;
                                        }


                                        var reserve = parseFloat(total_holi_Surch_fee);
                                        totalAmount += parseFloat(reserve);
                                        totalGrandItemAmount += parseFloat(reserve);

                                        tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                    }
                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus' + result.vehicle_info.VehTypeCode + '" >$' + totalAmount + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    k++;
                                    // })
                                    // tableHtml=' ';
                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+getJsonValue.total_passenger+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+getJsonValue.luggage_quantity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+result.vehicle_rate.std_price+'</div><p><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="myModal" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Modal Header</h4> </div> <div class="modal-body"> <p>Some text in the modal.</p> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>'
                                }
                            }
                        });
                    });
                    $("#pickupaddress").html(getJsonValue.pickuplocation);
                    $("#dropupaddress").html(getJsonValue.dropoff_location);
                    $('#jurneyDate').html(getJsonValue.pickup_date);
                    $('#jurneyTime').html(getJsonValue.pickup_time);
                    $('#showAllVehicle').html(getHTML);
                    selectVehicleClass.pessangerBooking();
                } else {
                    alert("vehicle not found ");
                }
            }
        });
    },

    getVehiclesBackOffice: function () {
        var getBlackOutDateInfo = window.localStorage.getItem("blackOutDateInformation");
        var getLocalStorageValue = window.localStorage.getItem("rateSetInformation");
        var isStopValue = window.localStorage.getItem("isStop");
        var StopsAllValue = window.localStorage.getItem("StopsAll");
        var getJsonValue = getLocalStorageValue;

        if (typeof(getLocalStorageValue) === "string") {
            getJsonValue = JSON.parse(getLocalStorageValue);


            if (typeof(getBlackOutDateInfo) === "string") {

                if (getJsonValue.serviceType != "HRLY" || getJsonValue.serviceType != "CH") {
                    selectVehicleClass.getBlackOutFunction(getBlackOutDateInfo);
                    return 0;
                } else {

                    selectVehicleClass.getBlackHourlyFunction(getBlackOutDateInfo);
                    return 0;
                }


            }
            if (getJsonValue.serviceType == "TTS") {


                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();

                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }


                        if (responseObj.code == 1006) {
                            var getHTML = '';

                            var k = 0;


                            $.each(responseObj.data, function (index, result) {


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var getSpecial2RequestHTML = '';
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }


                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" seq2=' + result.vehicle_info.VehTypeCode + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + '>' + result.total_rate + '</button></h4></div></div></div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';

                                } else {


                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var admin = 0;
                                    var totalGrandItemAmount = 0;
                                    //7
                                    var reserve = 0;

                                    var grandTotal = 0;

                                    var subtotal_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var stc_charge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);


                                    if (result.peak_hour_rate != 0) {
                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(result.total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);

                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);

                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);

                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);

                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);

                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }

                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);

                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }

                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);

                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }
                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            var calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);

                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }
                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }


                                        //done

                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);

                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);

                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);


                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);

                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //    tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //  }
                                        // if(result.mandatoryfees[0].fuel!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //    tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //  }
                                        //  if(result.mandatoryfees[0].stc!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //    tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        //  }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {

                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }


                                    }
                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    if (result.vehicle_toll_amount != '' && result.vehicle_toll_amount != undefined) {
                                        var vehicle_toll_amount = parseFloat(Math.round(result.vehicle_toll_amount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(vehicle_toll_amount);
                                        grandTotal += parseFloat(vehicle_toll_amount);
                                        totalGrandItemAmount += parseFloat(vehicle_toll_amount);

                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + vehicle_toll_amount + '</td></tr>';
                                    }


                                    /* sunil code start here */

                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);

                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }

                                    /*mandatory rate start here*/


                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {


                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {

                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }

                                                    // var totalAmout1=(parseFloat(total_rate)*customfeesArray[l])/100;
                                                    // var totalAmout2= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                    // totalAmount+= parseFloat(totalAmout2);
                                                    // tableHtml+='<tr><td>'+customfeesNameArray[l]+'</td><td class="text_alighn_right">'+totalAmout2+'</td></tr>';

                                                } else {

                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);

                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }


                                            }


                                        }
                                    }


                                    /*mandatory rate end here*/


                                    if (result.promoCodeArray[0] !== undefined) {


                                        var promocodeDiscountLocal = 0;
                                        var discountAmount = 0;
                                        // var grandTotal=0;
                                        var subTotal = 0;

                                        if (result.promoCodeArray[0].discount_type == "%") {


                                            var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                            $.each(promocodeType, function (index, apply_rate) {

                                                if (apply_rate == "GRT") {
                                                    promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    // //discountAmount=totalAmount;
                                                    // if(result.mandatoryfees[0].credit!=0 && result.mandatoryfees[0].credit!=null){
                                                    //  var totalAmout1=(parseFloat(total_rate)*result.mandatoryfees[0].credit)/100;
                                                    //  var cerdit_card_surcharge= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                    //  tableHtml+='<tr><td>Creadit Card Fees</td><td class="text_alighn_right">'+cerdit_card_surcharge+'</td></tr>';

                                                    // }

                                                    // grandTotal=(parseFloat(reserve)+parseFloat(admin)+parseFloat(cerdit_card_surcharge)+parseFloat(calculated_tax));

                                                    // promocodeDiscountLocal+=(parseFloat(grandTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;


                                                    //totalAmount-=parseFloat(promocodeDiscountLocal);
                                                }

                                                if (apply_rate == "SBT") {


                                                    subTotal = (parseFloat(fuel_charge) + parseFloat(stc_charge) + parseFloat(black_car_charge));
                                                    promocodeDiscountLocal += (parseFloat(subTotal) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                    //totalAmount-=parseFloat(promocodeDiscountLocal);

                                                }

                                                if (apply_rate == "BSR") {
                                                    promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }
                                                // totalAmount-=parseFloat(promocodeDiscountLocal);
                                            });
                                        } else {

                                            promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                            // totalAmount-=parseFloat(promocodeDiscountLocal);


                                        }

                                        var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                    }
                                    /* sunil code end here */


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seat Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    } else {

                                        getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + ' >BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    }
                                    k++;

                                }


                            });

                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);


                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices

                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            });

                            selectVehicleClass.pessangerBooking();

                        }
                    }
                });


            }
            else if (getJsonValue.serviceType == "FTS") {


                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }

                        if (responseObj.code == 1006) {
                            var getHTML = '';

                            var k = 0;


                            $.each(responseObj.data, function (index, result) {


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var getSpecial2RequestHTML = '';
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }


                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" seq2=' + result.vehicle_info.VehTypeCode + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + '>' + result.total_rate + '</button></h4></div></div></div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';

                                } else {


                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var admin = 0;
                                    //6
                                    var totalGrandItemAmount = 0;
                                    var reserve = 0;
                                    var grandTotal = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var stc_charge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);


                                    if (result.peak_hour_rate != 0) {
                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(result.total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }

                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);

                                    if (result.tollAmount != 0 && result.tollAmount != undefined)
                                    /* {
                                              var toll_amount= parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                              totalAmount+=parseFloat(toll_amount);
                                              tableHtml+='<tr><td>Estimated Tolls</td><td class="text_alighn_right">'+toll_amount+'</td></tr>';
                                          }*/
                                        if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                            var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);

                                            driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                            driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(driver_gratuty_rate);
                                            grandTotal += parseFloat(driver_gratuty_rate);
                                            totalGrandItemAmount += parseFloat(driver_gratuty_rate);

                                            tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                        }
                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);

                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }


                                    // if(typeof(result.airportRateExtraResult)!='undefined' && result.airportRateExtraResult!=0)
                                    //  {
                                    //      if(result.airportRateExtraResult[0].seaport_fee!=0)
                                    //          {
                                    //              var seaport_fee= parseFloat(Math.round(result.airportRateExtraResult[0].seaport_fee * 100) / 100).toFixed(2);
                                    //              totalAmount+= parseFloat(seaport_fee);
                                    //              tableHtml+='<tr><td>seaport fees</td><td class="text_alighn_right">'+seaport_fee+'</td></tr>';
                                    //          }


                                    //  }

                                    if (result.trainRateExtraResult != 0 && result.trainRateExtraResult != undefined) {

                                        var trainRateExtraResult = parseFloat(Math.round(result.trainRateExtraResult * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(trainRateExtraResult);
                                        grandTotal += parseFloat(trainRateExtraResult);
                                        totalGrandItemAmount += parseFloat(trainRateExtraResult);

                                        tableHtml += '<tr><td>Train Surcharge</td><td class="text_alighn_right">' + trainRateExtraResult + '</td></tr>';
                                    }


                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);

                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }
                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);

                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }

                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }

                                        //done

                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);

                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);

                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(result.total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);

                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //    tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //  }
                                        // if(result.mandatoryfees[0].fuel!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //    tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //  }
                                        //  if(result.mandatoryfees[0].stc!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //    tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        //  }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + result.mandatoryfees[0].reserve + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }


                                    }

                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    /*   toll rate start here*/


                                    /* toll rate end here*/

                                    if (result.vehicle_toll_amount != '' && result.vehicle_toll_amount != undefined) {
                                        var vehicle_toll_amount = parseFloat(Math.round(result.vehicle_toll_amount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(vehicle_toll_amount);
                                        grandTotal += parseFloat(vehicle_toll_amount);
                                        totalGrandItemAmount += parseFloat(vehicle_toll_amount);

                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + vehicle_toll_amount + '</td></tr>';
                                    }


                                    /*  if(result.promoCodeArray!=true)
                  {


                    var promocodeDiscountLocal=0;
                    if(result.promoCodeArray[0].discount_type=="%")
                    {
                      promocodeDiscountLocal=(parseFloat(result.total_rate)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                      totalAmount-=parseFloat(promocodeDiscountLocal);


                    }
                    else
                    {

                      promocodeDiscountLocal=result.promoCodeArray[0].discount_value;
                      totalAmount-=parseFloat(promocodeDiscountLocal);


                    }


                      tableHtml+='<tr><td>Discount</td><td class="text_alighn_right">- '+promocodeDiscountLocal+'</td></tr>';




                  }







*/

                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);

                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }

                                    /* manadatory fees start here*/

                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {

                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {
                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }


                                                } else {
                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);

                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }
                                            }


                                        }
                                    }


                                    /*mandatory fees end here*/


                                    if (result.promoCodeArray[0] !== undefined) {


                                        var promocodeDiscountLocal = 0;
                                        var discountAmount = 0;
                                        // var grandTotal=0;
                                        var subTotal = 0;

                                        if (result.promoCodeArray[0].discount_type == "%") {


                                            var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                            $.each(promocodeType, function (index, apply_rate) {

                                                if (apply_rate == "GRT") {
                                                    promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    //discountAmount=totalAmount;
                                                    // if(result.mandatoryfees[0].credit!=0 && result.mandatoryfees[0].credit!=null){
                                                    //  var totalAmout1=(parseFloat(total_rate)*result.mandatoryfees[0].credit)/100;
                                                    //  var cerdit_card_surcharge= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

                                                    //   tableHtml+='<tr><td>Creadit Card Fees</td><td class="text_alighn_right">'+cerdit_card_surcharge+'</td></tr>';
                                                    // }

                                                    // grandTotal=(parseFloat(reserve)+parseFloat(admin)+parseFloat(cerdit_card_surcharge)+parseFloat(calculated_tax));

                                                    // promocodeDiscountLocal+=(parseFloat(grandTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                                                    //totalAmount-=parseFloat(promocodeDiscountLocal);
                                                }

                                                // if(apply_rate=="SBT"){


                                                //   subTotal=(parseFloat(fuel_charge)+parseFloat(stc_charge)+parseFloat(black_car_charge));
                                                //   promocodeDiscountLocal+=(parseFloat(subTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;
                                                //   //totalAmount-=parseFloat(promocodeDiscountLocal);

                                                // }

                                                if (apply_rate == "BSR") {
                                                    promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }
                                                totalAmount -= parseFloat(promocodeDiscountLocal);
                                            });
                                        } else {

                                            promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }

                                        var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                    }

                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seat Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }

                                    if (result.isCheckSpecialRequest == "YesExists") {


                                        getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    } else {

                                        getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    }


                                    k++;

                                }


                            });

                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);

                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices
                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            })

                            selectVehicleClass.pessangerBooking();

                        }
                    }
                });


            }
            else if (getJsonValue.serviceType == "PPS") {


                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }
                        if (responseObj.code == 1006) {
                            var getHTML = '';

                            var k = 0;


                            $.each(responseObj.data, function (index, result) {

                                if (result.total_rate == "GET QUOTE") {




                                    //     var getSpecialRequestHTML='';
                                    //     if(result.isCheckChildSeat=="YesExists")
                                    //       {
                                    //           // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                    //           // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                    //               getSpecialRequestHTML="<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    //       }

                                    // var getSpecial2RequestHTML='';
                                    //    if(result.isCheckSpecialRequest=="YesExists")
                                    //       {
                                    //          // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                    //               getSpecial2RequestHTML="<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    //       }


                                    //         getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> '+getSpecialRequestHTML+getSpecial2RequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" seq2='+result.vehicle_info.VehTypeCode+'>'+result.total_rate+'</button></h4></div></div></div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';

                                }
                                else {


                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var admin = 0;

                                    //5
                                    var reserve = 0;
                                    var totalGrandItemAmount = 0;
                                    var grandTotal = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var stc_charge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);
                                    //var tableHtml='<tr><td>Base Rate</td><td class="text_alighn_right">'+result.total_rate+'</td></tr>';


                                    if (result.peak_hour_rate != 0) {


                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(result.total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);

                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);

                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {


                                        var driver_gratuty_rate = parseFloat(Math.round(result.mandatoryfees[0].driver_gratuty_rate * 100) / 100).toFixed(2);
                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);

                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }

                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.mandatoryfees[0].stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);


                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }

                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);


                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {

                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);

                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }

                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }

                                        //done

                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(totalAmout2);


                                            grandTotal += parseFloat(totalAmout2);
                                            totalGrandItemAmount += parseFloat(totalAmout2);

                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);

                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);

                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        var getHolidaySurcharge = 0;
                                        var getHolidaySurchargefee = 0;
                                        var getHolidaySurchargefee_type = 'fxdAMT';

                                        var getJson = {
                                            "action": "getHolidaySurcharge",
                                            "pick_date": getDate,
                                            "pick_time": getTime
                                        };
                                        $('#refresh_overlay').show();
                                        $.ajax({

                                            url: _SERVICEPATHServer,

                                            type: 'POST',

                                            data: getJson,

                                            success: function (response) {
                                                $('#refresh_overlay').hide();


                                                if (response != 0) {
                                                    getHolidaySurcharge = 1;
                                                    responseObj = JSON.parse(response);

                                                    getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                    getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                                } else {

                                                    getHolidaySurcharge = 0;
                                                }

                                            }

                                        });

                                        if (getHolidaySurcharge != 0) {
                                            var numberOfPasenger = $('#numberOfPasenger').val();

                                            if (getHolidaySurchargefee_type == 'fxdAMT') {
                                                var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                            } else {
                                                var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                                var total_holi_Surch_fee = per_amount;
                                            }


                                            var reserve = parseFloat(total_holi_Surch_fee);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //    tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //  }
                                        // if(result.mandatoryfees[0].fuel!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //    tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //  }
                                        //  if(result.mandatoryfees[0].stc!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //    tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        //  }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {

                                            var reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }


                                    }


                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    if (result.vehicle_toll_amount != '' && result.vehicle_toll_amount != undefined) {

                                        var vehicle_toll_amount = parseFloat(Math.round(result.vehicle_toll_amount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(vehicle_toll_amount);
                                        grandTotal += parseFloat(vehicle_toll_amount);
                                        totalGrandItemAmount += parseFloat(vehicle_toll_amount);

                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + vehicle_toll_amount + '</td></tr>';
                                    }


                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);

                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }

                                    /* manadatory fees start here*/

                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {

                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {
                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }


                                                } else {
                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);

                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }
                                            }


                                        }
                                    }


                                    /*mandatory fees end here*/


                                    if (result.promoCodeArray[0] !== undefined) {


                                        var promocodeDiscountLocal = 0;
                                        var discountAmount = 0;
                                        // var grandTotal=0;
                                        var subTotal = 0;

                                        if (result.promoCodeArray[0].discount_type == "%") {


                                            var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                            $.each(promocodeType, function (index, apply_rate) {

                                                if (apply_rate == "GRT") {

                                                    promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;

                                                    // //discountAmount=totalAmount;
                                                    // if(result.mandatoryfees[0].credit!=0 && result.mandatoryfees[0].credit!=null){
                                                    //  var totalAmout1=(parseFloat(total_rate)*result.mandatoryfees[0].credit)/100;
                                                    //  var cerdit_card_surcharge= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

                                                    //   tableHtml+='<tr><td>Creadit Card Fees</td><td class="text_alighn_right">'+cerdit_card_surcharge+'</td></tr>';
                                                    // }

                                                    // grandTotal=(parseFloat(reserve)+parseFloat(admin)+parseFloat(cerdit_card_surcharge)+parseFloat(calculated_tax));

                                                    // promocodeDiscountLocal+=(parseFloat(grandTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                                                    //totalAmount-=parseFloat(promocodeDiscountLocal);
                                                }

                                                if (apply_rate == "SBT") {


                                                    subTotal = (parseFloat(fuel_charge) + parseFloat(stc_charge) + parseFloat(black_car_charge));
                                                    promocodeDiscountLocal += (parseFloat(subTotal) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                    //totalAmount-=parseFloat(promocodeDiscountLocal);

                                                }

                                                if (apply_rate == "BSR") {
                                                    promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }
                                                totalAmount -= parseFloat(promocodeDiscountLocal);
                                            });
                                        } else {

                                            promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }

                                        var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                    }

                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seat Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }

                                    /*    if(result.isCheckSpecialRequest=="YesExists")
                                         {


                                        getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>'+getSpecialRequestHTML+mileageBaseTollRateHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus'+result.vehicle_info.VehTypeCode+'">$'+totalAmount.toFixed(2)+'</span>'+mileageBaseRateHTML+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg='+cerdit_surcharge+' vehicle_image='+result.vehicle_info.VehTypeImg1+' seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer">'+mileageBaseTollRateHTML+' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                     }
                                     else
                                     {

                                       getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li>'+getSpecialRequestHTML+' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus'+result.vehicle_info.VehTypeCode+'">$'+totalAmount.toFixed(2)+'</span>'+mileageBaseRateHTML+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg='+cerdit_surcharge+' vehicle_image='+result.vehicle_info.VehTypeImg1+' seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer">'+mileageBaseTollRateHTML+'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                     }*/


                                    /* alert(result.getHoliSurchargeFessResultFinal); Shuttle/Per Passenger Holiday Setup*/

                                    if (result.getHoliSurchargeFessResultFinal != 0) {
                                        if (result.getHoliSurchargeFessResultFinal[0].allowORES != 'yes') {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price">' + result.getHoliSurchargeFessResultFinal[0].blackout_Msg + '<h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>GET QUOTE</button></h4></div></div></div>';


                                        } else {


                                            if (result.isCheckSpecialRequest == "YesExists") {


                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            } else {

                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            }


                                        }


                                    } else {


                                        if (result.isCheckSpecialRequest == "YesExists") {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        } else {

                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        }

                                    }


                                    k++;

                                }


                            });

                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);

                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices
                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            })

                            selectVehicleClass.pessangerBooking();

                        }
                    }
                });


            }
            else if (getJsonValue.serviceType == "PTP") {


                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();


                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }


                        if (responseObj.code == 1006) {
                            var getHTML = '';

                            var k = 0;


                            $.each(responseObj.data, function (index, result) {


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var getSpecial2RequestHTML = '';
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }


                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';

                                } else {


                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var admin = 0;
                                    //4
                                    var reserve = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var stc_charge = 0;
                                    /*Edited by infograins for Fuel Surcharge*/
                                    var totalGrandItemAmount = 0;
                                    var totalAmountStdRate = 0;
                                    var grandTotalStdRate = 0;
                                    var totalGrandItemAmountStdRate = 0;
                                    var totalAmountSdcRate = 0;
                                    var grandTotalSdcRate = 0;
                                    var totalGrandItemAmountSdcRate = 0;
                                    /*Edited by infograins for Fuel Surcharge*/
                                    var cerdit_surcharge = 0;
                                    var grandTotal = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);

                                    if (result.peak_hour_rate != 0) {
                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);

                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);


                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);

                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);


                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }

                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);


                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }


                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);


                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';


                                        }


                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }


                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);


                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);


                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }

                                        var getHolidaySurcharge = 0;
                                        var getHolidaySurchargefee = 0;
                                        var getHolidaySurchargefee_type = 'fxdAMT';

                                        var getJson = {
                                            "action": "getHolidaySurcharge",
                                            "pick_date": getDate,
                                            "pick_time": getTime
                                        };
                                        $('#refresh_overlay').show();
                                        $.ajax({

                                            url: _SERVICEPATHServer,

                                            type: 'POST',

                                            data: getJson,

                                            success: function (response) {
                                                $('#refresh_overlay').hide();


                                                if (response != 0) {
                                                    getHolidaySurcharge = 1;
                                                    responseObj = JSON.parse(response);

                                                    getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                    getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                                } else {

                                                    getHolidaySurcharge = 0;
                                                }

                                            }

                                        });

                                        if (getHolidaySurcharge != 0) {
                                            var numberOfPasenger = $('#numberOfPasenger').val();

                                            if (getHolidaySurchargefee_type == 'fxdAMT') {
                                                var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                            } else {
                                                var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                                var total_holi_Surch_fee = per_amount;
                                            }


                                            var reserve = parseFloat(total_holi_Surch_fee);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //    tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //  }
                                        // if(result.mandatoryfees[0].fuel!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //    tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //  }
                                        //  if(result.mandatoryfees[0].stc!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //    tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        //  }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }


                                    }

                                    //alert(cerdit_surcharge);


                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    if (result.vehicle_toll_amount != '' && result.vehicle_toll_amount != undefined) {
                                        var vehicle_toll_amount = parseFloat(Math.round(result.vehicle_toll_amount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(vehicle_toll_amount);

                                        grandTotal += parseFloat(vehicle_toll_amount);
                                        totalGrandItemAmount += parseFloat(vehicle_toll_amount);
                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + vehicle_toll_amount + '</td></tr>';
                                    }


                                    /*
                                                          if(result.promoCodeArray!=true)
                                                       {


                                                         var promocodeDiscountLocal=0;
                                                         if(result.promoCodeArray[0].discount_type=="%")
                                                         {
                                                           promocodeDiscountLocal=(parseFloat(result.total_rate)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                                                           totalAmount-=parseFloat(promocodeDiscountLocal);


                                                         }
                                                         else
                                                         {

                                                           promocodeDiscountLocal=result.promoCodeArray[0].discount_value;
                                                           totalAmount-=parseFloat(promocodeDiscountLocal);


                                                         }


                                                           tableHtml+='<tr><td>Discount</td><td class="text_alighn_right">- '+promocodeDiscountLocal+'</td></tr>';




                                                       }*/

                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);


                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }


                                    /* manadatory fees start here*/

                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {

                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {
                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }


                                                } else {
                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);


                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }
                                            }


                                        }
                                    }


                                    /*mandatory fees end here*/


                                    /*Discount code start here*/
                                    if (result.promoCodeArray[0] !== undefined) {


                                        var promocodeDiscountLocal = 0;
                                        var discountAmount = 0;
                                        // var grandTotal=0;
                                        var subTotal = 0;

                                        if (result.promoCodeArray[0].discount_type == "%") {


                                            var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                            $.each(promocodeType, function (index, apply_rate) {


                                                if (apply_rate == "GRT") {

                                                    promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    // //discountAmount=totalAmount;
                                                    // if(result.mandatoryfees[0].credit!=0 && result.mandatoryfees[0].credit!=null){
                                                    //  var totalAmout1=(parseFloat(total_rate)*result.mandatoryfees[0].credit)/100;
                                                    //  var cerdit_card_surcharge= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

                                                    //   tableHtml+='<tr><td>Creadit Card Fees</td><td class="text_alighn_right">'+cerdit_card_surcharge+'</td></tr>';
                                                    // }

                                                    // grandTotal=(parseFloat(reserve)+parseFloat(admin)+parseFloat(cerdit_card_surcharge)+parseFloat(calculated_tax));

                                                    // promocodeDiscountLocal+=(parseFloat(grandTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;


                                                }


                                                if (apply_rate == "BSR") {
                                                    promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }
                                                totalAmount -= parseFloat(promocodeDiscountLocal);
                                            });
                                        } else {

                                            promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }

                                        var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                    }


                                    /*Discount code end here*/

                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seat Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>child seat available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }


                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }


                                    if (result.getHoliSurchargeFessResultFinal != 0) {
                                        if (result.getHoliSurchargeFessResultFinal[0].allowORES != 'yes') {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price">' + result.getHoliSurchargeFessResultFinal[0].blackout_Msg + '<h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>GET QUOTE</button></h4></div></div></div>';


                                        } else {


                                            if (result.isCheckSpecialRequest == "YesExists") {


                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            } else {

                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            }


                                        }


                                    } else {


                                        if (result.isCheckSpecialRequest == "YesExists") {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        } else {

                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        }

                                    }


                                    k++;

                                }


                            });

                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);

                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");
                                var get_image_url = $(this).attr('vehicle_image');
                                localStorage.setItem('selected_image', get_image_url);

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices
                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            })

                            selectVehicleClass.pessangerBooking();

                        }
                    }
                });


            }
            else if (getJsonValue.serviceType == "HRLY" || getJsonValue.serviceType == "CH") {
                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {
                            responseObj = JSON.parse(response);
                        }
                        if (responseObj.code == 1006) {
                            var getHTML = '';
                            var k = 0;
                            $.each(responseObj.data, function (index, result) {
                                if (result.total_rate == "mileageProblem") {
                                    getHTML = '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><h1><center>' + result.radius_msg_data[0].radius_msg + '</center></h1></div>';
                                    return false;
                                } else if (result.total_rate == "GET QUOTE") {
                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";
                                    }
                                    var getSpecial2RequestHTML = '';
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;12 Special Package Available</a></li>";
                                    }
                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';
                                } else {
                                    var baseText = "Base Rate";
                                    /*std calculation variable start here*/
                                    var baseRateStdRate = 0;
                                    var baseRateStdHour = 0;
                                    var totalAmountStdRate = 0;
                                    var grandTotalStdRate = parseFloat(0);
                                    var totalGrandItemAmountStdRate = parseFloat(0);
                                    var stdHiddenFieldHtml = [];
                                    /*std calculation variable end here*/
                                    /*dsc calculation variable start here*/
                                    var baserate = 0;
                                    var baserateHour = 0;
                                    var totalAmount = parseFloat(0);
                                    var grandTotal = parseFloat(0);
                                    var totalGrandItemAmount = parseFloat(0);
                                    var dscHiddenFieldHtml = [];
                                    /*dsc calculation variable end here*/
                                    /*sdc calculation variable start here*/
                                    var baseRateSdcRate = 0;
                                    var baseRateSdcHour = 0;
                                    var totalAmountSdcRate = 0;
                                    var grandTotalSdcRate = parseFloat(0);
                                    var totalGrandItemAmountSdcRate = parseFloat(0);
                                    var sdcHiddenFieldHtml = [];
                                    /*sdc calculation variable end here*/
                                    /* old variable start here*/
                                    var tableHtml = '';
                                    var admin = 0;
                                    var reserve = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var stc_charge = 0;
                                    var increase_hrs = 0;
                                    var cutoff_hrs = 0;
                                    /* old variable end here*/
                                    var vehicleRate2 = 0;
                                    if (typeof(result.vehicle_rate) == "object") {
                                        vehicleRate2 = result.vehicle_rate[0].increase_rate;
                                        vehicleRateType = result.vehicle_rate[0].rate_type;
                                        increase_hrs = result.vehicle_rate[0].increase_hrs;
                                        cut_off_time = result.vehicle_rate[0].cut_off_time;
                                    }
                                    if (typeof(result.vehicle_rate) == "object") {
                                        /* hourly rate setup here start here*/
                                        baseRateStdRate = result.vehicle_rate[0].std_price;
                                        baseRateStdHour = result.vehicle_rate[0].std_hrs;
                                        baserate = result.vehicle_rate[0].dsc_price;
                                        baserateHour = result.vehicle_rate[0].dsc_hrs;
                                        if ($('#blkout_found').val() != '') {
                                            if (vehicleRateType == 'fxdAMT') {
                                                vehicleRate2 = vehicleRate2;
                                            } else {
                                                vehicleRate2 = (baseRateStdRate * vehicleRate2) / 100;
                                            }
                                            var increase_rate_new = parseFloat(Math.round(vehicleRate2 * 100) / 100).toFixed(2);

                                            var aa = parseFloat(baseRateStdRate);
                                            var ab = parseFloat(increase_rate_new)
                                            baseRateStdRate = aa + ab;

                                        }
                                        baseRateSdcHour = result.vehicle_rate[0].sdc_hrs;
                                        baseRateSdcRate = result.vehicle_rate[0].sdc_price
                                        sdcHiddenFieldHtml.push({
                                            vehicle_title: "Base rate",
                                            baseRate: baseRateSdcRate
                                        });
                                        dscHiddenFieldHtml.push({vehicle_title: "Base rate", baseRate: baserate});
                                        stdHiddenFieldHtml.push({
                                            vehicle_title: "Hourly base rate",
                                            baseRate: baseRateStdRate
                                        });
                                        stdHiddenFieldHtml.push({
                                            vehicle_title: "Estimated Tolls",
                                            baseRate: result.vehicle_toll_amount
                                        });
                                        tableHtml += '<tr><td>Base rate</td><td class="text_alighn_right">' + baserate + '</td></tr>';
                                    }
                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.vehicle_rate[0].stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);
                                        /* std claculation start here*/
                                        totalAmountStdRate += parseFloat(stopRate);
                                        grandTotalStdRate += parseFloat(stopRate);
                                        totalGrandItemAmountStdRate += parseFloat(stopRate);
                                        /* std calculation end here*/
                                        /* sdc claculation start here*/
                                        totalAmountSdcRate += parseFloat(stopRate);
                                        grandTotalSdcRate += parseFloat(stopRate);
                                        totalGrandItemAmountSdcRate += parseFloat(stopRate);
                                        /* sdc calculation end here*/
                                        sdcHiddenFieldHtml.push({vehicle_title: "Stops Rates", baseRate: stopRate});
                                        dscHiddenFieldHtml.push({vehicle_title: "Stops Rates", baseRate: stopRate});
                                        stdHiddenFieldHtml.push({vehicle_title: "Stops Rates", baseRate: stopRate});
                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }
                                    var vehicleStdRate = 0;
                                    var vehicleSdcRate = 0;
                                    if (typeof(result.vehicle_rate) == "object") {
                                        result.vehicle_rate = baserate;
                                        vehicleStdRate = baseRateStdRate;
                                        vehicleSdcRate = baseRateSdcRate;
                                    }
                                    if (typeof(result.vehicle_rate) == 'number') {
                                        var vehicle_rate = parseFloat(Math.round(result.vehicle_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(vehicle_rate);
                                        grandTotal += parseFloat(vehicle_rate);
                                        totalGrandItemAmount += parseFloat(vehicle_rate);
                                        /* std claculation start here*/
                                        var vehicle_rate_std = parseFloat(Math.round(vehicleStdRate * 100) / 100).toFixed(2);
                                        totalAmountStdRate += parseFloat(vehicle_rate_std);
                                        grandTotalStdRate += parseFloat(vehicle_rate_std);
                                        totalGrandItemAmountStdRate += parseFloat(vehicle_rate_std);
                                        /* std calculation end here*/
                                        /* sdc claculation start here*/
                                        var vehicle_rate_sdc = parseFloat(Math.round(vehicleSdcRate * 100) / 100).toFixed(2);

                                        totalAmountSdcRate += parseFloat(vehicle_rate_sdc);
                                        grandTotalSdcRate += parseFloat(vehicle_rate_sdc);
                                        totalGrandItemAmountSdcRate += parseFloat(vehicle_rate_sdc);
                                        /* sdc calculation end here*/
                                        sdcHiddenFieldHtml.push({vehicle_title: baseText, baseRate: vehicle_rate_sdc});
                                        dscHiddenFieldHtml.push({vehicle_title: vehicle_rate, baseRate: vehicle_rate});
                                        stdHiddenFieldHtml.push({
                                            vehicle_title: vehicle_rate,
                                            baseRate: vehicle_rate_std
                                        });
                                        tableHtml += '<tr><td>' + baseText + '</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                    }
                                    if (vehicleRate2 != 0) {
                                        var increase_rate = parseFloat(Math.round(vehicleRate2 * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Increse rate</td><td class="text_alighn_right">' + increase_rate + '</td></tr>';
                                    }
                                    if (result.vehicle_toll_amount != 0 && result.vehicle_toll_amount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.vehicle_toll_amount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);
                                        /* std claculation start here*/
                                        totalAmountStdRate += parseFloat(toll_amount);
                                        grandTotalStdRate += parseFloat(toll_amount);
                                        totalGrandItemAmountStdRate += parseFloat(toll_amount);
                                        /* std calculation end here*/
                                        /* sdc claculation start here*/
                                        totalAmountSdcRate += parseFloat(toll_amount);
                                        grandTotalSdcRate += parseFloat(toll_amount);
                                        totalGrandItemAmountSdcRate += parseFloat(toll_amount);
                                        sdcHiddenFieldHtml.push({
                                            vehicle_title: "Estimated Tolls",
                                            baseRate: result.vehicle_toll_amount
                                        });
                                        dscHiddenFieldHtml.push({
                                            vehicle_title: "Estimated Tolls",
                                            baseRate: result.vehicle_toll_amount
                                        });
                                        /* sdc calculation end here*/
                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rateDsc = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);
                                        driver_gratuty_rateDsc = (parseFloat(result.vehicle_rate) * driver_gratuty_rateDsc) / 100;
                                        totalAmount += parseFloat(driver_gratuty_rateDsc);
                                        grandTotal += parseFloat(driver_gratuty_rateDsc);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rateDsc);
                                        /* std claculation start here*/
                                        var driver_gratuty_rateStd = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);
                                        driver_gratuty_rateStd = (parseFloat(baseRateStdRate) * driver_gratuty_rateStd) / 100;
                                        totalAmountStdRate += parseFloat(driver_gratuty_rateStd);
                                        grandTotalStdRate += parseFloat(driver_gratuty_rateStd);
                                        totalGrandItemAmountStdRate += parseFloat(driver_gratuty_rateStd);


                                        /* std calculation end here*/

                                        /* sdc claculation start here*/

                                        var driver_gratuty_rateSdc = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);
                                        driver_gratuty_rateSdc = (parseFloat(baseRateSdcRate) * driver_gratuty_rateDsc) / 100;

                                        driver_gratuty_rateSdc = parseFloat(Math.round(driver_gratuty_rateSdc * 100) / 100).toFixed(2);
                                        totalAmountSdcRate += parseFloat(driver_gratuty_rateSdc);
                                        grandTotalSdcRate += parseFloat(driver_gratuty_rateSdc);
                                        totalGrandItemAmountSdcRate += parseFloat(driver_gratuty_rateSdc);


                                        sdcHiddenFieldHtml.push({
                                            vehicle_title: "Driver Gratuty",
                                            baseRate: driver_gratuty_rateSdc
                                        });
                                        dscHiddenFieldHtml.push({
                                            vehicle_title: "Driver Gratuty",
                                            baseRate: driver_gratuty_rateDsc
                                        });
                                        stdHiddenFieldHtml.push({
                                            vehicle_title: "Driver Gratuty",
                                            baseRate: driver_gratuty_rateStd
                                        });

                                        /* sdc calculation end here*/

                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rateSdc + '</td></tr>';
                                    }


                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);

                                            /* std claculation start here*/

                                            totalAmountStdRate += parseFloat(admin);

                                            totalGrandItemAmountStdRate += parseFloat(admin);


                                            /* std calculation end here*/


                                            /* sdc claculation start here*/

                                            totalAmountSdcRate += parseFloat(admin);

                                            totalGrandItemAmountSdcRate += parseFloat(admin);
                                            sdcHiddenFieldHtml.push({
                                                vehicle_title: "Administrative Fees",
                                                baseRate: admin
                                            });
                                            dscHiddenFieldHtml.push({
                                                vehicle_title: "Administrative Fees",
                                                baseRate: admin
                                            });
                                            stdHiddenFieldHtml.push({
                                                vehicle_title: "Administrative Fees",
                                                baseRate: admin
                                            });

                                            /* sdc calculation end here*/

                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(result.vehicle_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);


                                            /* std claculation start here*/

                                            var calculated_taxStd = (parseFloat(baseRateStdRate) * subtotal_tax) / 100;
                                            totalAmountStdRate += parseFloat(calculated_taxStd);
                                            grandTotalStdRate += parseFloat(calculated_taxStd);
                                            totalGrandItemAmountStdRate += parseFloat(calculated_taxStd);


                                            /* std calculation end here*/
                                            /* sdc claculation start here*/

                                            var calculated_taxSdc = (parseFloat(baseRateSdcRate) * subtotal_tax) / 100;

                                            totalAmountSdcRate += parseFloat(calculated_taxSdc);
                                            grandTotalSdcRate += parseFloat(calculated_taxSdc);
                                            totalGrandItemAmountSdcRate += parseFloat(calculated_taxSdc);


                                            sdcHiddenFieldHtml.push({
                                                vehicle_title: "Tax",
                                                baseRate: calculated_taxSdc
                                            });
                                            dscHiddenFieldHtml.push({vehicle_title: "Tax", baseRate: calculated_tax});
                                            stdHiddenFieldHtml.push({
                                                vehicle_title: "Tax",
                                                baseRate: calculated_taxStd
                                            });

                                            /* sdc calculation end here*/

                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(result.vehicle_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);


                                            /* std claculation start here*/
                                            var totalAmout1_std_rate = (parseFloat(baseRateStdRate) * result.mandatoryfees[0].black_car) / 100;
                                            var black_car_chargeStd = parseFloat(Math.round(totalAmout1_std_rate * 100) / 100).toFixed(2);
                                            totalAmountStdRate += parseFloat(black_car_chargeStd);
                                            grandTotalStdRate += parseFloat(black_car_chargeStd);
                                            totalGrandItemAmountStdRate += parseFloat(black_car_chargeStd);


                                            /* std calculation end here*/

                                            /* sdc claculation start here*/

                                            var totalAmout1_sdc_rate = (parseFloat(baseRateSdcRate) * result.mandatoryfees[0].black_car) / 100;
                                            var black_car_chargeSdc = parseFloat(Math.round(totalAmout1_sdc_rate * 100) / 100).toFixed(2);

                                            totalAmountSdcRate += parseFloat(black_car_chargeSdc);
                                            grandTotalSdcRate += parseFloat(black_car_chargeSdc);
                                            totalGrandItemAmountSdcRate += parseFloat(black_car_chargeSdc);

                                            /* sdc calculation end here*/


                                            sdcHiddenFieldHtml.push({
                                                vehicle_title: "Black Car Fund",
                                                baseRate: black_car_chargeSdc
                                            });
                                            dscHiddenFieldHtml.push({
                                                vehicle_title: "Black Car Fund",
                                                baseRate: black_car_charge
                                            });
                                            stdHiddenFieldHtml.push({
                                                vehicle_title: "Black Car Fund",
                                                baseRate: black_car_chargeStd
                                            });


                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(result.vehicle_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);

                                            /* std claculation start here*/
                                            var totalAmout_std_rate = (parseFloat(baseRateStdRate) * result.mandatoryfees[0].fuel) / 100;
                                            var fuel_chargeStd = parseFloat(Math.round(totalAmout_std_rate * 100) / 100).toFixed(2);
                                            totalAmountStdRate += parseFloat(fuel_chargeStd);
                                            grandTotalStdRate += parseFloat(fuel_chargeStd);
                                            totalGrandItemAmountStdRate += parseFloat(fuel_chargeStd);


                                            /* std calculation end here*/

                                            /* sdc claculation start here*/

                                            var totalAmout1_sdc_rate = (parseFloat(baseRateSdcRate) * result.mandatoryfees[0].fuel) / 100;
                                            var fuel_chargeSdc = parseFloat(Math.round(totalAmout1_sdc_rate * 100) / 100).toFixed(2);

                                            totalAmountSdcRate += parseFloat(fuel_chargeSdc);
                                            grandTotalSdcRate += parseFloat(fuel_chargeSdc);
                                            totalGrandItemAmountSdcRate += parseFloat(fuel_chargeSdc);

                                            /* sdc calculation end here*/

                                            sdcHiddenFieldHtml.push({
                                                vehicle_title: "Fuel Surcharge",
                                                baseRate: fuel_chargeSdc
                                            });
                                            dscHiddenFieldHtml.push({
                                                vehicle_title: "Fuel Surcharge",
                                                baseRate: fuel_charge
                                            });
                                            stdHiddenFieldHtml.push({
                                                vehicle_title: "Fuel Surcharge",
                                                baseRate: fuel_chargeStd
                                            });


                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(result.vehicle_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            /* std claculation start here*/
                                            var totalAmout1_std_rate = (parseFloat(baseRateStdRate) * result.mandatoryfees[0].stc) / 100;
                                            var stc_chargeStd = parseFloat(Math.round(totalAmout1_std_rate * 100) / 100).toFixed(2);


                                            totalAmountStdRate += parseFloat(stc_chargeStd);
                                            grandTotalStdRate += parseFloat(stc_chargeStd);
                                            totalGrandItemAmountStdRate += parseFloat(stc_chargeStd);


                                            /* std calculation end here*/


                                            /* sdc claculation start here*/

                                            var totalAmout1_sdc_rate = (parseFloat(baseRateStdRate) * result.mandatoryfees[0].stc) / 100;
                                            var stc_chargeSdc = parseFloat(Math.round(totalAmout1_sdc_rate * 100) / 100).toFixed(2);

                                            totalAmountSdcRate += parseFloat(stc_chargeSdc);
                                            grandTotalSdcRate += parseFloat(stc_chargeSdc);
                                            totalGrandItemAmountSdcRate += parseFloat(stc_chargeSdc);


                                            /* sdc calculation end here*/


                                            sdcHiddenFieldHtml.push({vehicle_title: "STC", baseRate: stc_chargeSdc});
                                            dscHiddenFieldHtml.push({vehicle_title: "STC", baseRate: stc_charge});
                                            stdHiddenFieldHtml.push({vehicle_title: "STC", baseRate: stc_chargeStd});


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);
                                            /* std claculation start here*/
                                            totalAmountStdRate += parseFloat(reserve);
                                            totalGrandItemAmountStdRate += parseFloat(reserve);
                                            /* std calculation end here*/
                                            /* sdc claculation start here*/
                                            totalAmountSdcRate += parseFloat(reserve);
                                            totalGrandItemAmountSdcRate += parseFloat(reserve);
                                            /* sdc calculation end here*/
                                            sdcHiddenFieldHtml.push({vehicle_title: "Reserve", baseRate: reserve});
                                            dscHiddenFieldHtml.push({vehicle_title: "Reserve", baseRate: reserve});
                                            stdHiddenFieldHtml.push({vehicle_title: "Reserve", baseRate: reserve});
                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }
                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = result.mandatoryfees[0].credit;
                                            cerdit_surcharge = result.mandatoryfees[0].credit;


                                            // var totalAmout1 = (parseFloat(result.vehicle_rate) * result.mandatoryfees[0].credit) / 100;
                                            // cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }


                                    }


                                    var getHolidaySurcharge = 0;
                                    var getHolidaySurchargefee = 0;
                                    var getHolidaySurchargefee_type = 'fxdAMT';

                                    var getJson = {
                                        "action": "getHolidaySurcharge",
                                        "pick_date": getDate,
                                        "pick_time": getTime
                                    };
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHServer,
                                        type: 'POST',
                                        data: getJson,
                                        success: function (response) {
                                            $('#refresh_overlay').hide();
                                            if (response != 0) {
                                                getHolidaySurcharge = 1;
                                                responseObj = JSON.parse(response);
                                                getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;
                                            } else {
                                                getHolidaySurcharge = 0;
                                            }
                                        }
                                    });
                                    if (getHolidaySurcharge != 0) {
                                        var numberOfPasenger = $('#numberOfPasenger').val();

                                        if (getHolidaySurchargefee_type == 'fxdAMT') {
                                            var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                        } else {
                                            var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                            var total_holi_Surch_fee = per_amount;
                                        }


                                        var reserve = parseFloat(total_holi_Surch_fee);
                                        totalAmount += parseFloat(reserve);
                                        totalGrandItemAmount += parseFloat(reserve);

                                        tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                    }


                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);

                                            /* std claculation start here*/
                                            totalAmountStdRate += parseFloat(vehicle_rate);
                                            grandTotalStdRate += parseFloat(vehicle_rate);
                                            totalGrandItemAmountStdRate += parseFloat(vehicle_rate);
                                            /* std calculation end here*/
                                            /* sdc claculation start here*/
                                            totalAmountSdcRate += parseFloat(vehicle_rate);
                                            grandTotalSdcRate += parseFloat(vehicle_rate);
                                            totalGrandItemAmountSdcRate += parseFloat(vehicle_rate);
                                            /* sdc calculation end here*/
                                            sdcHiddenFieldHtml.push({
                                                vehicle_title: "Hours/Early Morning Pickups",
                                                baseRate: vehicle_rate
                                            });
                                            dscHiddenFieldHtml.push({
                                                vehicle_title: "Hours/Early Morning Pickups",
                                                baseRate: vehicle_rate
                                            });
                                            stdHiddenFieldHtml.push({
                                                vehicle_title: "Hours/Early Morning Pickups",
                                                baseRate: vehicle_rate
                                            });
                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }
                                    }
                                    /* manadatory fees start here*/
                                    if (result.mandatoryfees[0].custom_fee_name != null) {

                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        var subtotalCal = [];
                                        var totalItemCal = [];
                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);
                                            if (customerFee != 0) {
                                                if (type_rateArray[l] == "%") {
                                                    if (is_subtamount_check[l] == "subtotal") {

                                                        var totalAmout1 = (parseFloat(result.vehicle_rate) * customfeesArray[l]) / 100;
                                                        // var totalAmout1 = (parseFloat(result.vehicle_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        /* std claculation start here*/
                                                        var totalAmout1 = (parseFloat(baseRateStdRate) * customfeesArray[l]) / 100;
                                                        var totalAmout2Std = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmountStdRate += parseFloat(totalAmout2Std);
                                                        totalGrandItemAmountStdRate += parseFloat(totalAmout2Std);
                                                        /* std calculation end here*/
                                                        /* sdc claculation start here*/
                                                        var totalAmout1 = (parseFloat(baseRateSdcRate) * customfeesArray[l]) / 100;
                                                        var totalAmout2Sdc = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmountSdcRate += parseFloat(totalAmout2Sdc);
                                                        totalGrandItemAmountSdcRate += parseFloat(totalAmout2Sdc);
                                                        // var subtotalCal={
                                                        //     "vehicle_title":customfeesNameArray[l]
                                                        // };
                                                        subtotalCal.push({"vehicle_title": customfeesNameArray[l]});
                                                        localStorage.setItem("isSubTotal", JSON.stringify(subtotalCal));
                                                        /* sdc calculation end here*/
                                                        sdcHiddenFieldHtml.push({
                                                            "vehicle_title": customfeesNameArray[l],
                                                            "baseRate": totalAmout2Sdc
                                                        });
                                                        dscHiddenFieldHtml.push({
                                                            "vehicle_title": customfeesNameArray[l],
                                                            "baseRate": totalAmout2
                                                        });
                                                        stdHiddenFieldHtml.push({
                                                            "vehicle_title": customfeesNameArray[l],
                                                            "baseRate": totalAmout2Std
                                                        });
                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';
                                                    } else {


                                                        totalItemCal.push({"vehicle_title": customfeesNameArray[l]});
                                                        localStorage.setItem("isTotalItem", JSON.stringify(totalItemCal));

                                                        // var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;
                                                        // var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        // totalAmount += parseFloat(totalAmout2);
                                                        // totalGrandItemAmount += parseFloat(totalAmout2);
                                                        // /* std claculation start here*/
                                                        // totalAmountStdRate += parseFloat(totalAmout2);
                                                        // totalGrandItemAmountStdRate += parseFloat(totalAmout2);
                                                        // /* std calculation end here*/
                                                        // /* sdc claculation start here*/
                                                        // totalAmountSdcRate += parseFloat(totalAmout2);
                                                        // totalGrandItemAmountSdcRate += parseFloat(totalAmout2);
                                                        // /* sdc calculation end here*/
                                                        // sdcHiddenFieldHtml.push({ "vehicle_title": customfeesNameArray[l], "baseRate": totalAmout2 });
                                                        // dscHiddenFieldHtml.push({ "vehicle_title": customfeesNameArray[l], "baseRate": totalAmout2 });
                                                        // stdHiddenFieldHtml.push({ "vehicle_title": customfeesNameArray[l], "baseRate": totalAmout2 });
                                                        // tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                        sdcHiddenFieldHtml.push({
                                                            "vehicle_title": customfeesNameArray[l],
                                                            "baseRate": customfeesArray[l]
                                                        });
                                                        dscHiddenFieldHtml.push({
                                                            "vehicle_title": customfeesNameArray[l],
                                                            "baseRate": customfeesArray[l]
                                                        });
                                                        stdHiddenFieldHtml.push({
                                                            "vehicle_title": customfeesNameArray[l],
                                                            "baseRate": customfeesArray[l]
                                                        });
                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customfeesArray[l] + '</td></tr>';


                                                    }
                                                } else {
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);
                                                    /* std claculation start here*/
                                                    totalAmountStdRate += parseFloat(customerFee);
                                                    totalGrandItemAmountStdRate += parseFloat(customerFee);
                                                    /* std calculation end here*/
                                                    /* sdc claculation start here*/
                                                    totalAmountSdcRate += parseFloat(customerFee);
                                                    totalGrandItemAmountSdcRate += parseFloat(customerFee);
                                                    /* sdc calculation end here*/
                                                    sdcHiddenFieldHtml.push({
                                                        "vehicle_title": customfeesNameArray[l],
                                                        "baseRate": customerFee
                                                    });
                                                    dscHiddenFieldHtml.push({
                                                        "vehicle_title": customfeesNameArray[l],
                                                        "baseRate": customerFee
                                                    });
                                                    stdHiddenFieldHtml.push({
                                                        "vehicle_title": customfeesNameArray[l],
                                                        "baseRate": customerFee
                                                    });
                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';
                                                }
                                            }
                                        }
                                    }
                                    /*mandatory fees end here*/
                                    if (result.promoCodeArray[0] !== undefined) {
                                        /*dsc discount variable start here*/
                                        window.localStorage.setItem("combine_discount", result.promoCodeArray[0].is_combine_discount);
                                        if (result.promoCodeArray[0].promo_pref == 1) {
                                            localStorage.setItem('auto_promo_code', result.promoCodeArray[0].code);
                                            var promocodeDiscountLocal = 0;
                                            var discountAmount = 0;
                                            var subTotal = 0;
                                            /*dsc discount variable end here*/
                                            /*SDC DISCOUNT VARIABLE START HERE*/
                                            var promocodeDiscountLocalSdc = 0;
                                            var discountAmountSdc = 0;
                                            var subTotalSdc = 0;
                                            /*SDC DISCOUNT VARIABLE END HERE*/
                                            /*STD DISCOUNT VARIABLE START HERE*/
                                            var promocodeDiscountLocalStd = 0;
                                            var discountAmountStd = 0;
                                            var subTotalStd = 0;
                                            /*STD DISCOUNT VARIABLE END HERE*/
                                            if (result.promoCodeArray[0].discount_type == "%") {
                                                var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                                $.each(promocodeType, function (index, apply_rate) {
                                                    if (apply_rate == "GRT") {
                                                        promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                        promocodeDiscountLocalSdc += (parseFloat(totalGrandItemAmountSdcRate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                        promocodeDiscountLocalStd += (parseFloat(totalGrandItemAmountStdRate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                    }
                                                    if (apply_rate == "BSR") {
                                                        promocodeDiscountLocal += (parseFloat(baserate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                        promocodeDiscountLocalSdc += (parseFloat(baseRateSdcRate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                        promocodeDiscountLocalStd += (parseFloat(baseRateStdRate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;
                                                    }
                                                    totalAmount -= parseFloat(promocodeDiscountLocal);
                                                    /* std claculation start here*/
                                                    totalAmountStdRate += parseFloat(promocodeDiscountLocalStd);
                                                    /* std calculation end here*/
                                                    /* sdc claculation start here*/
                                                    totalAmountSdcRate += parseFloat(promocodeDiscountLocalSdc);
                                                    /* sdc calculation end here*/

                                                });
                                            } else {
                                                promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                                totalAmount -= parseFloat(promocodeDiscountLocal);
                                                /* std claculation start here*/
                                                promocodeDiscountLocalStd = result.promoCodeArray[0].discount_value;
                                                totalAmountStdRate -= parseFloat(promocodeDiscountLocalStd);
                                                /* std calculation end here*/
                                                /* sdc claculation start here*/
                                                promocodeDiscountLocalSdc = result.promoCodeArray[0].discount_value;
                                                totalAmountSdcRate -= parseFloat(promocodeDiscountLocalSdc);
                                                /* sdc calculation end here*/
                                            }

                                            var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);

                                            var final_discountStd = parseFloat(Math.round(promocodeDiscountLocalStd * 100) / 100).toFixed(2);
                                            var final_discountSdc = parseFloat(Math.round(promocodeDiscountLocalSdc * 100) / 100).toFixed(2);

                                            sdcHiddenFieldHtml.push({
                                                "vehicle_title": "Discount",
                                                "baseRate": final_discountSdc
                                            });
                                            dscHiddenFieldHtml.push({
                                                "vehicle_title": "Discount",
                                                "baseRate": customerFee
                                            });
                                            stdHiddenFieldHtml.push({
                                                "vehicle_title": "Discount",
                                                "baseRate": final_discountStd
                                            });


                                            tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                        }


                                    }


                                    sdcHiddenFieldHtml = JSON.stringify(sdcHiddenFieldHtml);
                                    dscHiddenFieldHtml = JSON.stringify(dscHiddenFieldHtml);
                                    stdHiddenFieldHtml = JSON.stringify(stdHiddenFieldHtml);

                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }


                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';

                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }

                                    totalAmountStdRate += parseFloat(baseRateStdRate);
                                    totalAmount += parseFloat(baserate);
                                    totalAmountSdcRate += parseFloat(baseRateSdcRate);

                                    if (result.isCheckSpecialRequest == "YesExists") {

                                        if ($('#blkout_found').val() != '') {

                                            baseRateStdHour = parseInt(baseRateStdHour) + parseInt(increase_hrs);
                                            baserateHour = parseInt(baserateHour) + parseInt(increase_hrs);
                                            baseRateSdcHour = parseInt(baseRateSdcHour) + parseInt(increase_hrs);

                                        }

                                        // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li><li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>'+getSpecialRequestHTML+' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus'+result.vehicle_info.VehTypeCode+'">$'+totalAmount.toFixed(2)+'</span>'+mileageBaseRateHTML+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> '+mileageBaseTollRateHTML+'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                        getHTML += "<div class='col-xs-12 col-sm-12 col-md-12 vehicle-padding'><input type='hidden' class='dsc_hidenClass_" + result.vehicle_info.VehTypeCode + "' value='" + dscHiddenFieldHtml + "'><input type='hidden' class='sdc_hidenClass_" + result.vehicle_info.VehTypeCode + "' value='" + sdcHiddenFieldHtml + "'><input type='hidden' class='std_hidenClass_" + result.vehicle_info.VehTypeCode + "' value='" + stdHiddenFieldHtml + "'><div class='col-xs-12 col-sm-4 col-md-4'><img src='" + result.vehicle_info.VehTypeImg1 + "' class='img-responsive'></div><div class='col-xs-12 col-sm-5 col-md-5'><h4><strong class='vehicleTitleDetail_" + result.vehicle_info.VehTypeCode + "'>" + result.vehicle_info.VehTypeTitle + "</strong></h4><ul class='vehicles-specs'> <li class='vehicle-specs input-group'> <span class='input-group-addon'><span class='glyphicon glyphicon-user'></span></span> <span class='input-group-addon bookPassengerDetail_" + result.vehicle_info.VehTypeCode + "'>" + result.vehicle_info.PassengerCapacity + "</span> </li> <li class='vehicle-specs input-group'> <span class='input-group-addon'><span class='glyphicon glyphicon-briefcase'></span></span> <span class='input-group-addon bookLuggageCapacity_" + result.vehicle_info.VehTypeCode + "'>" + result.vehicle_info.LuggageCapacity + "</span> </li><li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>" + getSpecialRequestHTML + " </ul></div><div class='col-xs-12 col-sm-3 col-md-3'><div class='vehicle-price'><div class='veh-price'><span class='totalAmoutPlus" + result.vehicle_info.VehTypeCode + "' style='font-size: 58%;'>$" + totalAmountStdRate.toFixed(2) + "/hr (" + baseRateStdHour + " hrs minimum)<br>$" + totalAmount.toFixed(2) + "/hr (" + baserateHour + " hrs minimum)<br>$" + totalAmountSdcRate.toFixed(2) + "/hr (" + baseRateSdcHour + " hrs minimum) <br></span>" + mileageBaseRateHTML + "</div><p><span style='color:red'>** All inclusive hrly rate</span></p><h4><a href='javascript:void(0)'><button type='button' style='font-size:12px;' class='btn btn-prim pessangerBookHrly' creadit_surcharg=" + cerdit_surcharge + " vehicle_image=" + result.vehicle_info.VehTypeImg1 + " seq=" + result.vehicle_info.VehTypeCode + " std_hrs=" + baseRateStdHour + " std_rate=" + totalAmountStdRate + " sdc_hrs=" + baseRateSdcHour + " sdc_rate=" + totalAmountSdcRate + " dsc_hrs=" + baserateHour + " dsc_rate=" + totalAmount + ">Select hrs & Book Now</button></a></h4></div></div></div> <div class='modal fade' id='rateDetail_" + k + "' role='dialog'> <div class='modal-dialog'> <!-- Modal content--> <div class='modal-content'>  <div class='modal-body'> <button type='button' class='close' data-dismiss='modal'>&times;</button> <h4 class='modal-title'>Rate Details</h4> <table class='table table-striped ors-table-rate-details'><tbody class='amountDetail_" + result.vehicle_info.VehTypeCode + "'>" + tableHtml + "</tbody></table> </div> <div class='modal-footer'> " + mileageBaseTollRateHTML + "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> </div> </div> </div> </div>";


                                    } else {

                                        if ($('#blkout_found').val() != '') {

                                            baseRateStdHour = parseInt(baseRateStdHour) + parseInt(increase_hrs);
                                            baserateHour = parseInt(baserateHour) + parseInt(increase_hrs);
                                            baseRateSdcHour = parseInt(baseRateSdcHour) + parseInt(increase_hrs);

                                        }
                                        getHTML += "<div class='col-xs-12 col-sm-12 col-md-12 vehicle-padding'><input type='hidden' class='dsc_hidenClass_" + result.vehicle_info.VehTypeCode + "' value='" + dscHiddenFieldHtml + "'><input type='hidden' class='sdc_hidenClass_" + result.vehicle_info.VehTypeCode + "' value='" + sdcHiddenFieldHtml + "'><input type='hidden' class='std_hidenClass_" + result.vehicle_info.VehTypeCode + "' value='" + stdHiddenFieldHtml + "'><div class='col-xs-12 col-sm-4 col-md-4'><img src='" + result.vehicle_info.VehTypeImg1 + "' class='img-responsive'></div><div class='col-xs-12 col-sm-5 col-md-5'><h4><strong class='vehicleTitleDetail_" + result.vehicle_info.VehTypeCode + "'>" + result.vehicle_info.VehTypeTitle + "</strong></h4><ul class='vehicles-specs'> <li class='vehicle-specs input-group'> <span class='input-group-addon'><span class='glyphicon glyphicon-user'></span></span> <span class='input-group-addon bookPassengerDetail_" + result.vehicle_info.VehTypeCode + "'>" + result.vehicle_info.PassengerCapacity + "</span> </li> <li class='vehicle-specs input-group'> <span class='input-group-addon'><span class='glyphicon glyphicon-briefcase'></span></span> <span class='input-group-addon bookLuggageCapacity_" + result.vehicle_info.VehTypeCode + "'>" + result.vehicle_info.LuggageCapacity + "</span> </li>" + getSpecialRequestHTML + " </ul></div><div class='col-xs-12 col-sm-3 col-md-3'><div class='vehicle-price'><div class='veh-price' infograins ><span class='totalAmoutPlus" + result.vehicle_info.VehTypeCode + "' style='font-size: 58%;'>$" + totalAmountStdRate.toFixed(2) + "/hr (" + baseRateStdHour + " hrs minimum)<br>$" + totalAmount.toFixed(2) + "/hr (" + baserateHour + " hrs minimum)<br>$" + totalAmountSdcRate.toFixed(2) + "/hr (" + baseRateSdcHour + " hrs minimum) <br></span>" + mileageBaseRateHTML + "</div><p><span style='color:red'>** All inclusive hrly rate</span></p><h4><a href='javascript:void(0)'><button type='button' style='font-size:12px;' class='btn btn-prim pessangerBookHrly' creadit_surcharg=" + cerdit_surcharge + " vehicle_image=" + result.vehicle_info.VehTypeImg1 + " seq=" + result.vehicle_info.VehTypeCode + " std_hrs=" + baseRateStdHour + " std_rate=" + totalAmountStdRate + " sdc_hrs=" + baseRateSdcHour + " sdc_rate=" + totalAmountSdcRate + " dsc_hrs=" + baserateHour + " dsc_rate=" + totalAmount + ">Select hrs & Book Now</button></a></h4></div></div></div> <div class='modal fade' id='rateDetail_" + k + "' role='dialog'> <div class='modal-dialog'> <!-- Modal content--> <div class='modal-content'>  <div class='modal-body'> <button type='button' class='close' data-dismiss='modal'>&times;</button> <h4 class='modal-title'>Rate Details</h4> <table class='table table-striped ors-table-rate-details'><tbody class='amountDetail_" + result.vehicle_info.VehTypeCode + "'>" + tableHtml + "</tbody></table> </div> <div class='modal-footer'> " + mileageBaseTollRateHTML + "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> </div> </div> </div> </div>";
                                        // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> '+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus'+result.vehicle_info.VehTypeCode+'">$'+totalAmount.toFixed(2)+'</span>'+mileageBaseRateHTML+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer">'+mileageBaseTollRateHTML+' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    }


                                    k++;
                                }

                            });


                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);
                            selectVehicleClass.pessangerBooking();

                        } else {

                            alert("vehicle not found ");

                        }


                    }
                });


            }
            else if (getJsonValue.serviceType == "AIRA") {

                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }
                        if (responseObj.code == 1006) {


                            var getHTML = '';

                            var k = 0;
                            console.log(responseObj);
                            $.each(responseObj.data, function (index, result) {


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";
                                    }

                                    var getSpecial2RequestHTML = '';
                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp; Special Package Available</a></li>";

                                    }

                                    if ($('#blkout_found').val() != '') {

                                        baseRateStdHour = parseInt(baseRateStdHour) + parseInt(increase_hrs);
                                        baserateHour = parseInt(baserateHour) + parseInt(increase_hrs);
                                        baseRateSdcHour = parseInt(baseRateSdcHour) + parseInt(increase_hrs);

                                    }
                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '" >' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';

                                }
                                else {
                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var totalGrandItemAmount = 0;
                                    var admin = 0;
                                    //1
                                    var reserve = 0;

                                    var grandTotal = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var stc_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);


                                    if (result.peak_hour_rate != 0) {

                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);

                                    if (result.tollAmount !== 0 && result.tollAmount !== undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);

                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }

                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {


                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);


                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);

                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);


                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }


                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);


                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }


                                    if (result.mandatoryfees != 0) {

                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);
                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);


                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }
                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }

                                        //done

                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);


                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);


                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);


                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }


                                    }
                                    if (result.airportRateExtraResult != 0 && result.airportRateExtraResult != true) {
                                        if (result.airportRateExtraResult[0].fhv_fee != 0) {
                                            /*Concierge  fees start here */
                                            var fhv_fee = parseFloat(Math.round(result.airportRateExtraResult[0].fhv_fee * 100) / 100).toFixed(2);
                                            //totalAmount+= parseFloat(fhv_fee);
                                            localStorage.setItem('concierge_fee', fhv_fee);
                                            //tableHtml+='<tr><td>Concierge  Fees</td><td class="text_alighn_right">'+fhv_fee+'</td></tr>';
                                        } else {

                                            localStorage.removeItem('concierge_fee');
                                        }
                                        if (result.airportRateExtraResult[0].fbo_fee != 0) {
                                            var fbo_fee = parseFloat(Math.round(result.airportRateExtraResult[0].fbo_fee * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fbo_fee);
                                            grandTotal += parseFloat(fbo_fee);
                                            totalGrandItemAmount += parseFloat(fbo_fee);


                                            tableHtml += '<tr><td>FBO Fees</td><td class="text_alighn_right">' + fbo_fee + '</td></tr>';
                                        }

                                    }


                                    if (result.airportRateVehicle != 0 && result.airportRateVehicle != true) {
                                        if (result.airportRateVehicle[0].fbo_srchg != 0) {
                                            var fbo_srchg = parseFloat(Math.round(result.airportRateVehicle[0].fbo_srchg * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fbo_srchg);
                                            grandTotal += parseFloat(fbo_srchg);
                                            totalGrandItemAmount += parseFloat(fbo_srchg);


                                            tableHtml += '<tr><td>FBO Fees</td><td class="text_alighn_right">' + fbo_srchg + '</td></tr>';
                                        }
                                        if (result.airportRateVehicle[0].fhv_access_fees != 0) {
                                            var fhv_access_fees = parseFloat(Math.round(result.airportRateVehicle[0].fhv_access_fees * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fhv_access_fees);
                                            grandTotal += parseFloat(fhv_access_fees);
                                            totalGrandItemAmount += parseFloat(fhv_access_fees);


                                            tableHtml += '<tr><td>FHV Access Fees</td><td class="text_alighn_right">' + fhv_access_fees + '</td></tr>';
                                        }
                                        if (result.airportRateVehicle[0].int_flt_schg != 0 && result.airportRateExtraResult[0].is_int_flight != 0 && getJsonValue.interNationFlightChecked == "yes") {
                                            var int_flt_schg = parseFloat(Math.round(result.airportRateVehicle[0].int_flt_schg * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(int_flt_schg);
                                            grandTotal += parseFloat(int_flt_schg);
                                            totalGrandItemAmount += parseFloat(int_flt_schg);


                                            tableHtml += '<tr><td>Int. flight srg.</td><td class="text_alighn_right">' + result.airportRateVehicle[0].int_flt_schg + '</td></tr>';
                                        } else if (result.airportRateVehicle[0].domestic_flt_schg != 0) {
                                            var domestic_flt_schg = parseFloat(Math.round(result.airportRateVehicle[0].domestic_flt_schg * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(domestic_flt_schg);
                                            grandTotal += parseFloat(domestic_flt_schg);
                                            totalGrandItemAmount += parseFloat(domestic_flt_schg);


                                            tableHtml += '<tr><td>Domestic Flight Fees</td><td class="text_alighn_right">' + domestic_flt_schg + '</td></tr>';

                                        }


                                        if (result.airportRateVehicle[0].per_hour_parking_fees != 0 && getJsonValue.ismeetGreetUpdateChecked == "yes") {
                                            var per_hour_parking_fees = parseFloat(Math.round(result.airportRateVehicle[0].per_hour_parking_fees * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(per_hour_parking_fees);
                                            grandTotal += parseFloat(per_hour_parking_fees);
                                            totalGrandItemAmount += parseFloat(per_hour_parking_fees);

                                            tableHtml += '<tr><td>Parking fees</td><td class="text_alighn_right">' + per_hour_parking_fees + '</td></tr>';
                                        }


                                    }


                                    var getHolidaySurcharge = 0;
                                    var getHolidaySurchargefee = 0;
                                    var getHolidaySurchargefee_type = 'fxdAMT';

                                    var getJson = {
                                        "action": "getHolidaySurcharge",
                                        "pick_date": getDate,
                                        "pick_time": getTime
                                    };
                                    $('#refresh_overlay').show();
                                    $.ajax({

                                        url: _SERVICEPATHServer,

                                        type: 'POST',

                                        data: getJson,

                                        success: function (response) {
                                            $('#refresh_overlay').hide();


                                            if (response != 0) {
                                                getHolidaySurcharge = 1;
                                                responseObj = JSON.parse(response);

                                                getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                            } else {

                                                getHolidaySurcharge = 0;
                                            }

                                        }

                                    });

                                    if (getHolidaySurcharge != 0) {
                                        var numberOfPasenger = $('#numberOfPasenger').val();

                                        if (getHolidaySurchargefee_type == 'fxdAMT') {
                                            var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                        } else {
                                            var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                            var total_holi_Surch_fee = per_amount;
                                        }


                                        var reserve = parseFloat(total_holi_Surch_fee);
                                        totalAmount += parseFloat(reserve);
                                        totalGrandItemAmount += parseFloat(reserve);

                                        tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                    }


                                    if (result.getHoliSurchargeFessResultFinal != 0) {
                                        if (result.getHoliSurchargeFessResultFinal[0].vehicle_rate != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.getHoliSurchargeFessResultFinal[0].vehicle_rate * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);


                                            tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }

                                    /*if (result.promoCodeArray != true) {


                                        var promocodeDiscountLocal = 0;
                                        if (result.promoCodeArray[0] !== undefined && result.promoCodeArray[0].discount_type == "%") {
                                            promocodeDiscountLocal = (parseFloat(result.total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;

                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }
                                        else {

                                            promocodeDiscountLocal = result.promoCodeArray[0] !== undefined ? result.promoCodeArray[0].discount_value : 0;
                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }


                                        tableHtml += '<tr><td> Discount</td><td class="text_alighn_right">- ' + promocodeDiscountLocal + '</td></tr>';


                                    }*/


                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);


                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }


                                    /* mandatory fees start here*/

                                    if (result.mandatoryfees != 0 && result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {


                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {

                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);


                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }


                                                    // var totalAmout1=(parseFloat(total_rate)*customfeesArray[l])/100;
                                                    // var totalAmout2= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                    // totalAmount+= parseFloat(totalAmout2);
                                                    // tableHtml+='<tr><td>'+customfeesNameArray[l]+'</td><td class="text_alighn_right">'+totalAmout2+'</td></tr>';

                                                } else {

                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);

                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }

                                            }


                                        }
                                    }


                                    /* mandatory fees end here*/


                                    if (result.promoCodeArray[0] !== undefined) {


                                        window.localStorage.setItem("combine_discount", result.promoCodeArray[0].is_combine_discount);

                                        if (result.promoCodeArray[0] !== undefined && result.promoCodeArray[0].promo_pref == 1) {

                                            localStorage.setItem('auto_promo_code', result.promoCodeArray[0].code);
                                            var promocodeDiscountLocal = 0;
                                            var discountAmount = 0;
                                            // var grandTotal=0;
                                            var subTotal = 0;


                                            if (result.promoCodeArray[0].discount_type == "%") {


                                                var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                                $.each(promocodeType, function (index, apply_rate) {

                                                    if (apply_rate == "GRT") {
                                                        promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;

                                                    }


                                                    if (apply_rate == "BSR") {
                                                        promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    }
                                                    totalAmount -= parseFloat(promocodeDiscountLocal);
                                                });
                                            } else {

                                                promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                                totalAmount -= parseFloat(promocodeDiscountLocal);


                                            }

                                            var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);


                                            tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                        }


                                    }


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seats Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }


                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }

                                    /*   if(result.isCheckSpecialRequest=="YesExists")
                         {

                         if($('#blkout_found').val()!='')
{

baseRateStdHour = parseInt(baseRateStdHour)+parseInt(increase_hrs);
baserateHour = parseInt(baserateHour)+parseInt(increase_hrs);
baseRateSdcHour = parseInt(baseRateSdcHour)+parseInt(increase_hrs);

}


                          getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li><li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>'+getSpecialRequestHTML+' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus'+result.vehicle_info.VehTypeCode+'">$'+totalAmount.toFixed(2)+'</span>'+mileageBaseRateHTML+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg='+cerdit_surcharge+' vehicle_image='+result.vehicle_info.VehTypeImg1+' seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> '+mileageBaseTollRateHTML+'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                         }
                        else
                        {

                          getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> '+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus'+result.vehicle_info.VehTypeCode+'">$'+totalAmount.toFixed(2)+'</span>'+mileageBaseRateHTML+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg='+cerdit_surcharge+' vehicle_image='+result.vehicle_info.VehTypeImg1+' seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer">'+mileageBaseTollRateHTML+' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                        }*/

                                    if ($('#blkout_found').val() != '') {

                                        baseRateStdHour = parseInt(baseRateStdHour) + parseInt(increase_hrs);
                                        baserateHour = parseInt(baserateHour) + parseInt(increase_hrs);
                                        baseRateSdcHour = parseInt(baseRateSdcHour) + parseInt(increase_hrs);

                                    }


                                    if (result.getHoliSurchargeFessResultFinal != 0) {
                                        if (result.getHoliSurchargeFessResultFinal[0].allowORES != 'yes') {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price">' + result.getHoliSurchargeFessResultFinal[0].blackout_Msg + '<h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>GET QUOTE</button></h4></div></div></div>';


                                        } else {


                                            if (result.isCheckSpecialRequest == "YesExists") {


                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            } else {

                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            }


                                        }


                                    } else {


                                        if (result.isCheckSpecialRequest == "YesExists") {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        } else {

                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        }

                                    }


                                    //          if(result.isCheckSpecialRequest=="YesExists")
                                    // {


                                    //         getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li><li><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</li>'+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus'+result.vehicle_info.VehTypeCode+'" >$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    // }

                                    // else
                                    // {

                                    //          getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li>'+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus'+result.vehicle_info.VehTypeCode+'" >$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    // }


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+result.total_rate+'</div><p>Rate Details</p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div>'


                                    k++;


                                }


                                /* error*/
                                // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+getJsonValue.total_passenger+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+getJsonValue.luggage_quantity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+parseFloat(parseFloat(result.vehicle_rate)+parseFloat(result.mandatoryfees[0].fuel)+parseFloat(result.mandatoryfees[0].reserve))+'</div><p>Rate Details</p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div>'

                            });
                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);


                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices

                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            })


                            selectVehicleClass.pessangerBooking();


                        } else {

                            alert("vehicle not found ");

                        }


                    }
                });


            }
            else if (getJsonValue.serviceType == "AIRD") {

                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }
                        if (responseObj.code == 1006) {


                            var getHTML = '';

                            var k = 0;
                            $.each(responseObj.data, function (index, result) {


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seats Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var getSpecial2RequestHTML = '';

                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }


                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';

                                } else {
                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var grandTotal = 0;
                                    var totalGrandItemAmount = 0;
                                    var reserve = 0;
                                    var admin = 0;

                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var stc_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);


                                    if (result.peak_hour_rate != 0) {
                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);

                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);

                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);

                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }

                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);


                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stope_rate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseInt(stope_rate);
                                        grandTotal += parseFloat(stope_rate);
                                        totalGrandItemAmount += parseFloat(stope_rate);


                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stope_rate + '</td></tr>';

                                    }

                                    if (result.mandatoryfees != 0) {
                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {

                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);


                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {

                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);


                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }

                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseInt(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }
                                        //done

                                        // if(result.mandatoryfees[0].custom_fee_name!=null){
                                        //     var customfeesNameArray=result.mandatoryfees[0].custom_fee_name.split(",");
                                        //   var customfeesArray=result.mandatoryfees[0].custom_fee.split(",");
                                        //   var type_rateArray=result.mandatoryfees[0].type_rate.split(",");
                                        //   for(var l=0; l<customfeesNameArray.length; l++)
                                        //   {
                                        //        var customerFee= parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                        //       if(customerFee!=0)
                                        //         {

                                        //           if(type_rateArray[l]=="%")
                                        //           {

                                        //             var totalAmout1=(parseFloat(total_rate)*customfeesArray[l])/100;
                                        //              black_car_charge= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        //              totalAmount+= parseFloat(black_car_charge);
                                        //              tableHtml+='<tr><td>'+customfeesNameArray[l]+'</td><td class="text_alighn_right">'+black_car_charge+'</td></tr>';

                                        //           }
                                        //           else
                                        //           {

                                        //               // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                        //             totalAmount+= parseFloat(customerFee);
                                        //             tableHtml+='<tr><td>'+customfeesNameArray[l]+'</td><td class="text_alighn_right">'+customerFee+'</td></tr>';

                                        //           }


                                        //         }


                                        //   }
                                        // }
                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);


                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseInt(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseInt(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);


                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseInt(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

                                            //localStorage.setItem('cerdit_card_surcharge',cerdit_card_surcharge);
                                            //tableHtml+='<tr><td>Creadit Card Fees</td><td class="text_alighn_right">'+cerdit_card_surcharge+'</td></tr>';

                                        }


                                    }


                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    if (result.airportRateExtraResult != 0 && result.airportRateExtraResult != true) {
                                        // if(result.airportRateExtraResult[0].fhv_fee!=0)
                                        //     {
                                        //       /* Concierge Fees start here */
                                        //       var fhv_fee= parseFloat(Math.round(result.airportRateExtraResult[0].fhv_fee * 100) / 100).toFixed(2);
                                        //         totalAmount+= parseFloat(fhv_fee);
                                        //         tableHtml+='<tr><td>Concierge Fees</td><td class="text_alighn_right">'+fhv_fee+'</td></tr>';
                                        //     }
                                        if (result.airportRateExtraResult[0].fbo_fee != 0) {
                                            var fbo_fee = parseFloat(Math.round(result.airportRateExtraResult[0].fbo_fee * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fbo_fee);
                                            grandTotal += parseFloat(fbo_fee);
                                            totalGrandItemAmount += parseFloat(fbo_fee);


                                            tableHtml += '<tr><td>FBO Fees</td><td class="text_alighn_right">' + fbo_fee + '</td></tr>';
                                        }


                                    }


                                    if (result.airportRateExtraResult != 0 && result.airportRateExtraResult != true) {
                                        if (result.airportRateExtraResult[0].fhv_fee != 0) {
                                            /*Concierge  fees start here */
                                            var fhv_fee = parseFloat(Math.round(result.airportRateExtraResult[0].fhv_fee * 100) / 100).toFixed(2);
                                            //totalAmount+= parseFloat(fhv_fee);


                                            localStorage.setItem('concierge_fee', fhv_fee);
                                            //tableHtml+='<tr><td>Concierge  Fees</td><td class="text_alighn_right">'+fhv_fee+'</td></tr>';
                                        } else {

                                            localStorage.removeItem('concierge_fee');
                                        }


                                    }

                                    if (result.airportRateVehicle != 0 && result.airportRateVehicle != true) {

                                        if (result.airportRateVehicle[0].fhv_access_fees != 0) {
                                            var fhv_access_fees = parseFloat(Math.round(result.airportRateVehicle[0].fhv_access_fees * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fhv_access_fees);
                                            grandTotal += parseFloat(fhv_access_fees);
                                            totalGrandItemAmount += parseFloat(fhv_access_fees);

                                            tableHtml += '<tr><td>FHV Access Fees</td><td class="text_alighn_right">' + fhv_access_fees + '</td></tr>';
                                        }


                                    }


                                    var getHolidaySurcharge = 0;
                                    var getHolidaySurchargefee = 0;
                                    var getHolidaySurchargefee_type = 'fxdAMT';

                                    var getJson = {
                                        "action": "getHolidaySurcharge",
                                        "pick_date": getDate,
                                        "pick_time": getTime
                                    };
                                    $('#refresh_overlay').show();
                                    $.ajax({

                                        url: _SERVICEPATHServer,

                                        type: 'POST',


                                        data: getJson,

                                        success: function (response) {
                                            $('#refresh_overlay').hide();


                                            if (response != 0) {
                                                getHolidaySurcharge = 1;
                                                responseObj = JSON.parse(response);

                                                getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                            } else {

                                                getHolidaySurcharge = 0;
                                            }

                                        }

                                    });

                                    if (getHolidaySurcharge != 0) {
                                        var numberOfPasenger = $('#numberOfPasenger').val();

                                        if (getHolidaySurchargefee_type == 'fxdAMT') {
                                            var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                        } else {
                                            var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                            var total_holi_Surch_fee = per_amount;
                                        }


                                        var reserve = parseFloat(total_holi_Surch_fee);
                                        totalAmount += parseFloat(reserve);
                                        totalGrandItemAmount += parseFloat(reserve);

                                        tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                    }


                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);


                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }


                                    /* manadatory fees start here*/


                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {

                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {

                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }


                                                } else {

                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);

                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }


                                            }


                                        }
                                    }


                                    /* manadatory fees end here */


                                    if (result.promoCodeArray[0] !== undefined) {


                                        window.localStorage.setItem("combine_discount", result.promoCodeArray[0].is_combine_discount);

                                        if (result.promoCodeArray[0].promo_pref == 1) {


                                            localStorage.setItem('auto_promo_code', result.promoCodeArray[0].code);

                                            var promocodeDiscountLocal = 0;
                                            var discountAmount = 0;
                                            // var grandTotal=0;
                                            var subTotal = 0;
                                            var cerdit_card_surcharge = 0;


                                            if (result.promoCodeArray[0].discount_type == "%") {


                                                var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                                $.each(promocodeType, function (index, apply_rate) {

                                                    if (apply_rate == "GRT") {

                                                        promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    }

                                                    if (apply_rate == "SBT") {


                                                        subTotal = (parseFloat(fuel_charge) + parseFloat(stc_charge) + parseFloat(black_car_charge));
                                                        promocodeDiscountLocal += (parseFloat(subTotal) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    }

                                                    if (apply_rate == "BSR") {
                                                        promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    }
                                                    totalAmount -= parseFloat(promocodeDiscountLocal);
                                                });
                                            } else {

                                                promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                                totalAmount -= parseFloat(promocodeDiscountLocal);


                                            }

                                            var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                            tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                        }


                                    }


                                    var getSpecialRequestHTML = '';
                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seats Available</li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";

                                    }


                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red'> Toll Price Will Applied</li>";


                                    }

                                    if ($('#blkout_found').val() != '') {

                                        baseRateStdHour = parseInt(baseRateStdHour) + parseInt(increase_hrs);
                                        baserateHour = parseInt(baserateHour) + parseInt(increase_hrs);
                                        baseRateSdcHour = parseInt(baseRateSdcHour) + parseInt(increase_hrs);

                                    }


                                    if (result.getHoliSurchargeFessResultFinal != 0) {
                                        if (result.getHoliSurchargeFessResultFinal[0].allowORES != 'yes') {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price">' + result.getHoliSurchargeFessResultFinal[0].blackout_Msg + '<h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>GET QUOTE</button></h4></div></div></div>';


                                        } else {


                                            if (result.isCheckSpecialRequest == "YesExists") {


                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            } else {

                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            }


                                        }


                                    } else {


                                        if (result.isCheckSpecialRequest == "YesExists") {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> <li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + mileageBaseTollRateHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        } else {

                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        }

                                    }


                                    // if(result.isCheckSpecialRequest=="YesExists")
                                    //   {

                                    //       getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> <li><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</li>'+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus'+result.vehicle_info.VehTypeCode+'" >$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    //   }
                                    // else
                                    //   {

                                    //       getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> '+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus'+result.vehicle_info.VehTypeCode+'" >$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    //   }


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+result.total_rate+'</div><p>Rate Details</p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div>'


                                    k++;


                                }


                                /* error*/
                                // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+getJsonValue.total_passenger+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+getJsonValue.luggage_quantity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+parseInt(parseInt(result.vehicle_rate)+parseInt(result.mandatoryfees[0].fuel)+parseInt(result.mandatoryfees[0].reserve))+'</div><p>Rate Details</p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div>'

                            });
                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);

                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices

                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            })

                            selectVehicleClass.pessangerBooking();
                        } else {

                            alert("vehicle not found ");

                        }


                    }
                });


            }
            else if (getJsonValue.serviceType == "SEAA") {

                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }
                        if (responseObj.code == 1006) {


                            var getHTML = '';
                            var k = 0;
                            $.each(responseObj.data, function (index, result) {

                                var mandatoryfeesFuel = 0;
                                var mandatoryfeesReserve = 0;
                                if (typeof(result.mandatoryfees) == "string") {
                                    mandatoryfeesFuel = result.mandatoryfees[0].fuel;

                                }
                                if (typeof(result.mandatoryfees) == "string") {


                                    mandatoryfeesReserve = result.mandatoryfees[0].reserve;
                                }


                                /* code start here */


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>Child Seats Available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var getSpecial2RequestHTML = '';

                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }


                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';

                                } else {
                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var admin = 0;
                                    var totalGrandItemAmount = 0;
                                    var grandTotal = 0;
                                    var reserve = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var stc_charge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);


                                    if (result.peak_hour_rate != 0) {
                                        if (result.peak_rate_type == 'dollar') {
                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            // total_rate+= parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);

                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);
                                    grandTotal = parseFloat(total_rate);
                                    totalGrandItemAmount = parseFloat(total_rate);


                                    if (result.conditionalSurchargeResultFinalResult != 0) {


                                        /* if(result.conditionalSurchargeResultFinalResult[0].seaport_fhv!=0)
                      {

                           totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_fhv);
                           tableHtml+='<tr><td>Conditiona Fhv Accces Fees </td><td class="text_alighn_right">'+parseFloat(Math.round(result.conditionalSurchargeResultFinalResult[0].seaport_fhv* 100) / 100).toFixed(2)+'</td></tr>';
                       }*/

                                        if (result.conditionalSurchargeResultFinalResult[0].seaport_pickup != 0) {

                                            totalAmount += parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_pickup);

                                            grandTotal += parseFloat(totalAmount);
                                            totalGrandItemAmount += parseFloat(totalAmount);


                                            tableHtml += '<tr><td>Conditional Seaport Pickup Fees </td><td class="text_alighn_right">' + parseFloat(Math.round(result.conditionalSurchargeResultFinalResult[0].seaport_pickup * 100) / 100).toFixed(2) + '</td></tr>';
                                        }

                                    }


                                    // // if(result.conditionalSurchargeResultFinalResult[0].airport_fhv!=0)
                                    // //  {
                                    // //    fhvVehicleRate=result.conditionalSurchargeResultFinalResult[0].airport_fhv;
                                    // //    totalAmount+=parseFloat(fhvVehicleRate);
                                    // //    tableHtml+='<tr><td>Fhv Fees </td><td>'+fhvVehicleRate+'</td></tr>';
                                    // //  }
                                    //   if(result.conditionalSurchargeResultFinalResult[0].concert_pickup!=0)
                                    //   {
                                    //     totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].concert_pickup);
                                    //     tableHtml+='<tr><td>Concert pickup </td><td>'+result.conditionalSurchargeResultFinalResult[0].concert_pickup+'</td></tr>';
                                    //   }
                                    //   // if(result.conditionalSurchargeResultFinalResult[0].domestic_flight!=0)
                                    //   // {
                                    //   //   totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].domestic_flight);
                                    //   //   tableHtml+='<tr><td>Domestic flight </td><td>'+result.conditionalSurchargeResultFinalResult[0].domestic_flight+'</td></tr>';
                                    //   // }
                                    //   // if(result.conditionalSurchargeResultFinalResult[0].fbo_pickup!=0)
                                    //   // {
                                    //   //   totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].fbo_pickup);

                                    //   //   tableHtml+='<tr><td>FBO Pickup </td><td>'+result.conditionalSurchargeResultFinalResult[0].fbo_pickup+'</td></tr>';
                                    //   // }

                                    //   // if(result.conditionalSurchargeResultFinalResult[0].int_flight!=0)
                                    //   // {

                                    //   //   totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].int_flight);

                                    //   //   tableHtml+='<tr><td>Internation Flight</td><td>'+result.conditionalSurchargeResultFinalResult[0].int_flight+'</td></tr>';
                                    //   // }

                                    //   if(result.conditionalSurchargeResultFinalResult[0].sport_event_pickup!=0)
                                    //   {
                                    //    totalAmount+= parseFloat(result.conditionalSurchargeResultFinalResult[0].sport_event_pickup);
                                    //    tableHtml+='<tr><td>Sport event pickup charge</td><td>'+result.conditionalSurchargeResultFinalResult[0].sport_event_pickup+'</td></tr>';
                                    //   }

                                    //   if(result.conditionalSurchargeResultFinalResult[0].term_park!=0)
                                    //   {
                                    //    totalAmount+= parseFloat(result.conditionalSurchargeResultFinalResult[0].term_park);
                                    //    tableHtml+='<tr><td>Term park charge</td><td>'+result.conditionalSurchargeResultFinalResult[0].term_park+'</td></tr>';
                                    //   }

                                    //   if(result.peak_hour_rate!=0)
                                    //   {

                                    //    totalAmount+= parseFloat(result.peak_hour_rate);
                                    //    tableHtml+='<tr><td>Peak Hour Rate</td><td>'+result.peak_hour_rate+'</td></tr>';
                                    //   }


                                    // }

                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);


                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }
                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);
                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);


                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }
                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);


                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }
                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);
                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);


                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }
                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }


                                        //done

                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);


                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);

                                            //copy and set all rate

                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);

                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //    tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //  }
                                        // if(result.mandatoryfees[0].fuel!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //    tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //  }
                                        //  if(result.mandatoryfees[0].stc!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //    tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        //  }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);


                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }

                                    }


                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    localStorage.removeItem('concierge_fee');

                                    if (result.airportRateExtraResult != "undefined" && result.airportRateExtraResult != true && result.airportRateExtraResult[0].seaport_fee != 0) {
                                        if (result.airportRateExtraResult[0].seaport_fee != 0) {

                                            /*Concierge  fees start here */
                                            var seaport_fee = parseFloat(Math.round(result.airportRateExtraResult[0].seaport_fee * 100) / 100).toFixed(2);
                                            //totalAmount+= parseFloat(fhv_fee);
                                            localStorage.setItem('seaport_fee', seaport_fee);
                                            //tableHtml+='<tr><td>Concierge  Fees</td><td class="text_alighn_right">'+fhv_fee+'</td></tr>';
                                        } else {

                                            localStorage.removeItem('seaport_fee');
                                        }


                                    }

                                    if (!result.airportRateExtraResult && typeof(result.airportRateExtraResult) != 'undefined' && result.airportRateExtraResult != 0) {

                                        if (result.airportRateExtraResult[0].seaport_fee != 0) {
                                            var seaport_fee = parseFloat(Math.round(result.airportRateExtraResult[0].seaport_fee * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(seaport_fee);
                                            grandTotal += parseFloat(seaport_fee);
                                            totalGrandItemAmount += parseFloat(seaport_fee);


                                            tableHtml += '<tr><td>seaport fees</td><td class="text_alighn_right">' + seaport_fee + '</td></tr>';
                                        }
                                        if (result.airportRateExtraResult[0].seaport_pickup_fee != 0) {
                                            var seaport_pickup_fee = parseFloat(Math.round(result.airportRateExtraResult[0].seaport_pickup_fee * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(seaport_pickup_fee);
                                            grandTotal += parseFloat(seaport_pickup_fee);
                                            totalGrandItemAmount += parseFloat(seaport_pickup_fee);


                                            tableHtml += '<tr><td>seaport pickup fees</td><td class="text_alighn_right">' + seaport_pickup_fee + '</td></tr>';
                                        }


                                    }
                                    if (result.airportRateVehicle != true && typeof(result.airportRateVehicle) != 'undefined' && result.airportRateVehicle != 0) {
                                        if (result.airportRateVehicle[0].sea_port_schg != 0) {
                                            var sea_port_schg = parseFloat(Math.round(result.airportRateVehicle[0].sea_port_schg * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(sea_port_schg);
                                            grandTotal += parseFloat(sea_port_schg);
                                            totalGrandItemAmount += parseFloat(sea_port_schg);


                                            tableHtml += '<tr><td>Seaport Surcharge</td><td class="text_alighn_right">' + sea_port_schg + '</td></tr>';
                                        }

                                        // console.log("om getJsonValue....", getJsonValue);
                                        if (result.airportRateVehicle[0].perhour_fees != 0 && result.airportRateExtraResult[0].inside_meet_text != 0 && getJsonValue.ismeetGreetUpdateChecked == "yes") {
                                            var perhour_fees = parseFloat(Math.round(result.airportRateVehicle[0].perhour_fees * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(perhour_fees);
                                            grandTotal += parseFloat(perhour_fees);
                                            totalGrandItemAmount += parseFloat(perhour_fees);


                                            tableHtml += '<tr><td>Seaport perhour parking fees</td><td class="text_alighn_right">' + perhour_fees + '</td></tr>';
                                        }
                                        if (result.airportRateVehicle[0].fhv_access_fee != 0) {
                                            var fhv_access_fee = parseFloat(Math.round(result.airportRateVehicle[0].fhv_access_fee * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fhv_access_fee);
                                            grandTotal += parseFloat(fhv_access_fee);
                                            totalGrandItemAmount += parseFloat(fhv_access_fee);


                                            tableHtml += '<tr><td>fhv access fee</td><td class="text_alighn_right">' + fhv_access_fee + '</td></tr>';
                                        }


                                    }


                                    var getHolidaySurcharge = 0;
                                    var getHolidaySurchargefee = 0;
                                    var getHolidaySurchargefee_type = 'fxdAMT';

                                    var getJson = {
                                        "action": "getHolidaySurcharge",
                                        "pick_date": getDate,
                                        "pick_time": getTime
                                    };
                                    $('#refresh_overlay').show();
                                    $.ajax({

                                        url: _SERVICEPATHServer,

                                        type: 'POST',

                                        data: getJson,

                                        success: function (response) {
                                            $('#refresh_overlay').hide();


                                            if (response != 0) {
                                                getHolidaySurcharge = 1;
                                                responseObj = JSON.parse(response);

                                                getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                            } else {

                                                getHolidaySurcharge = 0;
                                            }

                                        }

                                    });

                                    if (getHolidaySurcharge != 0) {
                                        var numberOfPasenger = $('#numberOfPasenger').val();

                                        if (getHolidaySurchargefee_type == 'fxdAMT') {
                                            var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                        } else {
                                            var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                            var total_holi_Surch_fee = per_amount;
                                        }


                                        var reserve = parseFloat(total_holi_Surch_fee);
                                        totalAmount += parseFloat(reserve);
                                        totalGrandItemAmount += parseFloat(reserve);

                                        tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                    }

                                    /*     if(result.promoCodeArray!=true)
                  {


                    var promocodeDiscountLocal=0;
                    if(result.promoCodeArray[0].discount_type=="%")
                    {
                      promocodeDiscountLocal=(parseFloat(result.total_rate)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                      totalAmount-=parseFloat(promocodeDiscountLocal);


                    }
                    else
                    {

                      promocodeDiscountLocal=result.promoCodeArray[0].discount_value;
                      totalAmount-=parseFloat(promocodeDiscountLocal);


                    }


                      tableHtml+='<tr><td> Discount</td><td class="text_alighn_right">-'+promocodeDiscountLocal+'</td></tr>';




                  }
*/

                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);


                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }


                                    /**/

                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {

                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {

                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }


                                                } else {

                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);

                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }


                                            }


                                        }
                                    }


                                    /*manadatry field end here*/

                                    if (result.promoCodeArray[0] !== undefined) {


                                        var promocodeDiscountLocal = 0;
                                        var discountAmount = 0;
                                        // var grandTotal=0;
                                        var subTotal = 0;

                                        if (result.promoCodeArray[0].discount_type == "%") {


                                            var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                            $.each(promocodeType, function (index, apply_rate) {

                                                if (apply_rate == "GRT") {

                                                    promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }


                                                if (apply_rate == "BSR") {
                                                    promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }
                                                totalAmount -= parseFloat(promocodeDiscountLocal);
                                            });
                                        } else {

                                            promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }

                                        var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                    }


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";

                                    }


                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';

                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }


                                    if (result.getHoliSurchargeFessResultFinal != 0) {
                                        if (result.getHoliSurchargeFessResultFinal[0].allowORES != 'yes') {

                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li><li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price"><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a data-msg="' + result.getHoliSurchargeFessResultFinal[0].blackout_Msg + '"  href="javascript:void(0)" onclick="Show_surcharge_msg(this)"><button type="button" class="btn btn-prim ">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';


                                        } else {


                                            if (result.isCheckSpecialRequest == "YesExists") {


                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li><li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price"><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            } else {

                                                getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer"> ' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                            }


                                        }


                                    } else {


                                        if (result.isCheckSpecialRequest == "YesExists") {


                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li><li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price"><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                        } else {

                                            getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' creadit_surcharg=' + cerdit_surcharge + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer"> ' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                        }

                                    }


                                    //    if(result.isCheckSpecialRequest=="YesExists")
                                    //  {


                                    //    getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> <li><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</li>'+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus'+result.vehicle_info.VehTypeCode+'" >$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'> BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    //   }
                                    // else
                                    //  {
                                    //      getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_'+result.vehicle_info.VehTypeCode+'">'+result.vehicle_info.LuggageCapacity+'</span> </li> '+getSpecialRequestHTML+'</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price totalAmoutPlus'+result.vehicle_info.VehTypeCode+'" >$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" seq='+result.vehicle_info.VehTypeCode+'>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_'+result.vehicle_info.VehTypeCode+'">'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    //   }

                                    k++;
                                }
                                /* code end here */
                            });
                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);


                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices

                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            });

                            selectVehicleClass.pessangerBooking();


                        } else {

                            alert("vehicle not found ");

                        }


                    }
                });


            }
            else if (getJsonValue.serviceType == "SEAD") {


                $('#refresh_overlay').show();
                $.ajax({
                    url: selectVehicleClass._SERVICEPATHServer,
                    type: 'POST',
                    data: getJsonValue,
                    success: function (response) {
                        $('#refresh_overlay').hide();
                        var responseObj = response;
                        if (typeof(response) == "string") {

                            responseObj = JSON.parse(response);
                        }
                        if (responseObj.code == 1006) {


                            var getHTML = '';
                            var k = 0;
                            $.each(responseObj.data, function (index, result) {

                                var mandatoryfeesFuel = 0;
                                var mandatoryfeesReserve = 0;
                                if (typeof(result.mandatoryfees) == "string") {
                                    mandatoryfeesFuel = result.mandatoryfees[0].fuel;

                                }
                                if (typeof(result.mandatoryfees) == "string") {


                                    mandatoryfeesReserve = result.mandatoryfees[0].reserve;
                                }


                                /* code start here */


                                if (result.total_rate == "GET QUOTE") {


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }

                                    var getSpecial2RequestHTML = '';

                                    if (result.isCheckSpecialRequest == "YesExists") {
                                        // getSpecial2RequestHTML="<li><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</li>";
                                        getSpecial2RequestHTML = "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#specialRequest'><span class='glyphicon glyphicon-gift'></span>&nbsp;&nbsp;Special Package Available</a></li>";


                                    }


                                    getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + getSpecial2RequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim getQuoteBtn" vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq2=' + result.vehicle_info.VehTypeCode + '>' + result.total_rate + '</button></h4></div></div></div>';


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><h4><button type="button" class="btn btn-prim">'+result.total_rate+'</button></h4></div></div></div>';

                                } else {
                                    var fhvVehicleRate = 0;
                                    var fhvVehicleRate = 0;
                                    var totalAmount = 0;
                                    var admin = 0;
                                    var totalGrandItemAmount = 0;
                                    var grandTotal = 0;
                                    var reserve = 0;
                                    var calculated_tax = 0;
                                    var fuel_charge = 0;
                                    var black_car_charge = 0;
                                    var cerdit_surcharge = 0;
                                    var stc_charge = 0;
                                    var total_rate = parseFloat(Math.round(result.total_rate * 100) / 100).toFixed(2);


                                    if (result.peak_hour_rate != 0) {
                                        if (result.peak_rate_type == 'dollar') {

                                            var peak_hour_rate = parseFloat(Math.round(result.peak_hour_rate * 100) / 100).toFixed(2);
                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);
                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);
                                        } else {
                                            var totalAmout1 = (parseFloat(total_rate) * result.peak_hour_rate) / 100;
                                            var peak_hour_rate = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);


                                            total_rate = parseFloat(total_rate) + parseFloat(peak_hour_rate);

                                            total_rate = parseFloat(Math.round(total_rate * 100) / 100).toFixed(2);


                                        }


                                        // tableHtml+='<tr><td>Peak Hour Rate</td><td class="text_alighn_right">'+peak_hour_rate+'</td></tr>';
                                    }


                                    var tableHtml = '<tr><td>Base Rate</td><td class="text_alighn_right">' + total_rate + '</td></tr>';
                                    totalAmount = parseFloat(total_rate);

                                    grandTotal = totalAmount;
                                    totalGrandItemAmount = totalAmount;


                                    if (result.conditionalSurchargeResultFinalResult != 0) {


                                        /*  if(result.conditionalSurchargeResultFinalResult[0].seaport_fhv!=0)
                      {

                           totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_fhv);
                           tableHtml+='<tr><td>Conditiona Fhv Accces Fees </td><td class="text_alighn_right">'+parseFloat(Math.round(result.conditionalSurchargeResultFinalResult[0].seaport_fhv* 100) / 100).toFixed(2)+'</td></tr>';
                       }*/

                                        if (result.conditionalSurchargeResultFinalResult[0].seaport_pickup != 0) {

                                            totalAmount += parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_pickup);
                                            grandTotal += parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_pickup);
                                            totalGrandItemAmount += parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_pickup);


                                            tableHtml += '<tr><td>Conditional Seaport Pickup Fees </td><td class="text_alighn_right">' + parseFloat(Math.round(result.conditionalSurchargeResultFinalResult[0].seaport_pickup * 100) / 100).toFixed(2) + '</td></tr>';
                                        }

                                    }


                                    // if(result.conditionalSurchargeResultFinalResult!=0)
                                    // {


                                    //     if(result.conditionalSurchargeResultFinalResult[0].seaport_fhv!=0)
                                    //     {

                                    //         totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_fhv);
                                    //         tableHtml+='<tr><td>Fhv Fees </td><td>'+result.conditionalSurchargeResultFinalResult[0].seaport_fhv+'</td></tr>';
                                    //     }

                                    //  if(result.conditionalSurchargeResultFinalResult[0].seaport_pickup!=0)
                                    //  {

                                    //    totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].seaport_pickup);
                                    //    tableHtml+='<tr><td>Fhv Fees </td><td>'+result.conditionalSurchargeResultFinalResult[0].seaport_pickup+'</td></tr>';
                                    //  }


                                    /* if(result.conditionalSurchargeResultFinalResult[0].airport_fhv!=0)
                                       {
                                        fhvVehicleRate=result.conditionalSurchargeResultFinalResult[0].airport_fhv;
                                         totalAmount+=parseFloat(fhvVehicleRate);
                                         tableHtml+='<tr><td>Fhv Fees </td><td>'+fhvVehicleRate+'</td></tr>';
                                      }*/
                                    //   if(result.conditionalSurchargeResultFinalResult[0].concert_pickup!=0)
                                    //   {
                                    //     totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].concert_pickup);
                                    //     tableHtml+='<tr><td>Concert pickup </td><td>'+result.conditionalSurchargeResultFinalResult[0].concert_pickup+'</td></tr>';
                                    //   }
                                    //   // if(result.conditionalSurchargeResultFinalResult[0].domestic_flight!=0)
                                    //   // {
                                    //   //   totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].domestic_flight);
                                    //   //   tableHtml+='<tr><td>Domestic flight </td><td>'+result.conditionalSurchargeResultFinalResult[0].domestic_flight+'</td></tr>';
                                    //   // }
                                    //   // if(result.conditionalSurchargeResultFinalResult[0].fbo_pickup!=0)
                                    //   // {
                                    //   //   totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].fbo_pickup);

                                    //   //   tableHtml+='<tr><td>FBO Pickup </td><td>'+result.conditionalSurchargeResultFinalResult[0].fbo_pickup+'</td></tr>';
                                    //   // }

                                    //   // if(result.conditionalSurchargeResultFinalResult[0].int_flight!=0)
                                    //   // {

                                    //   //   totalAmount+=parseFloat(result.conditionalSurchargeResultFinalResult[0].int_flight);

                                    //   //   tableHtml+='<tr><td>Internation Flight</td><td>'+result.conditionalSurchargeResultFinalResult[0].int_flight+'</td></tr>';
                                    //   // }

                                    //   if(result.conditionalSurchargeResultFinalResult[0].sport_event_pickup!=0)
                                    //   {
                                    //    totalAmount+= parseFloat(result.conditionalSurchargeResultFinalResult[0].sport_event_pickup);
                                    //    tableHtml+='<tr><td>Sport event pickup charge</td><td>'+result.conditionalSurchargeResultFinalResult[0].sport_event_pickup+'</td></tr>';
                                    //   }

                                    //   if(result.conditionalSurchargeResultFinalResult[0].term_park!=0)
                                    //   {
                                    //    totalAmount+= parseFloat(result.conditionalSurchargeResultFinalResult[0].term_park);
                                    //    tableHtml+='<tr><td>Term park charge</td><td>'+result.conditionalSurchargeResultFinalResult[0].term_park+'</td></tr>';
                                    //   }

                                    //   if(result.peak_hour_rate!=0)
                                    //   {

                                    //    totalAmount+= parseFloat(result.peak_hour_rate);
                                    //    tableHtml+='<tr><td>Peak Hour Rate</td><td>'+result.peak_hour_rate+'</td></tr>';
                                    //   }


                                    // }

                                    if (result.airportRateExtraResult != undefined && result.airportRateExtraResult != true && result.airportRateExtraResult[0].seaport_fee != 0) {
                                        /*Concierge  fees start here */
                                        var seaport_fee = parseFloat(Math.round(result.airportRateExtraResult[0].seaport_fee * 100) / 100).toFixed(2);
                                        //totalAmount+= parseFloat(fhv_fee);
                                        localStorage.setItem('seaport_fee', seaport_fee);
                                        //tableHtml+='<tr><td>Concierge  Fees</td><td class="text_alighn_right">'+fhv_fee+'</td></tr>';
                                    } else {

                                        localStorage.removeItem('seaport_fee');
                                    }


                                    if (result.tollAmount != 0 && result.tollAmount != undefined) {
                                        var toll_amount = parseFloat(Math.round(result.tollAmount * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(toll_amount);
                                        grandTotal += parseFloat(toll_amount);
                                        totalGrandItemAmount += parseFloat(toll_amount);


                                        tableHtml += '<tr><td>Estimated Tolls<a href="#" data-toggle="tooltip" data-placement="top" title="' + result.toll_disclaimer + '"><span class="glyphicon glyphicon-bell"></span></a></td><td class="text_alighn_right">' + toll_amount + '</td></tr>';
                                    }

                                    if (result.driver_gratuty_rate != 0 && result.driver_gratuty_rate != undefined) {
                                        var driver_gratuty_rate = parseFloat(Math.round(result.driver_gratuty_rate * 100) / 100).toFixed(2);

                                        driver_gratuty_rate = (parseFloat(total_rate) * driver_gratuty_rate) / 100;

                                        driver_gratuty_rate = parseFloat(Math.round(driver_gratuty_rate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(driver_gratuty_rate);
                                        grandTotal += parseFloat(driver_gratuty_rate);
                                        totalGrandItemAmount += parseFloat(driver_gratuty_rate);


                                        tableHtml += '<tr><td>Driver Gratuty</td><td class="text_alighn_right">' + driver_gratuty_rate + '</td></tr>';
                                    }

                                    if (result.stopRate != "No" && result.stopRate != undefined) {
                                        var stopRate = parseFloat(Math.round(result.stopRate * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(stopRate);
                                        grandTotal += parseFloat(stopRate);
                                        totalGrandItemAmount += parseFloat(stopRate);


                                        tableHtml += '<tr><td>Stops Rates</td><td class="text_alighn_right">' + stopRate + '</td></tr>';

                                    }
                                    if (result.mandatoryfees != 0) {


                                        if (result.mandatoryfees[0].admin != 0 && result.mandatoryfees[0].admin != null) {
                                            admin = parseFloat(Math.round(result.mandatoryfees[0].admin * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(admin);
                                            totalGrandItemAmount += parseFloat(admin);

                                            tableHtml += '<tr><td>Administrative Fees</td><td class="text_alighn_right">' + admin + '</td></tr>';
                                        }

                                        if (result.mandatoryfees[0].subtotal_tax != 0 && result.mandatoryfees[0].subtotal_tax != null) {
                                            var subtotal_tax = parseFloat(Math.round(result.mandatoryfees[0].subtotal_tax * 100) / 100).toFixed(2);
                                            calculated_tax = (parseFloat(total_rate) * subtotal_tax) / 100;
                                            totalAmount += parseFloat(calculated_tax);

                                            grandTotal += parseFloat(calculated_tax);
                                            totalGrandItemAmount += parseFloat(calculated_tax);


                                            tableHtml += '<tr><td>Tax</td><td class="text_alighn_right">' + calculated_tax.toFixed(2) + '</td></tr>';
                                        }

                                        // if(result.mandatoryfees[0].admin!=0)
                                        //   {
                                        //      totalAmount+= parseFloat(result.mandatoryfees[0].admin);
                                        //      tableHtml+='<tr><td>Administrative Fees</td><td>'+result.mandatoryfees[0].admin+'</td></tr>';
                                        //    }


                                        //done

                                        if (result.mandatoryfees[0].black_car != 0 && result.mandatoryfees[0].black_car != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].black_car) / 100;
                                            black_car_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(black_car_charge);
                                            grandTotal += parseFloat(black_car_charge);
                                            totalGrandItemAmount += parseFloat(black_car_charge);


                                            tableHtml += '<tr><td>Black Car Fund</td><td class="text_alighn_right">' + black_car_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].fuel != 0 && result.mandatoryfees[0].fuel != null) {

                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].fuel) / 100;
                                            fuel_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(fuel_charge);
                                            grandTotal += parseFloat(fuel_charge);
                                            totalGrandItemAmount += parseFloat(fuel_charge);


                                            tableHtml += '<tr><td>Fuel Surcharge</td><td class="text_alighn_right">' + fuel_charge + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].stc != 0 && result.mandatoryfees[0].stc != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].stc) / 100;
                                            stc_charge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(stc_charge);
                                            grandTotal += parseFloat(stc_charge);
                                            totalGrandItemAmount += parseFloat(stc_charge);


                                            tableHtml += '<tr><td>STC</td><td class="text_alighn_right">' + stc_charge + '</td></tr>';
                                        }


                                        //  if(result.mandatoryfees[0].black_car!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].black_car);
                                        //    tableHtml+='<tr><td>Black Car Fund</td><td>'+result.mandatoryfees[0].black_car+'</td></tr>';
                                        //  }
                                        // if(result.mandatoryfees[0].fuel!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].fuel);
                                        //    tableHtml+='<tr><td>Fuel Surcharge</td><td>'+result.mandatoryfees[0].fuel+'</td></tr>';
                                        //  }
                                        //  if(result.mandatoryfees[0].stc!=0)
                                        //  {
                                        //    totalAmount+= parseFloat(result.mandatoryfees[0].stc);
                                        //    tableHtml+='<tr><td>STC</td><td>'+result.mandatoryfees[0].stc+'</td></tr>';
                                        //  }

                                        if (result.mandatoryfees[0].reserve != 0 && result.mandatoryfees[0].reserve != null) {
                                            var reserve = parseFloat(Math.round(result.mandatoryfees[0].reserve * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(reserve);
                                            totalGrandItemAmount += parseFloat(reserve);

                                            tableHtml += '<tr><td>Reserve</td><td class="text_alighn_right">' + reserve + '</td></tr>';
                                        }


                                        if (result.mandatoryfees[0].credit != 0 && result.mandatoryfees[0].credit != null) {
                                            var totalAmout1 = (parseFloat(total_rate) * result.mandatoryfees[0].credit) / 100;
                                            cerdit_surcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                        }

                                    }


                                    var responseObj22 = '';
                                    $('#refresh_overlay').show();
                                    $.ajax({
                                        url: _SERVICEPATHSERVICECLIENT,
                                        type: 'POST',
                                        data: "action=getCreditSurchargerate",
                                        success: function (response) {
                                            $('#refresh_overlay').hide();

                                            if (typeof(response) == "string") {
                                                responseObj22 = JSON.parse(response);
                                                cerdit_surcharge = responseObj22[0].credit;
                                            }

                                        }

                                    });


                                    // if(typeof(result.airportRateExtraResult)!='undefined' && result.airportRateExtraResult!=0)
                                    // {
                                    //     if(result.airportRateExtraResult[0].seaport_fee!=0)
                                    //         {
                                    //              var seaport_fee= parseFloat(Math.round(result.airportRateExtraResult[0].seaport_fee * 100) / 100).toFixed(2);
                                    //             totalAmount+= parseFloat(seaport_fee);
                                    //             tableHtml+='<tr><td>seaport fees</td><td class="text_alighn_right">'+seaport_fee+'</td></tr>';
                                    //         }
                                    //         if(result.airportRateExtraResult[0].seaport_pickup_fee!=0)
                                    //         {
                                    //             var seaport_pickup_fee= parseFloat(Math.round(result.airportRateExtraResult[0].seaport_pickup_fee * 100) / 100).toFixed(2);
                                    //             totalAmount+= parseFloat(seaport_pickup_fee);
                                    //             tableHtml+='<tr><td>seaport pickup fees</td><td class="text_alighn_right">'+seaport_pickup_fee+'</td></tr>';
                                    //         }


                                    // }
                                    // if(typeof(result.airportRateVehicle)!='undefined' && result.airportRateVehicle!=0)
                                    // {
                                    //     if(result.airportRateVehicle[0].sea_port_schg!=0)
                                    //         {
                                    //             var sea_port_schg= parseFloat(Math.round(result.airportRateVehicle[0].sea_port_schg * 100) / 100).toFixed(2);
                                    //             totalAmount+= parseFloat(sea_port_schg);
                                    //             tableHtml+='<tr><td>Seaport Surcharge</td><td class="text_alighn_right">'+sea_port_schg+'</td></tr>';
                                    //         }
                                    //         if(result.airportRateVehicle[0].perhour_fees!=0)
                                    //         {
                                    //             var perhour_fees= parseFloat(Math.round(result.airportRateVehicle[0].perhour_fees * 100) / 100).toFixed(2);
                                    //             totalAmount+= parseFloat(perhour_fees);
                                    //             tableHtml+='<tr><td>Seaport perhour parking fees</td><td class="text_alighn_right">'+perhour_fees+'</td></tr>';
                                    //         }
                                    if (result.airportRateVehicle != undefined && result.airportRateVehicle != true && result.airportRateVehicle[0].fhv_access_fee != 0) {
                                        var fhv_access_fee = parseFloat(Math.round(result.airportRateVehicle[0].fhv_access_fee * 100) / 100).toFixed(2);
                                        totalAmount += parseFloat(fhv_access_fee);
                                        grandTotal += parseFloat(fhv_access_fee);
                                        totalGrandItemAmount += parseFloat(fhv_access_fee);


                                        tableHtml += '<tr><td>Seaport Fhv fee</td><td class="text_alighn_right">' + fhv_access_fee + '</td></tr>';
                                    }


                                    // }


                                    var getHolidaySurcharge = 0;
                                    var getHolidaySurchargefee = 0;
                                    var getHolidaySurchargefee_type = 'fxdAMT';

                                    var getJson = {
                                        "action": "getHolidaySurcharge",
                                        "pick_date": getDate,
                                        "pick_time": getTime
                                    };
                                    $('#refresh_overlay').show();
                                    $.ajax({

                                        url: _SERVICEPATHServer,

                                        type: 'POST',

                                        data: getJson,

                                        success: function (response) {
                                            $('#refresh_overlay').hide();


                                            if (response != 0) {
                                                getHolidaySurcharge = 1;
                                                responseObj = JSON.parse(response);

                                                getHolidaySurchargefee = responseObj[0].holiday_surcg_per_pax;
                                                getHolidaySurchargefee_type = responseObj[0].holiday_surcg_type;


                                            } else {

                                                getHolidaySurcharge = 0;
                                            }

                                        }

                                    });

                                    if (getHolidaySurcharge != 0) {
                                        var numberOfPasenger = $('#numberOfPasenger').val();

                                        if (getHolidaySurchargefee_type == 'fxdAMT') {
                                            var total_holi_Surch_fee = numberOfPasenger * getHolidaySurchargefee;

                                        } else {
                                            var per_amount = (total_rate * getHolidaySurchargefee) / 100;

                                            var total_holi_Surch_fee = per_amount;
                                        }


                                        var reserve = parseFloat(total_holi_Surch_fee);
                                        totalAmount += parseFloat(reserve);
                                        totalGrandItemAmount += parseFloat(reserve);

                                        tableHtml += '<tr><td>Holiday Surcharge</td><td class="text_alighn_right">' + reserve.toFixed(2) + '</td></tr>';
                                    }


                                    /*   if(result.promoCodeArray!=true)
                  {


                    var promocodeDiscountLocal=0;
                    if(result.promoCodeArray[0].discount_type=="%")
                    {
                      promocodeDiscountLocal=(parseFloat(result.total_rate)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                      totalAmount-=parseFloat(promocodeDiscountLocal);


                    }
                    else
                    {

                      promocodeDiscountLocal=result.promoCodeArray[0].discount_value;
                      totalAmount-=parseFloat(promocodeDiscountLocal);


                    }


                      tableHtml+='<tr><td>Discount</td><td class="text_alighn_right">-'+promocodeDiscountLocal+'</td></tr>';




                  }*/


                                    if (result.conditionalSurchargeDateTime != undefined && result.conditionalSurchargeDateTime != 0 && result.conditionalSurchargeDateTime != true) {
                                        if (result.conditionalSurchargeDateTime[0].amount != 0) {
                                            var vehicle_rate = parseFloat(Math.round(result.conditionalSurchargeDateTime[0].amount * 100) / 100).toFixed(2);
                                            totalAmount += parseFloat(vehicle_rate);
                                            grandTotal += parseFloat(vehicle_rate);
                                            totalGrandItemAmount += parseFloat(vehicle_rate);


                                            tableHtml += '<tr><td> Hours/Early Morning Pickups</td><td class="text_alighn_right">' + vehicle_rate + '</td></tr>';
                                        }


                                    }


                                    /*mandatory rate start here*/


                                    if (result.mandatoryfees[0].custom_fee_name != null) {
                                        var customfeesNameArray = result.mandatoryfees[0].custom_fee_name.split(",");
                                        var customfeesArray = result.mandatoryfees[0].custom_fee.split(",");
                                        var type_rateArray = result.mandatoryfees[0].type_rate.split(",");
                                        var is_subtamount_check = result.mandatoryfees[0].is_subtamount_check.split(",");

                                        for (var l = 0; l < customfeesNameArray.length; l++) {
                                            var customerFee = parseFloat(Math.round(customfeesArray[l] * 100) / 100).toFixed(2);

                                            if (customerFee != 0) {

                                                if (type_rateArray[l] == "%") {


                                                    if (is_subtamount_check[l] == "subtotal") {


                                                        var totalAmout1 = (parseFloat(result.total_rate) * customfeesArray[l]) / 100;
                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    } else {

                                                        var totalAmout1 = (parseFloat(grandTotal) * customfeesArray[l]) / 100;


                                                        var totalAmout2 = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                        totalAmount += parseFloat(totalAmout2);
                                                        totalGrandItemAmount += parseFloat(totalAmout2);

                                                        tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + totalAmout2 + '</td></tr>';


                                                    }

                                                    // var totalAmout1=(parseFloat(total_rate)*customfeesArray[l])/100;
                                                    // var totalAmout2= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                    // totalAmount+= parseFloat(totalAmout2);
                                                    // tableHtml+='<tr><td>'+customfeesNameArray[l]+'</td><td class="text_alighn_right">'+totalAmout2+'</td></tr>';

                                                } else {

                                                    // var totalAmout1=(parseFloat(result.total_rate)+customfeesArray[k]);
                                                    totalAmount += parseFloat(customerFee);
                                                    totalGrandItemAmount += parseFloat(customerFee);


                                                    tableHtml += '<tr><td>' + customfeesNameArray[l] + '</td><td class="text_alighn_right">' + customerFee + '</td></tr>';

                                                }


                                            }


                                        }
                                    }


                                    /*mandatory rate end here*/


                                    if (result.promoCodeArray[0] !== undefined) {

                                        var promocodeDiscountLocal = 0;
                                        var discountAmount = 0;
                                        // var grandTotal=0;
                                        var subTotal = 0;

                                        if (result.promoCodeArray[0].discount_type == "%") {


                                            var promocodeType = result.promoCodeArray[0].applyrate.split(",");
                                            $.each(promocodeType, function (index, apply_rate) {

                                                if (apply_rate == "GRT") {
                                                    promocodeDiscountLocal += (parseFloat(totalGrandItemAmount) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                    //discountAmount=totalAmount;
                                                    // if(result.mandatoryfees[0].credit!=0 && result.mandatoryfees[0].credit!=null){
                                                    //  var totalAmout1=(parseFloat(total_rate)*result.mandatoryfees[0].credit)/100;
                                                    //  var cerdit_card_surcharge= parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                                                    //  tableHtml+='<tr><td>Creadit Card Fees</td><td class="text_alighn_right">'+cerdit_card_surcharge+'</td></tr>';

                                                    // }

                                                    // grandTotal=(parseFloat(reserve)+parseFloat(admin)+parseFloat(cerdit_card_surcharge)+parseFloat(calculated_tax));

                                                    // promocodeDiscountLocal+=(parseFloat(grandTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;

                                                    //totalAmount-=parseFloat(promocodeDiscountLocal);
                                                }

                                                // if(apply_rate=="SBT"){


                                                //   subTotal=(parseFloat(fuel_charge)+parseFloat(stc_charge)+parseFloat(black_car_charge));
                                                //   promocodeDiscountLocal+=(parseFloat(subTotal)*parseFloat(result.promoCodeArray[0].discount_value))/100;
                                                //   //totalAmount-=parseFloat(promocodeDiscountLocal);

                                                // }

                                                if (apply_rate == "BSR") {
                                                    promocodeDiscountLocal += (parseFloat(total_rate) * parseFloat(result.promoCodeArray[0].discount_value)) / 100;


                                                }
                                                totalAmount -= parseFloat(promocodeDiscountLocal);
                                            });
                                        } else {

                                            promocodeDiscountLocal = result.promoCodeArray[0].discount_value;
                                            totalAmount -= parseFloat(promocodeDiscountLocal);


                                        }

                                        var final_discount = parseFloat(Math.round(promocodeDiscountLocal * 100) / 100).toFixed(2);
                                        tableHtml += '<tr><td>Discount</td><td class="text_alighn_right">- ' + final_discount + '</td></tr>';


                                    }


                                    var getSpecialRequestHTML = '';

                                    if (result.isCheckChildSeat == "YesExists") {
                                        // getSpecialRequestHTML="<li><span class='glyphicon glyphicon-th-list'></span>child seat available</li>";
                                        // getSpecialRequestHTML="<li ><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_"+result.vehicle_info.VehTypeCode+"'>Child Seats Available</span></li>";
                                        getSpecialRequestHTML = "<li ><a href='javascript:void(0)' data-toggle='modal' data-target='#childRequest'><span class='glyphicon glyphicon-th-list' ></span><span class='childSeatCheck_" + result.vehicle_info.VehTypeCode + "'>&nbsp;&nbsp;Child Seats Available</span></a></li>";


                                    }


                                    var mileageBaseRateHTML = '';
                                    var mileageBaseTollRateHTML = '';
                                    if (result.ismileageBaseRate == "yesMileageExist") {


                                        mileageBaseRateHTML = "<span class='red-star' style='color:red'>*</span>";
                                        //mileageBaseTollRateHTML="<li style='color:red;float:left'> Toll Price Will Applied</li>";


                                    }

                                    if (result.isCheckSpecialRequest == "YesExists") {


                                        getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li><li><a href="javascript:void(0)" data-toggle="modal" data-target="#specialRequest"><span class="glyphicon glyphicon-gift"></span>&nbsp;&nbsp;Special Package Available</a></li>' + getSpecialRequestHTML + ' </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price" ><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer">' + mileageBaseTollRateHTML + ' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    } else {

                                        getHTML += '<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="' + result.vehicle_info.VehTypeImg1 + '" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong class="vehicleTitleDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.VehTypeTitle + '</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon bookPassengerDetail_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.PassengerCapacity + '</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon bookLuggageCapacity_' + result.vehicle_info.VehTypeCode + '">' + result.vehicle_info.LuggageCapacity + '</span> </li> ' + getSpecialRequestHTML + '</ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price"><span class="totalAmoutPlus' + result.vehicle_info.VehTypeCode + '">$' + totalAmount.toFixed(2) + '</span>' + mileageBaseRateHTML + '</div><p><a  data-toggle="modal" data-target="#rateDetail_' + k + '">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim pessangerBook" creadit_surcharg=' + cerdit_surcharge + ' vehicle_image=' + result.vehicle_info.VehTypeImg1 + ' seq=' + result.vehicle_info.VehTypeCode + '>BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_' + k + '" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody class="amountDetail_' + result.vehicle_info.VehTypeCode + '">' + tableHtml + '</tbody></table> </div> <div class="modal-footer"> ' + mileageBaseTollRateHTML + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';

                                    }


                                    // getHTML+='<div class="col-xs-12 col-sm-12 col-md-12 vehicle-padding"><div class="col-xs-12 col-sm-4 col-md-4"><img src="'+result.vehicle_info.VehTypeImg1+'" class="img-responsive"></div><div class="col-xs-12 col-sm-5 col-md-5"><h4><strong>'+result.vehicle_info.VehTypeTitle+'</strong></h4><ul class="vehicles-specs"> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> <span class="input-group-addon">'+result.vehicle_info.PassengerCapacity+'</span> </li> <li class="vehicle-specs input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span> <span class="input-group-addon">'+result.vehicle_info.LuggageCapacity+'</span> </li> </ul></div><div class="col-xs-12 col-sm-3 col-md-3"><div class="vehicle-price"><div class="veh-price">$'+totalAmount+'</div><p><a  data-toggle="modal" data-target="#rateDetail_'+k+'">Rate Details</a></p><h4><a href="javascript:void(0)"><button type="button" class="btn btn-prim">BOOK NOW</button></a></h4></div></div></div> <div class="modal fade" id="rateDetail_'+k+'" role="dialog"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content">  <div class="modal-body"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Rate Details</h4> <table class="table table-striped ors-table-rate-details"><tbody>'+tableHtml+'</tbody></table> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>';
                                    k++;
                                }
                                /* code end here */
                            });
                            $("#pickupaddress").html(getJsonValue.pickuplocation);
                            $("#dropupaddress").html(getJsonValue.dropoff_location);
                            $('#jurneyDate').html(getJsonValue.pickup_date)
                            $('#jurneyTime').html(getJsonValue.pickup_time)
                            $('#showAllVehicle').html(getHTML);


                            $('.getQuoteBtn').on("click", function () {

                                var getSeq = $(this).attr("seq2");

                                var getChildSeatAvailable = $('.childSeatCheck_' + getSeq).html();

                                if (getChildSeatAvailable != undefined) {

                                    window.localStorage.setItem("isChildRide", "exist");
                                } else {

                                    window.localStorage.removeItem('isChildRide');
                                }


                                var finalJsonValue = [];

                                // var getHtmlpoupu=$('.amountDetail_'+getSeq).html()
                                var rateDetail = [];
                                var i = 0;
                                var newVariable = '';
                                $('.amountDetail_' + getSeq + ' td').each(function (index, result) {

                                    if (i == 0) {
                                        newVariable = $(this).html();
                                        i = 1;
                                    } else {
                                        rateDetail.push({"vehicle_title": newVariable, "baseRate": $(this).html()});
                                        // rateDetail[k][1]=$(this).html();

                                        i = 0
                                    }


                                });

                                var vehicleLuggageCapacity = $('.bookLuggageCapacity_' + getSeq).html();
                                var vehiclePassengerCapacity = $('.bookPassengerDetail_' + getSeq).html();
                                var vehicleTitle = $('.vehicleTitleDetail_' + getSeq).html();

                                var fullTotalAmount = $('.totalAmoutPlus' + getSeq).html();
                                var pickuplocation = $('#pickupaddress').html();
                                var dropupaddress = $('#dropupaddress').html();
                                var numberOfPasenger = $('#numberOfPasenger').val();

                                var numberOfLuggage = $('#numberOfLuggage').val();
                                var childValueStorhere = $('#childValueStorhere').html();
                                var updateVehicleInfoservices = $('#updateVehicleInfoservices').val();
                                var jurneyDate = $('#jurneyDate').html();
                                var jurneyTime = $('#jurneyTime').html();


                                finalJsonValue = {
                                    "AllRateJson": rateDetail,
                                    "vehicleLuggageCapacity": vehicleLuggageCapacity,
                                    "vehiclePassengerCapacity": vehiclePassengerCapacity,
                                    "vehicleTitle": vehicleTitle,
                                    "fullTotalAmount": fullTotalAmount,
                                    "pickuplocation": pickuplocation,
                                    "dropupaddress": dropupaddress,
                                    "numberOfPasenger": numberOfPasenger,
                                    "numberOfLuggage": numberOfLuggage,
                                    "childValueStorhere": childValueStorhere,
                                    "jurneyDate": jurneyDate,
                                    "jurneyTime": jurneyTime,
                                    "vehicle_code": getSeq,
                                    "updateVehicleInfoservices": updateVehicleInfoservices

                                }

                                window.localStorage.setItem("bookingInfo", JSON.stringify(finalJsonValue));
                                window.location.href = "quote_information.html";


                            });
                            selectVehicleClass.pessangerBooking();

                        } else {

                            alert("vehicle not found ");

                        }


                    }
                });


            }


        }


    }


};
$("#timePickerComman").blur(function () {
    var servicesType = $('#updateVehicleInfoservices').val();
    var getDate = $('#datePickerComman').val();
    var getTime = $('#timePickerComman').val();

    if ((new Date() > new Date(getDate + ' ' + getTime))) {
        $('#timePickerComman').val('');
        alert("Please select a valid date. You can't choose past date & time.");
    } else {
        if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {
            /* comman file function call here */
            selectVehicleClass.checkCutoffTime(getDate, getTime, servicesType);
            $.ajax({
                url : _SERVICEPATHServer,
                type: 'post',
                data : {
                    action: 'getHolidaySurcharge',
                    pick_date: getDate,
                    pick_time: getTime
                },
                success : function (response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if(res !== 0){
                        $('#CutoffTimeMain').css("display", "block").html(res[0].blackout_Msg);
                        $('#timePickerComman').val('');
                    }
                }
            });
        }
        if (servicesType == 'HRLY' && $.trim(getDate) != '') {
            var getLocation = '1701 Collins Avenue, Miami Beach FL 33139';
            selectVehicleClass.checkBlackOutDate(getDate, getTime, getLocation);
        }
        if (servicesType != 'HRLY' && $.trim(getDate) != '') {
            selectVehicleClass.checkSurchargeCutoffTime(getDate, getTime, servicesType);
        }
    }


});
$('#datePickerComman').on("change", function () {


    var servicesType = $('#updateVehicleInfoservices').val();
    var getDate = $('#datePickerComman').val();
    var getTime = $('#timePickerComman').val();


    if ((new Date() > new Date(getDate + ' ' + getTime))) {
        if (getTime != '') {
            $('#timePickerComman').val('');
            alert("Please select a valid date. You can't choose past date & time.");
        }
    } else {
        if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {
            /* comman file function call here */
            selectVehicleClass.checkCutoffTime(getDate, getTime, servicesType);
            $.ajax({
                url : _SERVICEPATHServer,
                type: 'post',
                data : {
                    action: 'getHolidaySurcharge',
                    pick_date: getDate,
                    pick_time: getTime
                },
                success : function (response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if(res !== 0){
                        $('#CutoffTimeMain').css("display", "block").html(res[0].blackout_Msg);
                        $('#timePickerComman').val('');
                    }
                }
            });
        }

        if (servicesType == 'HRLY' && $.trim(getTime) != '') {
            var getLocation = '1701 Collins Avenue, Miami Beach FL 33139';

            selectVehicleClass.checkBlackOutDate(getDate, getTime, getLocation);
        }

        if (servicesType != 'HRLY' && $.trim(getTime) != '') {


            selectVehicleClass.checkSurchargeCutoffTime(getDate, getTime, servicesType);
        }
    }


});

function Show_surcharge_msg(msg) {


    alert(msg.getAttribute("data-msg"));

}

selectVehicleClass.selectVehicleOnLoad();
selectVehicleClass.getVehiclesBackOffice();
selectVehicleClass.getUpdateVehicleInfo();
selectVehicleClass.getServiceTypes();


function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: {lat: 41.85, lng: -87.65}
    });
    directionsDisplay.setMap(map);
}

$(function () {
    if (localStorage.rateSetInformation !== undefined) {
        var bookingInfo = JSON.parse(localStorage.rateSetInformation);
        var pickuplocation = bookingInfo.pickuplocation;
        var dropoff_location = bookingInfo.dropoff_location;
        var jurneyDate = bookingInfo.pickup_date;
        var jurneyTime = bookingInfo.pickup_time;

        $("#pickupaddress").html(pickuplocation);
        $("#dropupaddress").html(dropoff_location);
        $("#jurneyDate").html(jurneyDate);
        $("#jurneyTime").html(jurneyTime);

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            directionsService.route({
                origin: pickuplocation,
                destination: dropoff_location,
                travelMode: 'DRIVING'
            }, function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map-container'), {
            zoom: 13,
            center: {lat: 37.65207, lng: -85.57618}
        });
        directionsDisplay.setMap(map);
        calculateAndDisplayRoute(directionsService, directionsDisplay);

    } else {
        window.location.href = WEBSITE_URL + '/frontend';
    }
});